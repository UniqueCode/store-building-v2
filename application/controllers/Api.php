<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    require APPPATH . '/libraries/Rest_Controller.php';
    class Api extends REST_Controller {
    
        function __construct() {
            // Construct the parent class
            parent::__construct();
            $this->load->library("ion_auth");
            $this->load->model(array("model_promotion","modelProduk"));
        }
        public function login_post(){
            $_POST = json_decode(file_get_contents('php://input'), true);
            $identity = $this->input->post("phone");
            $password = $this->input->post("password");
            $remember = NULL;
            $loginProcess = $this->ion_auth->login($identity,$password,$remember);
            if($loginProcess){
                $data = array (
                    'login' => true,
                    'username' =>  $this->ion_auth->user()->row()->first_name,
                    'id_toko' => $this->ion_auth->user()->row()->toko,
                );
                $this->response(array('status' => 200, 'message' => 'login success','data'=> $data));
            } else {
                $data = array (
                    'login' => false
                );
                $this->response(array('status' => 401, 'message' => 'incorrect username or password','data'=> $data));
            }


        }

        public function product_post() {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $id_toko = $this->input->post("id_toko");
            $data_kategori = $this->db->get("ap_kategori")->result();
              if($data_kategori){
                $new_arr = array();
                foreach ($data_kategori as $val){
                    $query = $this->modelProduk->daftarProdukSale($id_toko,$val->id_kategori);
                    $new_arr [] = array (
                        'nama_kategori' => $val->kategori,
                        'produk'    => $query->result_array()
                    );
                }
                $this->response(array('status' => 200, 'message' => 'data produk & kategori','data'=> $new_arr));
            }
            else {
                $this->response(array('status' => 204, 'message' => 'No data produk & kategori','data'=> 'data empty'));
            }
          
        }
        public function banner_get() {
            $data = $this->model_promotion->get_banner();
            if($data){
                $new_arr = array ();
                foreach($data as $val) {
                    $new_arr[] = array (
                        'id_banner' => $val->id_banner,
                        'banner' => $val->banner,
                        'banner_image' => base_url()."public/uploads/".$val->banner_image,
                    );
                }
                $this->response(array('status' => 200, 'message' => 'data banner','data'=> $new_arr));
            }
            else {
                $this->response(array('status' => 204, 'message' => 'No data banner','data'=> 'data empty'));
            }
          
        }
    }
    // <? echo base_url()."/public/uploads/
