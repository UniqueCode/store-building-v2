<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Customer extends BaseController{
	function __construct(){
		parent::__construct();
		$this->load->helper("url");
		$this->load->library('session');
		$this->load->model(array("model1","modelCustomer"));
		$this->load->database();

		$this->isLoggedIn($this->global['idUser'],2,40);
	}

	function index(){
		$total_rows = $this->model1->total_customer();
		$this->load->library('pagination');
		$config['base_url'] 			= base_url('parameter/customer');
		$config['total_rows']			= $total_rows;
		$config["per_page"]				= $per_page = 30;
		$config["uri_segment"]			= 3;
		$config["full_tag_open"] 		= '<ul class="pagination">';
		$config["full_tag_close"] 		= '</ul>';
		$config["first_link"] 			= "&laquo;";
		$config["first_tag_open"] 		= "<li>";
		$config["first_tag_close"] 		= "</li>";
		$config["last_link"] 			= "&raquo;";
		$config["last_tag_open"] 		= "<li>";
		$config["last_tag_close"] 		= "</li>";
		$config['next_link'] 			= '&gt;';
		$config['next_tag_open'] 		= '<li>';
		$config['next_tag_close'] 		= '<li>';
		$config['prev_link'] 			= '&lt;';
		$config['prev_tag_open'] 		= '<li>';
		$config['prev_tag_close'] 		= '<li>';
		$config['cur_tag_open'] 		= '<li class="active"><a href="#">';
		$config['cur_tag_close'] 		= '</a></li>';
		$config['num_tag_open'] 		= '<li>';
		$config['num_tag_close'] 		= '</li>';

		$this->pagination->initialize($config);

		$data['paging'] = $this->pagination->create_links();
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		if(empty($_GET['query'])){
			$data['customer'] = $this->model1->get_customer($per_page,$page);
		} else {
			$query = $_GET['query'];
			$data['customer'] = $this->model1->get_customer_sort($query);
		}

		$this->global['pageTitle'] = "Toko Bangunan - Data Customer";
		$this->loadViews("customer/body_customer",$this->global,$data,"footer_empty");
	}

	function add_customer(){
		$data['store'] = $this->db->get("ap_store")->result();
		$data['group_customer'] = $this->db->get("ap_customer_group");
		$data['provinsi'] = $this->db->get("ae_provinsi");
		$this->global['pageTitle'] = "Toko Bangunan - Tambah Customer";
		$this->loadViews("customer/body_add_customer",$this->global,$data,"footer");
	}

	function hapus_customer(){
		$id = $_GET['id'];

		$this->modelCustomer->hapus_customer($id);
		
		$affect = $this->db->affected_rows();

		if($affect > 0){
			$this->session->set_flashdata('message', 'Data Berhasil Dihapus !');
		} else {
			$this->session->set_flashdata('message', 'Data Gagal Dihapus !');
		}

		redirect("customer");
	}

	function edit_customer(){
		$data['group_customer'] = $this->db->get("ap_customer_group");
		$data['provinsi'] = $this->db->get("ae_provinsi");

		$id_customer 	= $this->input->get('id');
		$data['customer'] = $this->db->get_where("ap_customer",array("id_customer" => $id_customer));
		$this->global['pageTitle'] = "Toko Bangunan - Edit Customer";
		$this->loadViews("customer/body_edit_customer",$this->global,$data,"footer");
	}

	function edit_customer_sql(){
		$nama 	 	= $_POST['nama'];
		$kontak 	= $_POST['kontak'];
		$alamat 	= $_POST['alamat'];
		$provinsi 	= $_POST['provinsi'];
		$kabupaten 	= $_POST['kabupaten'];
		$kecamatan 	= $_POST['kecamatan'];
		$group 		= $_POST['group'];
		$idCustomer = $_POST['id_customer'];
		$lm_d 			= $_POST['limit_day'];
		$lm_k 			= $_POST['limit_kredit'];
		$jc 			= $_POST['jenis_cust'];
		$ppn 			= '';
	
		$data_customer = array(
								"nama"				=> $nama,
								"kontak"			=> $kontak,
								"tanggal_gabung"	=> date('Y-m-d'),
								"alamat"			=> $alamat,
								"id_provinsi"		=> $provinsi,
								"id_kabupaten"		=> $kabupaten,
								"id_kecamatan"		=> $kecamatan,
								"kategori"			=> $group,
								"limit_day"			=> $lm_d,
								"limit_kredit"		=> $lm_k,
								"jenis_cust"		=> $jc,
								"ppn"				=> $ppn
							  );
		
		
		$affect = $this->modelCustomer->editCustomer($idCustomer,$data_customer);

		// if($affect > 0){
		// 	$this->session->set_flashdata('message', 'Data Berhasil Diubah !');
		// } else {
		// 	$this->session->set_flashdata('message2', 'Data Gagal Diubah !');
		// }

		redirect("customer");	
	}

	function add_customer_sql(){
		$nama 	 		= $_POST['nama'];
		$kontak 		= $_POST['kontak'];
		$alamat 		= $_POST['alamat'];
		$provinsi 		= $_POST['provinsi'];
		$kabupaten 		= $_POST['kabupaten'];
		$kecamatan 		= $_POST['kecamatan'];
		$group 			= $_POST['group'];
		$lm_d 			= $_POST['limit_day'];
		$lm_k 			= $_POST['limit_kredit'];
		$jc 			= $_POST['jenis_cust'];
		$appp_store 	= $_POST['store'];
		$ppn 			= '';
		$tanggal_lahir	= $_POST['tanggal_lahir'];
		$group_customer		= $_POST['group'];	
		$password		= $_POST['password'];
		$email		= $_POST['email'];
		$group 			= "";
		$data_customer = array(
								"nama"				=> $nama,
								"kontak"			=> $kontak,
								"tanggal_lahir"		=> $tanggal_lahir,
								"tanggal_gabung"	=> date('Y-m-d'),
								"alamat"			=> $alamat,
								"id_provinsi"		=> $provinsi,
								"id_kabupaten"		=> $kabupaten,
								"id_kecamatan"		=> $kecamatan,
								"kategori"			=> $group,
								"limit_day"			=> $lm_d,
								"limit_kredit"		=> $lm_k,
								"jenis_cust"		=> $jc,
								"ppn"				=> $ppn,
								"id_toko"			=> $appp_store,
								"level_id"	=> $group_customer 
							  );
							  $additionalData = array(
								"toko"			=> $appp_store						
								);
		$this->ion_auth->register($kontak,$password,$email,$additionalData,$group);
		$affect = $this->modelCustomer->addCustomer($data_customer);

		if($affect > 0){
			$this->session->set_flashdata('message', 'Data Berhasil Ditambahkan !');
		} else {
			$this->session->set_flashdata('message', 'Data Gagal Ditambahkan !');
		}

		redirect("customer");
	}

	function list_kabupaten(){
		$id = $_POST['id'];

		$kabupaten = $this->db->get_where("ae_kabupaten", array("id_provinsi" => $id));

		foreach($kabupaten->result() as $dt){
			echo "<option value='".$dt->kabupaten_id."'>".$dt->nama_kabupaten."</option>";
		}
	}

	function list_kecamatan(){
		$id  = $_POST['id'];

		$kecamatan = $this->db->get_where("ae_kecamatan",array("kabupaten_id" => $id));

		foreach($kecamatan->result() as $dt){
			echo "<option value='".$dt->id_kecamatan."'>".$dt->kecamatan."</option>";
		}
	}
}