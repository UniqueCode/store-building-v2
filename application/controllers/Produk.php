<?php
defined('BASEPATH') or exit('No direct script access allowed');

include_once APPPATH . '/third_party/fpdf/fpdf.php';
require APPPATH . '/libraries/BaseController.php';

class Produk extends BaseController
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper("url");
		$this->load->database();
		$this->load->model(array('model1', 'modelProduk'));
		$this->load->library("session");

		$this->isLoggedIn($this->global['idUser'], 2, 1);
	}

	function index()
	{
		$this->global['pageTitle'] = "Toko Bangunan - Produk";
		$this->loadViews("produk/body_produk", $this->global, NULL, "produk/footerProduk");
	}

	function datatablesProduk()
	{
		$draw 		= $_REQUEST['draw'];
		$length 	= $_REQUEST['length'];
		$start 		= $_REQUEST['start'];
		$search 	= $_REQUEST['search']["value"];

		$total 			 			= $this->modelProduk->totalProdukActive();
		$output 					= array();
		$output['draw']	 			= $draw;
		$output['recordsTotal'] 	= $output['recordsFiltered'] = $total;
		$output['data'] 			= array();

		if ($search != "") {
			$query = $this->modelProduk->daftarProdukAll($length, $start, $search);
			$output['recordsTotal'] = $output['recordsFiltered'] = $query->num_rows();
		} else {
			$query = $this->modelProduk->daftarProdukAll($length, $start, $search);
		}

		$nomor_urut = $start + 1;
		foreach ($query->result_array() as $dt) {
			if ($dt['status'] == 1) {
				$status = "<label class='label label-success'>Aktif</label>";
			} elseif ($dt['status'] == 0) {
				$status = "<label class='label label-warning'>Non Aktif</label>";
			} else {
				$status = "";
			}

			$output['data'][] = array($nomor_urut, $dt['id_produk'], $dt['nama_produk'], $dt['satuan'], $dt['kategori'] . " / " . $dt['kategori_level_1'] . " / " . $dt['kategori_3'], $status, "<a href='" . base_url('produk/edit_produk?sku=' . $dt['id_produk']) . "'><i class='fa fa-pencil'></i></a> | <a onclick='return confirm('Yakin Hapus Data?')' href='" . base_url('produk/hapus_produk?sku=' . $dt['id_produk']) . "'><i class='fa fa-trash'></i></a>");
			$nomor_urut++;
		}

		echo json_encode($output);
	}

	function add_produk()
	{
		$this->global['pageTitle'] = "Toko Bangunan - Tambah Produk";
		$this->global['navigation'] = $this->model1->callNavigation();

		$this->db->select_max('id_produk');
        $this->db->from('ap_produk');
		$query = $this->db->get()->row()->id_produk;
		$d = substr($query, -3);
        $lastid = sprintf('%03d', $d+1);
		if($lastid){
			$idsku = $lastid;
		} else {
			$idsku = "001";
		}
		$data['idsku'] = $idsku;
		$toko_id = $this->ion_auth->user()->row()->toko;
		$data['store'] = $this->db->get_where("ap_store",array("id_store"=>$toko_id))->row();
		$data['satuan'] = $this->db->get("satuan")->result();
		$data['stand'] = $this->db->get("ap_stand")->result();
		$data['show_kategori'] = $this->db->get("ap_kategori");
		$this->loadViews("produk/body_add_produk", $this->global, $data, "produk/footer_add_produk");
	}

	function get_subkategori()
	{
		$id_kategori = $_POST['id_kategori'];

		$data['show_sub'] = $this->db->get_where("ap_kategori_1", array("id_kategori" => $id_kategori));

		$count = 0;

		foreach ($data['show_sub']->result() as $row) {
			$count = $count + $row->id;
		}

		if (!empty($_POST['id_kategori']) && $count > 0) {
			$this->load->view("bahan_baku/show_sub", $data);
		}
	}

	function spinner()
	{
		echo "<img src='" . base_url('assets/loading.gif') . "'/>";
	}

	function cekSKUIfExist()
	{
		$sku 	= $_POST['sku'];

		$this->load->model("modelProduk");
		$count = $this->modelProduk->cekSKUIfExist($sku);

		echo $count;
	}

	function tambahProdukNonProduksiSQL()
	{
		var_dump($_POST);
		$toko_id = $this->ion_auth->user()->row()->toko;
		$id_produk	 	= $_POST['sku'];
		$nama_produk 	= $_POST['namaProduk'];
		$kategori 		= $_POST['kategori'];
		$harga_beli 	= $_POST['hargaBeli'];
		$satuan 		= $_POST['satuan'];
		$pajak 			= $_POST['pajak'];
		if (empty($_POST['kategori2'])) {
			$subkategori = '';
		} else {
			$subkategori = $_POST['kategori2'];
		}

		if (empty($_POST['kategori3'])) {
			$subkategori3 = '';
		} else {
			$subkategori3 = $_POST['kategori3'];
		}


		$data_upload = array(
			"id_produk"			=> $id_produk,
			"nama_produk"		=> $nama_produk,
			"diskon"			=> 0,
			"hpp"				=> $harga_beli,
			"id_kategori"		=> $kategori,
			"id_subkategori"	=> $subkategori,
			"id_subkategori_2"	=> $subkategori3,
			"status"			=> 1,
			"satuan"			=> $satuan,
			"stok"				=> 0,
			"type"				=> 1,
			"tempat"			=> $toko_id,
			"pajak"				=> $pajak
		);

		$this->modelProduk->insertProduk($data_upload);
			$idStore 	= $_POST['idStore'];
			$hargaJual 	= $_POST['hargaJual'];
			$hargaJual2 	= $_POST['hargaJual2'];
			$hargaJual3 	= $_POST['hargaJual3'];
			$dataHarga = array(
				"id_toko"		=> $idStore,
				"id_produk"		=> $id_produk,
				"harga"			=> $hargaJual,
				"harga2"		=> $hargaJual2,
				"harga3"		=> $hargaJual3
			);
		$this->modelProduk->insertHargaJual($dataHarga);
	}

	function form_produk_non_produksi()
	{
		$this->db->select_max('id_produk');
        $this->db->from('ap_produk');
		$query = $this->db->get()->row()->id_produk;
		$d = substr($query, -3);

        $lastid = sprintf('%03d', $d+1);
		if($lastid){
			$idsku = $lastid;
		} else {
			$idsku = "001";
		}
		$data['idsku'] = $idsku;
		$data['show_kategori'] = $this->db->get("ap_kategori");
		$data['sku'] = $this->model1->count_produk();
		$data['satuan'] = $this->db->get("satuan")->result();
		$data['stand'] = $this->db->get("ap_stand")->result();
		$data['store'] = $this->db->get("ap_store")->row();
		$this->load->view("produk/form_produk_non_produksi", $data);
	}

	function edit_produk()
	{
		
		$data['show_kategori'] = $this->db->get("ap_kategori");
		$data['satuan'] = $this->db->get("satuan");
		$data['stand'] = $this->db->get("ap_stand")->result();
		$toko_id = $this->ion_auth->user()->row()->toko;
		$data['store'] = $this->db->get_where("ap_store",array("id_store"=>$toko_id))->result();
		
		$sku = $_GET['sku'];
		$data['sku'] = $sku;
		$this->load->model("modelProduk");
		$data['produk'] = $this->modelProduk->produkJoin($sku);
		$this->global['pageTitle'] = "Toko Bangunan - Edit Produk";
		$this->loadViews("produk/body_edit_produk", $this->global, $data, "produk/footer_edit_produk");
	}

	function editProdukNonProduksiSQL()
	{
		var_dump($_POST);
		$toko_id = $this->ion_auth->user()->row()->toko;
		$id_produk	 	= $_POST['sku'];
		$nama_produk 	= $_POST['namaProduk'];
		$hargaBeli 		= $_POST['hargaBeli'];
		$kategori 		= $_POST['kategori'];
		$subkategori    = $_POST['kategori2'];
		$subkategori3  	= $_POST['kategori3'];
		$satuan 		= $_POST['satuan'];
		$status 		= $_POST['status'];
		$pajak 			= $_POST['pajak'];
		$idStore 		= $_POST['idStore'];
		
		$data_upload = array(
			"nama_produk"		=> $nama_produk,
			"hpp"				=> $hargaBeli,
			"diskon"			=> 1,
			"id_kategori"		=> $kategori,
			"id_subkategori"	=> $subkategori,
			"id_subkategori_2"	=> $subkategori3,
			"status"			=> $status,
			"satuan"			=> $satuan,
			"stok"				=> 0,
			"type"				=> 1,
			"tempat"			=> $toko_id,
			"pajak"				=> $pajak
		);

		$this->modelProduk->updateProduk($id_produk, $data_upload);

			$idStore 	= $idStore;
			$hargaJual 	= $_POST['hargaJual'];
			$hargaJual2 	= $_POST['hargaJual2'];
			$hargaJual3 	= $_POST['hargaJual3'];
			$countIfStoreExist = $this->modelProduk->countIfStoreExist($idStore, $id_produk);
echo $countIfStoreExist;

			if ($countIfStoreExist > 0) {
				$dataHarga = array(
					"harga"			=> $hargaJual,
					"harga2"		=> $hargaJual2,
					"harga3"		=> $hargaJual3
				);

				$this->modelProduk->updateHargaPertoko($idStore, $id_produk, $dataHarga);
			}
			 else {
				$dataHarga = array(
					"id_toko"		=> $idStore,
					"id_produk"		=> $id_produk,
					"harga"			=> $hargaJual,
					"harga2"		=> $hargaJual2,
					"harga3"		=> $hargaJual3
				);

				$this->modelProduk->insertNewHargaPertoko($dataHarga);
			}
		
	}

	function hapus_produk()
	{
		$sku 		= $this->input->get("sku");

		$updateDataProduk = array(
			"status" => 2
		);

		$this->modelProduk->hapusProduk($sku, $updateDataProduk);
		redirect("produk");
	}

	function exportExcelProduk()
	{
		$this->load->library("excel/PHPExcel");

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'No')
			->setCellValue('B1', 'SKU')
			->setCellValue('C1', 'Nama Produk')
			->setCellValue('D1', 'Kategori')
			->setCellValue('E1', 'Harga Beli')
			->setCellValue('F1', 'Status');

		$data_stok = $this->model1->daftarProdukAll();

		$i = 2;
		foreach ($data_stok as $row) {

			$status = $row->status === '1' ? "Aktif" : "Non Aktif";


			$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $i - 1)
				->setCellValue('B' . $i, $row->id_produk)
				->setCellValue('C' . $i, $row->nama_produk)
				->setCellValue('D' . $i, $row->kategori . " / " . $row->kategori_level_1 . " / " . $row->kategori_3)
				->setCellValue('E' . $i, $row->harga)
				->setCellValue('F' . $i, $status);
			$i++;
		}


		//set title pada sheet (me rename nama sheet)
		$objPHPExcel->getActiveSheet()->setTitle('Sheet 1');

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Rifal")
			->setLastModifiedBy("Rifal")
			->setTitle("TukuWebsite | Simple PSB System")
			->setSubject("TukuWebsite | Simple PSB System")
			->setDescription("Export Data")
			->setKeywords("office 2007 openxml php")
			->setCategory("Data SO");

		//mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');


		//sesuaikan headernya 
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		//ubah nama file saat diunduh
		header('Content-Disposition: attachment;filename=ExportDataProduk.xlsx');
		//unduh file

		$objWriter->save("php://output");
	}

	function formEditNonProduksi()
	{
		$data['show_kategori'] = $this->db->get("ap_kategori");
		$data['satuan'] = $this->db->get("satuan");
		$data['stand'] = $this->db->get("ap_stand")->result();
		$toko_id = $this->ion_auth->user()->row()->toko;
		$data['store'] = $this->db->get_where("ap_store",array("id_store"=>$toko_id))->row();
		
		$sku = $_POST['sku'];

		$data['sku'] = $sku;

		$this->load->model("modelProduk");
		$data['produk'] = $this->modelProduk->produkJoin($sku);

		$this->load->view("produk/editNonProduksi", $data);
	}

	function massUpdate()
	{
		$uri = $this->uri->segment(3);
		$data['permitAccess'] = $this->model1->permitAccess($this->global['idUser']);
		$data['permitAccessSub'] = $this->model1->permitAccessSub($this->global['idUser']);
		$data['navigation'] = $this->model1->callNavigation();

		if ($uri == 'kategori') {
			$data['pageTitle'] = "Toko Bangunan - Mass Update Kategori";
			$this->load->view("navigation", $data);
			$this->massUpdateKategori();
		} elseif ($uri == 'hargaBeli') {
			$data['pageTitle'] = "Toko Bangunan - Mass Update Harga Beli";
			$this->load->view("navigation", $data);
			$this->massUpdateHargaBeli();
		} elseif ($uri == 'hargaJual') {
			$data['pageTitle'] = "Toko Bangunan - Mass Update Harga Jual";
			$this->load->view("navigation", $data);
			$this->massUpdateHargaJual();
		}
	}

	function massUpdateKategori()
	{
		$this->load->view("produk/massupdate/bodyMassKategori");
		$this->load->view("produk/massupdate/footerMassKategori");
	}

	function massUpdateHargaBeli()
	{
		$this->load->view("produk/massupdate/bodyMassHargaBeli");
		$this->load->view("produk/massupdate/footerMassHargaBeli");
	}

	function massUpdateHargaJual()
	{
		$data['toko'] = $this->db->get("ap_store")->result();
		$data['show_kategori'] = $this->db->get("ap_kategori")->result();
		$data['stand'] = $this->db->get("ap_stand")->result();
		$this->load->view("produk/massupdate/bodyMassHargaJual", $data);
		$this->load->view("produk/massupdate/footerMassHargaJual");
	}

	function templateUpdateKategori()
	{
		$this->load->library("excel/PHPExcel");

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'No')
			->setCellValue('B1', 'SKU')
			->setCellValue('C1', 'Nama Produk')
			->setCellValue('D1', 'Kategori')
			->setCellValue('E1', 'ID Kategori')
			->setCellValue('F1', 'ID Subkategori')
			->setCellValue('G1', 'ID Subkategori');

		$data_stok = $this->modelProduk->exportTemplateKategori();

		$i = 2;
		foreach ($data_stok as $row) {

			$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $i - 1)
				->setCellValue('B' . $i, $row->id_produk)
				->setCellValue('C' . $i, $row->nama_produk)
				->setCellValue('D' . $i, $row->kategori . " / " . $row->kategori_level_1 . " / " . $row->kategori_3)
				->setCellValue('E' . $i, $row->id_kategori)
				->setCellValue('F' . $i, $row->id_subkategori)
				->setCellValue('G' . $i, $row->id_subkategori_2);
			$i++;
		}


		//set title pada sheet (me rename nama sheet)
		$objPHPExcel->getActiveSheet()->setTitle('Sheet 1');

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Rifal")
			->setLastModifiedBy("Rifal")
			->setTitle("TukuWebsite | Simple PSB System")
			->setSubject("TukuWebsite | Simple PSB System")
			->setDescription("Export Data")
			->setKeywords("office 2007 openxml php")
			->setCategory("Data SO");

		//mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		//sesuaikan headernya 
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		//ubah nama file saat diunduh
		header('Content-Disposition: attachment;filename=Mass Update Kategori Template.xlsx');
		//unduh file
		$objWriter->save("php://output");
	}

	function templateUpdateHargaBeli()
	{
		$this->load->library("excel/PHPExcel");

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'No')
			->setCellValue('B1', 'SKU')
			->setCellValue('C1', 'Nama Produk')
			->setCellValue('D1', 'Kategori')
			->setCellValue('E1', 'Harga Beli');

		$data_stok = $this->modelProduk->exportTemplateKategori();

		$i = 2;
		foreach ($data_stok as $row) {

			$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $i - 1)
				->setCellValue('B' . $i, $row->id_produk)
				->setCellValue('C' . $i, $row->nama_produk)
				->setCellValue('D' . $i, $row->kategori . " / " . $row->kategori_level_1 . " / " . $row->kategori_3)
				->setCellValue('E' . $i, $row->hpp);
			$i++;
		}


		//set title pada sheet (me rename nama sheet)
		$objPHPExcel->getActiveSheet()->setTitle('Sheet 1');

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Rifal")
			->setLastModifiedBy("Rifal")
			->setTitle("TukuWebsite | Simple PSB System")
			->setSubject("TukuWebsite | Simple PSB System")
			->setDescription("Export Data")
			->setKeywords("office 2007 openxml php")
			->setCategory("Data SO");

		//mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		//sesuaikan headernya 
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		//ubah nama file saat diunduh
		header('Content-Disposition: attachment;filename=Mass Update Harga Beli Template.xlsx');
		//unduh file
		$objWriter->save("php://output");
	}

	function kategoriProdukJual()
	{
		$pdf = new FPDF('P', 'mm', 'A4');

		$pdf->AddPage();
		// setting jenis font yang akan digunakan
		$pdf->SetFont('Arial', 'B', 16);
		// mencetak string 
		$pdf->Cell(190, 7, 'CATEGORY TREE', 0, 1, 'C');
		$pdf->SetFont('Arial', 'B', 12);
		$pdf->Cell(190, 7, date('d F Y'), 0, 1, 'C');



		$kategori = $this->db->get("ap_kategori")->result();

		$pdf->SetFont('Arial', '', 10);
		foreach ($kategori as $row) {
			$pdf->Cell(8, 6, "(" . $row->id_kategori . ")", 0, 0);
			$pdf->Cell(100, 6, $row->kategori, 0, 1);

			$subkategori = $this->db->get_where("ap_kategori_1", array("id_kategori" => $row->id_kategori))->result();

			foreach ($subkategori as $dt) {
				$pdf->Cell(8, 6, "            (" . $dt->id . ")", 0, 0);
				$pdf->Cell(100, 6, "             " . $dt->kategori_level_1, 0, 1);

				$subsubkategori = $this->db->get_where("ap_kategori_2", array("id_kategori_1" => $dt->id))->result();

				foreach ($subsubkategori as $bk) {
					$pdf->Cell(8, 6, "                          (" . $bk->id . ")", 0, 0);
					$pdf->Cell(100, 6, "                        " . $bk->kategori_3, 0, 1);
				}
			}
		}

		$pdf->Output();
	}

	function massUpdateKategoriSQL()
	{
		$config['upload_path']          = './assets/temp/';
		$config['allowed_types']        = 'xls|xlsx';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file')) {
			$error = array('error' => $this->upload->display_errors());
			echo $error['error'];
		} else {
			$upload_data = $this->upload->data();
			$this->load->library('excel/PHPExcel');

			$file =  $upload_data['full_path'];
			$objPHPExcel = PHPExcel_IOFactory::load($file);

			$sheets = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

			//do update
			$i = 1;
			$dataUpdate = array();
			foreach ($sheets as $row) {
				if ($i > 1) {
					$id_produk = $row['B'];
					$idKategori = $row['E'];
					$subkategori = $row['F'];
					$subsubkategori = $row['G'];

					$dataUpdate[] = array(
						"id_produk" => $id_produk,
						"id_kategori" => $idKategori,
						"id_subkategori" => $subkategori,
						"id_subkategori_2" => $subsubkategori
					);
				}
				$i++;
			}

			$this->db->update_batch('ap_produk', $dataUpdate, 'id_produk');
			unlink($file);
		}
	}


	function massUpdateHargaBeliSQL()
	{
		$config['upload_path']          = './assets/temp/';
		$config['allowed_types']        = 'xls|xlsx';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file')) {
			$error = array('error' => $this->upload->display_errors());
			echo $error['error'];
		} else {
			$upload_data = $this->upload->data();
			$this->load->library('excel/PHPExcel');

			$file =  $upload_data['full_path'];
			$objPHPExcel = PHPExcel_IOFactory::load($file);

			$sheets = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

			//do update
			$i = 1;
			$dataUpdate = array();
			foreach ($sheets as $row) {
				if ($i > 1) {
					$id_produk = $row['B'];
					$hargaBeli = $row['E'];

					$dataUpdate[] = array(
						"id_produk" => $id_produk,
						"hpp"		=> $hargaBeli
					);
				}
				$i++;
			}

			$this->db->update_batch('ap_produk', $dataUpdate, 'id_produk');
			unlink($file);
		}
	}

	function templateUpdateHargaJual()
	{
		$this->load->library("excel/PHPExcel");

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'No')
			->setCellValue('B1', 'SKU')
			->setCellValue('C1', 'Nama Produk')
			->setCellValue('D1', 'Kategori')
			->setCellValue('E1', 'Tempat')
			->setCellValue('F1', 'Kode Toko')
			->setCellValue('G1', 'Toko')
			->setCellValue('H1', 'Harga Jual');

		$idKategori 		= $_POST['kategori'];

		if (!empty($_POST['subkategori_2'])) {
			$subkategori = $_POST['subkategori_2'];
		} else {
			$subkategori = '';
		}

		if (!empty($_POST['subkategori_3'])) {
			$subSubKategori 	= $_POST['subkategori_3'];
		} else {
			$subSubKategori = '';
		}

		$idToko 			= $_POST['toko'];
		$idStand 			= $_POST['stand'];

		$data_stok = $this->modelProduk->exportTemplateHargaJual($idToko, $idKategori, $subkategori, $subSubKategori, $idStand);

		$i = 2;
		foreach ($data_stok as $row) {

			$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $i - 1)
				->setCellValue('B' . $i, $row->id_produk)
				->setCellValue('C' . $i, $row->nama_produk)
				->setCellValue('D' . $i, $row->kategori . " / " . $row->kategori_level_1 . " / " . $row->kategori_3)
				->setCellValue('E' . $i, $row->stand)
				->setCellValue('F' . $i, $row->id_toko)
				->setCellValue('G' . $i, $row->store)
				->setCellValue('H' . $i, $row->harga);
			$i++;
		}


		//set title pada sheet (me rename nama sheet)
		$objPHPExcel->getActiveSheet()->setTitle('Sheet 1');

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Rifal")
			->setLastModifiedBy("Rifal")
			->setTitle("TukuWebsite | Simple PSB System")
			->setSubject("TukuWebsite | Simple PSB System")
			->setDescription("Export Data")
			->setKeywords("office 2007 openxml php")
			->setCategory("Data SO");

		//mulai menyimpan excel format xlsx, kalau ingin xls ganti Excel2007 menjadi Excel5          
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

		//sesuaikan headernya 
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		//ubah nama file saat diunduh
		header('Content-Disposition: attachment;filename=Mass Update Harga Jual Template.xlsx');
		//unduh file
		$objWriter->save("php://output");
	}

	function massUpdateHargaJualSQL()
	{
		$config['upload_path']          = './assets/temp/';
		$config['allowed_types']        = 'xls|xlsx';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file')) {
			$error = array('error' => $this->upload->display_errors());
			echo $error['error'];
		} else {
			$upload_data = $this->upload->data();
			$this->load->library('excel/PHPExcel');

			$file =  $upload_data['full_path'];
			$objPHPExcel = PHPExcel_IOFactory::load($file);

			$sheets = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

			//do update
			$i = 1;
			$dataUpdate = array();
			foreach ($sheets as $row) {
				if ($i > 1) {
					$id_produk = $row['B'];
					$hargaJual = $row['H'];
					$idToko = $row['F'];

					$dataUpdate = array(
						"harga"		=> $hargaJual
					);

					$this->db->where("id_toko", $idToko);
					$this->db->where("id_produk", $id_produk);
					$this->db->update("ap_produk_price", $dataUpdate);
				}
				$i++;
			}
			unlink($file);
		}
	}
}
