<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Welcome_page extends BaseController{
	function __construct(){
		parent::__construct();
		$this->load->helper("url");
		$this->load->model("model1");
	}

	function index(){
		$data['toko'] = $this->db->get("ap_store");
		$this->global['pageTitle'] = "Toko Bangunan";
		$data['navigation'] = $this->model1->callNavigation();
		$this->loadViews("bodyWelcome",$this->global,$data,"footer_empty");
	}
	function edit(){
		$id = $this->uri->segment(3);	
		$user_id = $this->ion_auth->user()->row()->id;
		$data_toko = array(
			"toko"		=> $id,
			
	  );

		$this->db->where("id",$user_id);
		$berhasil = $this->db->update("users",$data_toko);
		$affect = $this->db->affected_rows();
		if($berhasil){
			redirect("dashboard");
		}
		
	}

}