 <?php
    if (! function_exists('generateRandomString')) {
        function generateRandomString($merk='',$alpha = true, $nums = true, $usetime = false, $string = '', $length = 120) {
            $alpha = ($alpha == true) ? 'QWERTYUIOPASDFGHJKLZXCVBNM' : '';
            $nums = ($nums == true) ? '1234567890' : '';
            
            if ($alpha == true || $nums == true || !empty($string)) {
                if ($alpha == true) {
                    $alpha = $alpha;
                    $alpha .= strtoupper($alpha);
                } 
            }
            $randomstring = '';
            $totallength = $length;
                for ($na = 0; $na < $totallength; $na++) {
                        $var = (bool)rand(0,1);
                        if ($var == 1 && $alpha == true) {
                            $randomstring .= $alpha[(rand() % mb_strlen($alpha))];
                        } else {
                            $randomstring .= $nums[(rand() % mb_strlen($nums))];
                        }
                }
            if ($usetime == true) {
                $randomstring = $randomstring.time();
            }
            return $merk.'-'.$randomstring.'-';
            }
    }
    if (! function_exists('json_response')) {

        /**
         * Create json response
         *
         * @param $data
         * @param $code int
         * @return string
         */
        function json_response($data, $code = 200) {
            set_status_header($code);
            header("Content-Type: application/json");
            echo json_encode($data);
            exit();
        }
    }
    