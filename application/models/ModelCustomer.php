<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class ModelCustomer extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function hapus_customer($id){
		$this->db->delete("ap_customer",array("id_customer" => $id));
	}

	function editCustomer($idCustomer,$data_customer){
		$this->db->where("id_customer",$idCustomer);
		$this->db->update("ap_customer",$data_customer);
		$affect = $this->db->affected_rows();
		return $affect;
	}

	function addCustomer($data_customer){
		$this->db->insert("ap_customer",$data_customer);
		$affect = $this->db->affected_rows();
		return $affect;
	}

	function hapusGroup($id){
		$this->db->delete("ap_customer_group",array("id_group" => $id));
	}

	function addCustomerGroup($data_grup){
		$this->db->insert("ap_customer_group",$data_grup);
	}

	function updateGroup($id,$dataUpdate){
		$this->db->where("id_group",$id);
		$this->db->update("ap_customer_group",$dataUpdate);
	}
}