<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Model_dashboard extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function total_sales($tanggal){
		$this->db->select(array("SUM(ap_invoice_number.total) as total","SUM(ap_invoice_number.diskon) as diskon","SUM(ap_invoice_number.diskon_free) as diskon_free","SUM(ap_invoice_number.poin_value) as poin_value","SUM(ap_invoice_number.diskon_otomatis) as diskon_otomatis"));
		$this->db->from("ap_invoice_number");

		if(!empty($tanggal)){
			$this->db->where("DATE(ap_invoice_number.tanggal)",$tanggal);
		}

		$query = $this->db->get()->result();

		foreach($query as $row){
			$total 	= $row->total;
			$diskon = $row->diskon;
			$diskon_free =$row->diskon_free;
			$reimburs = $row->poin_value;
			$diskonOtomatis = $row->diskon_otomatis;

			return $total-($diskon+$diskon_free+$reimburs+$diskonOtomatis);
		}
	}

	function total_sales_perbulan($bulan,$tahun){
		$this->db->select(array("SUM(ap_invoice_number.total) as total","SUM(ap_invoice_number.diskon) as diskon","SUM(ap_invoice_number.diskon_free) as diskon_free","SUM(ap_invoice_number.poin_value) as poin_value","SUM(ap_invoice_number.diskon_otomatis) as diskon_otomatis"));
		$this->db->from("ap_invoice_number");

		if(!empty($bulan)){
			$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		}

		if(!empty($tahun)){
			$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		}

		$query = $this->db->get()->result();

		foreach($query as $row){
			$total 	= $row->total;
			$diskon = $row->diskon;
			$diskon_free =$row->diskon_free;
			$reimburs = $row->poin_value;
			$diskonOtomatis = $row->diskon_otomatis;

			return $total-($diskon+$diskon_free+$reimburs+$diskonOtomatis);
		}
	}

	function total_sales_pertahun($tahun){
		$this->db->select(array("SUM(ap_invoice_number.total) as total","SUM(ap_invoice_number.diskon) as diskon","SUM(ap_invoice_number.diskon_free) as diskon_free","SUM(ap_invoice_number.poin_value) as poin_value","SUM(ap_invoice_number.diskon_otomatis) as diskon_otomatis"));
		$this->db->from("ap_invoice_number");

		if(!empty($tahun)){
			$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		}

		$query = $this->db->get()->result();

		foreach($query as $row){
			$total 	= $row->total;
			$diskon = $row->diskon;
			$diskon_free =$row->diskon_free;
			$reimburs = $row->poin_value;
			$diskonOtomatis = $row->diskon_otomatis;

			return $total-($diskon+$diskon_free+$reimburs+$diskonOtomatis);
		}
	}

	function transaction($tanggal){
		$this->db->select("COUNT(ap_invoice_number.no_invoice) as trx");
		$this->db->from("ap_invoice_number");

		if(!empty($tanggal)){
			$this->db->where("DATE(ap_invoice_number.tanggal)",$tanggal);
		}

		$query = $this->db->get()->result();

		foreach($query as $row){
			return $row->trx;
		}
	}

	function transactionPerbulan($bulan,$tahun){
		$this->db->select("COUNT(ap_invoice_number.no_invoice) as trx");
		$this->db->from("ap_invoice_number");

		if(!empty($bulan)){
			$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		}

		if(!empty($tahun)){
			$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		}

		$query = $this->db->get()->result();

		foreach($query as $row){
			return $row->trx;
		}
	}

	function transactionPertahun($tahun){
		$this->db->select("COUNT(ap_invoice_number.no_invoice) as trx");
		$this->db->from("ap_invoice_number");

		if(!empty($tahun)){
			$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		}

		$query = $this->db->get()->result();

		foreach($query as $row){
			return $row->trx;
		}
	}

	function totalItemTerjual($tanggal){
		$this->db->select("SUM(ap_invoice_item.qty) as qty");
		$this->db->from("ap_invoice_item");
		$this->db->where("tanggal",$tanggal);
		$query = $this->db->get()->row();
		return $query->qty;		
	}

	function totalItemTerjualPerbulan($bulan,$tahun){
		$this->db->select("SUM(ap_invoice_item.qty) as qty");
		$this->db->from("ap_invoice_item");
		$this->db->where("MONTH(ap_invoice_item.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_item.tanggal)",$tahun);
		$query = $this->db->get()->row();
		return $query->qty;		
	}

	function totalItemTerjualPertahun($tahun){
		$this->db->select("SUM(ap_invoice_item.qty) as qty");
		$this->db->from("ap_invoice_item");
		$this->db->where("YEAR(ap_invoice_item.tanggal)",$tahun);
		$query = $this->db->get()->row();
		return $query->qty;		
	}

	function previous_transaction(){
		$bulan = date('m');
		$tahun  = date('Y');

		if($bulan==01){
			$last_month = 01;
			$last_year  = $tahun-1;
		} else {
			$last_month = sprintf("%02d",$bulan-1);
			$last_year  = $tahun;
		}

		$this->db->select("COUNT(ap_invoice_number.no_invoice) as trx");
		$this->db->from("ap_invoice_number");
		$this->db->where("MONTH(ap_invoice_number.tanggal)",$last_month);
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$last_year);
		$query = $this->db->get()->result();

		foreach($query as $row){
			return $row->trx;
		}
	}

	function customer(){
		$bulan = date('m');
		$tahun  = date('Y');

		$this->db->select("COUNT(ap_customer.id_customer) as customer");
		$this->db->from("ap_customer");
		$this->db->where("MONTH(ap_customer.tanggal_gabung)",$bulan);
		$this->db->where("YEAR(ap_customer.tanggal_gabung)",$tahun);
		$query = $this->db->get()->result();

		foreach($query as $row){
			return $row->customer;
		}
	}

	function previous_customer(){
		$bulan = date('m');
		$tahun  = date('Y');

		if($bulan==01){
			$last_month = 01;
			$last_year  = $tahun-1;
		} else {
			$last_month = sprintf("%02d",$bulan-1);
			$last_year  = $tahun;
		}

		$this->db->select("COUNT(ap_customer.id_customer) as customer");
		$this->db->from("ap_customer");
		$this->db->where("MONTH(ap_customer.tanggal_gabung)",$last_month);
		$this->db->where("YEAR(ap_customer.tanggal_gabung)",$last_year);
		$query = $this->db->get()->result();

		foreach($query as $row){
			return $row->customer;
		}
	}


	function sales_perkategori(){
		$bulan = date('m');
		$tahun  = date('Y');

		$this->db->select(array("ap_kategori.kategori","SUM((ap_invoice_item.harga_jual*ap_invoice_item.qty))  as total"));
		$this->db->from("ap_invoice_item");
		$this->db->join("ap_produk","ap_produk.id_produk = ap_invoice_item.id_produk","left");
		$this->db->join("ap_kategori","ap_kategori.id_kategori = ap_produk.id_kategori","left");
		//$this->db->join("ap_invoice_number","ap_invoice_number.no_invoice = ap_invoice_item.no_invoice","left");
		$this->db->where("MONTH(ap_invoice_item.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_item.tanggal)",$tahun);
		$this->db->group_by("ap_kategori.id_kategori");
		return $this->db->get()->result();
	}

	function discount_channel(){
		$bulan = date('m');
		$tahun  = date('Y');

		$this->db->select_sum("diskon");
		$this->db->from("ap_invoice_number");
		$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$query = $this->db->get()->result();

		foreach($query as $row){
			return $row->diskon;
		}
	}

	function discount(){
		$bulan = date('m');
		$tahun  = date('Y');

		$this->db->select_sum("diskon_free");
		$this->db->from("ap_invoice_number");
		$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$query = $this->db->get()->result();

		foreach($query as $row){
			return $row->diskon_free;
		}
	}

	function poin(){
		$bulan = date('m');
		$tahun  = date('Y');

		$this->db->select_sum("poin_value");
		$this->db->from("ap_invoice_number");
		$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$query = $this->db->get()->result();

		foreach($query as $row){
			return $row->poin_value;
		}
	}

	function customer_group(){
		$bulan = date('m');
		$tahun  = date('Y');

		$this->db->select(array("ap_customer_group.group_customer","SUM(ap_invoice_number.total) as total"));
		$this->db->from("ap_invoice_number");
		$this->db->join("ap_customer","ap_customer.id_customer = ap_invoice_number.id_customer","left");
		$this->db->join("ap_customer_group","ap_customer_group.id_group = ap_customer.kategori","left");
		$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$this->db->group_by("ap_customer_group.id_group");
		return $this->db->get()->result();
	}

	function pending_status(){
		$bulan = date('m');
		$tahun  = date('Y');

		$this->db->select("COUNT(ap_invoice_number.status) as status");
		$this->db->from("ap_invoice_number");
		$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$this->db->where("status",0);
		$query = $this->db->get()->result();

		foreach($query as $row){
			return $row->status;
		}
	}

	function on_process(){
		$bulan = date('m');
		$tahun  = date('Y');

		$this->db->select("COUNT(ap_invoice_number.status) as status");
		$this->db->from("ap_invoice_number");
		$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$this->db->where("status",1);
		$query = $this->db->get()->result();

		foreach($query as $row){
			return $row->status;
		}
	}

	function terkirim(){
		$bulan = date('m');
		$tahun  = date('Y');

		$this->db->select("COUNT(ap_invoice_number.status) as status");
		$this->db->from("ap_invoice_number");
		$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$this->db->where("status",2);
		$query = $this->db->get()->result();

		foreach($query as $row){
			return $row->status;
		}
	}

	function dibatalkan(){
		$bulan = date('m');
		$tahun  = date('Y');

		$this->db->select("COUNT(ap_invoice_number.status) as status");
		$this->db->from("ap_invoice_number");
		$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$this->db->where("status",3);
		$query = $this->db->get()->result();

		foreach($query as $row){
			return $row->status;
		}
	}

	function sales_by_province(){
		$bulan = date('m');
		$tahun  = date('Y');

		$this->db->select(array("SUM(ap_invoice_number.total) as total","ae_provinsi.nama_provinsi"));
		$this->db->from("ap_invoice_number");
		$this->db->join("ae_provinsi","ae_provinsi.id_provinsi = ap_invoice_number.id_provinsi","left");
		$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$this->db->group_by("ap_invoice_number.id_provinsi");
		$this->db->order_by("total","DESC");
		return $this->db->get()->result();
	}

	function year_to_month(){
		$tahun = date('Y');

		$this->db->select(array("SUM(ap_invoice_number.total) as total","ap_invoice_number.tanggal"));
		$this->db->from("ap_invoice_number");
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$this->db->group_by("MONTH(ap_invoice_number.tanggal)");
		return $this->db->get();
	}

	function penjualan_perstore($id_store){
		$bulan = date('m');
		$tahun  = date('Y');

		$this->db->select(array("SUM(ap_invoice_number.total) as total","SUM(ap_invoice_number.diskon) as diskon","SUM(ap_invoice_number.diskon_free) as diskon_free","SUM(ap_invoice_number.poin_value) as poin_value"));
		$this->db->from("ap_invoice_number");
		$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$this->db->where("ap_invoice_number.id_toko",$id_store);
		$query = $this->db->get()->result();

		foreach($query as $row){
			$total 	= $row->total;

			return $total;
		}
	}

	function salesByHour($date){
		$this->db->select(array("CONCAT(HOUR(ap_invoice_number.tanggal),':00-',HOUR(ap_invoice_number.tanggal)+1,':00') as tanggal","SUM(ap_invoice_number.total) as total"));
		$this->db->from("ap_invoice_number");
		$this->db->where("DATE(ap_invoice_number.tanggal)",$date);
		$this->db->group_by("HOUR(ap_invoice_number.tanggal)");
		return $this->db->get();
	}

	function salesByHourMonth($bulan,$tahun){
		$this->db->select(array("CONCAT(HOUR(ap_invoice_number.tanggal),':00-',HOUR(ap_invoice_number.tanggal)+1,':00') as tanggal","SUM(ap_invoice_number.total) as total"));
		$this->db->from("ap_invoice_number");
		$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$this->db->group_by("HOUR(ap_invoice_number.tanggal)");
		return $this->db->get();
	}

	function salesByHourYear($tahun){
		$this->db->select(array("CONCAT(HOUR(ap_invoice_number.tanggal),':00-',HOUR(ap_invoice_number.tanggal)+1,':00') as tanggal","SUM(ap_invoice_number.total) as total"));
		$this->db->from("ap_invoice_number");
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$this->db->group_by("HOUR(ap_invoice_number.tanggal)");
		return $this->db->get();
	}

	function salesPerkategori($date){
		$this->db->select(array("SUM(ap_invoice_item.harga_jual*ap_invoice_item.qty) as totalPenjualan","ap_kategori.kategori"));
		$this->db->from("ap_invoice_item");
		$this->db->join("ap_produk","ap_produk.id_produk = ap_invoice_item.id_produk","left");
		$this->db->join("ap_kategori","ap_kategori.id_kategori = ap_produk.id_kategori","left outer");
		$this->db->where("ap_invoice_item.tanggal",$date);
		$this->db->group_by("ap_kategori.id_kategori");
		$this->db->order_by("totalPenjualan","DESC");
		return $this->db->get();
	}

	function salesPerkategoriMonth($bulan,$tahun){
		$this->db->select(array("SUM(ap_invoice_item.harga_jual*ap_invoice_item.qty) as totalPenjualan","ap_kategori.kategori"));
		$this->db->from("ap_invoice_item");
		$this->db->join("ap_produk","ap_produk.id_produk = ap_invoice_item.id_produk","left");
		$this->db->join("ap_kategori","ap_kategori.id_kategori = ap_produk.id_kategori","left outer");
		$this->db->where("MONTH(ap_invoice_item.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_item.tanggal)",$tahun);
		$this->db->group_by("ap_kategori.id_kategori");
		$this->db->order_by("totalPenjualan","DESC");
		return $this->db->get();
	}

	function salesPerkategoriYear($tahun){
		$this->db->select(array("SUM(ap_invoice_item.harga_jual*ap_invoice_item.qty) as totalPenjualan","ap_kategori.kategori"));
		$this->db->from("ap_invoice_item");
		$this->db->join("ap_produk","ap_produk.id_produk = ap_invoice_item.id_produk","left");
		$this->db->join("ap_kategori","ap_kategori.id_kategori = ap_produk.id_kategori","left outer");
		$this->db->where("YEAR(ap_invoice_item.tanggal)",$tahun);
		$this->db->group_by("ap_kategori.id_kategori");
		$this->db->order_by("totalPenjualan","DESC");
		return $this->db->get();
	}

	function salesPerkasir($date){
		$this->db->select(array("users.first_name","SUM(ap_invoice_number.total) as total"));
		$this->db->from("ap_invoice_number");
		$this->db->join("users","users.id = ap_invoice_number.id_pic","left");
		$this->db->where("DATE(ap_invoice_number.tanggal)",$date);
		$this->db->group_by("ap_invoice_number.id_pic");
		$this->db->order_by("total","DESC");
		return $this->db->get();
	}

	function salesPerkasirMonth($bulan,$tahun){
		$this->db->select(array("users.first_name","SUM(ap_invoice_number.total) as total"));
		$this->db->from("ap_invoice_number");
		$this->db->join("users","users.id = ap_invoice_number.id_pic","left");
		$this->db->where("MONTH(ap_invoice_number.tanggal)",$bulan);
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$this->db->group_by("ap_invoice_number.id_pic");
		$this->db->order_by("total","DESC");
		return $this->db->get();
	}

	function salesPerkasirYear($tahun){
		$this->db->select(array("users.first_name","SUM(ap_invoice_number.total) as total"));
		$this->db->from("ap_invoice_number");
		$this->db->join("users","users.id = ap_invoice_number.id_pic","left");
		$this->db->where("YEAR(ap_invoice_number.tanggal)",$tahun);
		$this->db->group_by("ap_invoice_number.id_pic");
		$this->db->order_by("total","DESC");
		return $this->db->get();
	}
}