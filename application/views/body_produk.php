<div class="wraper container-fluid">
    <div class="page-title"> 
      <h3 class="title">Produk</h3> 
    </div>

    <div class="portlet"><!-- /primary heading -->
        <div id="portlet2" class="panel-collapse collapse in">
            <div class="portlet-body">
                <table class="table table-bordered" id="produkDatatables">
                    <thead>
                        <tr style="font-weight: bold;">
                            <td width="5%">No</td>
                            <td>SKU</td>
                            <td>Nama Produk</td>
                            <td>Satuan</td>
                            <td>Kategori</td>
                            <td>Status</td>
                            <td width="5%"></td>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div> <!-- /Portlet -->    
</div>

