<div class="wraper container-fluid">
    <div class="page-title"> 
    	<h3 class="title">Add Customer</h3> 
	</div>

    <div class="portlet"><!-- /primary heading -->
        <div id="portlet2" class="panel-collapse collapse in">
            <div class="portlet-body">
                <div class="row" style="margin-top: 30px;">
            		<div class="col-md-12">
            			<form action="<?php echo base_url('customer/add_customer_sql'); ?>" class="form-horizontal" role="form" method="post">                                    
                            <div class="form-group">
                                <label class="col-md-2 control-label">Nama Customer</label>
                                <div class="col-md-10">
                                	<input type="text" class="form-control" name="nama" required>
                              	</div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Kontak</label>
                                <div class="col-md-10">
                                	<input type="text" class="form-control" name="kontak" required>
                              	</div>
                            </div>
                            <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">Toko</label>
                          <div class="col-sm-10">
                            <select class="select2" id="store" name="store">
                              <option value="">--Pilih Toko--</option>
                              <?php
                                foreach($store as $st){
                              ?>
                              <option value="<?php echo $st->id_store; ?>"><?php echo $st->store; ?></option>
                              <?php } ?>
                            </select>
                            <p>Digunakan untuk admin toko sesuai dengan toko yang ditugaskan</p>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
                          <div class="col-sm-10">
                            <input type="password" class="form-control" name="password" id="password">
                          </div>
                      </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Email</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="email" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Tanggal Lahir</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control datepicker" name="tanggal_lahir" id="datepicker" required readonly>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Alamat</label>
                                <div class="col-md-10">
                                	<textarea class="form-control" name="alamat" required></textarea>
                              	</div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Provinsi</label>
                                <div class="col-md-10">
                                	<select class="select2" id="provinsi" name="provinsi" required>
                                		<option value="">--Pilih Provinsi--</option>
                                		<?php
                                			foreach($provinsi->result() as $pro){
                                		?>
                                		<option value="<?php echo $pro->id_provinsi; ?>"><?php echo $pro->nama_provinsi; ?></option>
                                		<?php } ?>
                                	</select>
                              	</div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Kabupaten</label>
                                <div class="col-md-10">
                                	<select class="select2" id="list-kabupaten" name="kabupaten" required>
                                		<option value="">--Pilih Kabupaten--</option>
                                	</select>
                              	</div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Kecamatan</label>
                                <div class="col-md-10">
                                	<select class="select2" id="list-kecamatan" name="kecamatan" required>
                                		<option value="">--Pilih Kecamatan--</option>
                                	</select>
                              	</div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Level</label>
                                <div class="col-md-10">
                                	<select class="form-control" name="group" id="group_customer" required>
                                		<option value="">--Pilih Level--</option>
                                		<?php
                                			foreach($group_customer->result() as $cs){
                                		?>
                                		<option value="<?php echo $cs->id_group; ?>"><?php echo $cs->group_customer; ?></option>
                                		<?php } ?>
                                	</select>
                              	</div>
                            </div>

                            <!-- <div class="form-group">
                                <label class="col-md-2 control-label">Diskon Harga</label>
                                <div class="col-md-10">
                                	<input type="number" name="diskon" class="form-control" min="0" max="100" required>
                              	</div>
                            </div> -->

                            <div class="form-group">
                                <label class="col-md-2 control-label">Plafon</label>
                                <div class="col-md-10">
                                	<input type="text" name="limit_kredit" class="form-control" required>
                              	</div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Hari</label>
                                <div class="col-md-10">
                                	<input type="text" name="limit_day" class="form-control" required>
                              	</div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Jenis</label>
                                <div class="col-md-10">
                                	<select class="form-control" name="jenis_cust" id="group_customer" required>
                                		<option value="">--Pilih Jenis--</option>
                                		<option value="tkp">TKP</option>
                                		<option value="non_tkp">NON TKP</option>
                                		
                                	</select>
                              	</div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="col-md-2 control-label">PPN</label>
                                <div class="col-md-10">
                                	<select class="form-control" name="ppn" id="group_customer" required>
                                		<option value="">--Pilih PPN--</option>
                                		<option value="1">YA</option>
                                		<option value="0">TIDAK</option>
                                		
                                	</select>
                              	</div>
                            </div> -->
                    

                            
                            <div class="form-group" id="username_form">
                            </div>

                            <div class="form-group" id="password_form">
                            </div>

                            <div class="form-group" style="text-align: right;">
                            	<div class="col-md-12">
                            		<input type="submit" class="btn btn-primary check" value="Submit">
                            	</div>
                            </div>
                     	</form>
            		</div>
            	</div>
            </div>
        </div>
    </div> <!-- /Portlet -->	
</div>


