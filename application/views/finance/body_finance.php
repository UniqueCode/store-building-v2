<div class="wraper container-fluid">
    <div class="page-title"> 
      <h3 class="title"><i class="fa fa-money"></i> Hutang PO</h3> 
    </div>

    <div class="portlet"><!-- /primary heading -->        
        <div id="portlet2" class="panel-collapse collapse in">
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12" style="padding: 20px;">
                        <!--<a href="<?php echo base_url('finance/analisa_umur_hutang'); ?>">Analisa Umur Hutang</a>-->
                        <br>
                        <table class="table table-bordered" id="datatable" style="font-size: 12px;">
                            <thead>
                                <tr style="font-weight: bold;">  
                                   <td width="5%">No</td> 
                                   <td width="10%">No PO</td>
                                   <td>Tanggal PO</td>
                                   <td>Jatuh Tempo</td>
                                   <td>PIC</td>
                                   <td>Keterangan</td>
                                   <td width="8%">Status</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- /Portlet -->	
</div>
