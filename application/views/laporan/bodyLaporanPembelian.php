<div class="wraper container-fluid">              
    <div class="page-title"> 
        <h3 class="title"><u>Laporan Purchase Order</u></h3> 
    </div>

    <div class="row">
        <a href="<?php echo base_url('laporan/purchaseOrder'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Purchase Order</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/72109.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

       <!--
        <a href="<?php echo base_url('laporan/pembelian_persupplier'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan Supplier</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/72109.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/pembelian_perbarang'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan Barang</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/4321-200.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/rangkuman_pembelian'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Rangkuman PO</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/search-text-512.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>
       

        <a href="<?php echo base_url('laporan/pembelian_berdasarkan_purchasing'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan Purchasing</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/img_172011.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>
         -->

        <a href="<?php echo base_url('laporan/hutang_po'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Hutang PO</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/89557-200.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/hutang_jatuh_tempo'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Hutang Jatuh Tempo</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/34484-200.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/hutang_terbayar'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Hutang Terbayar</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/payment_card_credit-512.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/analisa_umur_hutang'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Analisa Umur Hutang</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/time.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>
    </div>

    <!--    
    <div class="page-title" style="margin-top: 20px;"> 
        <h3 class="title"><u>Laporan Goods Received</u></h3> 
    </div>
    
    <div class="row">
        <a href="<?php echo base_url('laporan/gr_persupplier'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan Supplier</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/fright-trolley-2-512.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/gr_perbarang'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan Barang</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/img_149187.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/rangkuman_penerimaan'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Rangkuman Penerimaan</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/1-59-512.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/gr_berdasarkan_penerima'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan Penerima</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/handover.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/gr_berdasarkan_no_po'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan No PO</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/purchase_order1600.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>
    </div>
    -->

    <!--
    <div class="page-title" style="margin-top: 20px;"> 
        <h3 class="title"><u>Laporan Retur Pembelian</u></h3> 
    </div>

    <div class="row">
        <a href="<?php echo base_url('laporan/retur_persupplier'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan Supplier</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/return-arrival.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/retur_perbarang'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan Barang</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/package_return-512.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/retur_berdasarkan_no_po'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan No PO</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/1600.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>
    </div>

    <div class="page-title" style="margin-top: 20px;"> 
        <h3 class="title"><u>Laporan COGS</u></h3> 
    </div>

    <div class="row">
        <a href="<?php echo base_url('laporan/cogs_perbahan_baku'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">COGS Perbahan Baku</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/930448-200.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/cogs_pervarian'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">COGS Pervarian</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/20200-200.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/akumulasi_cogs'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Akumulasi COGS</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/sum.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/penggunaan_bahan_terbanyak'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Penggunaan Bahan Terbanyak</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/31977.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>
    </div>

    <div class="page-title" style="margin-top: 20px;"> 
        <h3 class="title"><u>Laporan Produksi</u></h3> 
    </div>

    <div class="row">
        <a href="<?php echo base_url('laporan/produksi_pervarian'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Produksi Pervarian</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/385816-200.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/kartu_stok_finish_goods'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Kartu Stok Finish Goods</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/1234.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/rangkuman_pembelian'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Waste</td>
                            <td style="text-align: right;"><img src="<?php echo base_url('assets/4337-200.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>
    -->
    </div>
</div>

