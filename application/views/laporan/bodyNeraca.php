 <div class="wraper container-fluid">
    <div class="row">

      <div class="col-md-6">
        <div class="page-title"> 
          <h3 class="title">Laporan Neraca</h3> 
        </div>
      </div>

    
        <div class="col-md-6" style="text-align: right;">
            <a class="btn btn-default" onclick="printContent('dataReport')"><i class="fa fa-print"></i> Print</a>
        </div>
    </div>

    

    <div class="row">
        <div class="col-lg-12">
            <div class="portlet">
            <div id="portlet2" class="panel-collapse collapse in">
              <div class="portlet-body" id="graph">
                <div class="row">
                  <div class="col-md-3">
                          <div class="form-group">
                            <label>Date Start</label>
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                              <input type="text" class="form-control datepicker" placeholder="Date Start" id="dateStart" readonly>
                            </div>
                          </div>
                  </div>
                  <div class="col-md-3" >
                          <div class="form-group">
                              <label>Date End</label>
                              <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control datepicker" placeholder="Date End" id="dateEnd" readonly>
                              </div>
                          </div>
                  </div>
                  <div class="col-md-3" >
                          <div class="form-group">
                          <label>Toko</label>
                          <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-bank"></i></span>
                              <select class="select2" id="toko">
                                <option value="">--Akumulasi--</option>
                                <?php
                                  foreach($toko as $tk){
                                ?>
                                <option value="<?php echo $tk->id_store; ?>"><?php echo $tk->store; ?></option>
                                <?php }  ?>
                              </select>
                            </div>
                        </div>
                  </div>
                  <div class="col-md-3">
                        <div class="form-group">
                        <label>&nbsp;</label>
                        <div class="input-group">
                            <button class="btn btn-info" id="viewReport">Submit</button>
                        </div>
				              	</div>
			            </div>
                </div>
              </div>
            </div>
            </div>
        </div> <!-- /Portlet -->            
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="portlet"><!-- /primary heading -->
                    
                    <div id="portlet2" class="panel-collapse collapse in">
                        <div class="portlet-body" id="graph">
                        <table id="neraca" class="display" style="width:100%" border="1" cellspacing="0" cellspadding="0">
                          <thead>
                              <tr>
                                  <th rowspan="3" width="20%"><center>Keterangan</center></th>
                              </tr>
                              <tr>
                                  <th colspan="2""><center>Neraca Saldo</center></th>           
                              </tr>
                              <tr>
                                  <th><center>Debit</center></th> 
                                  <th><center>Kredit</center></th>   
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                <td>Kas</td>
                                <td align="right">23.000.000</td>
                                <td align="right">150.000.000</td>
                              </tr>
                              <tr>
                                <td>Persediaan</td>
                                <td align="right">23.000.000</td>
                                <td align="right">150.000.000</td>
                              </tr>
                              <tr>
                                <td>Piutang Dagang</td>
                                <td align="right">23.000.000</td>
                                <td align="right">150.000.000</td>
                              </tr>
                              <tr>
                                <td>Utang Dagang</td>
                                <td align="right">23.000.000</td>
                                <td align="right">150.000.000</td>
                              </tr>
                              <tr>
                                <td>Modal</td>
                                <td align="right">23.000.000</td>
                                <td align="right">150.000.000</td>
                              </tr>
                              <tr>
                                <td>Penjualan</td>
                                <td align="right">23.000.000</td>
                                <td align="right">150.000.000</td>
                              </tr>
                              <tr>
                                <td>Harga Pokok Penjualan</td>
                                <td align="right">23.000.000</td>
                                <td align="right">150.000.000</td>
                              </tr>
                              <tr>
                                <td>Diskon Penjualan</td>
                                <td align="right">23.000.000</td>
                                <td align="right">150.000.000</td>
                              </tr>
                              <tr>
                                <td>Adm Bank</td>
                                <td align="right">23.000.000</td>
                                <td align="right">150.000.000</td>
                              </tr>
                              <tr>
                                <td>Bahan Habis Pakai</td>
                                <td align="right">23.000.000</td>
                                <td align="right">150.000.000</td>
                              </tr>
                          </tbody>
                          <tfoot>
                          <tr>
                                  <td align="center" width="20%"><h5><b>Total</b></h5></td>
                                  <td align="right"><h5><b>102.000.000</b></h5></td> 
                                  <td align="right"><h5><b>102.000.000</b></h5></td> 
                              </tr>
                          </tfoot>
                      </table>
                        </div>
                    </div>

                </div>
            </div> <!-- /Portlet --> 
    </div>
</div>

