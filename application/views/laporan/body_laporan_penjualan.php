<div class="wraper container-fluid">              
    <div class="page-title"> 
        <h3 class="title"><u>Akumulasi Penjualan</u></h3> 
    </div>

    <div class="row">
        <a href="<?php echo base_url('laporan/penjualan_perbarang'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan Barang</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/4321-200.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/penjualan_percustomer'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan Customer</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/58044-200.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/penjualan_perkategori_produk'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan Kategori Produk</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/purchase_order1600.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/penjualan_perkategori_customer'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Berdasarkan Kategori Customer</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/1600.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>


        <a href="<?php echo base_url('laporan/top_customer'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Top Customer</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/930448-200.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/penjualan_pertoko'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Penjualan Pertoko</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/store.png'); ?>" width="40px"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/penjualan_perkasir'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Penjualan Perkasir</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/58044-200.png'); ?>" width="40px"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/akumulasi_penjualan_produk'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Akumulasi Penjualan Produk</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/warehouse-2-512.png'); ?>" width="40px"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>


        <a href="<?php echo base_url('laporan/akumulasiPenjualanPerkriteria'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Laporan Penjualan Detail</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/categories-icon.png'); ?>" width="40px"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/akumulasiPenjualanProdukPerkriteria'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Laporan Penjualan Detail Perproduk</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/img_149187.png'); ?>" width="40px"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/penjualanPerkategori'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Penjualan Perkategori</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/img_149187.png'); ?>" width="40px"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/labaRugi'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Laporan Laba Rugi</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/payment_card_credit-512.png'); ?>" width="40px"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/bukuBesar'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Buku Besar</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/1600.png'); ?>" width="40px"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/neraca'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Neraca</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/1600.png'); ?>" width="40px"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

        <a href="<?php echo base_url('laporan/aktivaPasiva'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Aktiva Pasiva</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/1600.png'); ?>" width="40px"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>

    </div>

    <div class="page-title"> 
        <h3 class="title"><u>Retur Penjualan</u></h3> 
    </div>

    <div class="row">
        <a href="<?php echo base_url('laporan/returPenjualan'); ?>">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <table width="100%">
                        <tr>
                            <td width="70%">Retur Penjualan</td>
                            <td style="text-align: right"><img src="<?php echo base_url('assets/11765.png'); ?>"/></td> 
                        </tr>
                    </table>
                </div>
            </div>
        </a>
    </div>
</div>

