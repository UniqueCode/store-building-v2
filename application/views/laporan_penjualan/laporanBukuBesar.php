
<div class="portlet"><!-- /primary heading -->
                    <div id="portlet2" class="panel-collapse collapse in">
                        <div class="portlet-body" id="graph">
                        <p style="text-align: center;font-size: 18px;">Laporan Buku Besar Akumulasi <br> <?php echo $title; ?><br> Periode</p>
                        <p style="text-align: center;">
                            <?php
                                echo date_format(date_create($start),"d M Y")." - ".date_format(date_create($end),"d M Y");  
                            ?>
                        </p>

                        <table id="example" class="display" style="width:100%" border="1" cellspacing="0" cellspadding="0">
                          <thead>
                              <tr>
                                  <th rowspan="3" width="10%"><center>Tanggal</center></th>
                                  <th rowspan="3"><center>Debit</center></th>
                                  <th rowspan="3"><center>Kredit</center></th>
                              </tr>
                              <tr>
                                  <th colspan="2""><center>Saldo</center></th>           
                              </tr>
                              <tr>
                                  <th><center>Debit</center></th> 
                                  <th><center>Kredit</center></th>   
                              </tr>
                          </thead>
                          <tbody>
						  	<?php
                            $saldo = 0;
						 	foreach ($res as $a) {
                                 if($a->jenis=='debit'){
                                     $saldo+=$a->jml;
                                 }else{
                                     $saldo-=$a->jml;
                                 }
								//$resKredit = $this->db->query("SELECT SUM(qty*harga) kredit from purchase_item where tanggal='$a->tgl' group by tanggal")->row();
							?>
							<tr>
                                <td align="center"><?= date('d-m-Y', strtotime($a->tgl))?></td>
                                <td align="right"><?php if($a->jenis=='debit'){ echo 'Rp. '.number_format($a->jml,'0',',','.'); } ?></td>
                                <td align="right"><?php if($a->jenis=='kredit'){ echo 'Rp. '.number_format($a->jml,'0',',','.'); } ?></td>
                                <td align="right"><?php if($saldo>0){ echo 'Rp. '.number_format($saldo,'0',',','.'); } ?></td>
                                <td align="right"><?php if($saldo<0){ echo 'Rp. '.number_format(abs($saldo),'0',',','.'); } ?></td>
                              </tr>
							<?php
							 } 
						  	?>
                          </tbody>
                        </table>
                        </div>
                    </div>
            </div>