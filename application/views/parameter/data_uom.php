<table class="table table-bordered" style="font-size:12px;margin-top: 20px;">
    <tr style="font-weight: bold;">
        <td width="5%">No</td>
        <td width="25%">Satuan</td>
        <td>Keterangan</td>
        <td width="5%"></td>
   	</tr>

   	<?php
   		$i=1;
   		foreach($satuan->result() as $row){
   	?>
   	<tr>
   		<td style="text-align: center;"><?php echo $i; ?></td>
   		<td><?php echo $row->satuan; ?></td>
   		<td><?php echo $row->keterangan; ?></td>
   		<td style="text-align: center;width:7%"><a href="#satuan-modal" data-toggle="modal" class="edit_satuan" id="<?php echo $row->id_satuan; ?>"><i class="fa fa-pencil"></i></a> | <a class="hapus_uom" id="<?php echo $row->id_satuan; ?>"><i class="fa fa-trash"></i></a></td>
   	</tr>
   	<?php $i++; } ?>
</table>
<div class="modal fade" id="satuan-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Satuan</h4>
      </div>
      <div class="modal-body">
        <div class="form-group" id="change-satuan">
        	
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="simpan-satuan">Save changes</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$('.hapus_uom').on("click",function(){
        id 	= this.id;
        url = "<?php echo base_url('parameter/hapus_uom'); ?>";
        uom2 = "<?php echo base_url('parameter/data_uom'); ?>";

      swal({
  		  title: "Are you sure?",
  		  text: "You will not be able to recover this imaginary file!",
  		  type: "warning",
  		  showCancelButton: true,
  		  confirmButtonColor: "#DD6B55",
  		  confirmButtonText: "Yes, delete it!",
  		  closeOnConfirm: false
  		},
  		function(){
  		    swal("Deleted!", "Your imaginary file has been deleted.", "success");
  			
  			$.post(url,{id : id}, function(){
  	        	$('#data-uom').load(uom2);
  	        });
  		});
	});
	$('.edit_satuan').on("click",function(){
		id = this.id;
		url = "<?php echo base_url('parameter/form_edit_satuan'); ?>";
		$('#change-satuan').load(url,{id : id});
	});
</script>