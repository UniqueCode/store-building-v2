<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Tambah Produk</h3>
    </div>

    <div class="portlet">
        <!-- /primary heading -->
        <div id="portlet2" class="panel-collapse collapse in">
            <div class="portlet-body">
                <!--<div class="row" style="margin-top: 20px;">
            		<div class="col-md-6">
            			<div class="form-group">
            				<label>Jenis Produk</label>
            				<select style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;" id="jenis_produk">
            					<option value="">--Pilih Jenis Produk--</option>
                                <option value="1">Produksi</option>
            					<option value="2">Non Produksi</option>
            				</select>
            			</div>	        
            		</div>
            	</div>-->

                <div class="row">
                    <div class="col-md-12" id="form-tambah-produk">
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>SKU</label> <label id="skuAlert" style="color:red;"></label>
                                        <input type="text" id="sku" style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;" required readonly />
                                        <div></div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Produk</label> <label id="namaProdukAlert" style="color:red;"></label>
                                        <input type="text" id="namaProduk" style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Harga Beli</label>
                                        <input type="number" id="hargaBeli" style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;" value="0">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Satuan</label> <label id="satuanAlert" style="color:red;"></label>
                                        <select style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;" id="satuan" required>
                                            <option value="">--Pilih Satuan--</option>
                                            <?php
                                            foreach ($satuan as $st) {
                                                ?>
                                                <option value="<?php echo $st->satuan; ?>"><?php echo $st->satuan; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                               
                                <!-- <div class="col-md-6"> -->
                                    <!-- <div class="form-group">
                                        <label>Tempat</label>
                                        <select style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;" id="tempat" required="">
                                            <?php
                                            foreach ($stand as $dt) {
                                                ?>
                                                <option value="<?php echo $dt->id_stand; ?>"><?php echo $dt->stand; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div> -->
                                <!-- </div> -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Pajak</label> <br />
                                        <input type="checkbox" id="pajak" name="ppn" value="1" /> <i> centang jika ada pajak</i>

                                    </div>
                                </div>
                                <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kategori</label> <label id="kategoriAlert" style="color:red;"></label>
                                    <select style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;" id="kategori">
                                        <option value="">--Pilih Kategori--</option>
                                        <?php
                                        foreach ($show_kategori->result() as $kt) {
                                            ?>
                                            <option value="<?php echo $kt->id_kategori; ?>"><?php echo $kt->kategori; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="panel panel-color panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Harga Jual</h3>
                                    </div>

                                    <div class="panel-body">
                                            <div class="col-md-4">
                                                <div class="form-group">
                            
                                                    <input type="hidden" id="idStore" value="<?php echo $store->id_store; ?>">
                                                    <div class="form-grup">
                                                        level 1 
                                                        <input class="form-control" type="number" id="hargaJual" value="0" style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;">
                                                    </div>
                                                    <div class="form-grup">
                                                        <br/>
                                                        level 2
                                                        <input class="form-control" type="text" id="hargaJual2"  style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;">
                                                    </div>
                                                    <div class="form-grup">
                                                    <br/>
                                                        level 3
                                                        <input class="form-control" type="text" id="hargaJual3"  style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <div class="form-grup">
                                                        level 1 
                                                        <input class="form-control" type="text" readonly id="hargaJualpercent" value="" > 
                                                    </div>
                                                    <div class="form-grup">
                                                        <br/>
                                                        level 2
                                                        <input class="form-control" type="text" readonly id="hargaJual2percent" value="" > 
                                                    </div>
                                                    <div class="form-grup">
                                                    <br/>
                                                        level 3
                                                        <input class="form-control" type="text" readonly id="hargaJual3percent" value="" > 
                                                    </div>
                                                </div>
                                            </div>
                                        
                                    </div>
                                </div>
                            </div>


                          

                            <div class="col-md-4">
                                <div class="form-group" id="sub_kategori">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group" id="sub_kategori_2">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <a id="addProdukSql" class="btn btn-primary">Submit</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /Portlet -->
</div>