 <!-- Page Content Ends -->
 <!-- ================== -->

 <!-- Footer Start -->
 <footer class="footer">
     <?php echo $footer; ?>
 </footer>
 <!-- Footer Ends -->
 </section>
 <!-- Main Content Ends -->

 <!-- js placed at the end of the document so the pages load faster -->
 <script src="<?php echo base_url('assets'); ?>/js/jquery.js"></script>
 <script src="<?php echo base_url('assets'); ?>/js/bootstrap.min.js"></script>
 <script src="<?php echo base_url('assets'); ?>/js/modernizr.min.js"></script>
 <script src="<?php echo base_url('assets'); ?>/js/pace.min.js"></script>
 <script src="<?php echo base_url('assets'); ?>/js/wow.min.js"></script>
 <script src="<?php echo base_url('assets'); ?>/js/jquery.scrollTo.min.js"></script>
 <script src="<?php echo base_url('assets'); ?>/js/jquery.nicescroll.js" type="text/javascript"></script>
 <script src="<?php echo base_url('assets'); ?>/assets/chat/moment-2.2.1.js"></script>

 <!-- Counter-up -->
 <script src="<?php echo base_url('assets'); ?>/js/waypoints.min.js" type="text/javascript"></script>
 <script src="<?php echo base_url('assets'); ?>/js/jquery.counterup.min.js" type="text/javascript"></script>


 <script src="<?php echo base_url('assets'); ?>/js/jquery.app.js"></script>
 <!-- Chat -->
 <script src="<?php echo base_url('assets'); ?>/js/jquery.chat.js"></script>
 <!-- Dashboard -->
 <script src="<?php echo base_url('assets'); ?>/js/jquery.dashboard.js"></script>
 <script src="<?php echo base_url('assets'); ?>/assets/notifications/notify.min.js"></script>
 <script src="<?php echo base_url('assets'); ?>/assets/notifications/notify-metro.js"></script>
 <script src="<?php echo base_url('assets'); ?>/assets/notifications/notifications.js"></script>

 <!-- Todo -->
 <script src="<?php echo base_url('assets'); ?>/js/jquery.todo.js"></script>
 <script src="<?php echo base_url('assets'); ?>/assets/select2/select2.min.js" type="text/javascript"></script>
 <script src="<?php echo base_url('assets'); ?>/assets/datatables/jquery.dataTables.min.js"></script>
 <script src="<?php echo base_url('assets'); ?>/assets/datatables/dataTables.bootstrap.js"></script>
 <script src="<?php echo base_url('assets'); ?>/assets/timepicker/bootstrap-datepicker.js"></script>

 <script type="text/javascript">
     function randomString(length) {
         chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
         var result = '';
         for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
         return result;
     }
   
     /* ==============================================
             Counter Up
             =============================================== */
     var spinner = "<?php echo base_url('produk/spinner'); ?>";

     jQuery(document).ready(function($) {
         $('#jenis_produk').change(function() {
             id = $('#jenis_produk').val();

             produksi = "<?php echo base_url('produk/form_produk_produksi'); ?>";
             non_produksi = "<?php echo base_url('produk/form_produk_non_produksi'); ?>";

             if (id == 1) {
                 $('#form-tambah-produk').load(produksi);
             } else if (id == 2) {
                 $('#form-tambah-produk').load(non_produksi);
             } else {
                 $('#form-tambah-produk').empty();
             }
         });
     });

     $('#kategori').change(function() {
         kategori = $('#kategori').val();
         url = "<?php echo base_url('produk/get_subkategori'); ?>";

         $('#sub_kategori').load(url, {
             id_kategori: kategori
         });
     });

     $('#hargaJual').change(function(){
        var hargabeli = $('#hargaBeli').val();
        var hargajual = $('#hargaJual').val();
        var cariPercent1 =  (hargajual - hargabeli);
        var cariPercent2 = (cariPercent1 / hargabeli);
        var totPercent = 100 + (cariPercent2 * 100);
        $('#hargaJualpercent').val(totPercent.toFixed(2)+ " %");
     });
     $('#hargaJual2').change(function(){
        var hargabeli = $('#hargaBeli').val();
        var hargajual = $('#hargaJual2').val();
        var cariPercent1 =  (hargajual - hargabeli);
        var cariPercent2 = (cariPercent1 / hargabeli);
        var totPercent = 100 + (cariPercent2 * 100);
        $('#hargaJual2percent').val(totPercent.toFixed(2)+ " %");
     });
     $('#hargaJual3').change(function(){
        var hargabeli = $('#hargaBeli').val();
        var hargajual = $('#hargaJual3').val();
        var cariPercent1 =  (hargajual - hargabeli);
        var cariPercent2 = (cariPercent1 / hargabeli);
        var totPercent = 100 + (cariPercent2 * 100);
        $('#hargaJual3percent').val(totPercent.toFixed(2)+ " %");
     });

     $('#namaProduk').change(function() {
         var namaProduk = $('#namaProduk').val();
         var skus = namaProduk.toUpperCase();
         $('#sku').val(skus+'-'+randomString(5)+'-<?php echo $idsku; ?>');
     });

     $('#sku').change(function() {
         var sku = $('#sku').val();

         $.ajax({
             method: "POST",
             url: "<?php echo base_url('produk/cekSKUIfExist'); ?>",
             data: {
                 sku: sku
             },
             success: function(data) {
                 if (data == 1) {
                     $('#skuAlert').text("*SKU Telah Terpakai");
                     $('#sku').val('');
                 } else {
                     $('#skuAlert').empty();
                 }
             }
         });
     });

     $('#addProdukSql').on("click", function() {
        var checkbox_this = $('#pajak');
            if (checkbox_this.is(":checked") == true) {
                checkbox_this.attr('value', '1');
            } else {
                checkbox_this.prop('checked', true);
                checkbox_this.attr('value', '0');
            }
         var sku = $('#sku').val();
         var namaProduk = $('#namaProduk').val();
         var hargaBeli = $('#hargaBeli').val();
         var satuan = $('#satuan').val();
         var pajak = $('#pajak').val();
         var idStore = $('#idStore').val();
         var kategori = $('#kategori').val();
         var kategori2 = $('#subkategori_2').val();
         var kategori3 = $('#subkategori_3').val();
         var hargaJual  = $('#hargaJual').val();
         var hargaJual2  = $('#hargaJual2').val();
         var hargaJual3  = $('#hargaJual3').val();
    
         if (sku == '' || namaProduk == '' || satuan == '' || kategori == '') {
             if (sku == '') {
                 $('#skuAlert').text("*SKU Required");
             }

             if (namaProduk == '') {
                 $('#namaProdukAlert').text('*Nama Produk Required');
             }

             if (satuan == '') {
                 $('#satuanAlert').text('*Satuan Required');
             }

             if (kategori == '') {
                 $('#kategoriAlert').text("*Kategori Required");
             }
         } else {
             $.ajax({
                 method: "POST",
                 url: "<?php echo base_url('produk/tambahProdukNonProduksiSQL'); ?>",
                 data: {
                     sku: sku,
                     idStore: idStore,
                     namaProduk: namaProduk,
                     hargaBeli: hargaBeli,
                     satuan: satuan,
                     pajak: pajak,
                     kategori: kategori,
                     kategori2: kategori2,
                     kategori3: kategori3,
                     hargaJual: hargaJual,
                     hargaJual2: hargaJual2,
                     hargaJual3: hargaJual3
                 },
                 beforeSend: function() {
                     $('#form-tambah-produk').load(spinner);
                 }
             }).done(function() {
                 urlForm = "<?php echo base_url('produk/form_produk_non_produksi'); ?>";
                 $('#form-tambah-produk').load(urlForm);

                 $.Notification.notify('success', 'top right', 'Tambah Produk', 'Produk Berhasil Ditambahkan');
             });
         }

     });
 </script>
 </body>

 </html>