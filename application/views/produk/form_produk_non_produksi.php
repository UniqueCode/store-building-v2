<form>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>SKU</label> <label id="skuAlert" style="color:red;"></label>
				<input type="text" id="sku" style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;" required readonly />
				<div></div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				<label>Nama Produk</label> <label id="namaProdukAlert" style="color:red;"></label>
				<input type="text" id="namaProduk" style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Harga Beli</label>
				<input type="number" id="hargaBeli" style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;" value="0">
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group">
				<label>Satuan</label> <label id="satuanAlert" style="color:red;"></label>
				<select style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;" id="satuan" required>
					<option value="">--Pilih Satuan--</option>
					<?php
					foreach ($satuan as $st) {
						?>
						<option value="<?php echo $st->satuan; ?>"><?php echo $st->satuan; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
	<div class="row">
		<input type="hidden" id="tempat" value="tempat">
		<!-- <div class="col-md-6">
			<div class="form-group">
				<label>Tempat</label>
				<select style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;" id="tempat" required="">
					<?php
					foreach ($stand as $dt) {
						?>
						<option value="<?php echo $dt->id_stand; ?>"><?php echo $dt->stand; ?></option>
					<?php } ?>
				</select>
			</div>
		</div> -->
		<div class="col-md-6">
			<div class="form-group">
				<label>Pajak</label> <br />
				<input type="checkbox" id="pajak" name="ppn" value="1" /> <i> centang jika ada pajak</i>

			</div>

		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label>Kategori</label> <label id="kategoriAlert" style="color:red;"></label>
				<select style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;" id="kategori">
					<option value="">--Pilih Kategori--</option>
					<?php
					foreach ($show_kategori->result() as $kt) {
						?>
						<option value="<?php echo $kt->id_kategori; ?>"><?php echo $kt->kategori; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
	</div>
	<div class="col-lg-12 col-md-12">
		<div class="panel panel-color panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Harga Jual</h3>
			</div>

			<div class="panel-body">

				<div class="col-md-4">
					<div class="form-group">
						<label><?php echo $store->store; ?></label>
						<input type="hidden" id="idStore" value="<?php echo $store->id_store; ?>">
						<div class="form-grup">
							level 1
							<input class="form-control" type="number" id="hargaJual" value="0" style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;">
						</div>
						<div class="form-grup">
							<br />
							level 2
							<input class="form-control" type="text" id="hargaJual2" style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;">
						</div>
						<div class="form-grup">
							<br />
							level 3
							<input class="form-control" type="text" id="hargaJual3" style="border:0;border-bottom: solid 0.5px #ccc;width: 100%;">
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<div class="form-grup">
							level 1
							<input class="form-control" type="text" readonly id="hargaJualpercent" value="">
						</div>
						<div class="form-grup">
							<br />
							level 2
							<input class="form-control" type="text" readonly id="hargaJual2percent" value="">
						</div>
						<div class="form-grup">
							<br />
							level 3
							<input class="form-control" type="text" readonly id="hargaJual3percent" value="">
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>




	<div class="col-md-4">
		<div class="form-group" id="sub_kategori">
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group" id="sub_kategori_2">
		</div>
	</div>

	<div class="col-md-12">
		<div class="form-group">
			<a id="addProdukSql" class="btn btn-primary">Submit</a>
		</div>
	</div>
</form>

<script type="text/javascript">
	function randomString(length) {
		chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		var result = '';
		for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
		return result;
	}
	/* ==============================================
	        Counter Up
	        =============================================== */
	var spinner = "<?php echo base_url('produk/spinner'); ?>";

	jQuery(document).ready(function($) {
		$('#jenis_produk').change(function() {
			id = $('#jenis_produk').val();

			produksi = "<?php echo base_url('produk/form_produk_produksi'); ?>";
			non_produksi = "<?php echo base_url('produk/form_produk_non_produksi'); ?>";

			if (id == 1) {
				$('#form-tambah-produk').load(produksi);
			} else if (id == 2) {
				$('#form-tambah-produk').load(non_produksi);
			} else {
				$('#form-tambah-produk').empty();
			}
		});
	});

	$('#kategori').change(function() {
		kategori = $('#kategori').val();
		url = "<?php echo base_url('produk/get_subkategori'); ?>";

		$('#sub_kategori').load(url, {
			id_kategori: kategori
		});
	});


	$('#hargaJual').change(function() {
		var hargabeli = $('#hargaBeli').val();
		var hargajual = $('#hargaJual').val();
		var cariPercent1 = (hargajual - hargabeli);
		var cariPercent2 = (cariPercent1 / hargabeli);
		var totPercent = 100 + (cariPercent2 * 100);
		$('#hargaJualpercent').val(totPercent.toFixed(2) + " %");
	});
	$('#hargaJual2').change(function() {
		var hargabeli = $('#hargaBeli').val();
		var hargajual = $('#hargaJual2').val();
		var cariPercent1 = (hargajual - hargabeli);
		var cariPercent2 = (cariPercent1 / hargabeli);
		var totPercent = 100 + (cariPercent2 * 100);
		$('#hargaJual2percent').val(totPercent.toFixed(2) + " %");
	});
	$('#hargaJual3').change(function() {
		var hargabeli = $('#hargaBeli').val();
		var hargajual = $('#hargaJual3').val();
		var cariPercent1 = (hargajual - hargabeli);
		var cariPercent2 = (cariPercent1 / hargabeli);
		var totPercent = 100 + (cariPercent2 * 100);
		$('#hargaJual3percent').val(totPercent.toFixed(2) + " %");
	});

	$('#namaProduk').change(function() {
		var namaProduk = $('#namaProduk').val();
		var skus = namaProduk.toUpperCase();
		$('#sku').val(skus + '-' + randomString(5) + '-<?php echo $idsku; ?>');
	});

	$('#sku').change(function() {
		var sku = $('#sku').val();

		$.ajax({
			method: "POST",
			url: "<?php echo base_url('produk/cekSKUIfExist'); ?>",
			data: {
				sku: sku
			},
			success: function(data) {
				if (data == 1) {
					$('#skuAlert').text("*SKU Telah Terpakai");
					$('#sku').val('');
				} else {
					$('#skuAlert').empty();
				}
			}
		});
	});

	$('#addProdukSql').on("click", function() {
		var checkbox_this = $('#pajak');
		if (checkbox_this.is(":checked") == true) {
			checkbox_this.attr('value', '1');
		} else {
			checkbox_this.prop('checked', true);
			checkbox_this.attr('value', '0');
		}
		var sku = $('#sku').val();
		var namaProduk = $('#namaProduk').val();
		var hargaBeli = $('#hargaBeli').val();
		var satuan = $('#satuan').val();
		var pajak = $('#pajak').val();
		var kategori = $('#kategori').val();
		var kategori2 = $('#subkategori_2').val();
		var kategori3 = $('#subkategori_3').val();
		var idStore = $('#idStore').val();
		var hargaJual = $('#hargaJual').val();
		var hargaJual2 = $('#hargaJual2').val();
		var hargaJual3 = $('#hargaJual3').val();

		if (sku == '' || namaProduk == '' || satuan == '' || kategori == '') {
			if (sku == '') {
				$('#skuAlert').text("*SKU Required");
			}

			if (namaProduk == '') {
				$('#namaProdukAlert').text('*Nama Produk Required');
			}

			if (satuan == '') {
				$('#satuanAlert').text('*Satuan Required');
			}

			if (kategori == '') {
				$('#kategoriAlert').text("*Kategori Required");
			}
		} else {
			$.ajax({
				method: "POST",
				url: "<?php echo base_url('produk/tambahProdukNonProduksiSQL'); ?>",
				data: {
					sku: sku,
					namaProduk: namaProduk,
					hargaBeli: hargaBeli,
					satuan: satuan,
					pajak: pajak,
					kategori: kategori,
					kategori2: kategori2,
					kategori3: kategori3,
					idStore: idStore,
					hargaJual: hargaJual,
					hargaJual2: hargaJual2,
					hargaJual3: hargaJual3,
				},
				beforeSend: function() {
					$('#form-tambah-produk').load(spinner);
				}
			}).done(function() {
				urlForm = "<?php echo base_url('produk/form_produk_non_produksi'); ?>";
				$('#form-tambah-produk').load(urlForm);

				$.Notification.notify('success', 'top right', 'Tambah Produk', 'Produk Berhasil Ditambahkan');
			});
		}

	});
</script>