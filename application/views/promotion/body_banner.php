<div class="wraper container-fluid">
	<div class="page-title"> 
      <h3 class="title">Daftar Banner</h3> 
    </div>

    <div class="portlet"><!-- /primary heading -->        
        <div id="portlet2" class="panel-collapse collapse in">
            <div class="portlet-body">
            	
                <div class="row" style="margin-top: 30px;">
                  
                    <div class="col-md-12" style="padding:30px;">
                    <a href="<?php echo base_url()."promotion/add_banner"?>" style="margin-bottom: 20px" class="btn btn-primary" >Tambah Banner</a>
						<?php
							echo $this->session->userdata("message");
                        ?>
                
						<table class="table table-bordered" style="font-size:12px;">
							<thead>
								<tr style="font-weight: bold;">
									<td width="5%">No</td>
									
									<td>Nama Banner</td>
									<td>Image</td>
									
									<td width="20%"></td>
								</tr>
                            </thead>
                            <tbody>
                                <?php foreach ($banner as $val){
                                    ?>

                                   
                            <tr>
            					<td align="center">1</td>
            					<td align="center"><?php echo $val->banner ?></td>
            					<td align="center"><image width="100px" src="<? echo base_url()."/public/uploads/"?><?php echo $val->banner_image ?>"></td>
            					
                                <td align="center"><a href="<?php echo base_url()."promotion/delete_banner/"?><?php echo $val->id_banner ?>" onclick="return confirm('Apakah anda yakin menghapus data ini ?')" class="btn btn-icon btn-danger m-b-5"><i class="fa fa-trash"></i></a> <a href="<?php echo base_url()."promotion/edit_banner/"?><?php echo $val->id_banner ?>" class="btn btn-icon btn-info m-b-5"><i class="fa fa-pencil"></i></a></td>
                            </tr>
                            <?php
                                }?>
                            </tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div> <!-- /Portlet -->	
</div>





