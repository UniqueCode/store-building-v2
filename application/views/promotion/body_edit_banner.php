<div class="wraper container-fluid">
    <div class="page-title"> 
    	<h3 class="title">Edit Banner</h3> 
	</div>

    <div class="portlet"><!-- /primary heading -->
        <div id="portlet2" class="panel-collapse collapse in">
            <div class="portlet-body">
                <div class="row" style="margin-top: 30px;">
            		<div class="col-md-12">

            			<form action="<?php echo base_url('promotion/edit_banner_saved'); ?>" class="form-horizontal" role="form" method="post" enctype='multipart/form-data'>                                    
                            
                        <input type="hidden" name="id_banner" value="<?php echo $banner["id_banner"] ?>">
                        <div class="form-group">
                                <label class="col-md-2 control-label">Nama Banner</label>
                                <div class="col-md-10">
                                	<input type="text" class="form-control" name="nama" value="<?php echo $banner["banner"] ?>"  required>
                              	</div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Gambar</label>
                                <div class="col-md-10">
                                	<input type="file" class="form-control" accept="image/*" name="banner_image" required>
                              	</div>
                            </div>
                            <div class="form-group" style="text-align: right;">
                            	<div class="col-md-12">
                            		<input type="submit" class="btn btn-primary check" value="Submit">
                            	</div>
                            </div>
                     	</form>
            		</div>
            	</div>
            </div>
        </div>
    </div> <!-- /Portlet -->	
</div>


