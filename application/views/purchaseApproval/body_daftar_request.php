<div class="wraper container-fluid">
    <div class="page-title"> 
        <h3 class="title">Daftar Purchase Request</h3> 
    </div>
    
    <ul class="nav nav-tabs"> 
        <li  class="active"> 
            <a href="#" data-toggle="tab" aria-expanded="false" class="wait-approve-button"> 
                <span>Menunggu Approve</span> 
            </a> 
        </li> 

        <li> 
            <a href="#profile" data-toggle="tab" aria-expanded="true" class="telah-disetujui"> 
              	<span>Telah Disetujui</span> 
            </a> 
        </li> 

        <li> 
            <a href="#profile" data-toggle="tab" aria-expanded="true" class="ditolak-list"> 
                <span>Ditolak</span> 
            </a> 
        </li> 
    </ul>

    <div class="tab-content table-responsive" id="daftar-request-wait" align="center">  

	</div>
</div>


