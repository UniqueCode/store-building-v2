<div class="wraper container-fluid">
    <div class="page-title"> 
      <h3 class="title">Daftar PO</h3> 
    </div>

    <div class="portlet"><!-- /primary heading -->        
        <div id="portlet2" class="panel-collapse collapse in">
            <div class="portlet-body">
                <div class="row" style="margin-top: 20px;">
                    <div class="col-md-12" style="padding: 20px;">
                        <table class="table table-bordered" style="font-size:12px;" id="datatable">  
                           <thead>
                               <tr style="font-weight: bold;">
                                    <td width="5%" style="text-align: center;vertical-align: middle;">No</td>
                                    <td style="text-align: center;vertical-align: middle;" width="18%">No PO</td>
                                    <td style="text-align: center;vertical-align: middle;" width="15%">Tanggal PO</td>
                                    <td style="text-align: center;vertical-align: middle;" width="15%">Tanggal Kirim</td>
                                    <td style="text-align: center;vertical-align: middle;">Supplier</td>
                                    <td style="text-align: center;vertical-align: middle;" width="15%">PIC</td>
                                    <td style="text-align: center;vertical-align: middle;" width="8%">Status</td>
                               </tr>

                            </thead>
                        </table>
                   </div>
                </div>
            </div>
        </div>
    </div> <!-- /Portlet -->	
</div>
