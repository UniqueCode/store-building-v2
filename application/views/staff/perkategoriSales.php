<canvas id="pie-chart" class="chart-holder" width="600" height="600"></canvas>

</table>

<script type="text/javascript">
	var trxStatus = document.getElementById("pie-chart");

	var myChart = new Chart(trxStatus, {
			    	type: 'horizontalBar',
			    	data 	: { 
						        labels 	: <?php echo $kategori; ?>,
						        datasets: [{
						        				label: 'Penjualan Perkategori',
						            			data: <?php echo $sales; ?>,
						            			backgroundColor: [dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors(),dynamicColors()],
						            			borderWidth: 1
						        		  }]
						    },
							    options: {
							        scales: {
							            xAxes: [{
							                ticks: {
							                    beginAtZero:true,
							                    userCallback: function(value, index, values) {
						                            return addCommas(value);
						                        }
							                }
							            }]
							        },
							        tooltips: {
						                enabled: true,  
						                callbacks: {
						                    label: function(tooltipItems, data) { 
						                        return addCommas(tooltipItems.yLabel);
						                    }
						                }
						            }
							    }
	});
</script>