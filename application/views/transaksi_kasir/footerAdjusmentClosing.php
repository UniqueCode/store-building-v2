 <!-- Page Content Ends -->
            <!-- ================== -->

            <!-- Footer Start -->
            <footer class="footer">
                <?php echo $footer; ?>
            </footer>
            <!-- Footer Ends -->
        </section>
        <!-- Main Content Ends -->

        <!-- js placed at the end of the document so the pages load faster -->
        <script src="<?php echo base_url('assets'); ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/modernizr.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/pace.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/wow.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/jquery.scrollTo.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/jquery.nicescroll.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets'); ?>/assets/chat/moment-2.2.1.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/jquery.app.js"></script>
        <!-- Todo -->
        <script src="<?php echo base_url('assets'); ?>/js/jquery.todo.js"></script>
        <script src="<?php echo base_url('assets'); ?>/assets/select2/select2.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url('assets'); ?>/assets/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/assets/datatables/dataTables.bootstrap.js"></script>
        <script src="<?php echo base_url('assets'); ?>/assets/timepicker/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('assets'); ?>/assets/notifications/notify.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/assets/notifications/notify-metro.js"></script>
        <script src="<?php echo base_url('assets'); ?>/assets/notifications/notifications.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                var idUser  = "<?php echo $idUser; ?>";
                var tanggal = "<?php echo $tanggal; ?>";
                
                var url = "<?php echo base_url('kasir/dataAdjusmentKasir'); ?>";

                $('#viewTrxKasir').load(url,{idUser : idUser, tanggal : tanggal});
            });

            $('#search').on("keyup",function(){
                var search = $(this).val();
                var idUser  = "<?php echo $idUser; ?>";
                var tanggal = "<?php echo $tanggal; ?>";
                var url = "<?php echo base_url('kasir/dataAdjusmentKasirFilter'); ?>";
                $('#viewTrxKasir').load(url,{idUser : idUser, tanggal : tanggal, search : search});

            });
        </script>
    </body>
</html>
