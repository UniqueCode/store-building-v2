/*
 Navicat Premium Data Transfer

 Source Server         : toko bangunan
 Source Server Type    : MySQL
 Source Server Version : 100231
 Source Host           : 103.28.53.243:3306
 Source Schema         : akhidweb_etoko

 Target Server Type    : MySQL
 Target Server Version : 100231
 File Encoding         : 65001

 Date: 14/04/2020 10:38:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ae_kabupaten
-- ----------------------------
DROP TABLE IF EXISTS `ae_kabupaten`;
CREATE TABLE `ae_kabupaten` (
  `kabupaten_id` int(20) NOT NULL AUTO_INCREMENT,
  `id_provinsi` int(20) NOT NULL,
  `nama_kabupaten` varchar(100) NOT NULL,
  `kode` varchar(10) NOT NULL,
  PRIMARY KEY (`kabupaten_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=474 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ae_kabupaten
-- ----------------------------
BEGIN;
INSERT INTO `ae_kabupaten` VALUES (1, 1, 'Kab. Jembrana', 'DPS');
INSERT INTO `ae_kabupaten` VALUES (2, 1, 'Kab. Bangli', 'DPS');
INSERT INTO `ae_kabupaten` VALUES (3, 1, 'Kota Denpasar', 'DPS');
INSERT INTO `ae_kabupaten` VALUES (4, 1, 'Kab. Tabanan', 'DPS');
INSERT INTO `ae_kabupaten` VALUES (5, 1, 'Kab. Klungkung', 'DPS');
INSERT INTO `ae_kabupaten` VALUES (6, 1, 'Kab. Gianyar', 'DPS');
INSERT INTO `ae_kabupaten` VALUES (7, 1, 'Kab. Karangasem', 'DPS');
INSERT INTO `ae_kabupaten` VALUES (8, 1, 'Kab. Badung', 'DPS');
INSERT INTO `ae_kabupaten` VALUES (9, 1, 'Kab. Buleleng', 'DPS');
INSERT INTO `ae_kabupaten` VALUES (10, 2, 'Kab. Bangka Selatan', 'PGK');
INSERT INTO `ae_kabupaten` VALUES (11, 2, 'Kab. Bangka Barat', 'PGK');
INSERT INTO `ae_kabupaten` VALUES (12, 2, 'Kota Pangkal Pinang', 'PGK');
INSERT INTO `ae_kabupaten` VALUES (13, 2, 'Kab. Belitung', 'TJQ');
INSERT INTO `ae_kabupaten` VALUES (14, 2, 'Kab. Bangka Tengah', 'PGK');
INSERT INTO `ae_kabupaten` VALUES (15, 2, 'Kab. Bangka', 'PGK');
INSERT INTO `ae_kabupaten` VALUES (16, 2, 'Kab. Belitung Timur', 'TJQ');
INSERT INTO `ae_kabupaten` VALUES (17, 3, 'Kota Tangerang', 'TGR');
INSERT INTO `ae_kabupaten` VALUES (18, 3, 'Kota Serang', 'CLG');
INSERT INTO `ae_kabupaten` VALUES (19, 3, 'Kab. Pandeglang', 'CLG');
INSERT INTO `ae_kabupaten` VALUES (20, 3, 'Kab. Tangerang', 'TGR');
INSERT INTO `ae_kabupaten` VALUES (21, 3, 'Kab. Serang', 'CLG');
INSERT INTO `ae_kabupaten` VALUES (22, 3, 'Kab. Lebak', 'CLG');
INSERT INTO `ae_kabupaten` VALUES (23, 3, 'Kota Cilegon', 'CLG');
INSERT INTO `ae_kabupaten` VALUES (24, 4, 'Kota Bengkulu', 'BKS');
INSERT INTO `ae_kabupaten` VALUES (25, 4, 'Kab. Muko-Muko', 'BKS');
INSERT INTO `ae_kabupaten` VALUES (26, 4, 'Kab. Kepahiang', 'BKS');
INSERT INTO `ae_kabupaten` VALUES (27, 4, 'Kab. Bengkulu Selatan', 'BKS');
INSERT INTO `ae_kabupaten` VALUES (28, 4, 'Kab. Bengkulu Utara', 'BKS');
INSERT INTO `ae_kabupaten` VALUES (29, 4, 'Kab. Seluma', 'BKS');
INSERT INTO `ae_kabupaten` VALUES (30, 4, 'Kab. Lebong', 'BKS');
INSERT INTO `ae_kabupaten` VALUES (31, 4, 'Kab. Kaur', 'BKS');
INSERT INTO `ae_kabupaten` VALUES (32, 4, 'Kab. Rejang Lebong', 'BKS');
INSERT INTO `ae_kabupaten` VALUES (33, 5, 'Kab. Gunung Kidul', 'JOG');
INSERT INTO `ae_kabupaten` VALUES (34, 5, 'Kab. Sleman', 'JOG');
INSERT INTO `ae_kabupaten` VALUES (35, 5, 'Kota Yogyakarta', 'JOG');
INSERT INTO `ae_kabupaten` VALUES (36, 5, 'Kab. Kulon Progo', 'JOG');
INSERT INTO `ae_kabupaten` VALUES (37, 5, 'Kab. Bantul', 'JOG');
INSERT INTO `ae_kabupaten` VALUES (38, 6, 'Kab. Gorontalo Utara', 'GTO');
INSERT INTO `ae_kabupaten` VALUES (39, 6, 'Kab. Boalemo', 'GTO');
INSERT INTO `ae_kabupaten` VALUES (40, 6, 'Kota Gorontalo', 'GTO');
INSERT INTO `ae_kabupaten` VALUES (41, 6, 'Kab. Pahuwalo', 'GTO');
INSERT INTO `ae_kabupaten` VALUES (42, 6, 'Kota Bone Bolango', 'GTO');
INSERT INTO `ae_kabupaten` VALUES (43, 6, 'Kab. Gorontalo', 'GTO');
INSERT INTO `ae_kabupaten` VALUES (44, 7, 'Kota Administrasi Jakarta Pusat', 'CGK');
INSERT INTO `ae_kabupaten` VALUES (45, 7, 'Kota Administrasi Jakarta Barat', 'CGK');
INSERT INTO `ae_kabupaten` VALUES (46, 7, 'Kota Administrasi Kepulauan Seribu', 'CGK');
INSERT INTO `ae_kabupaten` VALUES (47, 7, 'Kota Administrasi Jakarta Utara', 'CGK');
INSERT INTO `ae_kabupaten` VALUES (48, 7, 'Kota Administrasi Jakarta Selatan', 'CGK');
INSERT INTO `ae_kabupaten` VALUES (49, 7, 'DKI Jakarta', 'CGK');
INSERT INTO `ae_kabupaten` VALUES (50, 7, 'Kota Administrasi Jakarta Timur', 'CGK');
INSERT INTO `ae_kabupaten` VALUES (51, 8, 'Kab. Tanjung Jabung Barat', 'DJB');
INSERT INTO `ae_kabupaten` VALUES (52, 8, 'Kab. Tanjung Jabung Timur', 'DJB');
INSERT INTO `ae_kabupaten` VALUES (53, 8, 'Kab. Muaro Jambi', 'DJB');
INSERT INTO `ae_kabupaten` VALUES (54, 8, 'Kab. Bungo', 'DJB');
INSERT INTO `ae_kabupaten` VALUES (55, 8, 'Kab. Merangin', 'DJB');
INSERT INTO `ae_kabupaten` VALUES (56, 8, 'Kota Jambi', 'DJB');
INSERT INTO `ae_kabupaten` VALUES (57, 8, 'Kab. Tebo', 'DJB');
INSERT INTO `ae_kabupaten` VALUES (58, 8, 'Kab. Sarolangun', 'DJB');
INSERT INTO `ae_kabupaten` VALUES (59, 8, 'Kab. Kerinci', 'DJB');
INSERT INTO `ae_kabupaten` VALUES (60, 8, 'Kab. Batang Hari', 'DJB');
INSERT INTO `ae_kabupaten` VALUES (61, 9, 'Kab. Sumedang', 'BDO');
INSERT INTO `ae_kabupaten` VALUES (62, 9, 'Kota Sukabumi', 'SMI');
INSERT INTO `ae_kabupaten` VALUES (63, 9, 'Kota Bogor', 'BOO');
INSERT INTO `ae_kabupaten` VALUES (64, 9, 'Kota Bekasi', 'BKI');
INSERT INTO `ae_kabupaten` VALUES (65, 9, 'Kab. Bandung', 'BDO');
INSERT INTO `ae_kabupaten` VALUES (66, 9, 'Kab. Karawang', 'KRW');
INSERT INTO `ae_kabupaten` VALUES (67, 9, 'Kab. Bandung Barat', 'BDO');
INSERT INTO `ae_kabupaten` VALUES (68, 9, 'Kab. Cirebon', 'CBN');
INSERT INTO `ae_kabupaten` VALUES (69, 9, 'Kab. Garut', 'BDO');
INSERT INTO `ae_kabupaten` VALUES (70, 9, 'Kab. Kuningan', 'CBN');
INSERT INTO `ae_kabupaten` VALUES (71, 9, 'Kab.Ciamis', 'BDO');
INSERT INTO `ae_kabupaten` VALUES (72, 9, 'Kota Cirebon', 'CBN');
INSERT INTO `ae_kabupaten` VALUES (73, 9, 'Kota Tasikmalaya', 'BDO');
INSERT INTO `ae_kabupaten` VALUES (74, 9, 'Kab. Cianjur', 'SMI');
INSERT INTO `ae_kabupaten` VALUES (75, 9, 'Kab. Bogor', 'BOO');
INSERT INTO `ae_kabupaten` VALUES (76, 9, 'Kab. Bekasi', 'BKI');
INSERT INTO `ae_kabupaten` VALUES (77, 9, 'Kota Cimahi', 'BDO');
INSERT INTO `ae_kabupaten` VALUES (78, 9, 'Kab. Sukabumi', 'SMI');
INSERT INTO `ae_kabupaten` VALUES (79, 9, 'Kab. Subang', 'BDO');
INSERT INTO `ae_kabupaten` VALUES (80, 9, 'Kota Bandung', 'BDO');
INSERT INTO `ae_kabupaten` VALUES (81, 9, 'Kab. Purwakarta', 'BDO');
INSERT INTO `ae_kabupaten` VALUES (82, 9, 'Kab. Majalengka', 'CBN');
INSERT INTO `ae_kabupaten` VALUES (83, 9, 'Kota Depok', 'DPK');
INSERT INTO `ae_kabupaten` VALUES (84, 9, 'Kab. Tasikmalaya', 'BDO');
INSERT INTO `ae_kabupaten` VALUES (85, 9, 'Kab. Indramayu', 'CBN');
INSERT INTO `ae_kabupaten` VALUES (86, 9, 'Kota Banjar', 'BDO');
INSERT INTO `ae_kabupaten` VALUES (87, 10, 'Kab. Rembang', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (88, 10, 'Kota Pekalongan', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (89, 10, 'Kab. Cilacap', 'CXP');
INSERT INTO `ae_kabupaten` VALUES (90, 10, 'Kab. Pemalang', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (91, 10, 'Kab. Jepara', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (92, 10, 'Kab. Sukoharjo', 'SOC');
INSERT INTO `ae_kabupaten` VALUES (93, 10, 'Kab. Pekalongan', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (94, 10, 'Kab. Kendal', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (95, 10, 'Kab. Temanggung', 'MGL');
INSERT INTO `ae_kabupaten` VALUES (96, 10, 'Kab. Klaten', 'SOC');
INSERT INTO `ae_kabupaten` VALUES (97, 10, 'Kab. Purbalingga', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (98, 10, 'Kab. Brebes', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (99, 10, 'Kab. Wonosobo', 'MGL');
INSERT INTO `ae_kabupaten` VALUES (100, 10, 'Kab. Boyolali', 'SOC');
INSERT INTO `ae_kabupaten` VALUES (101, 10, 'Kota Tegal', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (102, 10, 'Kab. Blora', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (103, 10, 'Kota Magelang', 'MGL');
INSERT INTO `ae_kabupaten` VALUES (104, 10, 'Kota Salatiga', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (105, 10, 'Kab. Banyumas', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (106, 10, 'Kab. Grobogan', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (107, 10, 'Kab. Kudus', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (108, 10, 'Kab. Wonogiri', 'SOC');
INSERT INTO `ae_kabupaten` VALUES (109, 10, 'Kab. Pati', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (110, 10, 'Kab. Magelang', 'MGL');
INSERT INTO `ae_kabupaten` VALUES (111, 10, 'Kota Semarang', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (112, 10, 'Kab. Sragen', 'SOC');
INSERT INTO `ae_kabupaten` VALUES (113, 10, 'Kab. Banjarnegara', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (114, 10, 'Kab. Demak', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (115, 10, 'Kab. Purworejo', 'MGL');
INSERT INTO `ae_kabupaten` VALUES (116, 10, 'Kab. Karang Anyar', 'SOC');
INSERT INTO `ae_kabupaten` VALUES (117, 10, 'Kab. Semarang', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (118, 10, 'Kab. Bojonegoro', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (119, 10, 'Kab. Kebumen', 'MGL');
INSERT INTO `ae_kabupaten` VALUES (120, 10, 'Kota Surakarta', 'SOC');
INSERT INTO `ae_kabupaten` VALUES (121, 10, 'Kab. Tegal', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (122, 10, 'Kab. Batang', 'SRG');
INSERT INTO `ae_kabupaten` VALUES (123, 11, 'Kab. Jember', 'JBR');
INSERT INTO `ae_kabupaten` VALUES (124, 11, 'Kab. Madiun', 'MDN');
INSERT INTO `ae_kabupaten` VALUES (125, 11, 'Kab. Lamongan', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (126, 11, 'Kota Mojokerto', 'MJK');
INSERT INTO `ae_kabupaten` VALUES (127, 11, 'Kab. Tulungagung', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (128, 11, 'Kota Surabaya', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (129, 11, 'Kab. Mojokerto', 'MJK');
INSERT INTO `ae_kabupaten` VALUES (130, 11, 'Kota Kediri', 'KDR');
INSERT INTO `ae_kabupaten` VALUES (131, 11, 'Kab. Sumenep', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (132, 11, 'Kab. Sampang', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (133, 11, 'Kab Situbondo', 'PBL');
INSERT INTO `ae_kabupaten` VALUES (134, 11, 'Kab. Malang', 'MXG');
INSERT INTO `ae_kabupaten` VALUES (135, 11, 'Kab. Pasuruan', 'PDN');
INSERT INTO `ae_kabupaten` VALUES (136, 11, 'Kab. Pacitan', 'MDN');
INSERT INTO `ae_kabupaten` VALUES (137, 11, 'Kab. Nganjuk', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (138, 11, 'Kota Probolinggo', 'PBL');
INSERT INTO `ae_kabupaten` VALUES (139, 11, 'Kota Blitar', 'MXG');
INSERT INTO `ae_kabupaten` VALUES (140, 11, 'Kab. Banyuwangi', 'JBR');
INSERT INTO `ae_kabupaten` VALUES (141, 11, 'Kab. Magetan', 'MDN');
INSERT INTO `ae_kabupaten` VALUES (142, 11, 'Kab. Bangkalan', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (143, 11, 'Kota Malang', 'MXG');
INSERT INTO `ae_kabupaten` VALUES (144, 11, 'Kab. Trenggalek', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (145, 11, 'Kota Madiun', 'MDN');
INSERT INTO `ae_kabupaten` VALUES (146, 11, 'Kab.Gresik', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (147, 11, 'Kab. Sidoarjo', 'MJK');
INSERT INTO `ae_kabupaten` VALUES (148, 11, 'Kab. Kediri', 'KDR');
INSERT INTO `ae_kabupaten` VALUES (149, 11, 'Kab. Tuban', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (150, 11, 'SURABAYA', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (151, 11, 'Kab. Sidoarjo', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (152, 11, 'Kota Pasuruan', 'PDN');
INSERT INTO `ae_kabupaten` VALUES (153, 11, 'Kab. Ponorogo', 'MDN');
INSERT INTO `ae_kabupaten` VALUES (154, 11, 'Kab. Pamekasan', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (155, 11, 'Kab. Lumajang', 'PBL');
INSERT INTO `ae_kabupaten` VALUES (156, 11, 'Kota Batu', 'MXG');
INSERT INTO `ae_kabupaten` VALUES (157, 11, 'Kab. Bondowoso', 'JBR');
INSERT INTO `ae_kabupaten` VALUES (158, 11, 'Kab. Ngawi', 'MDN');
INSERT INTO `ae_kabupaten` VALUES (159, 11, 'Kab. Jombang', 'SUB');
INSERT INTO `ae_kabupaten` VALUES (160, 11, 'Kab. Probolinggo', 'PBL');
INSERT INTO `ae_kabupaten` VALUES (161, 11, 'Kab. Blitar', 'MXG');
INSERT INTO `ae_kabupaten` VALUES (162, 12, 'Kab. Landak', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (163, 12, 'Kab. Kapuas Hulu', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (164, 12, 'Kota Singkawang', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (165, 12, 'Kab. Kubu Raya', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (166, 12, 'Kab. Ketapang', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (167, 12, 'Kab. Sambas', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (168, 12, 'Kab. Melawi', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (169, 12, 'Kab. Bengkayang', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (170, 12, 'Kab. Sintang', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (171, 12, 'Kab. Kayong Utara', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (172, 12, 'Kab. Sanggau', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (173, 12, 'Kab. Sekadau', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (174, 12, 'Kota Pontianak', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (175, 12, 'Kab. Pontianak', 'PNK');
INSERT INTO `ae_kabupaten` VALUES (176, 13, 'Kab. Tanah Laut', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (177, 13, 'Kab. Barito Kuala', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (178, 13, 'Kab. Tanah Bambu', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (179, 13, 'Kota Banjarbaru', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (180, 13, 'Kab. Barito Selatan', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (181, 13, 'Kab. Kota Baru', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (182, 13, 'Kab. Barito Timur', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (183, 13, 'Kab. Hulu Sungai Tengah', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (184, 13, 'Kab. Tampin', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (185, 13, 'Kab. Banjar', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (186, 13, 'Kab. Hulu Sungai Utara', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (187, 13, 'Kab. Balangan', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (188, 13, 'Kab. Tabalong', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (189, 13, 'Kab. Murung Raya', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (190, 13, 'Kab. Hulu Sungai Selatan', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (191, 13, 'Kab. Barito Utara', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (192, 13, 'Kota Banjarmasin', 'BDJ');
INSERT INTO `ae_kabupaten` VALUES (193, 14, 'Kota Palangkaraya', 'PKY');
INSERT INTO `ae_kabupaten` VALUES (194, 14, 'Kab. Sukamara', 'PKY');
INSERT INTO `ae_kabupaten` VALUES (195, 14, 'Kab. Lamandau', 'PKY');
INSERT INTO `ae_kabupaten` VALUES (196, 14, 'Kab. Kotawaringin Barat', 'PKY');
INSERT INTO `ae_kabupaten` VALUES (197, 14, 'Kab. Gunung Mas', 'PKY');
INSERT INTO `ae_kabupaten` VALUES (198, 14, 'Kab. Katingan', 'PKY');
INSERT INTO `ae_kabupaten` VALUES (199, 14, 'Kab. Pulang Pisau', 'PKY');
INSERT INTO `ae_kabupaten` VALUES (200, 14, 'Kab. Kotawaringin Timur', 'PKY');
INSERT INTO `ae_kabupaten` VALUES (201, 14, 'Kab. Seruyan', 'PKY');
INSERT INTO `ae_kabupaten` VALUES (202, 14, 'Kab. Kapuas', 'PKY');
INSERT INTO `ae_kabupaten` VALUES (203, 33, 'Kab. Tana Tidung', 'TRK');
INSERT INTO `ae_kabupaten` VALUES (204, 33, 'Kab. Malinau', 'TRK');
INSERT INTO `ae_kabupaten` VALUES (205, 15, 'Kab. Kutai Barat', 'SMD');
INSERT INTO `ae_kabupaten` VALUES (206, 33, 'Kota Tarakan', 'TRK');
INSERT INTO `ae_kabupaten` VALUES (207, 15, 'Kota Samarinda', 'SMD');
INSERT INTO `ae_kabupaten` VALUES (208, 15, 'Kota Bontang', 'BTG');
INSERT INTO `ae_kabupaten` VALUES (209, 15, 'Kab. Berau', 'BPN');
INSERT INTO `ae_kabupaten` VALUES (210, 15, 'Kab. Kutai Kartanegara', 'BPN');
INSERT INTO `ae_kabupaten` VALUES (211, 33, 'Kab. Nunukan', 'TRK');
INSERT INTO `ae_kabupaten` VALUES (212, 33, 'Kab. Bulungan', 'TRK');
INSERT INTO `ae_kabupaten` VALUES (213, 15, 'Kab. Kutai Kartanegara', 'SMD');
INSERT INTO `ae_kabupaten` VALUES (214, 15, 'Kab. Kutai Timur', 'BTG');
INSERT INTO `ae_kabupaten` VALUES (215, 15, 'Kab. Panajam Paser Utara', 'BPN');
INSERT INTO `ae_kabupaten` VALUES (216, 15, 'Kab. Paser', 'BPN');
INSERT INTO `ae_kabupaten` VALUES (217, 15, 'Kota Balikpapan', 'BPN');
INSERT INTO `ae_kabupaten` VALUES (218, 16, 'Kab. Natuna', 'BTH');
INSERT INTO `ae_kabupaten` VALUES (219, 16, 'Kota Batam', 'BTH');
INSERT INTO `ae_kabupaten` VALUES (220, 16, 'Kota Tanjung Pinang', 'TNJ');
INSERT INTO `ae_kabupaten` VALUES (221, 16, 'Kab. Karimun', 'BTH');
INSERT INTO `ae_kabupaten` VALUES (222, 16, 'Kab. Lingga', 'BTH');
INSERT INTO `ae_kabupaten` VALUES (223, 16, 'Kab. Bintan', 'TNJ');
INSERT INTO `ae_kabupaten` VALUES (224, 17, 'Kab. Lampung Timur', 'TKG');
INSERT INTO `ae_kabupaten` VALUES (225, 17, 'Kab. Lampung Barat', 'TKG');
INSERT INTO `ae_kabupaten` VALUES (226, 17, 'Kab. Lampung Utara', 'TKG');
INSERT INTO `ae_kabupaten` VALUES (227, 17, 'Kota Bandar Lampung', 'TKG');
INSERT INTO `ae_kabupaten` VALUES (228, 17, 'Kab. Tanggamus', 'TKG');
INSERT INTO `ae_kabupaten` VALUES (229, 17, 'Kab. Way Kanan', 'TKG');
INSERT INTO `ae_kabupaten` VALUES (230, 17, 'Kab. Lampung Tengah', 'TKG');
INSERT INTO `ae_kabupaten` VALUES (231, 17, 'Kota Metro', 'TKG');
INSERT INTO `ae_kabupaten` VALUES (232, 17, 'Kab. Lampung Selatan', 'TKG');
INSERT INTO `ae_kabupaten` VALUES (233, 17, 'Kab. Pesawaran', 'TKG');
INSERT INTO `ae_kabupaten` VALUES (234, 17, 'Kab. Tulang Bawang', 'TKG');
INSERT INTO `ae_kabupaten` VALUES (235, 18, 'Kab. Seram Bagian Timur', 'AMQ');
INSERT INTO `ae_kabupaten` VALUES (236, 18, 'Kab. Kepulauan Aru', 'AMQ');
INSERT INTO `ae_kabupaten` VALUES (237, 18, 'Kab. Maluku Tenggara', 'AMQ');
INSERT INTO `ae_kabupaten` VALUES (238, 18, 'Kota Ambon', 'AMQ');
INSERT INTO `ae_kabupaten` VALUES (239, 18, 'Kab. Maluku Tenggara Barat', 'AMQ');
INSERT INTO `ae_kabupaten` VALUES (240, 18, 'Kab. Seram Bagian Barat', 'AMQ');
INSERT INTO `ae_kabupaten` VALUES (241, 18, 'Kab. Buru', 'AMQ');
INSERT INTO `ae_kabupaten` VALUES (242, 18, 'Kab. Maluku Tengah', 'AMQ');
INSERT INTO `ae_kabupaten` VALUES (243, 19, 'Kab. Kepulauan Sula', 'TTE');
INSERT INTO `ae_kabupaten` VALUES (244, 19, 'Kab. Halmahera Timur', 'TTE');
INSERT INTO `ae_kabupaten` VALUES (245, 19, 'Kab. Halmahera Selatan', 'TTE');
INSERT INTO `ae_kabupaten` VALUES (246, 19, 'Kota Ternate', 'TTE');
INSERT INTO `ae_kabupaten` VALUES (247, 19, 'Kota Tidore', 'TTE');
INSERT INTO `ae_kabupaten` VALUES (248, 19, 'Kab. Halmahera Utara', 'TTE');
INSERT INTO `ae_kabupaten` VALUES (249, 19, 'Kab. Halmahera Tengah', 'TTE');
INSERT INTO `ae_kabupaten` VALUES (250, 19, 'Kab. Halmahera Barat', 'TTE');
INSERT INTO `ae_kabupaten` VALUES (251, 20, 'Kab. Pidie Jaya', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (252, 20, 'Kab. Aceh Selatan', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (253, 20, 'Kab. Gayo Lues', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (254, 20, 'Kab. Pidie', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (255, 20, 'Kab. Aceh Utara', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (256, 20, 'Kab. Aceh Tenggara', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (257, 20, 'Kab. Aceh Tamiang', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (258, 20, 'Kab. Aceh Besar', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (259, 20, 'Kab. Aceh Jaya', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (260, 20, 'Kota Langsa', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (261, 20, 'Kab. Simeuleu', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (262, 20, 'Kota Sabang', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (263, 20, 'Kab. Nagan Raya', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (264, 20, 'Kab. Aceh Tengah', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (265, 20, 'Kab. Bener Meriah', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (266, 20, 'Kab. Aceh Barat', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (267, 20, 'Kab. Aceh Timur', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (268, 20, 'Kab. Bireuen', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (269, 20, 'Kab. Aceh Singkil', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (270, 20, 'Kota Lhokseumawe', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (271, 20, 'Kota Subulussalam', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (272, 20, 'Kab. Aceh Barat Daya', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (273, 20, 'Kota Banda Aceh', 'BTJ');
INSERT INTO `ae_kabupaten` VALUES (274, 21, 'Kab. Lombok Timur', 'AMI');
INSERT INTO `ae_kabupaten` VALUES (275, 21, 'Kab. Dompu', 'AMI');
INSERT INTO `ae_kabupaten` VALUES (276, 21, 'Kota Mataram', 'AMI');
INSERT INTO `ae_kabupaten` VALUES (277, 21, 'Kab. Sumbawa Barat', 'AMI');
INSERT INTO `ae_kabupaten` VALUES (278, 21, 'Kab. Sumbawa', 'AMI');
INSERT INTO `ae_kabupaten` VALUES (279, 21, 'Kab. Lombok Tengah', 'AMI');
INSERT INTO `ae_kabupaten` VALUES (280, 21, 'Kota Bima', 'AMI');
INSERT INTO `ae_kabupaten` VALUES (281, 21, 'Kab. Bima', 'AMI');
INSERT INTO `ae_kabupaten` VALUES (282, 21, 'Kab. Lombok Barat', 'AMI');
INSERT INTO `ae_kabupaten` VALUES (283, 22, 'Kab. Belu', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (284, 22, 'Kab. Sumba Timur', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (285, 22, 'Kota Kupang', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (286, 22, 'Kab. Ende', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (287, 22, 'Kab. Lembata', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (288, 22, 'Kab. Manggarai', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (289, 22, 'Kab. Rote Ndao', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (290, 22, 'Kab. Flores Timur', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (291, 22, 'Kab. Manggarai Timur', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (292, 22, 'Kab. Alor', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (293, 22, 'Kab. Sumba Barat', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (294, 22, 'Kab. Sumba Tengah', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (295, 22, 'Kab. Ngada', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (296, 22, 'Kab. Kupang', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (297, 22, 'Kab. Timor Tengah Selatan', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (298, 22, 'Kab. Sumba Barat Daya', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (299, 22, 'Kab. Sikka', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (300, 22, 'Kab. Nagekeo', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (301, 22, 'Kab. Timor Tengah Utara', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (302, 22, 'Kab. Manggarai Barat', 'KOE');
INSERT INTO `ae_kabupaten` VALUES (303, 23, 'Kab. Sarmi', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (304, 23, 'Kab. Intan Jaya', 'NBX');
INSERT INTO `ae_kabupaten` VALUES (305, 23, 'Kota Jayapura', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (306, 23, 'Kab. Keerom', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (307, 23, 'Kab. Puncak Jaya', 'NBX');
INSERT INTO `ae_kabupaten` VALUES (308, 23, 'Kab. Asmat', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (309, 23, 'Kab. Dogiyai', 'NBX');
INSERT INTO `ae_kabupaten` VALUES (310, 23, 'Kab. Mamberamo Raya', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (311, 23, 'Kab. Yapen Waropen', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (312, 23, 'Kab. Waropen', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (313, 23, 'Kab. Supiori', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (314, 23, 'Kab. Deiyai', 'NBX');
INSERT INTO `ae_kabupaten` VALUES (315, 23, 'Kab. Jayapura', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (316, 23, 'Kab. Pegunungan Bintang', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (317, 23, 'Kab. Mappi', 'NBX');
INSERT INTO `ae_kabupaten` VALUES (318, 23, 'Kab. Boven Digoel', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (319, 23, 'Kab. Paniai', 'NBX');
INSERT INTO `ae_kabupaten` VALUES (320, 23, 'Kab. Jayawijaya', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (321, 23, 'Kab. Nabire', 'NBX');
INSERT INTO `ae_kabupaten` VALUES (322, 23, 'Kab. Yahukimo', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (323, 23, 'Kab. Merauke', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (324, 23, 'Kab. Mimika', 'TIM');
INSERT INTO `ae_kabupaten` VALUES (325, 23, 'Kab. Talikora', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (326, 23, 'Kab. Biak Numfor', 'DJJ');
INSERT INTO `ae_kabupaten` VALUES (327, 24, 'Kab. Fak-Fak', 'SOQ');
INSERT INTO `ae_kabupaten` VALUES (328, 24, 'Kab. Sorong', 'SOQ');
INSERT INTO `ae_kabupaten` VALUES (329, 24, 'Kab. Teluk Wondama', 'SOQ');
INSERT INTO `ae_kabupaten` VALUES (330, 24, 'Kab. Sorong Selatan', 'SOQ');
INSERT INTO `ae_kabupaten` VALUES (331, 24, 'Kab. Manokwari', 'SOQ');
INSERT INTO `ae_kabupaten` VALUES (332, 24, 'Kab. Kaimana', 'SOQ');
INSERT INTO `ae_kabupaten` VALUES (333, 24, 'Kota Sorong', 'SOQ');
INSERT INTO `ae_kabupaten` VALUES (334, 24, 'Kab. Teluk Bintuni', 'SOQ');
INSERT INTO `ae_kabupaten` VALUES (335, 24, 'Kab. Raja Ampat', 'SOQ');
INSERT INTO `ae_kabupaten` VALUES (336, 25, 'Kota Dumai', 'PKU');
INSERT INTO `ae_kabupaten` VALUES (337, 25, 'Kab. Siak', 'PKU');
INSERT INTO `ae_kabupaten` VALUES (338, 25, 'Kab. Palalawan', 'PKU');
INSERT INTO `ae_kabupaten` VALUES (339, 25, 'Kab. Rokan Hilir', 'PKU');
INSERT INTO `ae_kabupaten` VALUES (340, 25, 'Kab. Indragiri Hulu', 'PKU');
INSERT INTO `ae_kabupaten` VALUES (341, 25, 'Kab. Kampar', 'PKU');
INSERT INTO `ae_kabupaten` VALUES (342, 25, 'Kab. Meranti', 'PKU');
INSERT INTO `ae_kabupaten` VALUES (343, 25, 'Kota Pekanbaru', 'PKU');
INSERT INTO `ae_kabupaten` VALUES (344, 25, 'Kab. Rokan Hulu', 'PKU');
INSERT INTO `ae_kabupaten` VALUES (345, 25, 'Kab. Kuantan Sengingi', 'PKU');
INSERT INTO `ae_kabupaten` VALUES (346, 25, 'Kab. Indragiri Hilir', 'PKU');
INSERT INTO `ae_kabupaten` VALUES (347, 25, 'Kab. Bengkalis', 'PKU');
INSERT INTO `ae_kabupaten` VALUES (348, 26, 'Kab. Pangkajene Kepulauan', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (349, 26, 'Kota Palopo', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (350, 26, 'Kota Makasar', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (351, 26, 'Kab. Soppeng', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (352, 26, 'Kab. Maros', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (353, 26, 'Kab. Takalar', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (354, 26, 'Kab. Tana Toraja Utara', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (355, 26, 'Kab. Mamuju Utara', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (356, 26, 'Kab. Sinjai', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (357, 26, 'Kab. Jeneponto', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (358, 26, 'Kab. Luwu Timur', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (359, 26, 'Kab. Sindenreng Rappang', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (360, 26, 'Kab. Bulukumba', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (361, 26, 'Kota Pare-Pare', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (362, 26, 'Kab. Pinrang', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (363, 26, 'Kab. Bantaeng', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (364, 26, 'Kab. Selayar', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (365, 26, 'Kab. Majene', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (366, 26, 'Kab. Bone', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (367, 26, 'Kab. Mamuju', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (368, 26, 'Kab. Goa', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (369, 26, 'Kab. Tana Toraja', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (370, 26, 'Kab. Mamasa', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (371, 26, 'Kab. Wajo', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (372, 26, 'Kab. Enrekang', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (373, 26, 'Kab. Luwu Utara', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (374, 26, 'Kab. Polewali Mandar', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (375, 26, 'Kab. Barru', 'UPG');
INSERT INTO `ae_kabupaten` VALUES (376, 27, 'Kab. Donggala', 'PLW');
INSERT INTO `ae_kabupaten` VALUES (377, 27, 'Kab. Banggai Kepulauan', 'PLW');
INSERT INTO `ae_kabupaten` VALUES (378, 27, 'Kab. Poso', 'PLW');
INSERT INTO `ae_kabupaten` VALUES (379, 27, 'Kota Palu', 'PLW');
INSERT INTO `ae_kabupaten` VALUES (380, 27, 'Kab. Tojo Una-Una', 'PLW');
INSERT INTO `ae_kabupaten` VALUES (381, 27, 'Kab. Morowali', 'PLW');
INSERT INTO `ae_kabupaten` VALUES (382, 27, 'Kab. Buol', 'PLW');
INSERT INTO `ae_kabupaten` VALUES (383, 27, 'Kab. Toli-Toli', 'PLW');
INSERT INTO `ae_kabupaten` VALUES (384, 27, 'Kab. Banggai', 'PLW');
INSERT INTO `ae_kabupaten` VALUES (385, 27, 'Kab. Parigi Moutong', 'PLW');
INSERT INTO `ae_kabupaten` VALUES (386, 28, 'Kab. Muna', 'KDI');
INSERT INTO `ae_kabupaten` VALUES (387, 28, 'Kab. Kota Bau-Bau', 'KDI');
INSERT INTO `ae_kabupaten` VALUES (388, 28, 'Kab. Konawe Utara/ Selatan', 'KDI');
INSERT INTO `ae_kabupaten` VALUES (389, 28, 'Kab. Buton & Buton Utara', 'KDI');
INSERT INTO `ae_kabupaten` VALUES (390, 28, 'Kab. Konawe', 'KDI');
INSERT INTO `ae_kabupaten` VALUES (391, 28, 'Kab. Kolaka', 'KDI');
INSERT INTO `ae_kabupaten` VALUES (392, 28, 'Kota Kendari', 'KDI');
INSERT INTO `ae_kabupaten` VALUES (393, 28, 'Kab. Wakatobi', 'KDI');
INSERT INTO `ae_kabupaten` VALUES (394, 28, 'Kab. Kolaka Utara', 'KDI');
INSERT INTO `ae_kabupaten` VALUES (395, 28, 'Kab. Bombana', 'KDI');
INSERT INTO `ae_kabupaten` VALUES (396, 29, 'Kab. Bolaang Mongondow', 'MDC');
INSERT INTO `ae_kabupaten` VALUES (397, 29, 'Kota Tomohon', 'MDC');
INSERT INTO `ae_kabupaten` VALUES (398, 29, 'Kab. Minahasa Selatan', 'MDC');
INSERT INTO `ae_kabupaten` VALUES (399, 29, 'Kab. Kepulauan Talaud', 'MDC');
INSERT INTO `ae_kabupaten` VALUES (400, 29, 'Kota Bitung', 'MDC');
INSERT INTO `ae_kabupaten` VALUES (401, 29, 'Kab. Kepulauan Sangihe', 'MDC');
INSERT INTO `ae_kabupaten` VALUES (402, 29, 'Kota Manado', 'MDC');
INSERT INTO `ae_kabupaten` VALUES (403, 29, 'Kab. Minahasa Tenggara', 'MDC');
INSERT INTO `ae_kabupaten` VALUES (404, 29, 'Kab. Minahasa Utara', 'MDC');
INSERT INTO `ae_kabupaten` VALUES (405, 29, 'Kab. Bolaang Mongondow Utara', 'MDC');
INSERT INTO `ae_kabupaten` VALUES (406, 29, 'Kab. Minahasa', 'MDC');
INSERT INTO `ae_kabupaten` VALUES (407, 30, 'Kab. Solok Selatan', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (408, 30, 'Kab. Padang Pariaman', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (409, 30, 'Kab. Pasaman Barat', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (410, 30, 'Kab. Pasaman', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (411, 30, 'Kab. Lima Puluh Kota', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (412, 30, 'Kab. Tanah Datar', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (413, 30, 'Kota Padang Panjang', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (414, 30, 'Kota Sawahlunto', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (415, 30, 'Kab. Sijunjung', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (416, 30, 'Kota Pariaman', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (417, 30, 'Kab. Solok', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (418, 30, 'Kab. Pesisir Selatan', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (419, 30, 'Kab. Kepulauan Mentawai', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (420, 30, 'Kota Bukit Tinggi', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (421, 30, 'Kab. Dharmasraya', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (422, 30, 'Kota Padang', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (423, 30, 'Kota Solok', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (424, 30, 'Kab. Agam', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (425, 30, 'Kota Payakumbuh', 'PDG');
INSERT INTO `ae_kabupaten` VALUES (426, 31, 'Kab. Empat Lawang', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (427, 31, 'Kab. Musi Banyuasin', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (428, 31, 'Kota Pagar Alam', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (429, 31, 'Kab. Lahat', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (430, 31, 'Kab. Ogan Komering Ulu Selatan', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (431, 31, 'Kab. Ogan Komering Ulu', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (432, 31, 'Kab. Ogan Ilir', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (433, 31, 'Kota Palembang', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (434, 31, 'Kab. Banyuasin', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (435, 31, 'Kota Prabumulih', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (436, 31, 'Kab. Muara Enim', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (437, 31, 'Kota Lubuk Linggau', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (438, 31, 'Kab. Ogan Komering Ilir', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (439, 31, 'Kab. Ogan Komering Ulu Timur', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (440, 31, 'Kab. Musi Rawas', 'PLM');
INSERT INTO `ae_kabupaten` VALUES (441, 32, 'Kota Pematangsiantar', 'MES');
INSERT INTO `ae_kabupaten` VALUES (442, 32, 'Kab. Padang Lawas Utara', 'MES');
INSERT INTO `ae_kabupaten` VALUES (443, 32, 'Kab. Toba Samosir', 'MES');
INSERT INTO `ae_kabupaten` VALUES (444, 32, 'Kab. Tapanuli Selatan', 'MES');
INSERT INTO `ae_kabupaten` VALUES (445, 32, 'Kota Tebing Tinggi', 'MES');
INSERT INTO `ae_kabupaten` VALUES (446, 32, 'Kab. Dairi', 'MES');
INSERT INTO `ae_kabupaten` VALUES (447, 32, 'Kab. Samosir', 'MES');
INSERT INTO `ae_kabupaten` VALUES (448, 32, 'Kab. Langkat', 'MES');
INSERT INTO `ae_kabupaten` VALUES (449, 32, 'Kab. Tapanuli Tengah', 'MES');
INSERT INTO `ae_kabupaten` VALUES (450, 32, 'Kab. Padang Lawas', 'MES');
INSERT INTO `ae_kabupaten` VALUES (451, 32, 'Kab. Labuhan Batu', 'MES');
INSERT INTO `ae_kabupaten` VALUES (452, 32, 'Kota Medan', 'MES');
INSERT INTO `ae_kabupaten` VALUES (453, 32, 'Kab. Mandailing Natal', 'MES');
INSERT INTO `ae_kabupaten` VALUES (454, 32, 'Kota Padang Sidempuan', 'MES');
INSERT INTO `ae_kabupaten` VALUES (455, 32, 'Kab. Batubara', 'MES');
INSERT INTO `ae_kabupaten` VALUES (456, 32, 'Kota Binjai', 'MES');
INSERT INTO `ae_kabupaten` VALUES (457, 32, 'Kota Tanjung Balai', 'MES');
INSERT INTO `ae_kabupaten` VALUES (458, 32, 'Kab. Tebing Tinggi', 'MES');
INSERT INTO `ae_kabupaten` VALUES (459, 32, 'Kab. Deli Serdang', 'MES');
INSERT INTO `ae_kabupaten` VALUES (460, 32, 'Kab. Serdang Bedagai', 'MES');
INSERT INTO `ae_kabupaten` VALUES (461, 32, 'Kab. Tapanuli Utara', 'MES');
INSERT INTO `ae_kabupaten` VALUES (462, 32, 'Kota Sibolga', 'MES');
INSERT INTO `ae_kabupaten` VALUES (463, 32, 'Kab. Pakpak Barat', 'MES');
INSERT INTO `ae_kabupaten` VALUES (464, 32, 'Kab. Karo', 'MES');
INSERT INTO `ae_kabupaten` VALUES (465, 32, 'Kab. Asahan', 'MES');
INSERT INTO `ae_kabupaten` VALUES (466, 32, 'Kab. Nias Selatan', 'MES');
INSERT INTO `ae_kabupaten` VALUES (467, 32, 'Kab. Simalungun', 'MES');
INSERT INTO `ae_kabupaten` VALUES (468, 32, 'Kab. Humbang Hasudutan', 'MES');
INSERT INTO `ae_kabupaten` VALUES (469, 32, 'Kab. Nias', 'MES');
INSERT INTO `ae_kabupaten` VALUES (470, 100, 'Kepulauan Riau', 'KR');
INSERT INTO `ae_kabupaten` VALUES (471, 100, 'Papua Barat', 'PB');
INSERT INTO `ae_kabupaten` VALUES (472, 100, 'Kalimantan Utara', 'PB');
INSERT INTO `ae_kabupaten` VALUES (473, 100, 'Sulawesi Barat', 'SR');
COMMIT;

-- ----------------------------
-- Table structure for ae_kecamatan
-- ----------------------------
DROP TABLE IF EXISTS `ae_kecamatan`;
CREATE TABLE `ae_kecamatan` (
  `id_kecamatan` int(50) NOT NULL AUTO_INCREMENT,
  `kabupaten_id` int(50) NOT NULL,
  `kecamatan` varchar(150) NOT NULL,
  `kode` varchar(50) NOT NULL,
  `ongkir_reg` int(10) NOT NULL,
  `ongkir_oke` int(10) NOT NULL,
  `ongkir_yes` int(10) NOT NULL,
  `rpx` int(10) NOT NULL,
  `jnt` int(10) NOT NULL,
  `est_reg` varchar(15) NOT NULL,
  `est_oke` varchar(15) NOT NULL,
  PRIMARY KEY (`id_kecamatan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5646 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ae_kecamatan
-- ----------------------------
BEGIN;
INSERT INTO `ae_kecamatan` VALUES (1, 276, 'Mataram', 'AMI10000', 35000, 30000, 41000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2, 276, 'Ampenan', 'AMI10011', 35000, 30000, 41000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3, 276, 'Cakranegara', 'AMI10012', 35000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4, 280, 'Bima', 'AMI20100', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5, 280, 'Asakota', 'AMI20111', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (6, 280, 'Rasanae Barat', 'AMI20112', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (7, 280, 'Rasanae Timur', 'AMI20113', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (8, 280, 'Raba', 'AMI20114', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (9, 280, 'Mpunda', 'AMI20115', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (10, 275, 'Dompu', 'AMI20200', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (11, 275, 'Hu\'u', 'AMI20201', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (12, 275, 'Kempo', 'AMI20202', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (13, 275, 'Kilo', 'AMI20203', 83000, 0, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (14, 275, 'Pajo', 'AMI20204', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (15, 275, 'Pekat', 'AMI20205', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (16, 275, 'Woja', 'AMI20206', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (17, 275, 'Manggelewa', 'AMI20207', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (18, 279, 'Praya', 'AMI20300', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (19, 279, 'Batukliang', 'AMI20301', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (20, 279, 'Janapria', 'AMI20302', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (21, 279, 'Jonggat', 'AMI20303', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (22, 279, 'Kopang', 'AMI20304', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (23, 279, 'Praya Barat', 'AMI20305', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (24, 279, 'Praya Timur', 'AMI20306', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (25, 279, 'Pringgarata', 'AMI20307', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (26, 279, 'Pujut', 'AMI20308', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (27, 279, 'Batukliang Utara', 'AMI20310', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (28, 279, 'Praya Barat Daya', 'AMI20311', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (29, 279, 'Praya Tengah', 'AMI20312', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (30, 274, 'Selong', 'AMI20400', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (31, 274, 'Aikmel', 'AMI20401', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (32, 274, 'Keruak', 'AMI20402', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (33, 274, 'Mas Bagik', 'AMI20403', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (34, 274, 'Pringgabaya', 'AMI20404', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (35, 274, 'Sakra', 'AMI20405', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (36, 274, 'Sambelia', 'AMI20406', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (37, 274, 'Sikur', 'AMI20407', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (38, 274, 'Sukamulia', 'AMI20408', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (39, 274, 'Terara', 'AMI20409', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (40, 274, 'Jerowaru', 'AMI20411', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (41, 274, 'Montong Gading', 'AMI20413', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (42, 274, 'Pringgasela', 'AMI20414', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (43, 274, 'Sakra Barat', 'AMI20415', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (44, 274, 'Sakra Timur', 'AMI20416', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (45, 274, 'Sembalun', 'AMI20417', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (46, 274, 'Suela', 'AMI20418', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (47, 274, 'Suralaga', 'AMI20419', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (48, 274, 'Wanasaba', 'AMI20420', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (49, 274, 'Labuhan Haji', 'AMI20421', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (50, 278, 'Sumbawa Besar', 'AMI20500', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (51, 278, 'Alas', 'AMI20501', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (52, 278, 'Batu Lanteh', 'AMI20502', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (53, 278, 'Empang', 'AMI20503', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (54, 278, 'Lape-Lopok', 'AMI20505', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (55, 278, 'Lunyuk', 'AMI20506', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (56, 278, 'Moyohilir', 'AMI20507', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (57, 278, 'Moyohulu', 'AMI20508', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (58, 278, 'Plampang', 'AMI20509', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (59, 278, 'Ropang', 'AMI20510', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (60, 278, 'Utan-Rhee', 'AMI20513', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (61, 278, 'Alas Barat', 'AMI20514', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (62, 278, 'Labangka', 'AMI20515', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (63, 278, 'Labuhan Badas', 'AMI20516', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (64, 278, 'Sumbawa', 'AMI20517', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (65, 282, 'Gerung', 'AMI20800', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (66, 282, 'Bayan', 'AMI20801', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (67, 282, 'Gangga', 'AMI20802', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (68, 282, 'Kediri ', 'AMI20803', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (69, 282, 'Labu Api', 'AMI20804', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (70, 282, 'Narmada', 'AMI20805', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (71, 282, 'Sekotong Tengah', 'AMI20806', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (72, 282, 'Tanjung', 'AMI20807', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (73, 282, 'Batu Layar', 'AMI20808', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (74, 282, 'Gunung Sari', 'AMI20809', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (75, 282, 'Kayangan', 'AMI20810', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (76, 282, 'Kuripan ', 'AMI20811', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (77, 282, 'Lembar', 'AMI20812', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (78, 282, 'Lingsar', 'AMI20813', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (79, 282, 'Pemenang', 'AMI20814', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (80, 277, 'Taliwang', 'AMI20900', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (81, 277, 'Jereweh', 'AMI20901', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (82, 277, 'Seteluk', 'AMI20902', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (83, 277, 'Sekongkang', 'AMI20903', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (84, 277, 'Brang Rea', 'AMI20904', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (85, 281, 'Woha', 'AMI21100', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (86, 281, 'Ambalawi', 'AMI21101', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (87, 281, 'Belo', 'AMI21102', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (88, 281, 'Bolo', 'AMI21103', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (89, 281, 'Donggo', 'AMI21104', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (90, 281, 'Lambu', 'AMI21105', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (91, 281, 'Langgudu', 'AMI21106', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (92, 281, 'Mada Pangga', 'AMI21107', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (93, 281, 'Monta', 'AMI21108', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (94, 281, 'Sanggar', 'AMI21109', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (95, 281, 'Sape', 'AMI21110', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (96, 281, 'Tambora', 'AMI21111', 83000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (97, 281, 'Wawo', 'AMI21112', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (98, 281, 'Wera', 'AMI21113', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (99, 238, 'Ambon', 'AMQ10000', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (100, 238, 'Nusaniwe', 'AMQ10005', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (101, 238, 'Sirimau', 'AMQ10006', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (102, 238, 'Teluk Ambon Baguala', 'AMQ10007', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (103, 238, 'Teluk Ambon', 'AMQ10008', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (104, 238, 'Leitimur Selatan', 'AMQ10009', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (105, 242, 'Kota Masohi', 'AMQ20100', 108000, 93000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (106, 242, 'Banda', 'AMQ20101', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (107, 242, 'Leihitu', 'AMQ20107', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (108, 242, 'Pulau Haruku', 'AMQ20108', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (109, 242, 'Sala Hutu', 'AMQ20110', 137000, 117000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (110, 242, 'Seram Utara', 'AMQ20113', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (111, 242, 'Tehoru', 'AMQ20115', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (112, 242, 'Teon Nila Serua', 'AMQ20116', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (113, 242, 'Amahai', 'AMQ20118', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (114, 242, 'Nusa Laut', 'AMQ20119', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (115, 242, 'Saparua', 'AMQ20121', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (116, 237, 'Tual', 'AMQ20200', 108000, 93000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (117, 237, 'Kei Besar', 'AMQ20201', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (118, 237, 'Kei Besar Selatan', 'AMQ20208', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (119, 237, 'Kei Besar Utara Timur', 'AMQ20209', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (120, 237, 'Kei Kecil', 'AMQ20210', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (121, 237, 'Pulau-Pulau Kur', 'AMQ20211', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (122, 241, 'Namlea', 'AMQ20300', 108000, 93000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (123, 241, 'Air Buaya / Buru Utara Barat', 'AMQ20301', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (124, 241, 'Leksula / Buru Selatan', 'AMQ20302', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (125, 241, 'Waesama', 'AMQ20303', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (126, 241, 'Wayapo / Buru Utara Timur', 'AMQ20304', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (127, 236, 'Dobo', 'AMQ20400', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (128, 236, 'Pulau-Pulau Aru', 'AMQ20401', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (129, 236, 'Pulau-Pulau Aru Selatan', 'AMQ20402', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (130, 236, 'Pulau-Pulau Aru Tengah', 'AMQ20403', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (131, 240, 'Piru', 'AMQ20600', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (132, 240, 'Kairatu', 'AMQ20601', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (133, 240, 'Seram Barat', 'AMQ20602', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (134, 240, 'Taniwel', 'AMQ20603', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (135, 240, 'Waisala', 'AMQ20604', 204000, 0, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (136, 235, 'Dataran Hunimoa', 'AMQ20700', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (137, 235, 'Bula', 'AMQ20701', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (138, 235, 'Pulau-Pulau Gorong', 'AMQ20702', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (139, 235, 'Seram Timur', 'AMQ20703', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (140, 235, 'Werinama', 'AMQ20704', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (141, 239, 'Saumlaki', 'AMQ20800', 108000, 93000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (142, 239, 'Damer ', 'AMQ20801', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (143, 239, 'Kormomolin', 'AMQ20802', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (144, 239, 'Luser', 'AMQ20803', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (145, 239, 'Mola', 'AMQ20804', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (146, 239, 'Nirunmas', 'AMQ20805', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (147, 239, 'Pulau-Pulau Babar ', 'AMQ20806', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (148, 239, 'Pulau-Pulau Babar Timur', 'AMQ20807', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (149, 239, 'Pulau-Pulau Letimoa Lakor ', 'AMQ20808', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (150, 239, 'Pulau-Pulau Terselatan', 'AMQ20809', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (151, 239, 'Pulau-Pulau Wetar', 'AMQ20810', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (152, 239, 'Selaru', 'AMQ20811', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (153, 239, 'Tanimbar Selatan', 'AMQ20812', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (154, 239, 'Tanimbar Utara', 'AMQ20813', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (155, 239, 'Wer Makatian', 'AMQ20814', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (156, 239, 'Wertamrian', 'AMQ20815', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (157, 239, 'Wuarlabobar', 'AMQ20816', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (158, 239, 'Yaru', 'AMQ20817', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (159, 239, 'Serwaru', 'AMQ20818', 204000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (160, 192, 'Banjarmasin', 'BDJ10000', 41000, 36000, 51000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (161, 192, 'Banjarmasin Barat', 'BDJ10001', 41000, 36000, 51000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (162, 192, 'Banjarmasin Selatan ', 'BDJ10002', 41000, 36000, 51000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (163, 192, 'Banjarmasin Tengah ', 'BDJ10003', 41000, 36000, 51000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (164, 192, 'Banjarmasin Timur', 'BDJ10004', 41000, 36000, 51000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (165, 192, 'Banjarmasin Utara', 'BDJ10005', 41000, 36000, 51000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (166, 183, 'Barabai', 'BDJ10100', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (167, 183, 'Batang Alai Selatan', 'BDJ10101', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (168, 183, 'Batu Benawa', 'BDJ10102', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (169, 183, 'Batang Alai Utara', 'BDJ10103', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (170, 183, 'Haruyan', 'BDJ10104', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (171, 183, 'Labuan Amas Selatan', 'BDJ10105', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (172, 183, 'Labuan Amas Utara', 'BDJ10106', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (173, 183, 'Pandawan', 'BDJ10107', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (174, 183, 'Batang Alai Tengah', 'BDJ10108', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (175, 183, 'Hantakan ', 'BDJ10109', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (176, 190, 'Kandangan ', 'BDJ10200', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (177, 190, 'Angkinang', 'BDJ10201', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (178, 190, 'Daha Selatan', 'BDJ10202', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (179, 190, 'Daha Utara', 'BDJ10203', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (180, 190, 'Kalumpang', 'BDJ10204', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (181, 190, 'Loksado', 'BDJ10205', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (182, 190, 'Padang Batung', 'BDJ10206', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (183, 190, 'Simpur', 'BDJ10207', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (184, 190, 'Sungai Raya ', 'BDJ10208', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (185, 190, 'Telaga Langsat', 'BDJ10209', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (186, 190, 'Daha Barat ', 'BDJ10210', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (187, 181, 'Kota Baru', 'BDJ10300', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (188, 181, 'Hampang', 'BDJ10302', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (189, 181, 'Kelumpang Utara', 'BDJ10303', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (190, 181, 'Kelumpang Tengah', 'BDJ10304', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (191, 181, 'Kelumpang Hulu', 'BDJ10305', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (192, 181, 'Kelumpang Selatan', 'BDJ10306', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (193, 181, 'Pamukan Utara', 'BDJ10309', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (194, 181, 'Pamukan Selatan', 'BDJ10310', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (195, 181, 'Pulau Laut Selatan ', 'BDJ10311', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (196, 181, 'Pulau Laut Barat ', 'BDJ10312', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (197, 181, 'Pulau Laut Timur ', 'BDJ10313', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (198, 181, 'Pulau Sembilan ', 'BDJ10314', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (199, 181, 'Pulau Sebuku', 'BDJ10315', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (200, 181, 'Sampanahan', 'BDJ10316', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (201, 181, 'Sungai Durian', 'BDJ10318', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (202, 181, 'Pulau Laut Utara', 'BDJ10319', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (203, 188, 'Tanjung', 'BDJ10400', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (204, 188, 'Banua Lawas', 'BDJ10401', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (205, 188, 'Haruai', 'BDJ10402', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (206, 188, 'Jaro', 'BDJ10403', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (207, 188, 'Kelua', 'BDJ10404', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (208, 188, 'Muara Harus', 'BDJ10405', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (209, 188, 'Muara Uya', 'BDJ10406', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (210, 188, 'Murung Pudak', 'BDJ10407', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (211, 188, 'Pugaan', 'BDJ10408', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (212, 188, 'Tanta', 'BDJ10409', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (213, 188, 'Upau', 'BDJ10410', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (214, 188, 'Bintang Ara ', 'BDJ10412', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (215, 179, 'BanjarBaru', 'BDJ10500', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (216, 179, 'Cempaka ', 'BDJ10501', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (217, 179, 'Landasan Ulin', 'BDJ10502', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (218, 186, 'Amuntai', 'BDJ20100', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (219, 186, 'Amuntai Utara', 'BDJ20101', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (220, 186, 'Amuntai Selatan', 'BDJ20102', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (221, 186, 'Babirik', 'BDJ20104', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (222, 186, 'Danau Panggang', 'BDJ20106', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (223, 186, 'Sungai Pandan', 'BDJ20111', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (224, 186, 'Amuntai Tengah ', 'BDJ20112', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (225, 186, 'Banjang ', 'BDJ20113', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (226, 177, 'Marabahan', 'BDJ20200', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (227, 177, 'Alalak', 'BDJ20201', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (228, 177, 'Anjir Pasar', 'BDJ20202', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (229, 177, 'Anjir Muara', 'BDJ20203', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (230, 177, 'Barambai', 'BDJ20204', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (231, 177, 'Belawang', 'BDJ20205', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (232, 177, 'Cerbon', 'BDJ20206', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (233, 177, 'Kuripan ', 'BDJ20207', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (234, 177, 'Mandastana', 'BDJ20208', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (235, 177, 'Mekar Sari', 'BDJ20209', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (236, 177, 'Rantau Badauh', 'BDJ20210', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (237, 177, 'Tabunganen', 'BDJ20211', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (238, 177, 'Tabukan', 'BDJ20212', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (239, 177, 'Tamban', 'BDJ20213', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (240, 177, 'Bakumpai ', 'BDJ20214', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (241, 177, 'Wanaraya', 'BDJ20215', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (242, 185, 'Martapura ', 'BDJ20300', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (243, 185, 'Aluh Aluh', 'BDJ20301', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (244, 185, 'Astambul', 'BDJ20302', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (245, 185, 'Gambut', 'BDJ20303', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (246, 185, 'Karang Intan', 'BDJ20304', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (247, 185, 'Kertak Hanyar', 'BDJ20305', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (248, 185, 'Pengaron', 'BDJ20306', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (249, 185, 'Simpang Empat', 'BDJ20307', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (250, 185, 'Sungai Tabuk', 'BDJ20308', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (251, 185, 'Sungai Pinang', 'BDJ20309', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (252, 185, 'Aranio ', 'BDJ20310', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (253, 185, 'Mataraman ', 'BDJ20311', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (254, 176, 'Pelaihari', 'BDJ20400', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (255, 176, 'Bati-Bati', 'BDJ20401', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (256, 176, 'Jorong', 'BDJ20402', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (257, 176, 'Kintap', 'BDJ20403', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (258, 176, 'Kurau', 'BDJ20404', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (259, 176, 'Panyipatan', 'BDJ20405', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (260, 176, 'Takisung', 'BDJ20406', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (261, 176, 'Batu Ampar ', 'BDJ20407', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (262, 176, 'Tambang Ulang', 'BDJ20408', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (263, 184, 'Rantau', 'BDJ20500', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (264, 184, 'Bakarangan', 'BDJ20501', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (265, 184, 'Binuang', 'BDJ20502', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (266, 184, 'Bungur', 'BDJ20503', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (267, 184, 'Candi Laras Utara', 'BDJ20504', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (268, 184, 'Candi Laras Selatan', 'BDJ20505', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (269, 184, 'Hatungun', 'BDJ20506', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (270, 184, 'Lokpaikat', 'BDJ20507', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (271, 184, 'Piani', 'BDJ20508', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (272, 184, 'Salam Babaris', 'BDJ20509', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (273, 184, 'Tapin Tengah', 'BDJ20510', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (274, 184, 'Tapin Selatan', 'BDJ20511', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (275, 184, 'Tapin Utara', 'BDJ20512', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (276, 191, 'Muara Teweh', 'BDJ20700', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (277, 191, 'Gunung Purei', 'BDJ20701', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (278, 191, 'Gunung Timang', 'BDJ20702', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (279, 191, 'Lahei', 'BDJ20703', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (280, 191, 'Montalat', 'BDJ20704', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (281, 191, 'Teweh Timur', 'BDJ20705', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (282, 191, 'Teweh Tengah', 'BDJ20706', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (283, 182, 'Tamiang Layang', 'BDJ20800', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (284, 182, 'Awang', 'BDJ20801', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (285, 182, 'Benua Lima', 'BDJ20802', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (286, 182, 'Dusun Tengah', 'BDJ20803', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (287, 182, 'Patangkep Tutui', 'BDJ20804', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (288, 182, 'Pematang Karau', 'BDJ20805', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (289, 182, 'Dusun Timur', 'BDJ20806', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (290, 189, 'Puruk Cahu', 'BDJ20900', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (291, 189, 'Laung Tuhup', 'BDJ20901', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (292, 189, 'Permata Intan', 'BDJ20902', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (293, 189, 'Sumber Barito', 'BDJ20903', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (294, 189, 'Tanah Siang', 'BDJ20904', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (295, 189, 'Murung', 'BDJ20905', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (296, 180, 'Buntok', 'BDJ21100', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (297, 180, 'Dusun Hilir', 'BDJ21102', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (298, 180, 'Dusun Utara', 'BDJ21103', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (299, 180, 'Jenamas', 'BDJ21104', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (300, 180, 'Karau Kuala', 'BDJ21105', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (301, 180, 'Dusun Selatan ', 'BDJ21107', 66000, 57000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (302, 180, 'Gunung Bintang Awai ', 'BDJ21108', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (303, 187, 'Paringin', 'BDJ21200', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (304, 187, 'Awayan', 'BDJ21201', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (305, 187, 'Batu Mandi', 'BDJ21202', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (306, 187, 'Halong', 'BDJ21203', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (307, 187, 'Juai', 'BDJ21204', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (308, 187, 'Lampihong', 'BDJ21205', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (309, 178, 'Batu Licin', 'BDJ21300', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (310, 178, 'Kusan Hilir ', 'BDJ21301', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (311, 178, 'Kusan Hulu', 'BDJ21302', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (312, 178, 'Satui', 'BDJ21303', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (313, 178, 'Sungai Loban', 'BDJ21304', 66000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (314, 80, 'Bandung', 'BDO10000', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (315, 80, 'Cibiru', 'BDO10002', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (316, 80, 'Ujungberung', 'BDO10018', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (317, 80, 'Sukasari', 'BDO10037', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (318, 80, 'Cicadas', 'BDO10038', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (319, 80, 'Cidadap', 'BDO10039', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (320, 80, 'Lengkong ', 'BDO10040', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (321, 80, 'Andir', 'BDO10041', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (322, 80, 'Arcamanik', 'BDO10042', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (323, 80, 'Astanaanyar', 'BDO10043', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (324, 80, 'Babakanciparay', 'BDO10044', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (325, 80, 'Bandung Kidul', 'BDO10045', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (326, 80, 'Bandung Kulon', 'BDO10046', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (327, 80, 'Bandung Wetan', 'BDO10047', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (328, 80, 'Batununggal', 'BDO10048', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (329, 80, 'Bojongloa Kaler', 'BDO10049', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (330, 80, 'Bojongloa Kidul', 'BDO10050', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (331, 80, 'Cibeunying Kaler', 'BDO10051', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (332, 80, 'Cibeunying Kidul', 'BDO10052', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (333, 80, 'Cicendo', 'BDO10053', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (334, 80, 'Coblong', 'BDO10054', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (335, 80, 'Kiaracondong', 'BDO10055', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (336, 80, 'Margacinta', 'BDO10056', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (337, 80, 'Rancasari', 'BDO10057', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (338, 80, 'Regol', 'BDO10058', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (339, 80, 'Sukajadi', 'BDO10059', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (340, 80, 'Sumurbandung', 'BDO10060', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (341, 65, 'Soreang', 'BDO10100', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (342, 65, 'Arjasari', 'BDO10101', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (343, 65, 'Baleendah', 'BDO10102', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (344, 65, 'Banjaran ', 'BDO10103', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (345, 65, 'Bojong Soang', 'BDO10104', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (346, 65, 'Cangkuang', 'BDO10105', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (347, 65, 'Cicalengka', 'BDO10106', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (348, 65, 'Cikancung', 'BDO10107', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (349, 65, 'Cilengkrang', 'BDO10108', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (350, 65, 'Cileunyi', 'BDO10109', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (351, 65, 'Cimaung', 'BDO10110', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (352, 65, 'Cimenyan', 'BDO10111', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (353, 65, 'Ciparay', 'BDO10112', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (354, 65, 'Ciwidey', 'BDO10113', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (355, 65, 'Dayeuhkolot', 'BDO10114', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (356, 65, 'Ibun', 'BDO10115', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (357, 65, 'Kertasari', 'BDO10116', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (358, 65, 'Katapang', 'BDO10118', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (359, 65, 'Majalaya', 'BDO10119', 18000, 16000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (360, 65, 'Margaasih', 'BDO10120', 22000, 19000, 27000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (361, 65, 'Margahayu', 'BDO10121', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (362, 65, 'Nagreg', 'BDO10122', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (363, 65, 'Pacet ', 'BDO10123', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (364, 65, 'Pameungpeuk ', 'BDO10124', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (365, 65, 'Pangalengan', 'BDO10125', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (366, 65, 'Paseh', 'BDO10126', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (367, 65, 'Pasirjambu', 'BDO10127', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (368, 65, 'Rancabali', 'BDO10128', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (369, 65, 'Rancaekek', 'BDO10129', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (370, 65, 'Solokan Jeruk', 'BDO10130', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (371, 77, 'Cimahi ', 'BDO20100', 16000, 14000, 21000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (372, 77, 'Cimahi Selatan', 'BDO20117', 16000, 14000, 21000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (373, 77, 'Cimahi Tengah', 'BDO20118', 16000, 14000, 21000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (374, 77, 'Cimahi Utara', 'BDO20119', 16000, 14000, 21000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (375, 61, 'Sumedang', 'BDO20200', 18000, 16000, 21000, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (376, 61, 'Buahdua', 'BDO20201', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (377, 61, 'Cibugel', 'BDO20203', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (378, 61, 'Cimalaka', 'BDO20205', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (379, 61, 'Cimanggu', 'BDO20206', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (380, 61, 'Conggeang', 'BDO20208', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (381, 61, 'Darmaraja', 'BDO20209', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (382, 61, 'Ganeas', 'BDO20210', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (383, 61, 'Pamulihan ', 'BDO20214', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (384, 61, 'Paseh', 'BDO20215', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (385, 61, 'Rancakalong', 'BDO20216', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (386, 61, 'Situraja', 'BDO20217', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (387, 61, 'Sukasari ', 'BDO20218', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (388, 61, 'Surian', 'BDO20219', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (389, 61, 'Tanjungkerta', 'BDO20220', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (390, 61, 'Tanjungsari', 'BDO20221', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (391, 61, 'Tomo', 'BDO20222', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (392, 61, 'Ujung Jaya', 'BDO20223', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (393, 61, 'Wado', 'BDO20224', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (394, 61, 'Jatinangor', 'BDO20247', 16000, 14000, 21000, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (395, 61, 'Cisarua', 'BDO20248', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (396, 61, 'Cisitu', 'BDO20249', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (397, 61, 'Jatigede', 'BDO20250', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (398, 61, 'Jatinunggal', 'BDO20251', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (399, 61, 'Sumedang Selatan', 'BDO20252', 18000, 16000, 21000, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (400, 61, 'Sumedang Utara', 'BDO20253', 18000, 16000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (401, 61, 'Tanjungmedar', 'BDO20254', 22000, 19000, 0, 12800, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (402, 73, 'Tasikmalaya', 'BDO20300', 18000, 16000, 21000, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (403, 73, 'Cibeureum ', 'BDO20306', 18000, 16000, 21000, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (404, 73, 'Indihiang', 'BDO20317', 18000, 16000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (405, 73, 'Kawalu', 'BDO20321', 18000, 16000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (406, 73, 'Cihideung', 'BDO20333', 18000, 16000, 21000, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (407, 73, 'Cipedes', 'BDO20334', 18000, 16000, 21000, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (408, 73, 'Mangkubumi', 'BDO20335', 18000, 16000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (409, 73, 'Tamansari ', 'BDO20336', 18000, 16000, 21000, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (410, 73, 'Tawang', 'BDO20337', 18000, 16000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (411, 86, 'Banjar', 'BDO20400', 18000, 16000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (412, 86, 'Langensari', 'BDO20416', 18000, 16000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (413, 86, 'Pataruman', 'BDO20417', 18000, 16000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (414, 86, 'Purwaharja', 'BDO20418', 18000, 16000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (415, 71, 'Ciamis', 'BDO20500', 18000, 16000, 21000, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (416, 71, 'Cihaurbeuti', 'BDO20501', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (417, 71, 'Cijeungjing', 'BDO20502', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (418, 71, 'Cikoneng', 'BDO20503', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (419, 71, 'Cipaku', 'BDO20504', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (420, 71, 'Jatinagara', 'BDO20505', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (421, 71, 'Kawali', 'BDO20506', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (422, 71, 'Panumbangan', 'BDO20507', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (423, 71, 'Panawangan', 'BDO20508', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (424, 71, 'Panjalu', 'BDO20509', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (425, 71, 'Rajadesa', 'BDO20510', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (426, 71, 'Sadananya', 'BDO20511', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (427, 71, 'Sukadana ', 'BDO20512', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (428, 71, 'Banjarsari ', 'BDO20513', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (429, 71, 'Cimaragas', 'BDO20514', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (430, 71, 'Cigugur ', 'BDO20515', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (431, 71, 'Cijulang', 'BDO20516', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (432, 71, 'Cimerak', 'BDO20517', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (433, 71, 'Cisaga', 'BDO20518', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (434, 71, 'Kalipucang', 'BDO20519', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (435, 71, 'Lakbok', 'BDO20520', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (436, 71, 'Langkap lancar', 'BDO20521', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (437, 71, 'Padaherang', 'BDO20522', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (438, 71, 'Pamarican', 'BDO20523', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (439, 71, 'Pangandaran', 'BDO20524', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (440, 71, 'Parigi ', 'BDO20525', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (441, 71, 'Rancah', 'BDO20526', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (442, 71, 'Tambaksari', 'BDO20527', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (443, 71, 'Purwadadi ', 'BDO20528', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (444, 71, 'Cidolog ', 'BDO20529', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (445, 71, 'Baregbeg', 'BDO20530', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (446, 71, 'Lumbung', 'BDO20531', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (447, 71, 'Mangunjaya', 'BDO20532', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (448, 71, 'Sidamulih', 'BDO20533', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (449, 71, 'Sindangkasih', 'BDO20534', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (450, 84, 'Singaparna', 'BDO20600', 18000, 16000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (451, 84, 'Bojonggambir', 'BDO20601', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (452, 84, 'Cigalontang', 'BDO20602', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (453, 84, 'Leuwisari', 'BDO20603', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (454, 84, 'Sariwangi', 'BDO20605', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (455, 84, 'Salawu', 'BDO20606', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (456, 84, 'Sukarame', 'BDO20608', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (457, 84, 'Mangunreja', 'BDO20609', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (458, 84, 'Sodonghilir', 'BDO20610', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (459, 84, 'Taraju', 'BDO20611', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (460, 84, 'Bantarkalong', 'BDO20612', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (461, 84, 'Culamega', 'BDO20613', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (462, 84, 'Ciawi ', 'BDO20614', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (463, 84, 'Cibalong ', 'BDO20615', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (464, 84, 'Cikatomas', 'BDO20616', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (465, 84, 'Cikalong', 'BDO20617', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (466, 84, 'Cineam', 'BDO20618', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (467, 84, 'Cipatujah', 'BDO20619', 22000, 19000, 0, 0, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (468, 84, 'Cisayong', 'BDO20620', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (469, 84, 'Jamanis', 'BDO20621', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (470, 84, 'Karangnunggal', 'BDO20622', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (471, 84, 'Manonjaya', 'BDO20623', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (472, 84, 'Pancatengah', 'BDO20624', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (473, 84, 'Rajapolah', 'BDO20625', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (474, 84, 'Salopa', 'BDO20626', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (475, 84, 'Sukaraja ', 'BDO20627', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (476, 84, 'Kadipaten', 'BDO20629', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (477, 84, 'Bojongasih', 'BDO20630', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (478, 84, 'Gunung Tanjung ', 'BDO20631', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (479, 84, 'Jatiwaras', 'BDO20632', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (480, 84, 'Karangjaya', 'BDO20633', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (481, 84, 'Padakembang ', 'BDO20634', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (482, 84, 'Pagerageung', 'BDO20635', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (483, 84, 'Parungponteng', 'BDO20636', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (484, 84, 'Puspahiang', 'BDO20637', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (485, 84, 'Sukaratu', 'BDO20638', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (486, 84, 'Sukaresik', 'BDO20639', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (487, 84, 'Tanjungjaya ', 'BDO20640', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (488, 84, 'Sukahening ', 'BDO20642', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (489, 69, 'Garut, Kab. Garut', 'BDO20700', 18000, 16000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (490, 69, 'Banjarwangi', 'BDO20701', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (491, 69, 'Banyuresmi', 'BDO20702', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (492, 69, 'Bayongbong', 'BDO20703', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (493, 69, 'Balubur Limbangan ', 'BDO20704', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (494, 69, 'Bungbulang', 'BDO20705', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (495, 69, 'Cibatu ', 'BDO20706', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (496, 69, 'Cibalong ', 'BDO20707', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (497, 69, 'Cibiuk', 'BDO20708', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (498, 69, 'Cikelet', 'BDO20709', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (499, 69, 'Cikajang', 'BDO20710', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (500, 69, 'Cilawu', 'BDO20711', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (501, 69, 'Cisompet', 'BDO20712', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (502, 69, 'Cisurupan', 'BDO20713', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (503, 69, 'Cisewu', 'BDO20714', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (504, 69, 'Kadungora', 'BDO20715', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (505, 69, 'Karangpawitan ', 'BDO20716', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (506, 69, 'Leles ', 'BDO20717', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (507, 69, 'Leuwigoong', 'BDO20718', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (508, 69, 'Malangbong', 'BDO20719', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (509, 69, 'Pakenjeng', 'BDO20720', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (510, 69, 'Pameungpeuk ', 'BDO20721', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (511, 69, 'Pamulihan ', 'BDO20722', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (512, 69, 'Peundeuy', 'BDO20723', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (513, 69, 'Samarang', 'BDO20724', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (514, 69, 'Selaawi', 'BDO20725', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (515, 69, 'Singajaya', 'BDO20726', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (516, 69, 'Sukawening', 'BDO20727', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (517, 69, 'Talegong', 'BDO20728', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (518, 69, 'Wanaraja', 'BDO20730', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (519, 69, 'Caringin', 'BDO20734', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (520, 69, 'Karangtengah ', 'BDO20735', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (521, 69, 'Sukaresmi ', 'BDO20736', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (522, 69, 'Cigedug', 'BDO20737', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (523, 69, 'Cihurip', 'BDO20738', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (524, 69, 'Kersamanah', 'BDO20739', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (525, 69, 'Mekarmukti', 'BDO20740', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (526, 69, 'Pangatikan', 'BDO20741', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (527, 69, 'Pasirwangi', 'BDO20742', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (528, 69, 'Sucinaraja', 'BDO20743', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (529, 69, 'Tarogong Kaler ', 'BDO20744', 22000, 19000, 27000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (530, 69, 'Tarogong Kidul ', 'BDO20745', 22000, 19000, 27000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (531, 69, 'Garut Kota', 'BDO20746', 18000, 16000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (532, 81, 'Purwakarta ', 'BDO20800', 18000, 16000, 21000, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (533, 81, 'Jatiluhur ', 'BDO20801', 22000, 19000, 27000, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (534, 81, 'Campaka', 'BDO20802', 22000, 19000, 27000, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (535, 81, 'Pasawahan ', 'BDO20803', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (536, 81, 'Babakancikao ', 'BDO20804', 22000, 19000, 27000, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (537, 81, 'Bungursari', 'BDO20805', 22000, 19000, 27000, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (538, 81, 'Cibatu ', 'BDO20806', 22000, 19000, 27000, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (539, 81, 'Plered ', 'BDO20807', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (540, 81, 'Sukatani ', 'BDO20808', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (541, 81, 'Darangdan', 'BDO20809', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (542, 81, 'Maniis', 'BDO20810', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (543, 81, 'Tegalwaru ', 'BDO20811', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (544, 81, 'Wanayasa', 'BDO20812', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (545, 81, 'Sukasari ', 'BDO20813', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (546, 81, 'Pondoksalam', 'BDO20814', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (547, 81, 'Bojong ', 'BDO20820', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (548, 81, 'Kiarapedes', 'BDO20821', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (549, 67, 'Ngamprah', 'BDO21000', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (550, 67, 'Lembang', 'BDO21001', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (551, 67, 'Batujajar', 'BDO21002', 22000, 19000, 27000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (552, 67, 'Cikalong Wetan ', 'BDO21003', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (553, 67, 'Cililin', 'BDO21004', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (554, 67, 'Cipeundeuy', 'BDO21005', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (555, 67, 'Cipatat', 'BDO21006', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (556, 67, 'Cipongkor', 'BDO21007', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (557, 67, 'Cisarua ', 'BDO21008', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (558, 67, 'Gununghalu', 'BDO21009', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (559, 67, 'Padalarang', 'BDO21010', 22000, 19000, 27000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (560, 67, 'Sindangkerta', 'BDO21011', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (561, 67, 'Cihampelas ', 'BDO21012', 22000, 19000, 27000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (562, 67, 'Parongpong', 'BDO21013', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (563, 67, 'Rongga ', 'BDO21014', 22000, 19000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (564, 67, 'Cimareme', 'BDO21015', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (565, 79, 'Subang', 'BDO21100', 18000, 16000, 21000, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (566, 79, 'Cipeundeuy', 'BDO21101', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (567, 79, 'Binong', 'BDO21102', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (568, 79, 'Blanakan', 'BDO21103', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (569, 79, 'Ciasem', 'BDO21104', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (570, 79, 'Cibogo', 'BDO21105', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (571, 79, 'Cijambe', 'BDO21106', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (572, 79, 'Cisalak', 'BDO21107', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (573, 79, 'Compreng ', 'BDO21108', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (574, 79, 'Jalan Cagak ', 'BDO21109', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (575, 79, 'Kalijati', 'BDO21110', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (576, 79, 'Pabuaran ', 'BDO21111', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (577, 79, 'Pamanukan ', 'BDO21113', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (578, 79, 'Patok Beusi', 'BDO21114', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (579, 79, 'Purwadadi ', 'BDO21115', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (580, 79, 'Pusakanagara', 'BDO21116', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (581, 79, 'Sagalaherang', 'BDO21117', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (582, 79, 'Tanjung Siang', 'BDO21118', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (583, 79, 'Cikaum', 'BDO21119', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (584, 79, 'Cipunagara', 'BDO21120', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (585, 79, 'Legon Kulon', 'BDO21121', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (586, 79, 'Pagaden', 'BDO21122', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (587, 64, 'Bekasi', 'BKI10000', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (588, 64, 'Bantar Gebang', 'BKI10002', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (589, 64, 'Bekasi Barat ', 'BKI10039', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (590, 64, 'Bekasi Selatan ', 'BKI10040', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (591, 64, 'Bekasi Timur', 'BKI10041', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (592, 64, 'Bekasi Utara', 'BKI10042', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (593, 64, 'Jatiasih', 'BKI10043', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (594, 64, 'Jatisampurna ', 'BKI10044', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (595, 64, 'Medan Satria', 'BKI10045', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (596, 64, 'Mustika Jaya ', 'BKI10046', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (597, 64, 'Pondokgede ', 'BKI10047', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (598, 64, 'Pondok Melati ', 'BKI10048', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (599, 64, 'Rawalumbu', 'BKI10049', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (600, 76, 'Cikarang', 'BKI10100', 15000, 14000, 20000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (601, 76, 'Cikarang Barat', 'BKI10101', 15000, 14000, 20000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (602, 76, 'Cikarang Pusat ', 'BKI10102', 15000, 14000, 20000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (603, 76, 'Cikarang Selatan ', 'BKI10103', 15000, 14000, 20000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (604, 76, 'Cikarang Timur', 'BKI10104', 15000, 14000, 20000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (605, 76, 'Cikarang Utara', 'BKI10105', 15000, 14000, 20000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (606, 76, 'Cabangbungin ', 'BKI10106', 15000, 14000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (607, 76, 'Cibarusah', 'BKI10107', 15000, 14000, 20000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (608, 76, 'Cibitung ', 'BKI10108', 15000, 14000, 20000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (609, 76, 'Kedungwaringin ', 'BKI10110', 15000, 14000, 20000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (610, 76, 'Muara Gembong', 'BKI10111', 15000, 14000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (611, 76, 'Pebayuran', 'BKI10112', 15000, 14000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (612, 76, 'Serang Baru', 'BKI10113', 15000, 14000, 20000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (613, 76, 'Setu', 'BKI10114', 15000, 14000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (614, 76, 'Sukatani ', 'BKI10115', 15000, 14000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (615, 76, 'Tambelang', 'BKI10116', 15000, 14000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (616, 76, 'Karangbahagia', 'BKI10117', 15000, 14000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (617, 76, 'Sukakarya', 'BKI10118', 15000, 14000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (618, 76, 'Sukawangi', 'BKI10119', 15000, 14000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (619, 76, 'Tambun Selatan ', 'BKI10120', 15000, 14000, 20000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (620, 76, 'Tambun Utara', 'BKI10121', 15000, 14000, 20000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (621, 76, 'Tarumajaya', 'BKI10122', 15000, 14000, 20000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (622, 76, 'Babelan', 'BKI10123', 15000, 14000, 20000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (623, 76, 'Bojongmanggu', 'BKI10124', 15000, 14000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (624, 24, 'Bengkulu', 'BKS10000', 40000, 35000, 49000, 44800, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (625, 24, 'Gading Cempaka', 'BKS10020', 40000, 35000, 49000, 44800, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (626, 24, 'Kampung Melayu', 'BKS10021', 40000, 35000, 49000, 44800, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (627, 24, 'Muara Bangkahulu', 'BKS10022', 40000, 35000, 49000, 44800, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (628, 24, 'Ratu Agung', 'BKS10023', 40000, 35000, 49000, 44800, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (629, 24, 'Ratu Samban', 'BKS10024', 40000, 35000, 49000, 44800, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (630, 24, 'Selebar', 'BKS10025', 40000, 35000, 49000, 44800, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (631, 24, 'Sungai Serut', 'BKS10026', 40000, 35000, 49000, 44800, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (632, 24, 'Teluk Segara', 'BKS10027', 40000, 35000, 49000, 44800, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (633, 28, 'Arga Makmur', 'BKS20100', 47000, 40000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (634, 28, 'Batik Nau', 'BKS20101', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (635, 28, 'Giri Mulia', 'BKS20102', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (636, 28, 'Lais', 'BKS20103', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (637, 28, 'Padang Jaya', 'BKS20104', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (638, 28, 'Air Besi', 'BKS20105', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (639, 28, 'Air Napal', 'BKS20106', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (640, 28, 'Enggano', 'BKS20107', 88000, 0, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (641, 28, 'Karang Tinggi', 'BKS20108', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (642, 28, 'Kerkap', 'BKS20109', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (643, 28, 'Ketahun', 'BKS20110', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (644, 28, 'Pagar Jati', 'BKS20111', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (645, 28, 'Pematang Tiga', 'BKS20112', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (646, 28, 'Pondok Kelapa', 'BKS20113', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (647, 28, 'Taba Penanjung', 'BKS20114', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (648, 28, 'Napal Putih', 'BKS20115', 88000, 0, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (649, 28, 'Putri Hijau', 'BKS20116', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (650, 28, 'Talang Empat', 'BKS20118', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (651, 32, 'Curup', 'BKS20200', 47000, 40000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (652, 32, 'Kota Padang', 'BKS20205', 59000, 51000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (653, 32, 'Padang Ulak Tanding', 'BKS20208', 59000, 51000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (654, 32, 'Bermani Ulu', 'BKS20220', 59000, 51000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (655, 32, 'Selupu Rejang', 'BKS20221', 59000, 51000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (656, 32, 'Sindang Kelingi', 'BKS20222', 59000, 51000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (657, 27, 'Kota Manna', 'BKS20300', 47000, 40000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (658, 27, 'Kedurang', 'BKS20308', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (659, 27, 'Pino', 'BKS20313', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (660, 27, 'Manna', 'BKS20333', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (661, 27, 'Pinoraya', 'BKS20334', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (662, 27, 'Seginim', 'BKS20335', 59000, 51000, 0, 63600, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (663, 31, 'Kaur', 'BKS21100', 47000, 40000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (664, 31, 'Kaur Selatan', 'BKS21101', 47000, 40000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (665, 31, 'Kaur Tengah', 'BKS21102', 47000, 40000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (666, 31, 'Kaur Utara', 'BKS21103', 47000, 40000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (667, 31, 'Kinal', 'BKS21104', 59000, 51000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (668, 31, 'Nasal', 'BKS21105', 59000, 51000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (669, 31, 'Tanjung Kemuning', 'BKS21106', 59000, 51000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (670, 31, 'Maje', 'BKS21107', 59000, 51000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (671, 26, 'Kepahiang', 'BKS21200', 47000, 40000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (672, 26, 'Bermani Ilir', 'BKS21201', 59000, 51000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (673, 26, 'Tebat Karai', 'BKS21202', 59000, 51000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (674, 26, 'Ujan Mas', 'BKS21203', 59000, 51000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (675, 30, 'Lebong', 'BKS21300', 47000, 40000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (676, 30, 'Lebong Atas', 'BKS21301', 47000, 40000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (677, 30, 'Lebong Selatan', 'BKS21302', 47000, 40000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (678, 30, 'Lebong Tengah', 'BKS21303', 47000, 40000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (679, 30, 'Lebong Utara', 'BKS21304', 47000, 40000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (680, 30, 'Rimbo Pengadang', 'BKS21305', 59000, 51000, 0, 79600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (681, 25, 'Muko-muko', 'BKS21400', 47000, 40000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (682, 25, 'Lubuk Pinang', 'BKS21401', 59000, 51000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (683, 25, 'Muko-Muko Selatan', 'BKS21402', 47000, 40000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (684, 25, 'Muko-Muko Utara', 'BKS21403', 47000, 40000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (685, 25, 'Pondok Suguh', 'BKS21404', 59000, 51000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (686, 25, 'Teras Terunjam', 'BKS21405', 59000, 51000, 0, 63600, 42000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (687, 29, 'Tais', 'BKS23000', 47000, 40000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (688, 29, 'Air Periukan', 'BKS23001', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (689, 29, 'Ilir Talo', 'BKS23002', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (690, 29, 'Lubuk Sandi', 'BKS23003', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (691, 29, 'Seluma', 'BKS23004', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (692, 29, 'Seluma Barat', 'BKS23005', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (693, 29, 'Seluma Selatan', 'BKS23006', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (694, 29, 'Seluma Timur', 'BKS23007', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (695, 29, 'Seluma Utara', 'BKS23008', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (696, 29, 'Semidang Alas', 'BKS23009', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (697, 29, 'Semidang Alas Maras', 'BKS23010', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (698, 29, 'Sukaraja', 'BKS23011', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (699, 29, 'Talo', 'BKS23012', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (700, 29, 'Ulu Talo', 'BKS23013', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (701, 29, 'Talo Kecil', 'BKS23014', 59000, 51000, 0, 63600, 53000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (702, 63, 'Bogor', 'BOO10000', 15000, 14000, 20000, 10800, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (703, 63, 'Bogor Barat ', 'BOO10026', 15000, 14000, 20000, 10800, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (704, 63, 'Bogor Selatan ', 'BOO10027', 15000, 14000, 20000, 10800, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (705, 63, 'Bogor Tengah', 'BOO10028', 15000, 14000, 20000, 10800, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (706, 63, 'Bogor Timur ', 'BOO10029', 15000, 14000, 20000, 10800, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (707, 63, 'Bogor Utara', 'BOO10030', 15000, 14000, 20000, 10800, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (708, 63, 'Tanah Sereal', 'BOO10031', 15000, 14000, 20000, 10800, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (709, 75, 'Cibinong', 'BOO20100', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (710, 75, 'Caringin', 'BOO20120', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (711, 75, 'Cariu', 'BOO20121', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (712, 75, 'Ciawi ', 'BOO20122', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (713, 75, 'Ciampea', 'BOO20123', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (714, 75, 'Cibungbulang ', 'BOO20124', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (715, 75, 'Cigudeg', 'BOO20125', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (716, 75, 'Cijeruk', 'BOO20126', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (717, 75, 'Cileungsi', 'BOO20127', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (718, 75, 'Ciomas', 'BOO20128', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (719, 75, 'Cisarua ', 'BOO20129', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (720, 75, 'Citeureup', 'BOO20130', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (721, 75, 'Dramaga', 'BOO20131', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (722, 75, 'Gunung Sindur ', 'BOO20132', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (723, 75, 'Jasinga', 'BOO20133', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (724, 75, 'Jonggol', 'BOO20134', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (725, 75, 'Leuwiliang', 'BOO20135', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (726, 75, 'Megamendung', 'BOO20136', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (727, 75, 'Nanggung', 'BOO20137', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (728, 75, 'Parung', 'BOO20138', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (729, 75, 'Parung Panjang ', 'BOO20139', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (730, 75, 'Rumpin', 'BOO20140', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (731, 75, 'Tenjo', 'BOO20141', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (732, 75, 'Babakan Madang ', 'BOO20142', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (733, 75, 'Bojonggede', 'BOO20143', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (734, 75, 'Cigombong', 'BOO20144', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (735, 75, 'Ciseeng', 'BOO20145', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (736, 75, 'Kemang', 'BOO20146', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (737, 75, 'Klapanunggal ', 'BOO20147', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (738, 75, 'Leuwisadeng', 'BOO20148', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (739, 75, 'Pamijahan', 'BOO20149', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (740, 75, 'Ranca Bungur', 'BOO20150', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (741, 75, 'Sukajaya', 'BOO20151', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (742, 75, 'Sukamakmur', 'BOO20152', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (743, 75, 'Sukaraja ', 'BOO20153', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (744, 75, 'Tajurhalang', 'BOO20154', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (745, 75, 'Tamansari ', 'BOO20155', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (746, 75, 'Tanjungsari ', 'BOO20156', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (747, 75, 'Tenjolaya', 'BOO20157', 15000, 14000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (748, 75, 'Gunung Putri ', 'BOO20159', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (749, 217, 'Balikpapan', 'BPN10000', 43000, 38000, 46000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (750, 213, 'Samboja', 'BPN10008', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (751, 217, 'Balikpapan Barat ', 'BPN10009', 43000, 38000, 46000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (752, 217, 'Balikpapan Selatan', 'BPN10010', 43000, 38000, 46000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (753, 217, 'Balikpapan Tengah', 'BPN10011', 43000, 38000, 46000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (754, 217, 'Balikpapan Timur ', 'BPN10012', 43000, 38000, 46000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (755, 217, 'Balikpapan Utara', 'BPN10013', 43000, 38000, 46000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (756, 213, 'Muara Jawa', 'BPN10014', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (757, 216, 'Tanah Grogot', 'BPN20100', 68000, 58000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (758, 216, 'Batu Sopang ', 'BPN20101', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (759, 216, 'Kuaro', 'BPN20102', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (760, 216, 'Long Kali', 'BPN20103', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (761, 216, 'Long Ikis', 'BPN20104', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (762, 216, 'Muara Komam', 'BPN20105', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (763, 216, 'Pasir Balengkong ', 'BPN20106', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (764, 216, 'Batu Engau ', 'BPN20109', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (765, 216, 'Tanjung Harapan ', 'BPN20110', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (766, 216, 'Muara Samu', 'BPN20111', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (767, 209, 'Tanjung Redeb', 'BPN20200', 68000, 58000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (768, 209, 'Biduk Biduk', 'BPN20201', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (769, 209, 'Gunung Tabur ', 'BPN20202', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (770, 209, 'Kelay', 'BPN20203', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (771, 209, 'Pulau Derawan', 'BPN20204', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (772, 209, 'Sambaliung', 'BPN20205', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (773, 209, 'Segah', 'BPN20206', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (774, 209, 'Talisayan', 'BPN20207', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (775, 209, 'Maratua ', 'BPN20210', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (776, 209, 'Teluk Bayur ', 'BPN20211', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (777, 209, 'Tubaan', 'BPN20212', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (778, 215, 'Penajam', 'BPN20300', 68000, 58000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (779, 215, 'Waru', 'BPN20301', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (780, 215, 'Babulu ', 'BPN20302', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (781, 215, 'Sepaku', 'BPN20303', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (782, 208, 'Bontang', 'BTG10000', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (783, 208, 'Bontang Barat ', 'BTG10002', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (784, 208, 'Bontang Selatan ', 'BTG10003', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (785, 208, 'Bontang Utara', 'BTG10004', 54000, 47000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (786, 214, 'Sangatta', 'BTG20100', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (787, 214, 'Teluk Pandan', 'BTG20101', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (788, 214, 'Batu Ampar ', 'BTG20102', 108000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (789, 214, 'Bengalon ', 'BTG20103', 72000, 62000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (790, 214, 'Busang ', 'BTG20104', 108000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (791, 214, 'Kaliorang ', 'BTG20105', 72000, 62000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (792, 214, 'Karangan ', 'BTG20106', 108000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (793, 214, 'Kaubun ', 'BTG20107', 108000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (794, 214, 'Kongbeng ', 'BTG20108', 108000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (795, 214, 'Long Masengat ', 'BTG20109', 108000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (796, 214, 'Muara Ancalong', 'BTG20110', 108000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (797, 214, 'Muara Bengkal ', 'BTG20111', 108000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (798, 214, 'Muara Wahau ', 'BTG20112', 72000, 62000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (799, 214, 'Rantau Pulung ', 'BTG20113', 72000, 62000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (800, 214, 'Sandaran ', 'BTG20114', 108000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (801, 214, 'Sangatta Selatan', 'BTG20115', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (802, 214, 'Sangkulirang ', 'BTG20116', 72000, 62000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (803, 214, 'Telen', 'BTG20117', 108000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (804, 214, 'Sangkima', 'BTG20120', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (805, 214, 'Swarga Bara', 'BTG20121', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (806, 214, 'Singa Gembara', 'BTG20122', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (807, 219, 'Batam', 'BTH10000', 30000, 26000, 37000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (808, 219, 'Nongsa/ Kabil/ Lagoi', 'BTH10052', 30000, 26000, 37000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (809, 219, 'Sekupang/ Tanjung Ucang', 'BTH10054', 30000, 26000, 37000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (810, 219, 'Batu Ampar', 'BTH10080', 30000, 26000, 37000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (811, 219, 'Belakang Padang', 'BTH10081', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (812, 219, 'Bulang Galang', 'BTH10082', 30000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (813, 219, 'Galang ', 'BTH10083', 30000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (814, 219, 'Lubuk Baja', 'BTH10084', 30000, 26000, 37000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (815, 219, 'Sei Beduk', 'BTH10085', 30000, 26000, 37000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (816, 222, 'Daik', 'BTH10200', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (817, 222, 'Lingga', 'BTH10201', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (818, 222, 'Lingga Utara ', 'BTH10202', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (819, 222, 'Senayang', 'BTH10203', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (820, 222, 'Singkep', 'BTH10204', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (821, 222, 'Singkep Barat', 'BTH10205', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (822, 218, 'Ranai', 'BTH10300', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (823, 218, 'Bunguran Barat', 'BTH10301', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (824, 218, 'Bunguran Timur', 'BTH10302', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (825, 218, 'Bunguran Timur Laut', 'BTH10303', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (826, 218, 'Bunguran Tengah', 'BTH10304', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (827, 218, 'Bunguran Utara', 'BTH10305', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (828, 218, 'Jemaja', 'BTH10306', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (829, 218, 'Jemaja Timur ', 'BTH10307', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (830, 218, 'Midai', 'BTH10308', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (831, 218, 'Pal Matak', 'BTH10309', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (832, 218, 'Serasan', 'BTH10310', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (833, 218, 'Siantan', 'BTH10311', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (834, 218, 'Siantan Selatan ', 'BTH10312', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (835, 218, 'Siantan Timur', 'BTH10313', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (836, 218, 'Subi', 'BTH10314', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (837, 218, 'Pulau Laut', 'BTH10315', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (838, 218, 'Pulau Tiga', 'BTH10316', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (839, 221, 'Tanjung Balai Karimun', 'BTH10400', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (840, 221, 'Buru', 'BTH10401', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (841, 221, 'Karimun', 'BTH10402', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (842, 221, 'Kundur', 'BTH10403', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (843, 221, 'Kundur Barat', 'BTH10404', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (844, 221, 'Kundur Utara', 'BTH10405', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (845, 221, 'Meral', 'BTH10406', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (846, 221, 'Moro', 'BTH10407', 105000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (847, 221, 'Tebing', 'BTH10408', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (848, 221, 'Tanjung Batu', 'BTH10409', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (849, 273, 'Banda Aceh', 'BTJ10000', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (850, 273, 'Meuraksa', 'BTJ10015', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (851, 273, 'Jaya Baru', 'BTJ10016', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (852, 273, 'Banda Raya', 'BTJ10017', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (853, 273, 'Baiturrahman', 'BTJ10018', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (854, 273, 'Lueng Bata', 'BTJ10019', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (855, 273, 'Kuta Alam', 'BTJ10020', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (856, 273, 'Kuta Raja', 'BTJ10021', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (857, 273, 'Syiah Kuala', 'BTJ10022', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (858, 273, 'Ulee Kareng', 'BTJ10023', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (859, 260, 'Langsa', 'BTJ10100', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (860, 260, 'Langsa Barat', 'BTJ10118', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (861, 260, 'Langsa Kota', 'BTJ10119', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (862, 260, 'Langsa Lama', 'BTJ10120', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (863, 260, 'Langsa Teungoh', 'BTJ10121', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (864, 260, 'Langsa Timur', 'BTJ10122', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (865, 270, 'Lhokseumawe', 'BTJ10200', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (866, 270, 'Muara Dua', 'BTJ10209', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (867, 270, 'Banda Sakti', 'BTJ10226', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (868, 270, 'Blang Mangat', 'BTJ10227', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (869, 258, 'Janto', 'BTJ10300', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (870, 258, 'Darul Imarah', 'BTJ10301', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (871, 258, 'Darussalam', 'BTJ10302', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (872, 258, 'Indrapuri', 'BTJ10303', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (873, 258, 'Ingin Jaya', 'BTJ10304', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (874, 258, 'Kuta Baro', 'BTJ10305', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (875, 258, 'Lho\'Nga', 'BTJ10306', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (876, 258, 'Lhoong', 'BTJ10307', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (877, 258, 'Mesjid Raya', 'BTJ10308', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (878, 258, 'Montasik', 'BTJ10309', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (879, 258, 'Peukan Bada', 'BTJ10310', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (880, 258, 'Suka Makmur', 'BTJ10311', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (881, 258, 'Simpang tiga', 'BTJ10312', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (882, 258, 'Baitussalam', 'BTJ10313', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (883, 258, 'Darul Kamal', 'BTJ10314', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (884, 258, 'Krueng Barona Jaya', 'BTJ10315', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (885, 258, 'Kuta Cot Glie', 'BTJ10316', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (886, 258, 'Kuta Malaka', 'BTJ10317', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (887, 258, 'Lembah Seulawah', 'BTJ10318', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (888, 258, 'Leupung', 'BTJ10319', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (889, 258, 'Puroaceh', 'BTJ10320', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (890, 258, 'Seulimeum', 'BTJ10321', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (891, 268, 'Bireuen', 'BTJ20100', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (892, 268, 'Jeunieb', 'BTJ20101', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (893, 268, 'Peusangan', 'BTJ20102', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (894, 268, 'Peudada', 'BTJ20103', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (895, 268, 'Samalanga', 'BTJ20104', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (896, 268, 'Ganda Pura', 'BTJ20105', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (897, 268, 'Makmur', 'BTJ20106', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (898, 268, 'Kuala', 'BTJ20107', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (899, 268, 'Jangka', 'BTJ20108', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (900, 268, 'Jeumpa', 'BTJ20109', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (901, 268, 'Juli', 'BTJ20110', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (902, 268, 'Kota Juang', 'BTJ20111', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (903, 268, 'Kuta Blang', 'BTJ20112', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (904, 268, 'Pandrah', 'BTJ20113', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (905, 268, 'Peusangan Selatan', 'BTJ20114', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (906, 268, 'Peusangan Siblah Krueng', 'BTJ20115', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (907, 268, 'Primbang', 'BTJ20116', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (908, 268, 'Simpang Mamplam', 'BTJ20117', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (909, 256, 'Kutacane', 'BTJ20200', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (910, 256, 'Badar', 'BTJ20201', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (911, 256, 'Bambel', 'BTJ20202', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (912, 256, 'Lawe Alas', 'BTJ20205', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (913, 256, 'Lawe Sigala-gala', 'BTJ20206', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (914, 256, 'Babul Makmur', 'BTJ20209', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (915, 256, 'Babul Rahmat', 'BTJ20210', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (916, 256, 'Babussalam', 'BTJ20211', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (917, 256, 'Bukit Tusam', 'BTJ20212', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (918, 256, 'Darul Hasanah', 'BTJ20213', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (919, 256, 'Lawe Bulan', 'BTJ20214', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (920, 256, 'Semadam', 'BTJ20215', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (921, 266, 'Meulaboh', 'BTJ20300', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (922, 266, 'Kec. Kaway XVI', 'BTJ20304', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (923, 266, 'Samatiga', 'BTJ20308', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (924, 266, 'Sungai Mas', 'BTJ20315', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (925, 266, 'Woyla', 'BTJ20318', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (926, 266, 'Bubon', 'BTJ20319', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (927, 266, 'Arongan Lambalek', 'BTJ20320', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (928, 266, 'Johan Pahlawan', 'BTJ20321', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (929, 266, 'Woyla Barat', 'BTJ20322', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (930, 266, 'Woyla Timur', 'BTJ20323', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (931, 266, 'Pante Ceureumen', 'BTJ20324', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (932, 254, 'Sigli', 'BTJ20400', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (933, 254, 'Batee', 'BTJ20403', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (934, 254, 'Delima', 'BTJ20404', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (935, 254, 'Geumpang', 'BTJ20405', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (936, 254, 'Glumpang Tiga', 'BTJ20406', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (937, 254, 'Indrajaya', 'BTJ20407', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (938, 254, 'Kembang Tanjung', 'BTJ20408', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (939, 254, 'Mila', 'BTJ20410', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (940, 254, 'Muara Tiga', 'BTJ20411', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (941, 254, 'Mutiara', 'BTJ20412', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (942, 254, 'Padang Tiji', 'BTJ20413', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (943, 254, 'Peukan Baro', 'BTJ20414', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (944, 254, 'Pidie', 'BTJ20415', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (945, 254, 'Sakti', 'BTJ20416', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (946, 254, 'Simpang Tiga', 'BTJ20417', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (947, 254, 'Tangse', 'BTJ20418', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (948, 254, 'Tiro/Truseb', 'BTJ20419', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (949, 254, 'Titeua/Keumala', 'BTJ20420', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (950, 254, 'Gleumpang Baro', 'BTJ20423', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (951, 254, 'Grong-Grong', 'BTJ20424', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (952, 254, 'Mane', 'BTJ20425', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (953, 254, 'Mutiara Timur', 'BTJ20426', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (954, 264, 'Takengon', 'BTJ20500', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (955, 264, 'Bebesen', 'BTJ20502', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (956, 264, 'Bintang', 'BTJ20503', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (957, 264, 'Linge Isak', 'BTJ20505', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (958, 264, 'Pegasing', 'BTJ20506', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (959, 264, 'Silih Nara', 'BTJ20507', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (960, 264, 'Atu Lintang', 'BTJ20509', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (961, 264, 'Bies', 'BTJ20510', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (962, 264, 'Celala', 'BTJ20511', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (963, 264, 'Jagong Jeget', 'BTJ20512', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (964, 264, 'Kebayakan', 'BTJ20513', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (965, 264, 'Ketol', 'BTJ20514', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (966, 264, 'Kute Panang', 'BTJ20515', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (967, 264, 'LautTawar', 'BTJ20516', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (968, 264, 'Rusip Antara', 'BTJ20517', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (969, 252, 'Tapak Tuan', 'BTJ20600', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (970, 252, 'Bakongan', 'BTJ20601', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (971, 252, 'Kluet Utara', 'BTJ20603', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (972, 252, 'Kluet Selatan', 'BTJ20605', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (973, 252, 'Labuhan Haji', 'BTJ20606', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (974, 252, 'Meukek', 'BTJ20608', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (975, 252, 'Sama Dua', 'BTJ20610', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (976, 252, 'Trumon', 'BTJ20617', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (977, 252, 'Bakongan Timur', 'BTJ20619', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (978, 252, 'Kluet Tengah', 'BTJ20620', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (979, 252, 'Kluet Timur', 'BTJ20621', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (980, 252, 'Labuhan Haji Barat', 'BTJ20622', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (981, 252, 'Labuhan Haji Timur', 'BTJ20623', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (982, 252, 'Pasie raja', 'BTJ20624', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (983, 252, 'Trumon Timur', 'BTJ20625', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (984, 252, 'Sawang', 'BTJ20646', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (985, 262, 'Sabang', 'BTJ20700', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (986, 262, 'Sukakarya', 'BTJ20702', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (987, 262, 'Sukajaya', 'BTJ20703', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (988, 272, 'Blangpidie', 'BTJ21000', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (989, 272, 'Babah Rot', 'BTJ21001', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (990, 272, 'kuala Batee', 'BTJ21002', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (991, 272, 'Manggeng', 'BTJ21003', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (992, 272, 'Susoh', 'BTJ21004', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (993, 272, 'Tangan Tangan', 'BTJ21005', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (994, 259, 'Krueng Sabee/Calang', 'BTJ21100', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (995, 259, 'Jaya', 'BTJ21101', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (996, 259, 'Pangat', 'BTJ21102', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (997, 259, 'Sampo Niet', 'BTJ21103', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (998, 259, 'Setia Bakti', 'BTJ21104', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (999, 259, 'Teunom', 'BTJ21105', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1000, 269, 'Singkil', 'BTJ21200', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1001, 269, 'Danau Paris', 'BTJ21201', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1002, 269, 'Gunung Meriah', 'BTJ21202', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1003, 269, 'Kota Baharu', 'BTJ21203', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1004, 269, 'Pulau Banyak', 'BTJ21204', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1005, 269, 'Simpang Kanan', 'BTJ21205', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1006, 269, 'Singkil Utara', 'BTJ21206', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1007, 269, 'Singkohor', 'BTJ21207', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1008, 269, 'Suro Baru', 'BTJ21208', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1009, 257, 'Kuala Simpang', 'BTJ21300', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1010, 257, 'Bendahara', 'BTJ21301', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1011, 257, 'Karang Baru', 'BTJ21302', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1012, 257, 'Kejuruan Muda', 'BTJ21303', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1013, 257, 'Manyak Payed', 'BTJ21304', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1014, 257, 'Rantau', 'BTJ21305', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1015, 257, 'Seuruway', 'BTJ21306', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1016, 257, 'Tamiang Hulu', 'BTJ21307', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1017, 267, 'Idi Rayeuk', 'BTJ21400', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1018, 267, 'Banda Alam', 'BTJ21401', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1019, 267, 'Birem Bayeun', 'BTJ21402', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1020, 267, 'Darul Aman', 'BTJ21403', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1021, 267, 'Darul Iksan', 'BTJ21404', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1022, 267, 'Idi Tuning', 'BTJ21405', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1023, 267, 'Indra Makmur', 'BTJ21406', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1024, 267, 'Julok', 'BTJ21407', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1025, 267, 'Madat', 'BTJ21408', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1026, 267, 'Nurussalam', 'BTJ21409', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1027, 267, 'Pante Beudari', 'BTJ21410', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1028, 267, 'Peudawa', 'BTJ21411', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1029, 267, 'Peureulak', 'BTJ21412', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1030, 267, 'Peureulak Barat', 'BTJ21413', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1031, 267, 'Peureulak Timur', 'BTJ21414', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1032, 267, 'Rantau Selamat', 'BTJ21415', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1033, 267, 'Ranto Peureulak', 'BTJ21416', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1034, 267, 'Serba Jadi/Lokop', 'BTJ21417', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1035, 267, 'Simpang Jernih', 'BTJ21418', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1036, 267, 'Simpang Ulim', 'BTJ21419', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1037, 267, 'Sungai Raya', 'BTJ21420', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1038, 255, 'Lhoksukon', 'BTJ21500', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1039, 255, 'Baktia', 'BTJ21501', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1040, 255, 'Baktia Barat', 'BTJ21502', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1041, 255, 'Cot Girek', 'BTJ21503', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1042, 255, 'Dewantara', 'BTJ21504', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1043, 255, 'Kuta Makmur', 'BTJ21505', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1044, 255, 'Langkahan', 'BTJ21506', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1045, 255, 'Matang Kuli', 'BTJ21507', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1046, 255, 'Meurah Mulia', 'BTJ21508', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1047, 255, 'Muara Batu', 'BTJ21509', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1048, 255, 'Nibong', 'BTJ21510', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1049, 255, 'Nisam', 'BTJ21511', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1050, 255, 'Paya Bakong', 'BTJ21512', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1051, 255, 'Samudera', 'BTJ21513', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1052, 255, 'Sawang', 'BTJ21514', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1053, 255, 'Seunuddon', 'BTJ21515', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1054, 255, 'Simpang Keuramat', 'BTJ21516', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1055, 255, 'Syamtalira Aron', 'BTJ21517', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1056, 255, 'Syamtalira Bayu', 'BTJ21518', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1057, 255, 'Tanah Luas', 'BTJ21519', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1058, 255, 'Tanah Jambo Aye', 'BTJ21520', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1059, 255, 'Tanah Pasir', 'BTJ21521', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1060, 265, 'Simpang Tiga Redelon', 'BTJ21600', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1061, 265, 'Bandar', 'BTJ21601', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1062, 265, 'Bukit', 'BTJ21602', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1063, 265, 'Permata', 'BTJ21603', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1064, 265, 'Pintu Rime', 'BTJ21604', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1065, 265, 'Syiah Kuala', 'BTJ21605', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1066, 265, 'Timang Gajah', 'BTJ21606', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1067, 265, 'Wih Pesam', 'BTJ21607', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1068, 253, 'Blang Kejeren', 'BTJ21700', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1069, 253, 'Kuta Panjang', 'BTJ21701', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1070, 253, 'Pining', 'BTJ21702', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1071, 253, 'Rikit Gaib', 'BTJ21703', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1072, 253, 'Terangon', 'BTJ21704', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1073, 253, 'Putri Betung', 'BTJ21705', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1074, 253, 'Blang Pegayon', 'BTJ21706', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1075, 253, 'Dabung Gelang', 'BTJ21707', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1076, 253, 'Blang Jerango', 'BTJ21708', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1077, 253, 'Teripe Jaya', 'BTJ21709', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1078, 253, 'Pantan Jaya', 'BTJ21713', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1079, 263, 'Suka Makmue', 'BTJ21800', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1080, 263, 'Beutong', 'BTJ21801', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1081, 263, 'Darul Makmur', 'BTJ21802', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1082, 263, 'Kuala', 'BTJ21803', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1083, 263, 'Seunagan', 'BTJ21804', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1084, 263, 'Seunagan Timur', 'BTJ21805', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1085, 251, 'Meureudu', 'BTJ21900', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1086, 251, 'Ulim', 'BTJ21901', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1087, 251, 'Jangka Buya', 'BTJ21902', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1088, 251, 'Bandar Dua', 'BTJ21903', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1089, 251, 'Meurah Dua', 'BTJ21904', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1090, 251, 'Bandar Baru', 'BTJ21905', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1091, 251, 'Panteraja', 'BTJ21906', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1092, 251, 'Trienggading', 'BTJ21907', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1093, 261, 'Sinabang', 'BTJ22100', 109000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1094, 261, 'Alafan', 'BTJ22101', 109000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1095, 261, 'Salang', 'BTJ22102', 109000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1096, 261, 'Simeulue Tengah', 'BTJ22103', 109000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1097, 261, 'Simeulue Timur', 'BTJ22104', 109000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1098, 261, 'Simuelue Barat', 'BTJ22105', 109000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1099, 261, 'Teluk Dalam', 'BTJ22106', 109000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1100, 261, 'Teupah Selatan', 'BTJ22107', 109000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1101, 261, 'Teupah Barat', 'BTJ22108', 109000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1102, 271, 'Subulussalam', 'BTJ22200', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1103, 271, 'Simpang Kiri', 'BTJ22201', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1104, 271, 'Penanggalan', 'BTJ22202', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1105, 271, 'Rundeng', 'BTJ22203', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1106, 271, 'Sultan Daulat', 'BTJ22204', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1107, 271, 'Longkip', 'BTJ22205', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1108, 72, 'Cirebon', 'CBN10000', 8000, 7000, 11000, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1109, 72, 'Cirebon Barat', 'CBN10006', 8000, 7000, 11000, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1110, 477, 'Harjamukti ', 'CBN10034', 7000, 6000, 8000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1111, 474, 'Kejaksan ', 'CBN10035', 7000, 6000, 8000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1112, 478, 'Kesambi', 'CBN10036', 7000, 6000, 8000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1113, 479, 'Lemahwungkuk ', 'CBN10037', 7000, 6000, 8000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1114, 480, 'Pekalipan', 'CBN10038', 7000, 6000, 8000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1115, 85, 'Indramayu', 'CBN20100', 8000, 7000, 11000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1116, 85, 'Anjatan', 'CBN20101', 10000, 9000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1117, 85, 'Balongan', 'CBN20102', 8000, 7000, 11000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1118, 85, 'Bangodua', 'CBN20103', 8000, 7000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1119, 85, 'Bongas', 'CBN20104', 8000, 7000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1120, 85, 'Cikedung ', 'CBN20105', 10000, 9000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1121, 85, 'Gabus Wetan ', 'CBN20106', 10000, 9000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1122, 85, 'Haurgeulis ', 'CBN20107', 10000, 9000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1123, 85, 'Jatibarang', 'CBN20108', 8000, 7000, 11000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1124, 85, 'Juntiyuat ', 'CBN20109', 8000, 7000, 11000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1125, 85, 'Kandanghaur ', 'CBN20110', 10000, 9000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1126, 85, 'Karangampel ', 'CBN20111', 8000, 7000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1127, 85, 'Kertasemaya ', 'CBN20112', 8000, 7000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1128, 85, 'Krangkeng', 'CBN20113', 8000, 7000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1129, 85, 'Kroya', 'CBN20114', 8000, 7000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1130, 85, 'Lelea', 'CBN20115', 10000, 9000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1131, 85, 'Lohbener', 'CBN20116', 10000, 9000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1132, 85, 'Losarang', 'CBN20117', 10000, 9000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1133, 85, 'Sliyeg', 'CBN20118', 10000, 9000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1134, 85, 'Sukra', 'CBN20119', 10000, 9000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1135, 85, 'Widasari', 'CBN20120', 8000, 7000, 11000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1136, 85, 'Arahan', 'CBN20121', 10000, 9000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1137, 85, 'Cantigi', 'CBN20122', 10000, 9000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1138, 85, 'Gantar', 'CBN20123', 11000, 10000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1139, 85, 'Kedokan Bunder', 'CBN20124', 11000, 10000, 16000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1140, 85, 'Sindang', 'CBN20125', 11000, 10000, 16000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1141, 85, 'Sukagumiwang', 'CBN20126', 11000, 10000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1142, 85, 'Trisi', 'CBN20127', 11000, 10000, 0, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1143, 70, 'Kuningan', 'CBN20200', 9000, 8000, 13000, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1144, 70, 'Ciawi Gebang', 'CBN20202', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1145, 70, 'Cibingbin', 'CBN20203', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1146, 70, 'Cibeureum ', 'CBN20204', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1147, 70, 'Cidahu', 'CBN20205', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1148, 70, 'Cigugur', 'CBN20206', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1149, 70, 'Cilimus', 'CBN20208', 11000, 10000, 16000, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1150, 70, 'Cilebak', 'CBN20209', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1151, 70, 'Cimahi', 'CBN20210', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1152, 70, 'Ciniru', 'CBN20211', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1153, 70, 'Cipicung', 'CBN20212', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1154, 70, 'Darma', 'CBN20213', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1155, 70, 'Garawangi', 'CBN20214', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1156, 70, 'Hantara', 'CBN20215', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1157, 70, 'Jalaksana', 'CBN20216', 11000, 10000, 16000, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1158, 70, 'Japara', 'CBN20217', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1159, 70, 'Kadugede ', 'CBN20218', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1160, 70, 'Kalimanggis', 'CBN20219', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1161, 70, 'Karangkancana', 'CBN20220', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1162, 70, 'Kramatmulya', 'CBN20221', 11000, 10000, 16000, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1163, 70, 'Lebakwangi', 'CBN20222', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1164, 70, 'Luragung', 'CBN20224', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1165, 70, 'Maleber ', 'CBN20225', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1166, 70, 'Mandirancan ', 'CBN20226', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1167, 70, 'Nusaherang ', 'CBN20228', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1168, 70, 'Pancalang ', 'CBN20230', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1169, 70, 'Pasawahan ', 'CBN20231', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1170, 70, 'Selajambe', 'CBN20232', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1171, 70, 'Sindangagung ', 'CBN20233', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1172, 70, 'Subang', 'CBN20234', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1173, 70, 'Cigandamekar ', 'CBN20237', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1174, 70, 'Ciwaru ', 'CBN20238', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1175, 82, 'Majalengka', 'CBN20300', 9000, 8000, 13000, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1176, 82, 'Argapura', 'CBN20301', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1177, 82, 'Bantarujeg', 'CBN20302', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1178, 82, 'Cikijing', 'CBN20303', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1179, 82, 'Dawuan', 'CBN20304', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1180, 82, 'Jatiwangi', 'CBN20305', 9000, 8000, 13000, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1181, 82, 'Jatitujuh', 'CBN20306', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1182, 82, 'Kadipaten', 'CBN20307', 9000, 8000, 13000, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1183, 82, 'Kertajati', 'CBN20308', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1184, 82, 'Lemahsugih', 'CBN20309', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1185, 82, 'Leuwimunding', 'CBN20310', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1186, 82, 'Ligung', 'CBN20311', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1187, 82, 'Maja ', 'CBN20312', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1188, 82, 'Palasah', 'CBN20313', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1189, 82, 'Panyingkiran', 'CBN20314', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1190, 82, 'Rajagaluh', 'CBN20315', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1191, 82, 'Sukahaji', 'CBN20317', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1192, 82, 'Sumber Jaya ', 'CBN20318', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1193, 82, 'Talaga', 'CBN20319', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1194, 82, 'Banjaran ', 'CBN20320', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1195, 82, 'Cigasong ', 'CBN20321', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1196, 82, 'Cingambul ', 'CBN20322', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1197, 82, 'Sindangwangi ', 'CBN20323', 11000, 10000, 0, 0, 10000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1198, 484, 'Sumber', 'CBN20400', 9000, 8000, 13000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1199, 68, 'Arjawinangun', 'CBN20402', 7000, 6000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1200, 68, 'Astanajapura', 'CBN20403', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1201, 68, 'Babakan', 'CBN20404', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1202, 68, 'Beber', 'CBN20405', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1203, 68, 'Ciledug ', 'CBN20406', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1204, 68, 'Cirebon Utara', 'CBN20407', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1205, 68, 'Cirebon Selatan', 'CBN20408', 7000, 6000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1206, 68, 'Ciwaringin', 'CBN20409', 7000, 6000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1207, 492, 'Depok ', 'CBN20410', 11000, 10000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1208, 68, 'Dukupuntang', 'CBN20411', 7000, 6000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1209, 68, 'Gebang ', 'CBN20412', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1210, 68, 'Gegesik', 'CBN20413', 7000, 6000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1211, 68, 'Gempol ', 'CBN20414', 7000, 6000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1212, 68, 'Kaliwedi', 'CBN20415', 7000, 6000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1213, 68, 'Kapetakan', 'CBN20416', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1214, 68, 'Karangsembung ', 'CBN20417', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1215, 68, 'Karangwareng', 'CBN20418', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1216, 489, 'Kedawung', 'CBN20419', 11000, 10000, 16000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1217, 488, 'Klangenan', 'CBN20420', 11000, 10000, 16000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1218, 68, 'Lemahabang', 'CBN20421', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1219, 68, 'Losari', 'CBN20422', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1220, 68, 'Mundu', 'CBN20423', 8000, 7000, 11000, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1221, 68, 'Pabedilan', 'CBN20424', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1222, 68, 'Susukan ', 'CBN20425', 7000, 6000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1223, 68, 'Pabuaran ', 'CBN20426', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1224, 68, 'Susukanlebak', 'CBN20427', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1225, 487, 'Palimanan', 'CBN20428', 9000, 8000, 13000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1226, 68, 'Waled', 'CBN20429', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1227, 68, 'Pangenan', 'CBN20430', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1228, 481, 'Weru', 'CBN20431', 11000, 10000, 16000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1229, 68, 'Panguragan', 'CBN20432', 7000, 6000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1230, 68, 'Pasaleman', 'CBN20433', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1231, 486, 'Plered ', 'CBN20434', 11000, 10000, 16000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1232, 485, 'Plumbon', 'CBN20435', 11000, 10000, 16000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1233, 482, 'Tengah Tani ', 'CBN20436', 11000, 10000, 16000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1234, 68, 'Sedong', 'CBN20438', 8000, 7000, 0, 0, 9000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1235, 49, 'Jakarta', 'CGK10000', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1236, 45, 'Jakarta Barat', 'CGK10100', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1237, 45, 'Grogol ', 'CGK10101', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1238, 45, 'Kalideres ', 'CGK10102', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1239, 45, 'Kebon Jeruk ', 'CGK10103', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1240, 45, 'Kembangan ', 'CGK10104', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1241, 45, 'Palmerah ', 'CGK10105', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1242, 45, 'Taman Sari ', 'CGK10106', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1243, 45, 'Tambora', 'CGK10107', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1244, 45, 'Cengkareng ', 'CGK10108', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1245, 48, 'Jakarta Selatan', 'CGK10200', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1246, 48, 'Jagakarsa ', 'CGK10201', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1247, 48, 'Kebayoran Baru', 'CGK10202', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1248, 48, 'Kebayoran Lama', 'CGK10203', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1249, 48, 'Mampang Prapatan', 'CGK10204', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1250, 48, 'Pancoran ', 'CGK10205', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1251, 48, 'Pasar Minggu ', 'CGK10206', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1252, 48, 'Pesanggrahan ', 'CGK10207', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1253, 48, 'Setiabudi ', 'CGK10208', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1254, 48, 'Tebet', 'CGK10209', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1255, 48, 'Cilandak', 'CGK10211', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1256, 44, 'Jakarta Pusat', 'CGK10300', 9000, 0, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1257, 44, 'Cempaka Putih ', 'CGK10301', 9000, 0, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1258, 44, 'Gambir ', 'CGK10302', 9000, 0, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1259, 44, 'Johar Baru ', 'CGK10303', 9000, 0, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1260, 44, 'Kemayoran ', 'CGK10304', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1261, 44, 'Menteng ', 'CGK10305', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1262, 44, 'Sawah Besar ', 'CGK10306', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1263, 44, 'Senen ', 'CGK10307', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1264, 44, 'Tanah Abang', 'CGK10308', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1265, 47, 'Jakarta Utara', 'CGK10400', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1266, 47, 'Kelapa Gading ', 'CGK10401', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1267, 47, 'Koja ', 'CGK10402', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1268, 47, 'Pademangan ', 'CGK10403', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1269, 47, 'Penjaringan ', 'CGK10404', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1270, 47, 'Tanjung Priok', 'CGK10405', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1271, 47, 'Cilincing ', 'CGK10406', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1272, 50, 'Jakarta Timur', 'CGK10500', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1273, 50, 'Cakung ', 'CGK10501', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1274, 50, 'Cipayung ', 'CGK10502', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1275, 50, 'Ciracas', 'CGK10503', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1276, 50, 'Duren Sawit ', 'CGK10504', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1277, 50, 'Jatinegara ', 'CGK10505', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1278, 50, 'Kramat Jati ', 'CGK10506', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1279, 50, 'Makassar ', 'CGK10507', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1280, 50, 'Matraman ', 'CGK10508', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1281, 50, 'Pasar Rebo ', 'CGK10509', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1282, 50, 'Pulo Gadung', 'CGK10510', 13000, 11000, 18000, 10800, 12000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1283, 46, 'Pulau Pramuka', 'CGK10600', 13000, 11000, 0, 10800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1284, 46, 'Kepulauan Seribu Selatan', 'CGK10601', 13000, 11000, 0, 10800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1285, 46, 'Kepulauan Seribu Utara', 'CGK10602', 13000, 11000, 0, 10800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1286, 23, 'Cilegon', 'CLG10000', 16000, 14000, 21000, 28000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1287, 23, 'Merak', 'CLG10004', 16000, 14000, 21000, 28000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1288, 23, 'Cibeber', 'CLG10037', 16000, 14000, 21000, 28000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1289, 23, 'Citangkil', 'CLG10039', 16000, 14000, 21000, 28000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1290, 23, 'Ciwandan', 'CLG10040', 16000, 14000, 21000, 28000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1291, 23, 'Gerogol', 'CLG10041', 16000, 14000, 21000, 28000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1292, 23, 'Jombang ', 'CLG10042', 16000, 14000, 21000, 28000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1293, 23, 'Purwakarta', 'CLG10043', 16000, 14000, 21000, 28000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1294, 19, 'Pandeglang', 'CLG20100', 18000, 16000, 21000, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1295, 19, 'Banjar ', 'CLG20101', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1296, 19, 'Bojong ', 'CLG20103', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1297, 19, 'Cadas Sari', 'CLG20105', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1298, 19, 'Cibaliung', 'CLG20106', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1299, 19, 'Cikeusik', 'CLG20108', 22000, 19000, 0, 50400, 16000, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (1300, 19, 'Cimanggu ', 'CLG20109', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1301, 19, 'Cimanuk', 'CLG20110', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1302, 19, 'Jiput', 'CLG20112', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1303, 19, 'Labuan', 'CLG20113', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1304, 19, 'Mandalawangi', 'CLG20114', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1305, 19, 'Menes', 'CLG20115', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1306, 19, 'Munjul', 'CLG20116', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1307, 19, 'Pagelaran ', 'CLG20118', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1308, 19, 'Saketi', 'CLG20120', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1309, 19, 'Sumur', 'CLG20121', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1310, 19, 'Angsana', 'CLG20122', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1311, 19, 'Carita', 'CLG20123', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1312, 19, 'Cibitung ', 'CLG20124', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1313, 19, 'Cigeulis', 'CLG20125', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1314, 19, 'Cikedal', 'CLG20126', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1315, 19, 'Cipeucang', 'CLG20128', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1316, 19, 'Cisata', 'CLG20129', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1317, 19, 'Kaduhejo', 'CLG20130', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1318, 19, 'Karangtanjung', 'CLG20131', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1319, 19, 'Panimbang', 'CLG20133', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1320, 19, 'Patia', 'CLG20134', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1321, 19, 'Picung', 'CLG20135', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1322, 19, 'Sukaresmi ', 'CLG20136', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1323, 22, 'Rangkasbitung ', 'CLG20200', 18000, 16000, 21000, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1324, 22, 'Banjarsari ', 'CLG20201', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1325, 22, 'Bayah', 'CLG20202', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1326, 22, 'Bojongmanik', 'CLG20203', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1327, 22, 'Cibadak ', 'CLG20204', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1328, 22, 'Cibeber ', 'CLG20205', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1329, 22, 'Cijaku', 'CLG20206', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1330, 22, 'Cikulur', 'CLG20207', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1331, 22, 'Cileles', 'CLG20208', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1332, 22, 'Cimarga', 'CLG20209', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1333, 22, 'Cipanas ', 'CLG20210', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1334, 22, 'Gunungkencana', 'CLG20211', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1335, 22, 'Leuwidamar ', 'CLG20212', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1336, 22, 'Maja ', 'CLG20213', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1337, 22, 'Malingping', 'CLG20214', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1338, 22, 'Muncang', 'CLG20215', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1339, 22, 'Panggarangan ', 'CLG20216', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1340, 22, 'Sajira', 'CLG20217', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1341, 22, 'Wanasalam', 'CLG20218', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1342, 22, 'Warunggunung', 'CLG20219', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1343, 22, 'Curugbitung ', 'CLG20221', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1344, 22, 'Sobang ', 'CLG20223', 22000, 19000, 0, 50400, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1345, 18, 'Serang', 'CLG20300', 17000, 15000, 21000, 14000, 15000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1346, 18, 'Curug', 'CLG20310', 17000, 15000, 21000, 14000, 15000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1347, 18, 'Kasemen', 'CLG20312', 17000, 15000, 21000, 14000, 15000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1348, 18, 'Taktakan', 'CLG20323', 17000, 15000, 21000, 14000, 15000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1349, 18, 'Walantaka', 'CLG20327', 17000, 15000, 0, 14000, 15000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1350, 18, 'Cipocok Jaya ', 'CLG20328', 17000, 15000, 21000, 14000, 15000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1351, 21, 'Baros ', 'CLG20500', 18000, 16000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1352, 21, 'Anyar', 'CLG20501', 18000, 16000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1353, 21, 'Binuang', 'CLG20502', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1354, 21, 'Bojonegara ', 'CLG20503', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1355, 21, 'Carenang', 'CLG20504', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1356, 21, 'Cikande', 'CLG20505', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1357, 21, 'Cikeusal', 'CLG20506', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1358, 21, 'Cinangka', 'CLG20507', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1359, 21, 'Ciomas', 'CLG20508', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1360, 21, 'Ciruas', 'CLG20509', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1361, 21, 'Jawilan', 'CLG20510', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1362, 21, 'Kibin', 'CLG20511', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1363, 21, 'Kopo', 'CLG20512', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1364, 21, 'Kragilan', 'CLG20513', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1365, 21, 'Kramatwatu', 'CLG20514', 22000, 19000, 27000, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1366, 21, 'Mancak', 'CLG20515', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1367, 21, 'Pabuaran ', 'CLG20516', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1368, 21, 'Padarincang', 'CLG20517', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1369, 21, 'Pamarayan', 'CLG20518', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1370, 21, 'Petir', 'CLG20519', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1371, 21, 'Pontang', 'CLG20520', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1372, 21, 'Puloampel', 'CLG20521', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1373, 21, 'Tanara', 'CLG20522', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1374, 21, 'Tirtayasa', 'CLG20523', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1375, 21, 'Tunjung Teja', 'CLG20524', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1376, 21, 'Waringinkurung', 'CLG20525', 22000, 19000, 0, 14000, 20000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1377, 89, 'Cilacap', 'CXP10000', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1378, 89, 'Adipala', 'CXP10001', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1379, 89, 'Binangun', 'CXP10002', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1380, 89, 'Cimanggu ', 'CXP10003', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1381, 89, 'Cipari', 'CXP10004', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1382, 89, 'Dayeuhluhur', 'CXP10005', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1383, 89, 'Gandrungmangu', 'CXP10006', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1384, 89, 'Jeruklegi', 'CXP10007', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1385, 89, 'Karangpucung', 'CXP10008', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1386, 89, 'Kawunganten', 'CXP10009', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1387, 89, 'Kedungrejo', 'CXP10010', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1388, 89, 'Kesugihan', 'CXP10011', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1389, 89, 'Kroya ', 'CXP10012', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1390, 89, 'Majenang', 'CXP10013', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1391, 89, 'Maos', 'CXP10014', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1392, 89, 'Nusawungu', 'CXP10015', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1393, 89, 'Sampang', 'CXP10016', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1394, 89, 'Sidareja', 'CXP10017', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1395, 89, 'Wanar', 'CXP10018', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1396, 89, 'Cilacap Selatan', 'CXP10019', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1397, 89, 'Cilacap Tengah', 'CXP10020', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1398, 89, 'Cilacap Utara', 'CXP10021', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1399, 89, 'Patimuan', 'CXP10022', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1400, 89, 'Bantarsari', 'CXP10023', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1401, 89, 'Kampung Laut', 'CXP10024', 31000, 27000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (1402, 56, 'Jambi', 'DJB10000', 28000, 25000, 32000, 0, 25000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1403, 56, 'Danau Teluk', 'DJB10010', 28000, 25000, 0, 0, 25000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1404, 56, 'Jambi Selatan', 'DJB10011', 28000, 25000, 32000, 0, 25000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1405, 56, 'Jambi Timur', 'DJB10012', 28000, 25000, 32000, 0, 25000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1406, 56, 'Jelutung', 'DJB10013', 28000, 25000, 32000, 0, 25000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1407, 56, 'Kota Baru', 'DJB10014', 28000, 25000, 32000, 0, 25000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1408, 56, 'Pasar Jambi', 'DJB10015', 28000, 25000, 32000, 0, 25000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1409, 56, 'Pelayangan', 'DJB10016', 28000, 25000, 0, 0, 25000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1410, 56, 'Telanaipura', 'DJB10017', 28000, 25000, 32000, 0, 25000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1411, 51, 'Kualatungkal', 'DJB10100', 42000, 37000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1412, 51, 'Betara', 'DJB10101', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1413, 51, 'Merlung', 'DJB10104', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1414, 51, 'Pengabuan', 'DJB10107', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1415, 51, 'Tungkal Ulu', 'DJB10110', 42000, 37000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1416, 51, 'Tungkal Ilir', 'DJB10112', 42000, 37000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1417, 55, 'Bangko', 'DJB20100', 42000, 37000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1418, 55, 'Jangkat', 'DJB20102', 80000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1419, 55, 'Muara Siau', 'DJB20104', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1420, 55, 'Sungai Manau', 'DJB20107', 80000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1421, 55, 'Tabir', 'DJB20108', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1422, 55, 'Pamenang', 'DJB20109', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1423, 55, 'Tabir Ulu', 'DJB20110', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1424, 60, 'Muara Bulian', 'DJB20200', 42000, 37000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1425, 60, 'Batin XXIV', 'DJB20201', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1426, 60, 'Maro Sebo Ulu', 'DJB20202', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1427, 60, 'Mersam', 'DJB20203', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1428, 60, 'Muara Tembesi', 'DJB20204', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1429, 60, 'Pemayung', 'DJB20205', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1430, 60, 'Bajubang', 'DJB20206', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1431, 60, 'Maro Sebo Ilir', 'DJB20207', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1432, 54, 'Muara Bungo', 'DJB20300', 42000, 37000, 0, 0, 38000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1433, 54, 'Jujuhan', 'DJB20301', 80000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1434, 54, 'Pelepat', 'DJB20302', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1435, 54, 'Rantau Pandan', 'DJB20303', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1436, 54, 'Tanah Sepenggal', 'DJB20306', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1437, 54, 'Tanah Tumbuh', 'DJB20307', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1438, 54, 'Bathin II Babeko', 'DJB20314', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1439, 54, 'Limbur Lubuk Mengkuang', 'DJB20315', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1440, 54, 'Muko Muko Batin VII', 'DJB20316', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1441, 54, 'Pelepat Ilir', 'DJB20317', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1442, 59, 'Sungai Penuh', 'DJB20400', 42000, 37000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1443, 59, 'Air Hangat', 'DJB20401', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1444, 59, 'Batang Merangin', 'DJB20402', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1445, 59, 'Danau Kerinci', 'DJB20403', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1446, 59, 'Gunung Kerinci', 'DJB20404', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1447, 59, 'Gunung Raya', 'DJB20405', 80000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1448, 59, 'Hamparan Rawang', 'DJB20406', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1449, 59, 'Kayu Aro', 'DJB20407', 80000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1450, 59, 'Keliling Danau', 'DJB20408', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1451, 59, 'Sitinjau Laut', 'DJB20409', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1452, 59, 'Air Hangat Timur', 'DJB20411', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1453, 53, 'Sengeti', 'DJB20600', 42000, 37000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1454, 53, 'Jambi Luar Kota', 'DJB20601', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1455, 53, 'Kumpeh', 'DJB20602', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1456, 53, 'Kumpeh Ulu', 'DJB20603', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1457, 53, 'Maro Sebo', 'DJB20604', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1458, 53, 'Mestong', 'DJB20605', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1459, 53, 'Sekernan', 'DJB20606', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1460, 53, 'Sungai Bahar', 'DJB20607', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1461, 58, 'Sarolangun', 'DJB20700', 42000, 37000, 0, 0, 38000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1462, 58, 'Batang Asai', 'DJB20701', 80000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1463, 58, 'Muara Limun', 'DJB20702', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1464, 58, 'Mandiangin', 'DJB20703', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1465, 58, 'Pauh', 'DJB20704', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1466, 58, 'Pelawan Singkut', 'DJB20705', 53000, 46000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1467, 52, 'Muara sabak', 'DJB20800', 42000, 37000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1468, 52, 'Dendang', 'DJB20801', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1469, 52, 'Mendahara', 'DJB20802', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1470, 52, 'Nipah Panjang', 'DJB20803', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1471, 52, 'Rantau Rasau', 'DJB20804', 80000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1472, 52, 'Sadu', 'DJB20805', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1473, 57, 'Muara Tebo', 'DJB20900', 42000, 37000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1474, 57, 'Rimbo Bujang', 'DJB20901', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1475, 57, 'Rimbo Ilir', 'DJB20902', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1476, 57, 'Rimbo Ulu', 'DJB20903', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1477, 57, 'Sumay', 'DJB20904', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1478, 57, 'Tebo Ilir', 'DJB20905', 42000, 37000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1479, 57, 'Tebo Tengah', 'DJB20906', 42000, 37000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1480, 57, 'Tebo Ulu', 'DJB20907', 42000, 37000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1481, 57, 'Tengah Ilir', 'DJB20908', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1482, 57, 'VII Koto', 'DJB20909', 53000, 46000, 0, 0, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1483, 305, 'Jayapura', 'DJJ10000', 101000, 86000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1484, 305, 'Abepura', 'DJJ10001', 101000, 86000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1485, 315, 'Demta', 'DJJ10004', 101000, 86000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1486, 315, 'Depapre ', 'DJJ10005', 101000, 86000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1487, 315, 'Kareuh', 'DJJ10006', 101000, 86000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1488, 315, 'Kemtuk Gresie ', 'DJJ10007', 101000, 86000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1489, 315, 'Nimboran', 'DJJ10011', 101000, 86000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1490, 315, 'Sentani', 'DJJ10015', 130000, 112000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1491, 315, 'Unurum Guay', 'DJJ10018', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1492, 315, 'Kemtuk', 'DJJ10025', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1493, 315, 'Nimbokrang', 'DJJ10026', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1494, 315, 'Sentani Barat', 'DJJ10027', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1495, 315, 'Sentani Timur', 'DJJ10028', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1496, 305, 'Jayapura Selatan', 'DJJ10029', 101000, 86000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1497, 305, 'Jayapura Utara', 'DJJ10030', 101000, 86000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1498, 305, 'Muara Tam', 'DJJ10031', 101000, 86000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1499, 326, 'Biak Kota', 'DJJ20100', 130000, 112000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1500, 326, 'Biak Barat', 'DJJ20101', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1501, 326, 'Biak timur', 'DJJ20102', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1502, 326, 'Biak Utara', 'DJJ20103', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1503, 326, 'Numfor Barat', 'DJJ20104', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1504, 326, 'Numfor Timur', 'DJJ20105', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1505, 326, 'Kepulauan Padaidio', 'DJJ20109', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1506, 326, 'Samofa', 'DJJ20110', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1507, 326, 'Warsa', 'DJJ20111', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1508, 326, 'Yendidori', 'DJJ20112', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1509, 323, 'Merauke', 'DJJ20400', 130000, 112000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1510, 323, 'Kimaam', 'DJJ20407', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1511, 323, 'Muting', 'DJJ20411', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1512, 323, 'Okaba', 'DJJ20414', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1513, 323, 'Distrik Ulilin', 'DJJ20418', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1514, 323, 'Eligobel', 'DJJ20419', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1515, 323, 'Jagebob', 'DJJ20420', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1516, 323, 'Kurik', 'DJJ20421', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1517, 323, 'Semangga', 'DJJ20422', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1518, 323, 'Sota', 'DJJ20423', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1519, 323, 'Tanah Miring', 'DJJ20424', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1520, 311, 'Serui', 'DJJ20600', 130000, 112000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1521, 311, 'Yapen Barat', 'DJJ20603', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1522, 311, 'Yapen Timur', 'DJJ20604', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1523, 311, 'Angkaisera', 'DJJ20606', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1524, 311, 'Poom', 'DJJ20607', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1525, 311, 'Yapen Selatan', 'DJJ20609', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1526, 320, 'Wamena', 'DJJ20700', 130000, 112000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1527, 320, 'Asologaima', 'DJJ20701', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1528, 320, 'Kelila', 'DJJ20704', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1529, 320, 'Kurulu', 'DJJ20705', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1530, 320, 'Maki', 'DJJ20708', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1531, 320, 'Tiom', 'DJJ20711', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1532, 320, 'Abenaho', 'DJJ20712', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1533, 320, 'Apalapsili', 'DJJ20713', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1534, 320, 'Bolakma', 'DJJ20714', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1535, 320, 'Gamelia', 'DJJ20715', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1536, 320, 'Hubikosi', 'DJJ20716', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1537, 320, 'Kenyam', 'DJJ20717', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1538, 320, 'Kobakma', 'DJJ20718', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1539, 320, 'Mapenduma', 'DJJ20719', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1540, 320, 'Pirime', 'DJJ20720', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1541, 308, 'Agats', 'DJJ20800', 130000, 112000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1542, 308, 'Akat', 'DJJ20801', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1543, 308, 'Atsy', 'DJJ20802', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1544, 308, 'Fayit', 'DJJ20803', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1545, 308, 'Pantai Kasuari', 'DJJ20804', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1546, 308, 'Sawaerma', 'DJJ20805', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1547, 308, 'Suator', 'DJJ20806', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1548, 318, 'Tanah Merah/ Boven Digoel', 'DJJ20900', 130000, 112000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1549, 318, 'Distrik Bomakia', 'DJJ20901', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1550, 318, 'Jair', 'DJJ20902', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1551, 318, 'Kouh', 'DJJ20903', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1552, 318, 'Mandobo', 'DJJ20904', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1553, 318, 'Mindiptana', 'DJJ20905', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1554, 318, 'Waropko', 'DJJ20906', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1555, 306, 'Waris', 'DJJ21000', 130000, 112000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1556, 306, 'Arso', 'DJJ21001', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1557, 306, 'Senggi', 'DJJ21002', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1558, 306, 'Skanto', 'DJJ21003', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1559, 306, 'Web', 'DJJ21004', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1560, 316, 'Oksibil', 'DJJ21300', 130000, 112000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1561, 316, 'Batom', 'DJJ21301', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1562, 316, 'Borme', 'DJJ21302', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1563, 316, 'Iwur ', 'DJJ21303', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1564, 316, 'Kiwirok', 'DJJ21304', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1565, 316, 'Okbibab', 'DJJ21305', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1566, 303, 'Sarmi', 'DJJ21500', 130000, 112000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1567, 303, 'Bonggo', 'DJJ21501', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1568, 303, 'Pantai Barat', 'DJJ21502', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1569, 303, 'Pantai Timur', 'DJJ21503', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1570, 303, 'Tor Atas', 'DJJ21504', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1571, 313, 'Sorendiweri', 'DJJ21600', 130000, 112000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1572, 313, 'Supiori Selatan', 'DJJ21601', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1573, 313, 'Supiori Timur ', 'DJJ21602', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1574, 313, 'Supiori Utara', 'DJJ21603', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1575, 325, 'Karubaga', 'DJJ21700', 130000, 112000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1576, 325, 'Bokondini', 'DJJ21701', 163000, 139000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1577, 325, 'Kanggime', 'DJJ21702', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1578, 325, 'Kembu', 'DJJ21703', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1579, 312, 'Botawa', 'DJJ21800', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1580, 312, 'Masirei', 'DJJ21801', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1581, 312, 'Waropen Bawah', 'DJJ21802', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1582, 322, 'Sumohai', 'DJJ21900', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1583, 322, 'Anggruk', 'DJJ21901', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1584, 322, 'Kurima', 'DJJ21902', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1585, 322, 'Ninia', 'DJJ21903', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1586, 310, 'Burmeso', 'DJJ22000', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1587, 310, 'Mamberamo Tengah', 'DJJ22001', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1588, 310, 'Mamberamo Ilir', 'DJJ22002', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1589, 310, 'Mamberamo Ulu', 'DJJ22003', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1590, 310, 'Mamberamo Tengah Timur ', 'DJJ22004', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1591, 310, 'Rufaer ', 'DJJ22005', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1592, 310, 'Waropen Atas', 'DJJ22006', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1593, 310, 'Benuki', 'DJJ22007', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1594, 310, 'Sawai', 'DJJ22008', 245000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1595, 83, 'Depok', 'DPK10000', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1596, 83, 'Cibubur', 'DPK10050', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1597, 83, 'Cimanggis', 'DPK10052', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1598, 83, 'Sawangan ', 'DPK10058', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1599, 83, 'Beji ', 'DPK10059', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1600, 83, 'Limo', 'DPK10060', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1601, 83, 'Pancoran Mas', 'DPK10061', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1602, 83, 'Sukmajaya', 'DPK10062', 15000, 14000, 20000, 0, 13000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1603, 3, 'Denpasar', 'DPS10000', 28000, 25000, 31000, 20400, 25000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1604, 3, 'Denpasar Barat', 'DPS10009', 28000, 25000, 31000, 20400, 25000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1605, 3, 'Denpasar Selatan', 'DPS10010', 28000, 25000, 31000, 20400, 25000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1606, 3, 'Denpasar Timur', 'DPS10011', 28000, 25000, 31000, 20400, 25000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1607, 7, 'Amlapura', 'DPS20100', 33000, 29000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1608, 7, 'Abang', 'DPS20101', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1609, 7, 'Bebandem ', 'DPS20102', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1610, 7, 'Kubu ', 'DPS20103', 42000, 37000, 0, 31600, 30000, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (1611, 7, 'Manggis', 'DPS20104', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1612, 7, 'Rendang', 'DPS20105', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1613, 7, 'Selat', 'DPS20106', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1614, 7, 'Sidemen', 'DPS20107', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1615, 7, 'Karangasem', 'DPS20108', 33000, 29000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1616, 2, 'Bangli', 'DPS20200', 33000, 29000, 0, 42400, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1617, 2, 'Kintamani ', 'DPS20201', 42000, 37000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1618, 2, 'Susut', 'DPS20202', 42000, 37000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1619, 2, 'Tembuku', 'DPS20203', 42000, 37000, 0, 42400, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1620, 6, 'Gianyar', 'DPS20300', 33000, 29000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1621, 6, 'Blahbatuh', 'DPS20301', 42000, 37000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1622, 6, 'Payangan', 'DPS20302', 42000, 37000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1623, 6, 'Sukawati', 'DPS20303', 42000, 37000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1624, 6, 'Tampaksiring', 'DPS20304', 42000, 37000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1625, 6, 'Tegal Lalang', 'DPS20305', 42000, 37000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1626, 6, 'Ubud', 'DPS20306', 42000, 37000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1627, 1, 'Negara', 'DPS20400', 33000, 29000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1628, 1, 'Melaya', 'DPS20401', 42000, 37000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1629, 1, 'Mendoyo', 'DPS20402', 42000, 37000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1630, 1, 'Pekutatan', 'DPS20403', 42000, 37000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1631, 5, 'Semarapura', 'DPS20500', 33000, 29000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1632, 5, 'Dawan', 'DPS20501', 42000, 37000, 0, 42400, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1633, 5, 'Klungkung', 'DPS20502', 33000, 29000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1634, 5, 'Nusapenida', 'DPS20503', 42000, 37000, 0, 42400, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1635, 5, 'Banjarangkan', 'DPS20504', 42000, 37000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1636, 9, 'Singaraja', 'DPS20600', 33000, 29000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1637, 9, 'Banjar ', 'DPS20601', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1638, 9, 'Busung Biu', 'DPS20602', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1639, 9, 'Gerokgak', 'DPS20603', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1640, 9, 'Kubutambahan ', 'DPS20604', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1641, 9, 'Sawan', 'DPS20605', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1642, 9, 'Seririt', 'DPS20606', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1643, 9, 'Sukasada', 'DPS20607', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1644, 9, 'Tejakula', 'DPS20608', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1645, 9, 'Buleleng', 'DPS20609', 42000, 37000, 0, 31600, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1646, 4, 'Tabanan', 'DPS20700', 33000, 29000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1647, 4, 'Baturiti', 'DPS20701', 42000, 37000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1648, 4, 'Kediri ', 'DPS20702', 42000, 37000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1649, 4, 'Kerambitan', 'DPS20703', 42000, 37000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1650, 4, 'Marga', 'DPS20704', 42000, 37000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1651, 4, 'Pupuan', 'DPS20705', 42000, 37000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1652, 4, 'Penebel', 'DPS20706', 42000, 37000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1653, 4, 'Selemadeg', 'DPS20707', 42000, 37000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1654, 4, 'Selemadeg Barat', 'DPS20709', 42000, 37000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1655, 4, 'Selemadeg Timur', 'DPS20710', 42000, 37000, 0, 53200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1656, 8, 'Menguwi', 'DPS21100', 33000, 29000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1657, 8, 'Abiansemal', 'DPS21101', 42000, 37000, 0, 42400, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1658, 8, 'Kuta', 'DPS21102', 28000, 25000, 31000, 20400, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1659, 8, 'Legian ', 'DPS21103', 28000, 25000, 31000, 0, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1660, 8, 'Ngurah Rai', 'DPS21104', 28000, 25000, 31000, 0, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1661, 8, 'Sanur', 'DPS21105', 28000, 25000, 31000, 0, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1662, 8, 'Jimbaran', 'DPS21106', 28000, 25000, 31000, 0, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1663, 8, 'Nusa Dua', 'DPS21107', 28000, 25000, 31000, 0, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1664, 8, 'Petang', 'DPS21108', 42000, 37000, 0, 63200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1665, 40, 'Gorontalo', 'GTO10000', 58000, 50000, 0, 74400, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1666, 40, 'Dungingi', 'GTO10008', 58000, 50000, 0, 74400, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1667, 40, 'Kota Barat', 'GTO10009', 58000, 50000, 0, 74400, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1668, 40, 'Kota Selatan', 'GTO10010', 58000, 50000, 0, 74400, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1669, 40, 'Kota Timur', 'GTO10011', 58000, 50000, 0, 74400, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1670, 40, 'Kota Utara', 'GTO10012', 58000, 50000, 0, 74400, 52000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1671, 43, 'Limboto', 'GTO20100', 64000, 55000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1672, 43, 'Atinggola', 'GTO20101', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1673, 43, 'Batudaa', 'GTO20102', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1674, 43, 'Sumalata', 'GTO20108', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1675, 43, 'Tibawa', 'GTO20109', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1676, 43, 'Anggrek', 'GTO20111', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1677, 43, 'Boliyohuto', 'GTO20112', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1678, 43, 'Bongomeme', 'GTO20113', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1679, 43, 'Limboto Barat', 'GTO20114', 64000, 55000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1680, 43, 'Mootilango', 'GTO20115', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1681, 43, 'Pulubala', 'GTO20116', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1682, 43, 'Telaga', 'GTO20117', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1683, 43, 'Tolangohula', 'GTO20118', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1684, 43, 'Tolinggula', 'GTO20119', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1685, 39, 'Tilamuta', 'GTO20200', 64000, 55000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1686, 39, 'Botumoito', 'GTO20201', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1687, 39, 'Dulupi', 'GTO20202', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1688, 39, 'Mananggu', 'GTO20203', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1689, 39, 'Paguyaman', 'GTO20204', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1690, 39, 'Wonosari', 'GTO20206', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1691, 42, 'Suwawa', 'GTO20300', 64000, 55000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1692, 42, 'Bone Panta', 'GTO20301', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1693, 42, 'Kabila', 'GTO20302', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1694, 42, 'Tapa', 'GTO20303', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1695, 38, 'Kwandang', 'GTO20400', 64000, 55000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1696, 38, 'Anggrek', 'GTO20401', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1697, 38, 'Atinggota', 'GTO20402', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1698, 38, 'Sumalata', 'GTO20403', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1699, 38, 'Tolingula', 'GTO20404', 80000, 69000, 0, 90800, 58000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1700, 41, 'Marisa', 'GTO20500', 64000, 55000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1701, 41, 'Lemito', 'GTO20501', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1702, 41, 'Paguat', 'GTO20502', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1703, 41, 'Patilanggio', 'GTO20503', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1704, 41, 'Popayato', 'GTO20504', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1705, 41, 'Randangan', 'GTO20505', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1706, 41, 'Taluditi', 'GTO20506', 80000, 69000, 0, 90800, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1707, 123, 'Jember', 'JBR10000', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1708, 123, 'Ambulu', 'JBR10001', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1709, 123, 'Arjasa ', 'JBR10002', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1710, 123, 'Balung', 'JBR10003', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1711, 123, 'Bangsalsari', 'JBR10004', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1712, 123, 'Gumukmas', 'JBR10005', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1713, 123, 'Jelbuk', 'JBR10006', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1714, 123, 'Jenggawah ', 'JBR10007', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1715, 123, 'Kalisat', 'JBR10008', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1716, 123, 'Kencong', 'JBR10009', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1717, 123, 'Ledokombo ', 'JBR10010', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1718, 123, 'Mayang', 'JBR10011', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1719, 123, 'Mumbulsari', 'JBR10012', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1720, 123, 'Pakusari', 'JBR10013', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1721, 123, 'Panti', 'JBR10014', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1722, 123, 'Puger', 'JBR10015', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1723, 123, 'Rambipuji', 'JBR10016', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1724, 123, 'Silo', 'JBR10017', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1725, 123, 'Sukorambi', 'JBR10018', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1726, 123, 'Sukowono', 'JBR10019', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1727, 123, 'Sumberbaru', 'JBR10020', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1728, 123, 'Sumberjambe', 'JBR10021', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1729, 123, 'Tanggul', 'JBR10022', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1730, 123, 'Tempurejo', 'JBR10023', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1731, 123, 'Umbulsari', 'JBR10024', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1732, 123, 'Wuluhan', 'JBR10025', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1733, 123, 'Ajung', 'JBR10026', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1734, 123, 'Jombang ', 'JBR10027', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1735, 123, 'Kaliwates', 'JBR10028', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1736, 123, 'Patrang', 'JBR10029', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1737, 123, 'Semboro', 'JBR10030', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1738, 123, 'Sumbersari', 'JBR10031', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1739, 140, 'Banyuwangi', 'JBR20100', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1740, 140, 'Bangorejo', 'JBR20101', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1741, 140, 'Cluring', 'JBR20102', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1742, 140, 'Gambiran', 'JBR20103', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1743, 140, 'Genteng ', 'JBR20104', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1744, 140, 'Glenmore', 'JBR20105', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1745, 140, 'Kabat', 'JBR20106', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1746, 140, 'Kalibaru', 'JBR20107', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1747, 140, 'Muncar', 'JBR20108', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1748, 140, 'Pesanggaran', 'JBR20109', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1749, 140, 'Purwoharjo', 'JBR20110', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1750, 140, 'Rogojampi', 'JBR20111', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1751, 140, 'Singojuruh', 'JBR20112', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1752, 140, 'Songgon', 'JBR20113', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1753, 140, 'Srono', 'JBR20114', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1754, 140, 'Tegaldlimo', 'JBR20115', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1755, 140, 'Wongsorejo', 'JBR20116', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1756, 140, 'Glagah', 'JBR20117', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1757, 140, 'Giri', 'JBR20118', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1758, 140, 'Kalipuro', 'JBR20119', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1759, 140, 'Licin', 'JBR20120', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1760, 140, 'Sempu', 'JBR20121', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1761, 140, 'Siliragung', 'JBR20122', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1762, 140, 'Tegalsari', 'JBR20123', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1763, 157, 'Bondowoso', 'JBR20200', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1764, 157, 'Cermee', 'JBR20201', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1765, 157, 'Curahdami', 'JBR20202', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1766, 157, 'Grujugan', 'JBR20203', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1767, 157, 'Klabang', 'JBR20204', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1768, 157, 'Maesan', 'JBR20205', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1769, 157, 'Pakem ', 'JBR20206', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1770, 157, 'Prajekan', 'JBR20207', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1771, 157, 'Pujer', 'JBR20208', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1772, 157, 'Sempol', 'JBR20209', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1773, 157, 'Sukosari', 'JBR20210', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1774, 157, 'Tamanan', 'JBR20211', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1775, 157, 'Tapen', 'JBR20212', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1776, 157, 'Tegalampel', 'JBR20213', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1777, 157, 'Tenggarang', 'JBR20214', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1778, 157, 'Tlogosari', 'JBR20215', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1779, 157, 'Wonosari', 'JBR20216', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1780, 157, 'Wringin', 'JBR20217', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1781, 157, 'Binakal', 'JBR20218', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1782, 157, 'Sumberwringin', 'JBR20219', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1783, 35, 'Yogyakarta', 'JOG10000', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1784, 35, 'Tegalrejo', 'JOG10001', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1785, 35, 'Jetis ', 'JOG10002', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1786, 35, 'Danurejan', 'JOG10003', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1787, 35, 'Gedongtengen', 'JOG10004', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1788, 35, 'Gondokusuman', 'JOG10005', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1789, 35, 'Gondomanan', 'JOG10006', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1790, 35, 'Kotagede', 'JOG10007', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1791, 35, 'Kraton ', 'JOG10008', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1792, 35, 'Mantrijeron', 'JOG10009', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1793, 35, 'Mergangsan', 'JOG10010', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1794, 35, 'Ngampilan', 'JOG10011', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1795, 35, 'Pakualaman', 'JOG10012', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1796, 35, 'Umbulharjo', 'JOG10013', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1797, 35, 'Wirobrajan', 'JOG10014', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1798, 37, 'Bantul', 'JOG20100', 19000, 17000, 29000, 43600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1799, 37, 'Bambanglipuro', 'JOG20101', 31000, 27000, 35000, 43600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1800, 37, 'Dlingo', 'JOG20102', 31000, 27000, 35000, 29200, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1801, 37, 'Imogiri', 'JOG20103', 31000, 27000, 35000, 29200, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1802, 37, 'Jetis ', 'JOG20104', 31000, 27000, 35000, 29200, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1803, 37, 'Kretek', 'JOG20105', 31000, 27000, 35000, 43600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1804, 37, 'Pajangan', 'JOG20106', 31000, 27000, 35000, 29200, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1805, 37, 'Pandak', 'JOG20107', 31000, 27000, 35000, 29200, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1806, 37, 'Piyungan', 'JOG20108', 31000, 27000, 35000, 29200, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1807, 37, 'Pleret', 'JOG20109', 31000, 27000, 35000, 29200, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1808, 37, 'Pundong', 'JOG20110', 31000, 27000, 35000, 43600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1809, 37, 'Sanden', 'JOG20111', 31000, 27000, 35000, 43600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1810, 37, 'Sedayu', 'JOG20112', 31000, 27000, 35000, 29200, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1811, 37, 'Srandakan', 'JOG20113', 31000, 27000, 35000, 43600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1812, 37, 'Banguntapan', 'JOG20114', 31000, 27000, 35000, 29200, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1813, 37, 'Kasihan', 'JOG20115', 31000, 27000, 35000, 29200, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1814, 37, 'Sewon', 'JOG20116', 31000, 27000, 35000, 29200, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1815, 34, 'Sleman', 'JOG20300', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1816, 34, 'Berbah', 'JOG20301', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1817, 34, 'Cangkringan', 'JOG20302', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1818, 34, 'Godean', 'JOG20303', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1819, 34, 'Kalasan', 'JOG20304', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1820, 34, 'Minggir', 'JOG20305', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1821, 34, 'Moyudan', 'JOG20306', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1822, 34, 'Ngaglik', 'JOG20307', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1823, 34, 'Ngemplak ', 'JOG20308', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1824, 34, 'Pakem ', 'JOG20309', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1825, 34, 'Prambanan', 'JOG20310', 19000, 17000, 29000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1826, 34, 'Seyegan', 'JOG20311', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1827, 34, 'Tempel', 'JOG20312', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1828, 34, 'Turi', 'JOG20313', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1829, 34, 'Depok ', 'JOG20314', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1830, 34, 'Gamping', 'JOG20315', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1831, 34, 'Mlati', 'JOG20316', 31000, 27000, 35000, 15600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1832, 36, 'Wates', 'JOG20400', 25000, 21000, 29000, 43600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1833, 36, 'Galur', 'JOG20401', 31000, 27000, 0, 26400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1834, 36, 'Girimulyo', 'JOG20402', 31000, 27000, 0, 26400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1835, 36, 'Kalibawang ', 'JOG20403', 31000, 27000, 0, 26400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1836, 36, 'Kokap', 'JOG20404', 31000, 27000, 0, 26400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1837, 36, 'Lendah', 'JOG20405', 31000, 27000, 0, 26400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1838, 36, 'Nanggulan', 'JOG20406', 31000, 27000, 0, 26400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1839, 36, 'Panjatan', 'JOG20407', 31000, 27000, 0, 26400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1840, 36, 'Pengasih', 'JOG20408', 31000, 27000, 0, 26400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1841, 36, 'Samigaluh', 'JOG20409', 31000, 27000, 0, 26400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1842, 36, 'Sentolo', 'JOG20410', 31000, 27000, 0, 26400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1843, 36, 'Temon', 'JOG20411', 31000, 27000, 0, 43600, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1844, 33, 'Wonosari', 'JOG20500', 25000, 21000, 29000, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1845, 33, 'Karangmojo', 'JOG20501', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1846, 33, 'Ngawen ', 'JOG20502', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1847, 33, 'Nglipar', 'JOG20503', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1848, 33, 'Paliyan', 'JOG20504', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1849, 33, 'Panggang', 'JOG20505', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1850, 33, 'Patuk', 'JOG20506', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1851, 33, 'Playen', 'JOG20507', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1852, 33, 'Ponjong', 'JOG20508', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1853, 33, 'Rongkop', 'JOG20509', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1854, 33, 'Semin', 'JOG20510', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1855, 33, 'Semanu', 'JOG20511', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1856, 33, 'Tepus', 'JOG20512', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1857, 33, 'Gedangsari', 'JOG20513', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1858, 33, 'Girisubo', 'JOG20514', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1859, 33, 'Purwosari ', 'JOG20515', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1860, 33, 'Saptosari', 'JOG20516', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1861, 33, 'Tanjungsari ', 'JOG20517', 31000, 27000, 0, 48400, 17000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1862, 392, 'Kendari', 'KDI10000', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1863, 392, 'Abeli', 'KDI10014', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1864, 392, 'Baruga', 'KDI10015', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1865, 392, 'Kendar', 'KDI10016', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1866, 392, 'Barat ', 'KDI10017', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1867, 392, 'Mandonga', 'KDI10018', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1868, 392, 'Poasia', 'KDI10019', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1869, 387, 'Bau - Bau', 'KDI20100', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1870, 387, 'Bungi', 'KDI20103', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1871, 387, 'Betoambari', 'KDI20120', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1872, 387, 'Kokalukuna', 'KDI20121', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1873, 387, 'Murhum', 'KDI20122', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1874, 387, 'Sorowalio', 'KDI20123', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1875, 387, 'Wolio', 'KDI20124', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1876, 391, 'Kolaka', 'KDI20200', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1877, 391, 'Ladongi', 'KDI20202', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1878, 391, 'Mowewe', 'KDI20204', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1879, 391, 'Pomalaa', 'KDI20206', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1880, 391, 'Tirawuta', 'KDI20209', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1881, 391, 'Watubangga', 'KDI20210', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1882, 391, 'Wolo', 'KDI20211', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1883, 391, 'Wundulako', 'KDI20212', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1884, 391, 'Baula', 'KDI20213', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1885, 391, 'Lambadia', 'KDI20214', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1886, 391, 'Latambaga', 'KDI20215', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1887, 391, 'Samaturu', 'KDI20216', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1888, 391, 'Tanggetada', 'KDI20217', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1889, 391, 'Uluiwoi', 'KDI20218', 138000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1890, 386, 'Raha', 'KDI20300', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1891, 386, 'Bonegunu', 'KDI20301', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1892, 386, 'Duruka Bone', 'KDI20302', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1893, 386, 'Kabangka', 'KDI20304', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1894, 386, 'Kabawo', 'KDI20305', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1895, 386, 'Kulisusu', 'KDI20306', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1896, 386, 'Lawa', 'KDI20307', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1897, 386, 'Lohia', 'KDI20308', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1898, 386, 'Napabalano', 'KDI20309', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1899, 386, 'Parigi ', 'KDI20310', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1900, 386, 'Sawerigadi', 'KDI20312', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1901, 386, 'Tongkuno', 'KDI20314', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1902, 386, 'Wakorumba Selatan', 'KDI20316', 138000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1903, 386, 'Batalaiworu', 'KDI20318', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1904, 386, 'Katobu', 'KDI20319', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1905, 386, 'Kontunaga', 'KDI20320', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1906, 386, 'Kulisusu Barat', 'KDI20321', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1907, 386, 'Kulisusu Utara', 'KDI20322', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1908, 386, 'Kusambi', 'KDI20323', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1909, 386, 'Lasalepa', 'KDI20324', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1910, 386, 'Maginti', 'KDI20325', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1911, 386, 'Maligano', 'KDI20326', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1912, 386, 'Pasir Putih', 'KDI20327', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1913, 386, 'Tikep', 'KDI20328', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1914, 386, 'Tiworo Tengah', 'KDI20329', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1915, 386, 'Wakorumba', 'KDI20330', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1916, 386, 'Barangka', 'KDI20332', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1917, 386, 'Kambowa', 'KDI20333', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1918, 386, 'Watopute', 'KDI20334', 92000, 79000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (1919, 390, 'Unaaha', 'KDI20400', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1920, 390, 'Abuki', 'KDI20401', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1921, 390, 'Lambuya', 'KDI20402', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1922, 390, 'Pondidaha', 'KDI20403', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1923, 390, 'Wawotobi', 'KDI20404', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1924, 390, 'Asera', 'KDI20405', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1925, 390, 'Lasolo', 'KDI20406', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1926, 390, 'Soropia', 'KDI20407', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1927, 390, 'Wawonii', 'KDI20408', 138000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1928, 390, 'Waworete', 'KDI20409', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1929, 390, 'Bondoala Sampara', 'KDI20410', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1930, 390, 'Latoma', 'KDI20411', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1931, 390, 'Sawa', 'KDI20412', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1932, 390, 'Tongauna', 'KDI20413', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1933, 390, 'Uepai', 'KDI20414', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1934, 390, 'Wonggeduku', 'KDI20415', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1935, 395, 'Rumbia', 'KDI20500', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1936, 395, 'Kabaena', 'KDI20501', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1937, 395, 'Kabaena Timur ', 'KDI20502', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1938, 395, 'Poleang', 'KDI20503', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1939, 395, 'Poleang Barat', 'KDI20504', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1940, 395, 'Poleang Timur', 'KDI20505', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1941, 395, 'Rarowatu', 'KDI20506', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1942, 389, 'Pasar Wajo', 'KDI20600', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1943, 389, 'Batauga', 'KDI20601', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1944, 389, 'Batu Atas', 'KDI20602', 138000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1945, 389, 'Gu', 'KDI20603', 138000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1946, 389, 'Kadatua', 'KDI20604', 138000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1947, 389, 'Kapontori', 'KDI20605', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1948, 389, 'Lakudo', 'KDI20606', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1949, 389, 'Lasalimu', 'KDI20607', 138000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1950, 389, 'Lasalimu Selatan', 'KDI20608', 138000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1951, 389, 'Mawasangka', 'KDI20609', 138000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1952, 389, 'Mawasangka Timur', 'KDI20610', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1953, 389, 'Sampolawa', 'KDI20611', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1954, 389, 'Siompu', 'KDI20612', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1955, 389, 'Talaga Raya', 'KDI20613', 138000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1956, 389, 'Buranga', 'KDI20614', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1957, 394, 'Lasusua', 'KDI20700', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1958, 394, 'Batu Putih ', 'KDI20701', 138000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1959, 394, 'Kodeoha', 'KDI20702', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1960, 394, 'Ngapa', 'KDI20703', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1961, 394, 'Pakue', 'KDI20704', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1962, 394, 'Ranteangin', 'KDI20705', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1963, 388, 'Andolo/Wanggodo', 'KDI20800', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1964, 388, 'Angata', 'KDI20801', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1965, 388, 'Kolono', 'KDI20802', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1966, 388, 'Konda', 'KDI20803', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1967, 388, 'Lainea', 'KDI20804', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1968, 388, 'Landono', 'KDI20805', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1969, 388, 'Laonti', 'KDI20806', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1970, 388, 'Moramo', 'KDI20807', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1971, 388, 'Palangga', 'KDI20808', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1972, 388, 'Ranomeeto', 'KDI20809', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1973, 388, 'Tinanggea', 'KDI20810', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1974, 393, 'Wangi-Wangi', 'KDI20900', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1975, 393, 'Wangi-Wangi Selatan', 'KDI20901', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1976, 393, 'Kaledupa', 'KDI20902', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1977, 393, 'Tomia', 'KDI20903', 138000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1978, 393, 'Binongko', 'KDI20904', 138000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1979, 130, 'Kediri', 'KDR10000', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1980, 148, 'Gampengrejo', 'KDR10001', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1981, 148, 'Grogol ', 'KDR10002', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1982, 148, 'Gurah', 'KDR10003', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1983, 148, 'Kandat', 'KDR10004', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1984, 148, 'Kras', 'KDR10005', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1985, 148, 'Kunjang', 'KDR10006', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1986, 148, 'Mojo', 'KDR10007', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1987, 148, 'Ngadiluwih', 'KDR10008', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1988, 148, 'Pagu', 'KDR10009', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1989, 148, 'Papar', 'KDR10010', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1990, 148, 'Plemahan', 'KDR10011', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1991, 148, 'Plosoklaten', 'KDR10012', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1992, 148, 'Purwoasri', 'KDR10013', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1993, 148, 'Semen', 'KDR10014', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1994, 148, 'Tarokan', 'KDR10015', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1995, 148, 'Wates', 'KDR10016', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1996, 148, 'Pare', 'KDR10017', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1997, 148, 'Kandangan', 'KDR10018', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1998, 148, 'Kepung', 'KDR10019', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (1999, 148, 'Ngancar', 'KDR10020', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2000, 148, 'Puncu', 'KDR10021', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2001, 148, 'Banyakan', 'KDR10022', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2002, 148, 'Kayen Kidul', 'KDR10023', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2003, 148, 'Ringinrejo', 'KDR10024', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2004, 130, 'Mojoroto', 'KDR10025', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2005, 130, 'Pesantren', 'KDR10026', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2006, 285, 'Kupang', 'KOE10000', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2007, 285, 'Alak', 'KOE10029', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2008, 285, 'Kelapa Lima', 'KOE10030', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2009, 285, 'Mawlapa', 'KOE10031', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2010, 285, 'Oebobo', 'KOE10032', 54000, 47000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2011, 294, 'Waibakul', 'KOE10100', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2012, 294, 'Mamboro', 'KOE10101', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2013, 294, 'Umbu Ratu Nggay', 'KOE10102', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2014, 294, 'Umbu Ratu Nggay Barat', 'KOE10103', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2015, 294, 'Katikutana', 'KOE10104', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2016, 283, 'Atambua', 'KOE20100', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2017, 283, 'Kobalima', 'KOE20101', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2018, 283, 'Lamakmen', 'KOE20102', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2019, 283, 'Malaka Timur ', 'KOE20103', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2020, 283, 'Malaka Tengah', 'KOE20104', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2021, 283, 'Malaka Barat', 'KOE20106', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2022, 283, 'Tasefeto Barat', 'KOE20108', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2023, 283, 'Tasifeto Timur', 'KOE20109', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2024, 283, 'Kakuluk Mesak', 'KOE20110', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2025, 283, 'Raihat', 'KOE20111', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2026, 283, 'Rinhat', 'KOE20112', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2027, 283, 'Sasita Mean ', 'KOE20113', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2028, 292, 'Kalabahi', 'KOE20200', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2029, 292, 'Alor Timur', 'KOE20201', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2030, 292, 'Pantar', 'KOE20202', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2031, 292, 'Alor Selatan', 'KOE20203', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2032, 292, 'Alor Barat Daya', 'KOE20204', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2033, 292, 'Alor Barat Laut', 'KOE20205', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2034, 292, 'Alor Tengah Utara', 'KOE20206', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2035, 292, 'Alor Timur Laut', 'KOE20207', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2036, 292, 'Pantar Barat', 'KOE20208', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2037, 292, 'Teluk Mutiara', 'KOE20209', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2038, 301, 'Kefamenanu', 'KOE20300', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2039, 301, 'Biboki Selatan', 'KOE20301', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2040, 301, 'Biboki Utara', 'KOE20302', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2041, 301, 'Insana', 'KOE20303', 102000, 87000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (2042, 301, 'Miomafo Timur', 'KOE20304', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2043, 301, 'Miomafo Barat', 'KOE20305', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2044, 301, 'Biboki Anleu', 'KOE20306', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2045, 301, 'Insana Utara', 'KOE20307', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2046, 301, 'Neomuti', 'KOE20309', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2047, 290, 'Larantuka', 'KOE20400', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2048, 290, 'Adonara Barat', 'KOE20401', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2049, 290, 'Adonara Timur', 'KOE20402', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2050, 290, 'Solor Barat', 'KOE20409', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2051, 290, 'Solor Timur', 'KOE20410', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2052, 290, 'Tanjung Bunga', 'KOE20411', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2053, 290, 'Wulanggitan', 'KOE20412', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2054, 290, 'Ile Boleng', 'KOE20414', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2055, 290, 'Ile Mandiri', 'KOE20415', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2056, 290, 'Kelubagolit', 'KOE20416', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2057, 290, 'Titihena', 'KOE20417', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2058, 290, 'Witihama', 'KOE20418', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2059, 290, 'Wotan Ulu mado', 'KOE20419', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2060, 299, 'Maumere', 'KOE20500', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2061, 299, 'Bola', 'KOE20501', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2062, 299, 'Kewapante', 'KOE20502', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2063, 299, 'Lela', 'KOE20503', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2064, 299, 'Nitta', 'KOE20504', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2065, 299, 'Paga', 'KOE20505', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2066, 299, 'Talibura', 'KOE20506', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2067, 299, 'Alok', 'KOE20507', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2068, 299, 'Mego', 'KOE20508', 152000, 0, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (2069, 299, 'Palue', 'KOE20509', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2070, 299, 'Waigete', 'KOE20510', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2071, 288, 'Ruteng', 'KOE20600', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2072, 288, 'Cibal', 'KOE20603', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2073, 288, 'Lambaleda', 'KOE20611', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2074, 288, 'Reo', 'KOE20613', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2075, 288, 'Satarmese', 'KOE20614', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2076, 288, 'Komba', 'KOE20616', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2077, 288, 'Mborong', 'KOE20617', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2078, 288, 'Ponco Ranaka', 'KOE20618', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2079, 288, 'Sambi Rambas', 'KOE20619', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2080, 288, 'Wae Rii', 'KOE20620', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2081, 297, 'Soe', 'KOE20700', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2082, 297, 'Amanatun Utara', 'KOE20701', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2083, 297, 'Amanatun Selatan', 'KOE20702', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2084, 297, 'Amanuban Selatan', 'KOE20704', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2085, 297, 'Amanuban Barat', 'KOE20706', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2086, 297, 'Mollo Utara', 'KOE20707', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2087, 297, 'Mollo Selatan', 'KOE20708', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2088, 297, 'Amanatun Tengah', 'KOE20709', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2089, 297, 'Amanatun Timur', 'KOE20710', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2090, 297, 'Batu Putih ', 'KOE20711', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2091, 297, 'Boking', 'KOE20712', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2092, 297, 'Fatumnasi', 'KOE20713', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2093, 297, 'Kalbano', 'KOE20714', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2094, 297, 'Kie', 'KOE20715', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2095, 297, 'Kot\'olin', 'KOE20716', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2096, 297, 'Kualin', 'KOE20717', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2097, 297, 'Kuan Fatu', 'KOE20718', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2098, 297, 'Nunkolo', 'KOE20719', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2099, 297, 'Oenino', 'KOE20720', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2100, 297, 'Polen', 'KOE20721', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2101, 297, 'Tionas', 'KOE20722', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2102, 286, 'Ende', 'KOE20800', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2103, 286, 'Detusoko', 'KOE20801', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2104, 286, 'Nanga Panda', 'KOE20803', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2105, 286, 'Ndona', 'KOE20804', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2106, 286, 'Wolo Waru', 'KOE20805', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2107, 286, 'Detukeli', 'KOE20806', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2108, 286, 'Ende Selatan', 'KOE20807', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2109, 286, 'Ende Tengah', 'KOE20808', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2110, 286, 'Ende Timur', 'KOE20809', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2111, 286, 'Ende Utara', 'KOE20810', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2112, 286, 'Kelimutu', 'KOE20811', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2113, 286, 'Kotabaru ', 'KOE20812', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2114, 286, 'Lio Timur', 'KOE20813', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2115, 286, 'Maukaro', 'KOE20814', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2116, 286, 'Ndona Timur', 'KOE20815', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2117, 286, 'Pulau Ende', 'KOE20816', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2118, 286, 'Wewaria', 'KOE20817', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2119, 286, 'Wolojita', 'KOE20818', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2120, 286, 'Magekoba/Maurole', 'KOE20820', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2121, 295, 'Bajawa', 'KOE20900', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2122, 295, 'Aimere', 'KOE20902', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2123, 295, 'Riung', 'KOE20907', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2124, 295, 'Jere Buu', 'KOE20908', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2125, 295, 'Ngada Bawa', 'KOE20912', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2126, 295, 'Riung Barat', 'KOE20913', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2127, 295, 'Soa', 'KOE20914', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2128, 295, 'Wogomang Ulewa', 'KOE20915', 102000, 87000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2129, 295, 'Golewa', 'KOE20917', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2130, 284, 'Waingapu', 'KOE21000', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2131, 284, 'Lewa', 'KOE21001', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2132, 284, 'Paberiwai', 'KOE21002', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2133, 284, 'Pahunga Lodu', 'KOE21003', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2134, 284, 'Tabundung', 'KOE21005', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2135, 284, 'Haharu', 'KOE21006', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2136, 284, 'Kahaungu Eti', 'KOE21007', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2137, 284, 'Karera', 'KOE21008', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2138, 284, 'Matawai Lapau', 'KOE21010', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2139, 284, 'Nggaha Oriangu', 'KOE21011', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2140, 284, 'Pandawai', 'KOE21012', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2141, 284, 'Pinu Pahar', 'KOE21013', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2142, 284, 'Rindi', 'KOE21014', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2143, 284, 'Umalulu', 'KOE21015', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2144, 284, 'Wulla Waijelu', 'KOE21016', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2145, 293, 'Waikabubak', 'KOE21100', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2146, 293, 'Laratama', 'KOE21103', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2147, 293, 'Walakaka', 'KOE21104', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2148, 302, 'Labuan Bajo', 'KOE21200', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2149, 302, 'Komodo', 'KOE21201', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2150, 302, 'Kuwus', 'KOE21202', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2151, 302, 'Lembor', 'KOE21203', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2152, 302, 'Macang Pacar', 'KOE21204', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2153, 302, 'Sanonggoang', 'KOE21205', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2154, 291, 'Borong', 'KOE21300', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2155, 291, 'Elar', 'KOE21301', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2156, 291, 'Lamba Leda', 'KOE21302', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2157, 291, 'Poco Ranaka', 'KOE21303', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2158, 291, 'Lomba', 'KOE21304', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2159, 291, 'Sambi Rampas', 'KOE21305', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2160, 300, 'Mbay', 'KOE21400', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2161, 300, 'Aesesa', 'KOE21401', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2162, 300, 'Boawae', 'KOE21403', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2163, 300, 'Mauponggo', 'KOE21404', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2164, 300, 'Nangaroro', 'KOE21405', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2165, 300, 'Keo Tengah', 'KOE21406', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2166, 300, 'Wolo Wae', 'KOE21407', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2167, 300, 'Aesesa Selatan', 'KOE21408', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2168, 289, 'Baa', 'KOE21500', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2169, 289, 'Lobalain', 'KOE21501', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2170, 289, 'Pantai Baru', 'KOE21502', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2171, 289, 'Rote Barat Daya', 'KOE21503', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2172, 289, 'Rote Barat Laut', 'KOE21504', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2173, 289, 'Rote Tengah', 'KOE21505', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2174, 289, 'Rote Timur', 'KOE21506', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2175, 298, 'Tambolaka', 'KOE21600', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2176, 298, 'Kodi', 'KOE21601', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2177, 298, 'Wewewa Barat', 'KOE21602', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2178, 298, 'Wewewa Timur', 'KOE21603', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2179, 298, 'Kodi Bangedo', 'KOE21604', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2180, 298, 'Kodi Utara', 'KOE21605', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2181, 298, 'Laura', 'KOE21606', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2182, 298, 'Wewewa Selatan', 'KOE21607', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2183, 298, 'Wewewa Utara', 'KOE21608', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2184, 287, 'Lembata', 'KOE21800', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2185, 287, 'Buyasuri', 'KOE21801', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2186, 287, 'Omesuri', 'KOE21802', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2187, 287, 'Lebatukan', 'KOE21803', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2188, 287, 'Ile Ape', 'KOE21804', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2189, 287, 'Nubatukan', 'KOE21805', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2190, 287, 'Atadei', 'KOE21806', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2191, 287, 'Nagawutung', 'KOE21807', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2192, 287, 'Wulandoni', 'KOE21808', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2193, 296, 'Oelamasi', 'KOE21900', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2194, 296, 'Amabioefeto Timur ', 'KOE21901', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2195, 296, 'Amarasi Barat', 'KOE21902', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2196, 296, 'Amarasi Selatan', 'KOE21903', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2197, 296, 'Amarasi Timur', 'KOE21904', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2198, 296, 'Amarasi', 'KOE21905', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2199, 296, 'Amfoang Barat Daya', 'KOE21906', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2200, 296, 'Amfoang Barat Laut', 'KOE21907', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2201, 296, 'Amfoang Selatan', 'KOE21908', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2202, 296, 'Amfoang Utara', 'KOE21909', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2203, 296, 'Fatuleu', 'KOE21910', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2204, 296, 'Fatuleu Barat', 'KOE21911', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2205, 296, 'Fatuleu Tengah', 'KOE21912', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2206, 296, 'Hawu Mehara', 'KOE21913', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2207, 296, 'Kupang Barat', 'KOE21914', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2208, 296, 'Kupang Tengah', 'KOE21915', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2209, 296, 'Kupang Timur', 'KOE21916', 81000, 70000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2210, 296, 'Nekemese', 'KOE21917', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2211, 296, 'Raijua', 'KOE21918', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2212, 296, 'Sabu Barat', 'KOE21919', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2213, 296, 'Sabu Liae', 'KOE21920', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2214, 296, 'Sabu Timur', 'KOE21921', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2215, 296, 'Semau', 'KOE21922', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2216, 296, 'Semau Selatan', 'KOE21923', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2217, 296, 'Sulamu', 'KOE21924', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2218, 296, 'Taebenu', 'KOE21925', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2219, 296, 'Takari', 'KOE21926', 152000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2220, 66, 'Karawang', 'KRW10000', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2221, 66, 'Batujaya', 'KRW10001', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2222, 66, 'Cibuaya', 'KRW10002', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2223, 66, 'Cikampek', 'KRW10003', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2224, 66, 'Cilamaya Kulon', 'KRW10004', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2225, 66, 'Jatisari', 'KRW10005', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2226, 66, 'Klari', 'KRW10006', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2227, 66, 'LEMAHABANG WADAS', 'KRW10007', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2228, 66, 'Pakisjaya', 'KRW10008', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2229, 66, 'Pangkalan', 'KRW10009', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2230, 66, 'Pedes', 'KRW10010', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2231, 66, 'Rawamerta', 'KRW10011', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2232, 66, 'Rengasdengklok', 'KRW10012', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2233, 66, 'Talagasari', 'KRW10013', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2234, 66, 'Telukjambe Barat', 'KRW10014', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2235, 66, 'Tempuran ', 'KRW10015', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2236, 66, 'Tirtamulya', 'KRW10016', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2237, 66, 'Tegalwaru ', 'KRW10017', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2238, 66, 'Banyusari', 'KRW10018', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2239, 66, 'Ciampel', 'KRW10019', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2240, 66, 'Cilamaya Wetan', 'KRW10020', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2241, 66, 'Cilebar', 'KRW10021', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2242, 66, 'Jayakerta', 'KRW10022', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2243, 66, 'Karawang Barat', 'KRW10023', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2244, 66, 'Karawang Timur', 'KRW10024', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2245, 66, 'Kotabaru ', 'KRW10025', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2246, 66, 'Kutawaluya', 'KRW10026', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2247, 66, 'Majalaya', 'KRW10027', 16000, 14000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2248, 66, 'Purwasari', 'KRW10028', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2249, 66, 'Telukjambe Timur ', 'KRW10029', 16000, 14000, 21000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2250, 66, 'Tirtajaya', 'KRW10030', 19000, 17000, 0, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2251, 402, 'Manado', 'MDC10000', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2252, 402, 'Bunaken', 'MDC10021', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2253, 402, 'Malalayang', 'MDC10022', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2254, 402, 'Mapanget', 'MDC10023', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2255, 402, 'Sario', 'MDC10024', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2256, 402, 'Singkil', 'MDC10025', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2257, 402, 'Tikala', 'MDC10026', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2258, 402, 'Tuminting', 'MDC10027', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2259, 402, 'Wanea', 'MDC10028', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2260, 402, 'Wenang', 'MDC10029', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2261, 396, 'Kotamobagu', 'MDC20100', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2262, 396, 'Bintauna', 'MDC20101', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2263, 396, 'Bolaang', 'MDC20102', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2264, 396, 'BOLAANG ITANG', 'MDC20103', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2265, 396, 'BOLAANG UKI', 'MDC20104', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2266, 396, 'Kaidipang', 'MDC20106', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2267, 396, 'Kotabunan', 'MDC20107', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2268, 396, 'Lolayan', 'MDC20108', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2269, 396, 'Lolak', 'MDC20109', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2270, 396, 'Modayag', 'MDC20110', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2271, 396, 'Passi', 'MDC20111', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2272, 396, 'Pinolosian', 'MDC20112', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2273, 396, 'Poigar', 'MDC20113', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2274, 396, 'Sangtombolang', 'MDC20114', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2275, 396, 'Dumoga Barat', 'MDC20115', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2276, 396, 'Dumoga Timur', 'MDC20116', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2277, 396, 'Dumoga Utara', 'MDC20117', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2278, 396, 'Nuangan', 'MDC20118', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2279, 396, 'Pinogaluman', 'MDC20119', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2280, 396, 'Posigadan', 'MDC20120', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2281, 401, 'Tahuna', 'MDC20200', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2282, 401, 'Kendahe', 'MDC20204', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2283, 401, 'Manganitu', 'MDC20206', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2284, 401, 'Siau Timur', 'MDC20209', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2285, 401, 'Siau Barat', 'MDC20210', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2286, 401, 'Tabukan Utara', 'MDC20211', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2287, 401, 'Tabukan Selatan', 'MDC20212', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2288, 401, 'Tabukan Tengah', 'MDC20213', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2289, 401, 'Tagulandang', 'MDC20214', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2290, 401, 'Tamako', 'MDC20215', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2291, 401, 'Biaro', 'MDC20216', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2292, 401, 'Manganitu Selatan', 'MDC20217', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2293, 401, 'Nusa Tabukan', 'MDC20218', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2294, 401, 'Siau Barat Selatan', 'MDC20219', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2295, 401, 'Siau Timur Selatan', 'MDC20220', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2296, 401, 'Tagulandang Utara', 'MDC20221', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2297, 401, 'Tatoareng', 'MDC20222', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2298, 406, 'Tondano', 'MDC20300', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2299, 406, 'Eris', 'MDC20302', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2300, 406, 'Kakas', 'MDC20303', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2301, 406, 'Kawangkoan', 'MDC20304', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2302, 406, 'Kombi', 'MDC20305', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2303, 406, 'Remboken', 'MDC20308', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2304, 406, 'Sonder', 'MDC20309', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2305, 406, 'Tompaso', 'MDC20311', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2306, 406, 'Langowan Barat', 'MDC20313', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2307, 406, 'Langowan Selatan', 'MDC20314', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2308, 406, 'Langowan Timur', 'MDC20315', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2309, 406, 'Lembean Timur', 'MDC20316', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2310, 406, 'Pineleng', 'MDC20317', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2311, 406, 'Tombariri', 'MDC20318', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2312, 406, 'Tombulu', 'MDC20319', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2313, 406, 'Tondano Barat', 'MDC20320', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2314, 406, 'Tondano Selatan', 'MDC20321', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2315, 406, 'Tondano Timur', 'MDC20322', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2316, 406, 'Tondano Utara', 'MDC20323', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2317, 400, 'Bitung', 'MDC20400', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2318, 400, 'Bitung Barat', 'MDC20401', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2319, 400, 'Bitung Selatan', 'MDC20402', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2320, 400, 'Bitung Tengah', 'MDC20403', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2321, 400, 'Bitung Timur', 'MDC20404', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2322, 400, 'Bitung Utara', 'MDC20405', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2323, 405, 'Boroko', 'MDC20600', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2324, 405, 'Bintauna', 'MDC20601', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2325, 405, 'Bolaang Itang Barat', 'MDC20602', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2326, 405, 'Bolaang Itang Timur', 'MDC20603', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2327, 405, 'Kaidipang', 'MDC20604', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2328, 405, 'Pinogaluman', 'MDC20605', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2329, 405, 'Sangkub', 'MDC20606', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2330, 399, 'Melonguane', 'MDC20700', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2331, 399, 'Beo', 'MDC20701', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2332, 399, 'Essang', 'MDC20702', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2333, 399, 'Gemeh', 'MDC20703', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2334, 399, 'Kabaruan', 'MDC20704', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2335, 399, 'Lirung', 'MDC20705', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2336, 399, 'Nanusa', 'MDC20707', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2337, 399, 'Rainis', 'MDC20708', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2338, 404, 'Airmadidi', 'MDC20800', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2339, 404, 'Dimembe', 'MDC20801', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2340, 404, 'Kalawat', 'MDC20802', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2341, 404, 'Kauditan', 'MDC20803', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2342, 404, 'Kema', 'MDC20804', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2343, 404, 'Likupang Barat', 'MDC20805', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2344, 404, 'Likupang Timur', 'MDC20806', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2345, 404, 'Wori', 'MDC20807', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2346, 398, 'Amurang', 'MDC20900', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2347, 398, 'Modoinding', 'MDC20901', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2348, 398, 'Motoling', 'MDC20902', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2349, 398, 'Ranoyapo', 'MDC20903', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2350, 398, 'Sinon Sayang', 'MDC20904', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2351, 398, 'Tareran', 'MDC20905', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2352, 398, 'Tenga', 'MDC20906', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2353, 398, 'Tombasian', 'MDC20907', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2354, 398, 'Tompaso Baru', 'MDC20908', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2355, 398, 'Tumpaan', 'MDC20909', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2356, 403, 'Ratahan', 'MDC21000', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2357, 403, 'Belang', 'MDC21001', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2358, 403, 'Tombatu', 'MDC21002', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2359, 403, 'Touluaan', 'MDC21003', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2360, 403, 'Ratatotok', 'MDC21004', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2361, 403, 'Pusomaen', 'MDC21005', 90000, 76000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2362, 397, 'Tomohon', 'MDC21100', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2363, 397, 'Tomohon Barat', 'MDC21101', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2364, 397, 'Tomohon Selatan', 'MDC21102', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2365, 397, 'Tomohon Tengah', 'MDC21103', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2366, 397, 'Tomohon Timur', 'MDC21104', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2367, 397, 'Tomohon Utara', 'MDC21105', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2368, 145, 'Madiun', 'MDN10000', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2369, 145, 'Kartoharjo ', 'MDN10016', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2370, 145, 'Manguharjo', 'MDN10017', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2371, 145, 'Taman', 'MDN10018', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2372, 124, 'Caruban', 'MDN10100', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2373, 124, 'Balerejo', 'MDN10101', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2374, 124, 'Dagangan', 'MDN10102', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2375, 124, 'Dolopo', 'MDN10103', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2376, 124, 'Geger ', 'MDN10104', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2377, 124, 'Gemarang', 'MDN10105', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2378, 124, 'Jiwan', 'MDN10106', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2379, 124, 'Kare', 'MDN10107', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2380, 124, 'Kebonsari', 'MDN10108', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2381, 124, 'Madiun', 'MDN10109', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2382, 124, 'Mejayan', 'MDN10110', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2383, 124, 'Pilangkenceng', 'MDN10111', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2384, 124, 'Saradan', 'MDN10112', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2385, 124, 'Sawahan', 'MDN10113', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2386, 124, 'Wonoasri', 'MDN10114', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2387, 124, 'Wungu', 'MDN10115', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2388, 141, 'Magetan', 'MDN20100', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2389, 141, 'Bendo', 'MDN20101', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2390, 141, 'Karangrejo ', 'MDN20102', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2391, 141, 'Kawedanan', 'MDN20104', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2392, 141, 'Lembeyan', 'MDN20105', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2393, 141, 'Maospati', 'MDN20106', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2394, 141, 'Panekan', 'MDN20107', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2395, 141, 'Parang', 'MDN20108', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2396, 141, 'Plaosan', 'MDN20109', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2397, 141, 'Poncol', 'MDN20110', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2398, 141, 'Sukomoro ', 'MDN20111', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2399, 141, 'Takeran', 'MDN20112', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2400, 141, 'Barat ', 'MDN20113', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2401, 141, 'Karas', 'MDN20114', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2402, 141, 'Kartoharjo ', 'MDN20115', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2403, 141, 'Ngariboyo', 'MDN20116', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2404, 141, 'Nguntoronadi', 'MDN20117', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2405, 158, 'Ngawi', 'MDN20200', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2406, 158, 'Bringin ', 'MDN20201', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2407, 158, 'Geneng', 'MDN20202', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2408, 158, 'Jogorogo', 'MDN20203', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2409, 158, 'Karangjati', 'MDN20204', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2410, 158, 'Kedunggalar', 'MDN20205', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2411, 158, 'Kendal ', 'MDN20206', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2412, 158, 'Kwadungan', 'MDN20207', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2413, 158, 'Mantingan', 'MDN20208', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2414, 158, 'Ngrambe', 'MDN20209', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2415, 158, 'Padas', 'MDN20210', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2416, 158, 'Pangkur', 'MDN20211', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2417, 158, 'Paron', 'MDN20212', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2418, 158, 'Pitu', 'MDN20213', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2419, 158, 'Sine', 'MDN20214', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2420, 158, 'Widodaren', 'MDN20215', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2421, 158, 'Karanganyar ', 'MDN20216', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2422, 158, 'Kendungan', 'MDN20217', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2423, 136, 'Pacitan', 'MDN20300', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2424, 136, 'Arjosari', 'MDN20301', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2425, 136, 'Bandar ', 'MDN20302', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2426, 136, 'Donorojo', 'MDN20303', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2427, 136, 'Kebonagung ', 'MDN20304', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2428, 136, 'Nawangan', 'MDN20305', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2429, 136, 'Ngadirojo', 'MDN20306', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2430, 136, 'Pringkuku', 'MDN20307', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2431, 136, 'Punung', 'MDN20308', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2432, 136, 'Sudimoro', 'MDN20309', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2433, 136, 'Tegalombo', 'MDN20310', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2434, 136, 'Tulakan', 'MDN20311', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2435, 153, 'Ponorogo', 'MDN20400', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2436, 153, 'Babadan', 'MDN20401', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2437, 153, 'Badegan', 'MDN20402', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2438, 153, 'Balong', 'MDN20403', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2439, 153, 'Bungkal', 'MDN20404', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2440, 153, 'Jenangan', 'MDN20405', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2441, 153, 'Jetis ', 'MDN20406', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2442, 153, 'Kauman ', 'MDN20407', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2443, 153, 'Mlarak', 'MDN20408', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2444, 153, 'Ngebel', 'MDN20409', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2445, 153, 'Ngrayun', 'MDN20410', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2446, 153, 'Pulung', 'MDN20411', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2447, 153, 'Sambit', 'MDN20412', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2448, 153, 'Sampung', 'MDN20413', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2449, 153, 'Sawoo', 'MDN20414', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2450, 153, 'Siman', 'MDN20415', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2451, 153, 'Slahung', 'MDN20416', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2452, 153, 'Sooko ', 'MDN20417', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2453, 153, 'Sukorejo', 'MDN20418', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2454, 153, 'Jambon', 'MDN20419', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2455, 153, 'Pudak', 'MDN20420', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2456, 452, 'Medan', 'MES10000', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2457, 452, 'Medan Amplas', 'MES10017', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2458, 452, 'Medan Area', 'MES10018', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2459, 452, 'Medan Barat', 'MES10019', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2460, 452, 'Medan Baru', 'MES10020', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2461, 452, 'Medan Deli', 'MES10021', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2462, 452, 'Medan Denai', 'MES10022', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2463, 452, 'Medan Helvetia', 'MES10023', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2464, 452, 'Medan Johor', 'MES10024', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2465, 452, 'Medan Kota', 'MES10025', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2466, 452, 'Medan Labuhan', 'MES10026', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2467, 452, 'Medan Maimun', 'MES10027', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2468, 452, 'Medan Marelan', 'MES10028', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2469, 452, 'Medan Perjuangan', 'MES10029', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2470, 452, 'Medan Petisah', 'MES10030', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2471, 452, 'Medan Polonia', 'MES10031', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2472, 452, 'Medan Selayang', 'MES10032', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2473, 452, 'Medan Tembung', 'MES10033', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2474, 452, 'Medan Timur', 'MES10034', 36000, 31000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2475, 452, 'Medan Tuntungan', 'MES10035', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2476, 452, 'Medan Belawan', 'MES10045', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2477, 452, 'Medan Sunggal', 'MES10046', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2478, 465, 'Kisaran', 'MES10100', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2479, 465, 'Air Joman', 'MES10102', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2480, 465, 'Air Batu', 'MES10103', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2481, 465, 'Bandar Pasir Mandoge', 'MES10104', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2482, 465, 'Bandar Pulau', 'MES10105', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2483, 465, 'Buntu Pane', 'MES10106', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2484, 465, 'Meranti', 'MES10110', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2485, 465, 'Pulau Rakyat', 'MES10111', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2486, 465, 'Simpang Empat', 'MES10112', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2487, 465, 'Tanjung Balai Asahan', 'MES10115', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2488, 465, 'Aek Kuasan', 'MES10117', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2489, 465, 'Sei Kepayang', 'MES10118', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2490, 465, 'Kisaran Barat', 'MES10126', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2491, 465, 'Kisaran Timur', 'MES10127', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2492, 449, 'Sibolga', 'MES10200', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2493, 449, 'Barus', 'MES10201', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2494, 449, 'Kolang', 'MES10202', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2495, 449, 'Manduamas', 'MES10203', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2496, 449, 'Sorkam', 'MES10204', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2497, 449, 'Adman Dewi', 'MES10205', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2498, 449, 'Badiri', 'MES10206', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2499, 449, 'Lumut', 'MES10207', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2500, 449, 'Sibabangun', 'MES10208', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2501, 449, 'Sirandorung', 'MES10209', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2502, 449, 'Sitahuis', 'MES10210', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2503, 449, 'Sorkam Barat', 'MES10211', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2504, 449, 'Sosor Gadong', 'MES10212', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2505, 449, 'Tapian Nauli', 'MES10213', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2506, 449, 'Tukka', 'MES10214', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2507, 462, 'Sibolga Sambas', 'MES10216', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2508, 462, 'Sibolga Selatan', 'MES10217', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2509, 462, 'Sibolga Utara', 'MES10218', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2510, 462, 'Sibolga Kota', 'MES10219', 49000, 42000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (2511, 446, 'Sidikalang', 'MES10300', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2512, 446, 'Parbuluan', 'MES10302', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2513, 446, 'Siempat Nempu', 'MES10304', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2514, 446, 'Siempat Nempu Hilir', 'MES10305', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2515, 446, 'Silima Pungga-Pungga', 'MES10306', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2516, 446, 'Sumbul', 'MES10307', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2517, 446, 'Tanah Pinem', 'MES10308', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2518, 446, 'Tiga Lingga', 'MES10309', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2519, 446, 'Pegagan Hilir', 'MES10310', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2520, 446, 'Siempat Nempu Hulu', 'MES10311', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2521, 446, 'Berampu', 'MES10312', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2522, 446, 'Gunung Sitember', 'MES10313', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2523, 446, 'Lae Parira', 'MES10314', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2524, 459, 'Lubuk Pakam', 'MES10600', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2525, 459, 'Deli Tua', 'MES10601', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2526, 459, 'Hamparan Perak', 'MES10602', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2527, 459, 'Kutalimbaru', 'MES10603', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2528, 459, 'Labuhan Deli', 'MES10604', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2529, 459, 'Batang Kuis', 'MES10605', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2530, 459, 'Pancur Batu', 'MES10606', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2531, 459, 'Percut Sei Tuan', 'MES10607', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2532, 459, 'Petumbak', 'MES10608', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2533, 459, 'Sibolangit', 'MES10609', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2534, 459, 'Biru-Biru', 'MES10610', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2535, 459, 'Sinembah Tanjungmuda Hilir', 'MES10611', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2536, 459, 'Sunggal', 'MES10612', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2537, 459, 'Tanjung Morawa', 'MES10613', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2538, 459, 'Pantai Labu', 'MES10614', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2539, 459, 'Bangun Purba', 'MES10615', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2540, 459, 'Beringin', 'MES10616', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2541, 459, 'Galang', 'MES10617', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2542, 459, 'Gunung Meriah', 'MES10619', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2543, 459, 'Namo Rambe', 'MES10620', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2544, 459, 'Pagar Marbau', 'MES10621', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2545, 459, 'Sinembah Tanjungmuda Hulu', 'MES10622', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2546, 443, 'Balige', 'MES20100', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2547, 443, 'Habinsaran', 'MES20101', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2548, 443, 'Lagu Boti', 'MES20103', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2549, 443, 'Lumban Julu', 'MES20104', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2550, 443, 'Porsea', 'MES20109', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2551, 443, 'Silaen', 'MES20111', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2552, 443, 'Ajibata', 'MES20116', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2553, 443, 'Bor Bor', 'MES20117', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2554, 443, 'Nassau', 'MES20118', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2555, 443, 'Pintu Pohan Meranti', 'MES20119', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2556, 443, 'Siantar Narumonda', 'MES20120', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2557, 443, 'Sigumpar', 'MES20121', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2558, 443, 'Tampahan', 'MES20122', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2559, 443, 'Uluan', 'MES20123', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2560, 456, 'Binjai Kota', 'MES20200', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2561, 456, 'Binjai ', 'MES20201', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2562, 456, 'Binjai Barat', 'MES20207', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2563, 456, 'Binjai Selatan', 'MES20208', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2564, 456, 'Binjai Timur', 'MES20209', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2565, 456, 'Binjai Utara', 'MES20210', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2566, 469, 'Gunung Sitoli', 'MES20300', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2567, 469, 'Afulu', 'MES20301', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2568, 469, 'Alasa', 'MES20302', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2569, 469, 'Bawolato', 'MES20303', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2570, 469, 'Gido', 'MES20304', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2571, 469, 'Hiliduho', 'MES20306', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2572, 469, 'Idano Gawo', 'MES20307', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2573, 469, 'Lahewa', 'MES20308', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2574, 469, 'Lotu', 'MES20311', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2575, 469, 'Mandrehe', 'MES20312', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2576, 469, 'Sirombo', 'MES20317', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2577, 469, 'Tuhemberua', 'MES20320', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2578, 469, 'Lolofitu Moi', 'MES20326', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2579, 454, 'Padang Sidempuan', 'MES20500', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2580, 454, 'Padang Sidempuan Batu Nadua', 'MES20536', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2581, 454, 'Padang Sidempuan Hutaimbaru', 'MES20537', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2582, 454, 'Padang Sidempuan Selatan', 'MES20538', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2583, 454, 'Padang Sidempuan Tenggara', 'MES20539', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2584, 454, 'Padang Sidempuan Utara', 'MES20540', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2585, 467, 'Pematang Raya', 'MES20600', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2586, 467, 'Bandar', 'MES20601', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2587, 467, 'Bosar Maligas', 'MES20602', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2588, 467, 'Dolok Silau', 'MES20603', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2589, 467, 'Dolok Pardamean', 'MES20604', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2590, 467, 'Dolok Panribuan', 'MES20605', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2591, 467, 'Dolok Batunanggar', 'MES20606', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2592, 467, 'Girsang Sipangan Bolon', 'MES20607', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2593, 467, 'Hutabayu Raja', 'MES20608', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2594, 467, 'Jorlang Hataran', 'MES20609', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2595, 467, 'Panei', 'MES20610', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2596, 467, 'Pematang Bandar', 'MES20611', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2597, 467, 'Purba', 'MES20612', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2598, 467, 'Raya', 'MES20613', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2599, 467, 'Raya Kahean', 'MES20614', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2600, 467, 'Siantar', 'MES20615', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2601, 467, 'Sidamanik', 'MES20616', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2602, 467, 'Silau Kahean', 'MES20617', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2603, 467, 'Silimakuta', 'MES20618', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2604, 467, 'Tanah Jawa', 'MES20619', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2605, 467, 'Tapian Dolok', 'MES20620', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2606, 467, 'Ujung Padang', 'MES20621', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2607, 467, 'Bandar Huluan', 'MES20622', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2608, 467, 'Bandar Masilam', 'MES20623', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2609, 467, 'Gunung Malela', 'MES20624', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2610, 467, 'Gunung Maligas', 'MES20625', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2611, 467, 'Haranggaol Horisanz', 'MES20626', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2612, 467, 'Hatonduhan', 'MES20627', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2613, 467, 'Jawa Maraja Bah Jambi', 'MES20628', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2614, 467, 'Panombeian Pane', 'MES20629', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2615, 467, 'Pematang Sidamanik', 'MES20630', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2616, 451, 'Rantau Prapat', 'MES20700', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2617, 451, 'Aek Natas', 'MES20701', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2618, 451, 'Bilah Hilir', 'MES20702', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2619, 451, 'Kampung Rakyat', 'MES20703', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2620, 451, 'Kota Pinang', 'MES20704', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2621, 451, 'Kualuh Hulu', 'MES20705', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2622, 451, 'Kualuh Hilir', 'MES20706', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2623, 451, 'Marbau', 'MES20707', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2624, 451, 'Na IX-X', 'MES20708', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2625, 451, 'Panai Hilir', 'MES20709', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2626, 451, 'Panai Tengah', 'MES20710', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2627, 451, 'Aek Kuo', 'MES20713', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2628, 451, 'Bilah Barat', 'MES20714', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2629, 451, 'Bilah Hulu', 'MES20715', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2630, 451, 'Kualuh Leidong', 'MES20716', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2631, 451, 'Kualuh Selatan', 'MES20717', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2632, 451, 'Panai Hulu', 'MES20718', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2633, 451, 'Pangkatan', 'MES20719', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2634, 451, 'Rantau Selatan', 'MES20720', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2635, 451, 'Rantau Utara', 'MES20721', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2636, 451, 'Silangkitang', 'MES20722', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2637, 451, 'Sungai Kanan', 'MES20723', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2638, 451, 'Torgamba', 'MES20724', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2639, 464, 'Kabanjahe', 'MES20800', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2640, 464, 'Barusjahe', 'MES20801', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2641, 464, 'Berastagi', 'MES20802', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2642, 464, 'Juhar', 'MES20803', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2643, 464, 'Kuta Buluh', 'MES20804', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2644, 464, 'Laubaleng', 'MES20805', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2645, 464, 'Mardinding', 'MES20806', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2646, 464, 'Merek', 'MES20807', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2647, 464, 'Munte', 'MES20808', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2648, 464, 'Payung', 'MES20809', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2649, 464, 'Simpang Empat', 'MES20810', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2650, 464, 'Tiga Binanga', 'MES20811', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2651, 464, 'Tiga Panah', 'MES20812', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2652, 448, 'Stabat', 'MES20900', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2653, 448, 'Babalan', 'MES20901', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2654, 448, 'Besitang', 'MES20902', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2655, 448, 'Gebang', 'MES20903', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2656, 448, 'Hinai', 'MES20904', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2657, 448, 'Padang Tualang', 'MES20905', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2658, 448, 'Pangkalan Susu', 'MES20906', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2659, 448, 'Secanggang', 'MES20907', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2660, 448, 'Tanjung Pura', 'MES20908', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2661, 448, 'Batang Serangan', 'MES20909', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2662, 448, 'Brandan Barat', 'MES20910', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2663, 448, 'Salapian', 'MES20911', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2664, 448, 'Sawit Seberang', 'MES20912', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2665, 448, 'Sei Lepan', 'MES20913', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2666, 448, 'Wampu', 'MES20914', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2667, 448, 'Binjai', 'MES20915', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2668, 448, 'Bohorok', 'MES20916', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2669, 448, 'Kuala', 'MES20917', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2670, 448, 'Sei Bingai', 'MES20918', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2671, 448, 'Selesai', 'MES20919', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2672, 461, 'Tarutung', 'MES21000', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2673, 461, 'Adiankoting', 'MES21001', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2674, 461, 'Garoga', 'MES21003', 61000, 52000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (2675, 461, 'Pagaran', 'MES21006', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2676, 461, 'Pahae Julu', 'MES21007', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2677, 461, 'Pahae Jae', 'MES21008', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2678, 461, 'Pangaribuan', 'MES21010', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2679, 461, 'Parmonangan', 'MES21011', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2680, 461, 'Siborong-Borong', 'MES21013', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2681, 461, 'Sipoholon', 'MES21014', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2682, 461, 'Sipahutar', 'MES21015', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2683, 461, 'Muara', 'MES21021', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2684, 461, 'Purbatua', 'MES21022', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2685, 461, 'Siatas Barita', 'MES21023', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2686, 461, 'Simangumban', 'MES21024', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2687, 445, 'Tebing Tinggi', 'MES21100', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2688, 458, 'Padang Hilir', 'MES21119', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2689, 458, 'Padang Hulu', 'MES21120', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2690, 458, 'Rambutan', 'MES21121', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2691, 442, 'Gunung Tua', 'MES21400', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2692, 442, 'Batang Onang', 'MES21401', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2693, 442, 'Dolok', 'MES21402', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2694, 442, 'Dolok Sigompulon', 'MES21403', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2695, 442, 'Halongonan', 'MES21404', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2696, 442, 'Padang Bolak', 'MES21405', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2697, 442, 'Padang Bolak Julu', 'MES21406', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2698, 442, 'Poribi', 'MES21407', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2699, 442, 'Simangambat', 'MES21408', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2700, 455, 'Limapuluh', 'MES21500', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2701, 455, 'Medang Deras', 'MES21501', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2702, 455, 'Air Putih', 'MES21503', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2703, 455, 'Talawi', 'MES21504', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2704, 455, 'Tanjung Tiram', 'MES21505', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2705, 455, 'Sei Balai', 'MES21506', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2706, 455, 'Sei Suka', 'MES21508', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2707, 468, 'Dolok Sanggul', 'MES21600', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2708, 468, 'Baktiraja', 'MES21601', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2709, 468, 'Lintong Nihuta', 'MES21602', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2710, 468, 'Onan Ganjang', 'MES21603', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2711, 468, 'Pakkat', 'MES21604', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2712, 468, 'Paranginan', 'MES21605', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2713, 468, 'Parlilitan', 'MES21606', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2714, 468, 'Pollung', 'MES21607', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2715, 468, 'Sijama Polang', 'MES21608', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2716, 468, 'Tarabintang', 'MES21609', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2717, 453, 'Panyabungan', 'MES21700', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2718, 453, 'Batahan', 'MES21701', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2719, 453, 'Batang Natal', 'MES21702', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2720, 453, 'Bukit Malintang', 'MES21703', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2721, 453, 'Kotanopan', 'MES21704', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2722, 453, 'Lembah Sorik Merapi', 'MES21705', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2723, 453, 'Lingga Bayu', 'MES21706', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2724, 453, 'Muara Batang Gadis', 'MES21707', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2725, 453, 'Muara Sipongi', 'MES21708', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2726, 453, 'Natal', 'MES21709', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2727, 453, 'Panyabungan Barat', 'MES21710', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2728, 453, 'Panyabungan Kota', 'MES21711', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2729, 453, 'Panyabungan Selatan', 'MES21712', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2730, 453, 'Panyabungan Timur', 'MES21713', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2731, 453, 'Panyabungan Utara', 'MES21714', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2732, 453, 'Siabu', 'MES21715', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2733, 453, 'Tambangan', 'MES21716', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2734, 453, 'Ulu Pungkut', 'MES21717', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2735, 466, 'Teluk Dalam', 'MES21800', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2736, 466, 'Amandraya', 'MES21801', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2737, 466, 'Gomo', 'MES21802', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2738, 466, 'Hibala', 'MES21803', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2739, 466, 'Lahusa', 'MES21804', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2740, 466, 'Lolo Wa\'u', 'MES21805', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2741, 466, 'Lolomatua', 'MES21806', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2742, 466, 'Pulau-Pulau Batu', 'MES21807', 92000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2743, 450, 'Sibuhuan', 'MES21900', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2744, 450, 'Barumun', 'MES21901', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2745, 450, 'Barumun Tengah', 'MES21902', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2746, 450, 'Batang Bulu Sutam', 'MES21903', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2747, 450, 'Huristak', 'MES21904', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2748, 450, 'Huta Raja Tinggi', 'MES21905', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2749, 450, 'Lubuk Barumun', 'MES21906', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2750, 450, 'Sosa', 'MES21907', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2751, 450, 'Sosopan', 'MES21908', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2752, 450, 'Ulu Barumun', 'MES21909', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2753, 463, 'Salak', 'MES22100', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2754, 463, 'Kerajaan', 'MES22101', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2755, 463, 'Sitellu Tai Urang Jehe', 'MES22102', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2756, 463, 'Sitellu Tai Urang Julu', 'MES22103', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2757, 463, 'Pergetteng-getteng Sengkut', 'MES22104', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2758, 463, 'Pagindar', 'MES22105', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2759, 463, 'Siempat Rube', 'MES22106', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2760, 463, 'Tinada', 'MES22107', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2761, 447, 'Pangururan', 'MES22200', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2762, 447, 'Harian', 'MES22201', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2763, 447, 'Nainggolan', 'MES22202', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2764, 447, 'Onan Runggu', 'MES22203', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2765, 447, 'Palipi', 'MES22204', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2766, 447, 'Ronggur Nihuta', 'MES22205', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2767, 447, 'Sianjur Mula Mula', 'MES22206', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2768, 447, 'Simanindo', 'MES22207', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2769, 447, 'Sitiotio', 'MES22208', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2770, 460, 'Sei Rampah', 'MES22300', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2771, 460, 'Bandar Khalipah', 'MES22301', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2772, 460, 'Dolok Masihul', 'MES22302', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2773, 460, 'Dolok Merawan', 'MES22303', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2774, 460, 'Kotarih', 'MES22304', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2775, 460, 'Pantai Cermin', 'MES22305', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2776, 460, 'Perbaungan', 'MES22306', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2777, 460, 'Sipispis', 'MES22307', 61000, 52000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (2778, 460, 'Tanjung Beringin', 'MES22308', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2779, 460, 'Tebingtinggi', 'MES22309', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2780, 460, 'Teluk Mengkudu', 'MES22310', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2781, 444, 'Sipirok', 'MES22400', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2782, 444, 'Aek Bilah', 'MES22401', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2783, 444, 'Arse', 'MES22402', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2784, 444, 'Barumun', 'MES22403', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2785, 444, 'Barumun Tengah', 'MES22404', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2786, 444, 'Batang Angkola', 'MES22405', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2787, 444, 'Batang Lubu Sutam', 'MES22406', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2788, 444, 'Batang Onang', 'MES22407', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2789, 444, 'Batang Toru', 'MES22408', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2790, 444, 'Dolok', 'MES22409', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2791, 444, 'Dolok Sigompulon', 'MES22410', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2792, 444, 'Halongonan', 'MES22411', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2793, 444, 'Huristak', 'MES22412', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2794, 444, 'Huta Raja Tinggi', 'MES22413', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2795, 444, 'Lubuk Barumun', 'MES22414', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2796, 444, 'Marancar', 'MES22415', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2797, 444, 'Padang Bolak', 'MES22416', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2798, 444, 'Padang Bolak Julu', 'MES22417', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2799, 444, 'Padang Sidempuan Barat', 'MES22418', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2800, 444, 'Padang Sidempuan Timur', 'MES22419', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2801, 444, 'Portibi', 'MES22420', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2802, 444, 'Saipar Dolok Hole', 'MES22421', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2803, 444, 'Sayur Matinggi', 'MES22422', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2804, 444, 'Siais', 'MES22423', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2805, 444, 'Simangambat', 'MES22424', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2806, 444, 'Sosa', 'MES22425', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2807, 444, 'Sosopan', 'MES22426', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2808, 444, 'Ulu Barumun', 'MES22427', 61000, 52000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2809, 457, 'Tanjung Balai', 'MES22500', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2810, 457, 'Datuk Bandar', 'MES22501', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2811, 457, 'Sei Tualang Raso', 'MES22502', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2812, 457, 'Tanjung Balai Selatan', 'MES22503', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2813, 457, 'Tanjung Balai Utara', 'MES22504', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2814, 457, 'Teluk Nibung', 'MES22505', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2815, 441, 'Pematang Siantar', 'MES22600', 49000, 42000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (2816, 441, 'Siantar Barat', 'MES22601', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2817, 441, 'Siantar Marihat', 'MES22602', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2818, 441, 'Siantar Martoba', 'MES22603', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2819, 441, 'Siantar Selatan', 'MES22604', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2820, 441, 'Siantar Timur', 'MES22605', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2821, 441, 'Siantar Utara', 'MES22606', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2822, 103, 'Magelang', 'MGL10000', 22000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2823, 103, 'Magelang Selatan', 'MGL10022', 22000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2824, 103, 'Magelang Tengah', 'MGL10023', 22000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2825, 103, 'Magelang Utara', 'MGL10024', 22000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2826, 119, 'Kebumen', 'MGL10100', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2827, 119, 'Adimulyo', 'MGL10101', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2828, 119, 'Aliyan', 'MGL10102', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2829, 119, 'Ambal', 'MGL10103', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2830, 119, 'Bulupesantren', 'MGL10104', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2831, 119, 'Karanganyar ', 'MGL10105', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2832, 119, 'Karanggayam', 'MGL10106', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2833, 119, 'Klirong', 'MGL10107', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2834, 119, 'Kutowinangun', 'MGL10108', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2835, 119, 'Kuwarasan', 'MGL10109', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2836, 119, 'Mirit', 'MGL10110', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2837, 119, 'Pejagoan', 'MGL10111', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2838, 119, 'Petanahan', 'MGL10112', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2839, 119, 'Prembun', 'MGL10113', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2840, 119, 'Puring', 'MGL10114', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2841, 119, 'Sadang', 'MGL10115', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2842, 119, 'Sruweng', 'MGL10116', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2843, 119, 'Gombong', 'MGL10117', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2844, 119, 'Ayah', 'MGL10118', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2845, 119, 'Buayan', 'MGL10119', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2846, 119, 'Rowokele', 'MGL10120', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2847, 119, 'Bonorowo', 'MGL10121', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2848, 119, 'Karangsambung', 'MGL10122', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2849, 119, 'Padureso', 'MGL10123', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2850, 119, 'Poncowarno', 'MGL10124', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2851, 119, 'Sempor', 'MGL10125', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2852, 99, 'Wonosobo', 'MGL10200', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2853, 99, 'Garung', 'MGL10201', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2854, 99, 'Kalikajar', 'MGL10202', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2855, 99, 'Kaliwiro', 'MGL10203', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2856, 99, 'Kejajar', 'MGL10204', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2857, 99, 'Kepil', 'MGL10205', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2858, 99, 'Kertek', 'MGL10206', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2859, 99, 'Leksono', 'MGL10207', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2860, 99, 'Mojotengah', 'MGL10208', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2861, 99, 'Sapuran', 'MGL10209', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2862, 99, 'Selomerto', 'MGL10210', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2863, 99, 'Wadaslintang', 'MGL10211', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2864, 99, 'Watumalang', 'MGL10212', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2865, 99, 'Kalibawang ', 'MGL10214', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2866, 99, 'Sukoharjo ', 'MGL10215', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2867, 115, 'Purworejo', 'MGL10300', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2868, 115, 'Bagelen', 'MGL10301', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2869, 115, 'Banyuurip', 'MGL10302', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2870, 115, 'Bener', 'MGL10303', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2871, 115, 'Gebang ', 'MGL10304', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2872, 115, 'Kaligesing', 'MGL10305', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2873, 115, 'Loano', 'MGL10306', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2874, 115, 'Ngombol', 'MGL10307', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2875, 115, 'Purwodadi', 'MGL10308', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2876, 115, 'Kutoarjo', 'MGL10309', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2877, 115, 'Kemiri', 'MGL10310', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2878, 115, 'Pituruh', 'MGL10311', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2879, 115, 'Butuh', 'MGL10312', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2880, 115, 'Bruno', 'MGL10313', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2881, 115, 'Grabag ', 'MGL10314', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2882, 115, 'Bayan', 'MGL10315', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2883, 95, 'Temanggung', 'MGL10400', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2884, 95, 'Bulu ', 'MGL10401', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2885, 95, 'Candiroto', 'MGL10402', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2886, 95, 'Jumo', 'MGL10403', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2887, 95, 'Kaloran', 'MGL10404', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2888, 95, 'Kandangan ', 'MGL10405', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2889, 95, 'Kedu', 'MGL10406', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2890, 95, 'Kranggan', 'MGL10407', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2891, 95, 'Ngadirejo', 'MGL10408', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2892, 95, 'Parakan', 'MGL10409', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2893, 95, 'Pringsurat', 'MGL10410', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2894, 95, 'Tembarak', 'MGL10411', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2895, 95, 'Tretep', 'MGL10412', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2896, 95, 'Bejen', 'MGL10414', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2897, 95, 'Tlogomulyo', 'MGL10415', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2898, 95, 'Kledung', 'MGL10416', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2899, 95, 'Wonoboyo', 'MGL10418', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2900, 95, 'Gemawang', 'MGL10419', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2901, 95, 'Bansari', 'MGL10420', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2902, 95, 'Selopampang', 'MGL10421', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2903, 110, 'Mungkid', 'MGL10500', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2904, 110, 'Salam', 'MGL10501', 22000, 19000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (2905, 110, 'Bandongan', 'MGL10502', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2906, 110, 'Candimulyo', 'MGL10503', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2907, 110, 'Grabag ', 'MGL10504', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2908, 110, 'Kajoran', 'MGL10505', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2909, 110, 'Kaliangkrik', 'MGL10506', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2910, 110, 'MERTOYUDAN', 'MGL10507', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2911, 110, 'Ngablak', 'MGL10508', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2912, 110, 'Pakis ', 'MGL10509', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2913, 110, 'Salaman', 'MGL10510', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2914, 110, 'Secang', 'MGL10511', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2915, 110, 'Tegalrejo', 'MGL10512', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2916, 110, 'Tempuran ', 'MGL10513', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2917, 110, 'WINDUSARI', 'MGL10514', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2918, 110, 'Srumbung', 'MGL10515', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2919, 110, 'Dukun ', 'MGL10516', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2920, 110, 'Sawangan ', 'MGL10517', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2921, 110, 'Muntilan', 'MGL10518', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2922, 110, 'Ngluwar', 'MGL10519', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2923, 110, 'Borobudur', 'MGL10520', 22000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2924, 129, 'Mojokerto', 'MJK10000', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2925, 129, 'Bangsal', 'MJK10001', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2926, 129, 'Dawarblandong', 'MJK10002', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2927, 129, 'Dlanggu', 'MJK10003', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2928, 129, 'Gedeg', 'MJK10004', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2929, 129, 'Gondang ', 'MJK10005', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2930, 129, 'Jatirejo', 'MJK10006', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2931, 129, 'Jetis ', 'MJK10007', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2932, 129, 'Kemlagi', 'MJK10008', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2933, 129, 'Kutorejo', 'MJK10009', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2934, 129, 'Mojosari', 'MJK10010', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2935, 129, 'Ngoro ', 'MJK10011', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2936, 129, 'Pacet ', 'MJK10012', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2937, 129, 'Pungging', 'MJK10013', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2938, 129, 'Puri', 'MJK10014', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2939, 129, 'Sooko ', 'MJK10015', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2940, 129, 'Trawas', 'MJK10016', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2941, 129, 'Trowulan', 'MJK10017', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2942, 151, 'Balongbendo', 'MJK10020', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2943, 129, 'Mojoanyar', 'MJK10021', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2944, 126, 'Magersari', 'MJK10022', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2945, 126, 'Parajurit Kulon', 'MJK10023', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2946, 151, 'Tarik', 'MJK10025', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2947, 151, 'Prambon ', 'MJK10026', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2948, 143, 'Malang', 'MXG10000', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2949, 143, 'Blimbing', 'MXG10035', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2950, 143, 'Kedungkandang', 'MXG10036', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2951, 143, 'Klojen', 'MXG10037', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2952, 143, 'Lowokwaru', 'MXG10038', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2953, 143, 'Sukun', 'MXG10039', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2954, 161, 'Blitar', 'MXG20100', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2955, 161, 'Bakung', 'MXG20101', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2956, 161, 'Binangun', 'MXG20102', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2957, 161, 'Doko', 'MXG20103', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2958, 161, 'Gandusari ', 'MXG20104', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2959, 161, 'Garum', 'MXG20105', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2960, 161, 'Kademangan', 'MXG20106', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2961, 161, 'Kanigoro', 'MXG20107', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2962, 161, 'Kesamben ', 'MXG20108', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2963, 161, 'Nglegok', 'MXG20109', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2964, 161, 'Panggungrejo', 'MXG20110', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2965, 161, 'Ponggok', 'MXG20111', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2966, 161, 'Sanankulon', 'MXG20112', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2967, 161, 'Selorejo', 'MXG20113', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2968, 161, 'Srengat', 'MXG20114', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2969, 161, 'Sutojayan', 'MXG20115', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2970, 161, 'Talun', 'MXG20116', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2971, 161, 'Udanawu', 'MXG20117', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2972, 161, 'Wates', 'MXG20118', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2973, 161, 'Wlingi', 'MXG20119', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2974, 161, 'Wonodadi', 'MXG20120', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2975, 161, 'Wonotirto', 'MXG20121', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2976, 161, 'Selopuro', 'MXG20122', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2977, 139, 'Kepanjenkidul', 'MXG20123', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2978, 139, 'Sananwetan', 'MXG20124', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2979, 139, 'Sukorejo', 'MXG20125', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2980, 156, 'Batu', 'MXG20200', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2981, 156, 'Bumiaji', 'MXG20204', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2982, 156, 'Junrejo', 'MXG20205', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2983, 134, 'Kepanjen', 'MXG20300', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2984, 134, 'Ampelgading ', 'MXG20301', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2985, 134, 'Bantur', 'MXG20302', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2986, 134, 'Bululawang', 'MXG20303', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2987, 134, 'Dampit', 'MXG20304', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2988, 134, 'Dau', 'MXG20305', 40000, 35000, 43000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2989, 134, 'Donomulyo', 'MXG20306', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2990, 134, 'Gedangan ', 'MXG20307', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2991, 134, 'Gondanglegi', 'MXG20308', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2992, 134, 'Jabung', 'MXG20309', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2993, 134, 'Kalipare', 'MXG20310', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2994, 134, 'Karangploso', 'MXG20311', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2995, 134, 'Kromengan', 'MXG20312', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2996, 134, 'Ngajum', 'MXG20313', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2997, 134, 'Pagak', 'MXG20314', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2998, 134, 'Pakis ', 'MXG20315', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (2999, 134, 'Pakisaji', 'MXG20316', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3000, 134, 'Poncokusumo', 'MXG20317', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3001, 134, 'Singosari', 'MXG20318', 40000, 35000, 43000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3002, 134, 'Sumberpucung', 'MXG20319', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3003, 134, 'Sumbermanjing Wetan', 'MXG20320', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3004, 134, 'Tajinan', 'MXG20321', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3005, 134, 'Tirtoyudo', 'MXG20322', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3006, 134, 'Turen', 'MXG20323', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3007, 134, 'Tumpang', 'MXG20324', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3008, 134, 'Wagir', 'MXG20325', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3009, 134, 'Wajak', 'MXG20326', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3010, 134, 'Wonosari', 'MXG20327', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3011, 134, 'Ngantang', 'MXG20328', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3012, 134, 'Pujon', 'MXG20329', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3013, 134, 'Kasembon', 'MXG20330', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3014, 134, 'Lawang', 'MXG20331', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3015, 134, 'Pagelaran ', 'MXG20332', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3072, 160, 'Probolinggo', 'PBL10000', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3073, 160, 'Banyuanyar', 'PBL10001', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3074, 160, 'Bantaran', 'PBL10002', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3075, 160, 'Besuk', 'PBL10003', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3076, 160, 'Dringu', 'PBL10004', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3077, 160, 'Gending', 'PBL10005', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3078, 160, 'Kotaanyar', 'PBL10006', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3079, 160, 'Kraksaan', 'PBL10007', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3080, 160, 'Krejengan', 'PBL10008', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3081, 160, 'Krucil', 'PBL10009', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3082, 160, 'Kuripan ', 'PBL10010', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3083, 160, 'Leces', 'PBL10011', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3084, 160, 'Lumbang ', 'PBL10012', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3085, 160, 'Maron', 'PBL10013', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3086, 160, 'Paiton', 'PBL10014', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3087, 160, 'Pajarakan', 'PBL10015', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3088, 160, 'Pakuniran', 'PBL10016', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3089, 160, 'Sukapura', 'PBL10017', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3090, 160, 'Sumber ', 'PBL10018', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3091, 160, 'Sumberasih', 'PBL10019', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3092, 160, 'Tegalsiwalan', 'PBL10020', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3093, 160, 'Tiris', 'PBL10021', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3094, 160, 'Tongas', 'PBL10022', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3095, 160, 'Wonomerto', 'PBL10024', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3096, 138, 'Kademangan', 'PBL10025', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3097, 138, 'Mayangan', 'PBL10026', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3098, 160, 'Gading', 'PBL10027', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3099, 138, 'Wonoasih', 'PBL10028', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3100, 138, 'Kedopok', 'PBL10029', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3101, 138, 'Kanigaran', 'PBL10030', 29000, 26000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3102, 155, 'Lumajang', 'PBL20100', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3103, 155, 'Candipuro', 'PBL20101', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3104, 155, 'Gucialit', 'PBL20102', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3105, 155, 'Jatiroto', 'PBL20103', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3106, 155, 'Klakah', 'PBL20104', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3107, 155, 'Kedungjajang', 'PBL20105', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3108, 155, 'Kunir', 'PBL20106', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3109, 155, 'Pasirian', 'PBL20107', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3110, 155, 'Pronojiwo', 'PBL20108', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3111, 155, 'Randuagung', 'PBL20109', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3112, 155, 'Ranuyoso', 'PBL20110', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3113, 155, 'Rowokangkung', 'PBL20111', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3114, 155, 'Senduro', 'PBL20112', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3115, 155, 'Sukodono', 'PBL20113', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3116, 155, 'Tekung', 'PBL20114', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3117, 155, 'Tempursari', 'PBL20115', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3118, 155, 'Tempeh', 'PBL20116', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3119, 155, 'Yosowilangun', 'PBL20117', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3120, 155, 'Padang', 'PBL20118', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3121, 155, 'Pasrujambe', 'PBL20119', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3122, 155, 'Sumbersoko', 'PBL20120', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3123, 133, 'Situbondo', 'PBL20200', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3124, 133, 'Arjasa ', 'PBL20201', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3125, 133, 'Asembagus', 'PBL20202', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3126, 133, 'Banyuputih', 'PBL20203', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3127, 133, 'Banyuglugur', 'PBL20204', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3128, 133, 'Besuki ', 'PBL20205', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3129, 133, 'Bungatan', 'PBL20206', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3130, 133, 'Jangkar', 'PBL20207', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3131, 133, 'Jatibanteng', 'PBL20208', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3132, 133, 'Kapongan', 'PBL20209', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3133, 133, 'Kendit', 'PBL20210', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3134, 133, 'Mangaran', 'PBL20211', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3135, 133, 'Mlandingan', 'PBL20212', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3136, 133, 'Panarukan', 'PBL20213', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3137, 133, 'Suboh', 'PBL20214', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3138, 133, 'Sumber Malang', 'PBL20215', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3139, 133, 'Panji', 'PBL20216', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3140, 422, 'Padang', 'PDG10000', 31000, 27000, 36000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3141, 422, 'Bungus Teluk Kabung', 'PDG10008', 31000, 27000, 36000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3142, 422, 'Koto Tangah', 'PDG10009', 31000, 27000, 36000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3143, 422, 'Kuranji', 'PDG10010', 31000, 27000, 36000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3144, 422, 'Lubuk Begalung', 'PDG10011', 31000, 27000, 36000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3145, 422, 'Lubuk Kilangan', 'PDG10012', 31000, 27000, 36000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3146, 422, 'Nanggalo', 'PDG10013', 31000, 27000, 36000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3147, 422, 'Padang Barat', 'PDG10014', 31000, 27000, 36000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3148, 422, 'Padang Selatan', 'PDG10015', 31000, 27000, 36000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3149, 422, 'Padang Timur', 'PDG10016', 31000, 27000, 36000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3150, 422, 'Padang Utara', 'PDG10017', 31000, 27000, 36000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3151, 422, 'Pauh', 'PDG10018', 31000, 27000, 36000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3152, 412, 'Batu Sangkar', 'PDG20100', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3153, 412, 'Batipuh', 'PDG20101', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3154, 412, 'Lintau Buo', 'PDG20103', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3155, 412, 'Pariangan', 'PDG20106', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3156, 412, 'Rambatan', 'PDG20107', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3157, 412, 'Salimpaung', 'PDG20108', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3158, 412, 'Sungai Tarab', 'PDG20110', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3159, 412, 'Sungayang', 'PDG20111', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3160, 412, 'Tanjung Emas', 'PDG20112', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3161, 412, 'Batipuh Selatan', 'PDG20114', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3162, 412, 'Lima Kaum', 'PDG20115', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3163, 412, 'Lintau Buo Utara', 'PDG20116', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3164, 412, 'Padang Ganting', 'PDG20117', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3165, 412, 'Sepuluh Koto', 'PDG20118', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3166, 412, 'Tanjung Baru', 'PDG20119', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3167, 420, 'Bukit Tinggi', 'PDG20200', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3168, 420, 'Aur Birugo Tigo Baleh', 'PDG20211', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3169, 420, 'Mandiangin Koto Selayan', 'PDG20212', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3170, 420, 'Guguk Panjang', 'PDG20213', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3171, 410, 'Lubuk Sikaping', 'PDG20300', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3172, 410, 'Bonjol', 'PDG20301', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3173, 410, 'Panti', 'PDG20305', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3174, 410, 'RAO MAPAT TUNGGUL', 'PDG20308', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3175, 410, 'III Nagari', 'PDG20314', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3176, 410, 'Mapat Tunggul Selatan', 'PDG20315', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3177, 410, 'II Koto', 'PDG20316', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3178, 410, 'Mapat Tunggul', 'PDG20325', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3179, 418, 'Painan', 'PDG20500', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3180, 418, 'Batang Kapas', 'PDG20501', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3181, 418, 'Bayang', 'PDG20503', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3182, 418, 'Koto XI Tarusan', 'PDG20505', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3183, 418, 'Lengayang', 'PDG20506', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3184, 418, 'Pancung Soal', 'PDG20510', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3185, 418, 'Ranah Pesisir', 'PDG20511', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3186, 418, 'Basa IV Balai Tapan', 'PDG20513', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3187, 418, 'Bayang Utara', 'PDG20514', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3188, 418, 'Linggo Sari Baganti', 'PDG20515', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3189, 418, 'Lunang Silaut', 'PDG20516', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3190, 418, 'Sutera', 'PDG20517', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3191, 418, 'IV Jurai', 'PDG20518', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3192, 408, 'Pariaman', 'PDG20600', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3193, 408, 'Batang Anai', 'PDG20601', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3194, 408, 'II.X.XI.VI Lingkung', 'PDG20602', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3195, 408, 'V Koto Kp Dalam', 'PDG20604', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3196, 408, 'Lubuk Alung', 'PDG20606', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3197, 408, 'Nan Sabaris', 'PDG20608', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3198, 408, 'Sungai Limau', 'PDG20610', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3199, 408, 'Sungai Geringging', 'PDG20612', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3200, 408, 'VII Koto Sungai Sarik', 'PDG20614', 55000, 48000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3201, 408, 'Batang Gasan', 'PDG20616', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3202, 408, 'Padang Sago', 'PDG20619', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3203, 408, 'Patamuan', 'PDG20620', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3204, 408, 'Sintuk Toboh Gadang', 'PDG20621', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3205, 408, 'Ulakan Tapakis', 'PDG20624', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3206, 416, 'Pariaman Selatan', 'PDG20625', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3207, 416, 'Pariaman Tengah', 'PDG20626', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3208, 416, 'Pariaman Utara', 'PDG20627', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3209, 408, 'IV Koto Aur Malintang', 'PDG20628', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3210, 408, 'V Koto Timur', 'PDG20629', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3211, 408, 'VI Lingkung', 'PDG20630', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3212, 408, '2 X 11 Kayu Tanam', 'PDG20632', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3213, 425, 'Payakumbuh', 'PDG20700', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3214, 425, 'Payakumbuh Barat', 'PDG20721', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3215, 425, 'Payakumbuh Timur', 'PDG20722', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3216, 425, 'Payakumbuh Utara', 'PDG20723', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3217, 414, 'Sawahlunto', 'PDG20800', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3218, 414, 'Barangin', 'PDG20803', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3219, 414, 'Lembah Segar', 'PDG20804', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3220, 414, 'Silungkang', 'PDG20805', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3221, 414, 'Talawi', 'PDG20806', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3222, 423, 'Solok', 'PDG20900', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3223, 423, 'Lubuk Sikarah', 'PDG20939', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3224, 423, 'Tanjung Harapan', 'PDG20940', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3225, 413, 'Padang Panjang', 'PDG21100', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3226, 413, 'Padang Panjang Barat', 'PDG21102', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3227, 413, 'Padang Panjang Timur', 'PDG21103', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3228, 421, 'Pulau Punjung', 'PDG21200', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3229, 421, 'Koto Baru', 'PDG21201', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3230, 421, 'Sitiung', 'PDG21203', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3231, 421, 'Sungai Rumbai', 'PDG21204', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3232, 411, 'Sarilamak', 'PDG21300', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3233, 411, 'Akabiluru', 'PDG21301', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3234, 411, 'Bukit Barisan', 'PDG21302', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3235, 411, 'Guguk', 'PDG21303', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3236, 411, 'Gunung Mas', 'PDG21304', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3237, 411, 'Harau', 'PDG21305', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3238, 411, 'Kapur IX', 'PDG21306', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3239, 411, 'Lareh Sago Halaban', 'PDG21307', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3240, 411, 'Mungka', 'PDG21309', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3241, 411, 'Pangkalan Koto Baru', 'PDG21310', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3242, 411, 'Payakumbuh', 'PDG21311', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3243, 411, 'Situjuh Lima Nagari', 'PDG21312', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3244, 411, 'Suliki Gunung Mas', 'PDG21313', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3245, 411, 'Luhak', 'PDG21315', 55000, 48000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3246, 419, 'Tuapejat', 'PDG21400', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3247, 419, 'Pagai Selatan', 'PDG21401', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3248, 419, 'Pagai Utara', 'PDG21402', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3249, 419, 'Siberut Selatan', 'PDG21403', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3250, 419, 'Siberut Utara', 'PDG21404', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3251, 419, 'Sipora', 'PDG21405', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3252, 409, 'Simpang Empat', 'PDG21500', 44000, 38000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3253, 409, 'Gunung Tuleh', 'PDG21501', 55000, 48000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3254, 409, 'Kinali', 'PDG21502', 55000, 48000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3255, 409, 'Lembah Melintang', 'PDG21503', 55000, 48000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3256, 409, 'Luhak Nan Duo', 'PDG21504', 55000, 48000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3257, 409, 'Pasaman', 'PDG21505', 55000, 48000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3258, 409, 'Ranah Balingka', 'PDG21506', 55000, 48000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3259, 409, 'Ranah Batahan', 'PDG21507', 55000, 48000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3260, 409, 'Ranah Pesisir', 'PDG21508', 55000, 48000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3261, 409, 'Sungai Aur', 'PDG21509', 55000, 48000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3262, 409, 'Sungai Beremas', 'PDG21510', 55000, 48000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3263, 409, 'Talamau', 'PDG21511', 55000, 48000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3264, 417, 'Arosuka', 'PDG21700', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3265, 417, 'IX Koto Sungai Lasi', 'PDG21701', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3266, 417, 'X Koto Diatas', 'PDG21702', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3267, 417, 'X Koto Singkarak', 'PDG21703', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3268, 417, 'Bukit Sundi', 'PDG21704', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3269, 417, 'Danau Kembar', 'PDG21705', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3270, 417, 'Gunung Talang', 'PDG21706', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3271, 417, 'Hiliran Gumanti', 'PDG21707', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3272, 417, 'Junjung Sirih', 'PDG21708', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3273, 417, 'Kubung', 'PDG21709', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3274, 417, 'Lembah Gumanti', 'PDG21710', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3275, 417, 'Lembang Jaya', 'PDG21711', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3276, 417, 'Pantai Cermin', 'PDG21712', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3277, 417, 'Payung Sekaki', 'PDG21713', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3278, 417, 'Tigo Lurah', 'PDG21714', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3279, 407, 'Padang Aro', 'PDG21800', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3280, 407, 'Koto Parik Gadang Diateh', 'PDG21801', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3281, 407, 'Sangir', 'PDG21802', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3282, 407, 'Sangir Batanghari', 'PDG21803', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3283, 407, 'Sangir Jujuan', 'PDG21804', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3284, 407, 'Sungai Pagu', 'PDG21805', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3285, 415, 'Muaro Sijunjung', 'PDG21900', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3286, 415, 'Koto Tujuh', 'PDG21901', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3287, 415, 'IV Nagari', 'PDG21903', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3288, 415, 'Kamang Baru', 'PDG21904', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3289, 415, 'Kupitan', 'PDG21905', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3290, 415, 'Lubuk Tarok', 'PDG21906', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3291, 415, 'Sumpur Kudus', 'PDG21907', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3292, 415, 'Tanjung Gadang', 'PDG21908', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3293, 424, 'Lubuk Basung', 'PDG22200', 44000, 38000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3294, 424, 'Palupuh', 'PDG22201', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3295, 424, 'Tanjung Mutiara', 'PDG22203', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3296, 424, 'Tanjung Raya', 'PDG22204', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3297, 424, 'IV Angkat Canduang', 'PDG22205', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3298, 424, 'Banuhampu', 'PDG22206', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3299, 424, 'Baso', 'PDG22207', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3300, 424, 'IV Koto', 'PDG22208', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3301, 424, 'Candung', 'PDG22209', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3302, 424, 'Matur', 'PDG22210', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3303, 424, 'Palembayan', 'PDG22211', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3304, 424, 'Tilatang Kamang', 'PDG22212', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3305, 424, 'Ampek Nagari', 'PDG22213', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3306, 424, 'Kamang Magek', 'PDG22214', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3307, 424, 'Sungai Puar', 'PDG22215', 55000, 48000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3308, 135, 'Pandaan', 'PDN10000', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3309, 135, 'Bangil', 'PDN10001', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3310, 135, 'Beji ', 'PDN10002', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3311, 135, 'Gempol ', 'PDN10003', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3312, 135, 'Gondang Wetan', 'PDN10004', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3313, 135, 'Grati', 'PDN10005', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3314, 135, 'Kejayan', 'PDN10006', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3315, 135, 'Kraton ', 'PDN10007', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3316, 135, 'Lekok', 'PDN10008', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3317, 135, 'Lumbang ', 'PDN10009', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3318, 135, 'Nguling', 'PDN10010', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3319, 135, 'Pasrepan', 'PDN10012', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3320, 135, 'Pohjentrek', 'PDN10013', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3321, 135, 'Prigen', 'PDN10014', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3322, 135, 'Purwodadi ', 'PDN10015', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3323, 135, 'Purwosari ', 'PDN10016', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3324, 135, 'Puspo', 'PDN10017', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3325, 135, 'Rejoso ', 'PDN10018', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3326, 135, 'Rembang ', 'PDN10019', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3327, 135, 'Sukorejo ', 'PDN10020', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3328, 135, 'Tosari', 'PDN10021', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3329, 135, 'Tutur', 'PDN10022', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3330, 135, 'Winongan', 'PDN10023', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3331, 135, 'Wonorejo', 'PDN10024', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3332, 135, 'Kedawung', 'PDN10025', 40000, 35000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3333, 135, 'Nongko Jajar', 'PDN10026', 40000, 35000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3334, 152, 'Pasuruan', 'PDN20100', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3335, 152, 'Bugulkidul', 'PDN20101', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3336, 152, 'Gadingrejo', 'PDN20102', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3337, 152, 'Purworejo', 'PDN20103', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3338, 12, 'Pangkal Pinang', 'PGK10000', 27000, 24000, 35000, 44000, 24000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3339, 12, 'Rangkui', 'PGK10014', 27000, 24000, 35000, 44000, 24000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3340, 12, 'Bukit Intan', 'PGK10015', 27000, 24000, 35000, 44000, 24000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3341, 12, 'Pangkal Balam', 'PGK10016', 27000, 24000, 35000, 44000, 24000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3342, 12, 'Gerunggang', 'PGK10017', 27000, 24000, 35000, 44000, 24000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3343, 15, 'Sungai liat', 'PGK10200', 40000, 35000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3344, 15, 'Belinyu', 'PGK10201', 40000, 35000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3345, 15, 'Riau Silip', 'PGK10202', 49000, 42000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3346, 15, 'Bakam', 'PGK10203', 49000, 42000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3347, 15, 'Pemali', 'PGK10204', 49000, 42000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3348, 15, 'Mendo Barat', 'PGK10205', 49000, 42000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3349, 15, 'Merawang', 'PGK10206', 49000, 42000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3350, 15, 'Puding Besar', 'PGK10207', 49000, 42000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3351, 11, 'Kelapa', 'PGK10300', 40000, 35000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3352, 11, 'Mentok', 'PGK10301', 40000, 35000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3353, 11, 'Tempilang', 'PGK10302', 49000, 42000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3354, 11, 'Simpang Teritip', 'PGK10303', 49000, 42000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3355, 11, 'Jebus', 'PGK10304', 40000, 35000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3356, 14, 'Koba', 'PGK10400', 40000, 35000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3357, 14, 'Sungai Selan', 'PGK10401', 49000, 42000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3358, 14, 'Simpang Katis', 'PGK10402', 49000, 42000, 0, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3359, 14, 'Pangkalan Banteng', 'PGK10403', 49000, 42000, 53000, 49600, 36000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3360, 10, 'Toboali', 'PGK10500', 40000, 35000, 0, 49600, 33000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3361, 10, 'Lepar Pongok', 'PGK10501', 49000, 42000, 0, 49600, 33000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3362, 10, 'Payung', 'PGK10502', 49000, 42000, 0, 49600, 33000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3363, 10, 'Air Gegas', 'PGK10503', 49000, 42000, 0, 49600, 33000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3364, 10, 'Simpang Rima', 'PGK10504', 49000, 42000, 0, 49600, 33000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3365, 343, 'Pekanbaru', 'PKU10000', 32000, 28000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3366, 343, 'Payung', 'PKU10009', 32000, 28000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3367, 343, 'Lima Puluh', 'PKU10010', 32000, 28000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3368, 343, 'Marpoyan Damai', 'PKU10011', 32000, 28000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3369, 343, 'Sail', 'PKU10012', 32000, 28000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3370, 343, 'Senapelan', 'PKU10013', 32000, 28000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3371, 343, 'Sukajadi', 'PKU10014', 32000, 28000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3372, 343, 'Bukit Raya', 'PKU10016', 32000, 28000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3373, 343, 'Tampan', 'PKU10017', 32000, 28000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3374, 343, 'Rumbai Pesisir', 'PKU10018', 32000, 28000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3375, 343, 'Tenayan Raya', 'PKU10019', 32000, 28000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3376, 343, 'Rumbai', 'PKU10020', 46000, 39000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3377, 336, 'Dumai', 'PKU10100', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3378, 336, 'Bukit Kapur', 'PKU10101', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3379, 336, 'Dumai Barat', 'PKU10108', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3380, 336, 'Dumai Timur', 'PKU10109', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3381, 336, 'Medang Kampai', 'PKU10110', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3382, 336, 'Sungai Sembilan', 'PKU10111', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3383, 341, 'Bangkinang', 'PKU20100', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3384, 341, 'Kampar', 'PKU20101', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3385, 341, 'Siak Hulu', 'PKU20106', 58000, 50000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3386, 341, 'Kampar Kiri', 'PKU20111', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3387, 341, 'Bangkinang Barat', 'PKU20112', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3388, 341, 'Bangkinang Seberang', 'PKU20113', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3389, 341, 'Gunung Sahilan', 'PKU20114', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3390, 341, 'Kampar Kiri Hilir', 'PKU20115', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3391, 341, 'Kampar Kiri Hulu', 'PKU20116', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3392, 341, 'Kampar Timur', 'PKU20117', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3393, 341, 'Kampar Utara', 'PKU20118', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3394, 341, 'Perhentian Raja', 'PKU20119', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3395, 341, 'Rumbio Jaya', 'PKU20120', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3396, 341, 'Salo', 'PKU20121', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3397, 341, 'Tambang', 'PKU20122', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3398, 341, 'Tapung', 'PKU20123', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3399, 341, 'Tapung Hilir', 'PKU20124', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3400, 341, 'Tapung Hulu', 'PKU20125', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3401, 341, 'XIII Koto Kampar', 'PKU20135', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3402, 347, 'Bengkalis', 'PKU20200', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3403, 347, 'Bukit Batu', 'PKU20201', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3404, 347, 'Mandau', 'PKU20208', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3405, 347, 'Rupat', 'PKU20209', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3406, 347, 'Bantan', 'PKU20210', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3407, 347, 'Pinggir', 'PKU20211', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3408, 347, 'Rupat Utara', 'PKU20214', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3409, 347, 'Siak Kecil', 'PKU20215', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3410, 347, 'Duri Mandau', 'PKU20217', 46000, 39000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3411, 340, 'Rengat', 'PKU20300', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3412, 340, 'Pasir Penyu', 'PKU20305', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3413, 340, 'Peranap', 'PKU20306', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3414, 340, 'Seberida', 'PKU20307', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3415, 340, 'Lirik', 'PKU20310', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3416, 340, 'Batang Cenaku', 'PKU20311', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3417, 340, 'Batang Gansal', 'PKU20312', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3418, 340, 'Kelayang', 'PKU20313', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3419, 340, 'Rengat Barat', 'PKU20314', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3420, 346, 'Tembilahan', 'PKU20400', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3421, 346, 'Batang Tuaka', 'PKU20401', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3422, 346, 'Enok', 'PKU20402', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3423, 346, 'Gaung Anak Serka', 'PKU20403', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3424, 346, 'Kateman', 'PKU20404', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3425, 346, 'Keritang', 'PKU20405', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3426, 346, 'Kuala Indragiri', 'PKU20406', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3427, 346, 'Mandah', 'PKU20407', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3428, 346, 'Reteh', 'PKU20408', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3429, 346, 'Tanah Merah', 'PKU20409', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3430, 346, 'Tempuling', 'PKU20410', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3431, 346, 'Gaung', 'PKU20411', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3432, 346, 'Kemuning', 'PKU20412', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3433, 346, 'Pelangiran', 'PKU20413', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3434, 346, 'Pulau Burung', 'PKU20414', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3435, 346, 'Teluk Balengkong', 'PKU20415', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3436, 346, 'Tembilahan Hulu', 'PKU20416', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3437, 339, 'Bagan Siapi-api', 'PKU20500', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3438, 339, 'Kubu', 'PKU20502', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3439, 339, 'Rimba Melintang', 'PKU20503', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3440, 339, 'Tanah Putih', 'PKU20506', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3441, 339, 'Bagan Sinembah', 'PKU20507', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3442, 339, 'Bangko', 'PKU20508', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3443, 339, 'Bangko Pusako', 'PKU20509', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3444, 339, 'Batu Hampar', 'PKU20510', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3445, 339, 'Pasir Limau Kapas', 'PKU20511', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3446, 339, 'Pujud', 'PKU20512', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3447, 339, 'Rantau Kopar', 'PKU20513', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3448, 339, 'Simpang Kanan', 'PKU20514', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3449, 339, 'Sinaboi', 'PKU20515', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3450, 339, 'Tanah Putih Tanjung Melawan', 'PKU20516', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3451, 345, 'Teluk Kuantan', 'PKU20900', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3452, 345, 'Kuantan Hilir', 'PKU20901', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3453, 345, 'Kuantan Mudik', 'PKU20902', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3454, 345, 'Benai', 'PKU20903', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3455, 345, 'Cerenti', 'PKU20904', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3456, 345, 'Gunung Toar', 'PKU20905', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3457, 345, 'Hulu Kuantan', 'PKU20906', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3458, 345, 'Inuman', 'PKU20907', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3459, 345, 'Kuantan Tengah', 'PKU20908', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3460, 345, 'Logas Tanah Darat', 'PKU20909', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3461, 345, 'Pangean', 'PKU20910', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3462, 345, 'Singingi', 'PKU20911', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3463, 345, 'Singingi Hilir', 'PKU20912', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3464, 338, 'Pangkalan Kerinci', 'PKU21000', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3465, 338, 'Bunut', 'PKU21001', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3466, 338, 'Kerumutan', 'PKU21002', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3467, 338, 'Kuala Kampar', 'PKU21003', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3468, 338, 'Langgam', 'PKU21004', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3469, 338, 'Pangkalan Kuras', 'PKU21005', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3470, 338, 'Pangkalan Lesung', 'PKU21006', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3471, 338, 'Pelalawan', 'PKU21007', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3472, 338, 'Teluk Meranti', 'PKU21008', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3473, 338, 'Ukui', 'PKU21009', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3474, 338, 'Sorek', 'PKU21011', 58000, 50000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3475, 344, 'Pasir Pangaraian', 'PKU21100', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3476, 344, 'Bangun Purba', 'PKU21101', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3477, 344, 'Kabun', 'PKU21102', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3478, 344, 'Kepenuhan', 'PKU21103', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3479, 344, 'Kuntodarussalam', 'PKU21104', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3480, 344, 'Rambah', 'PKU21105', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3481, 344, 'Rambah Hilir', 'PKU21106', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3482, 344, 'Rambah Samo', 'PKU21107', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3483, 344, 'Rokan IV Koto', 'PKU21108', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3484, 344, 'Tandun', 'PKU21109', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3485, 344, 'Tembusai', 'PKU21110', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3486, 344, 'Tembusai Utara', 'PKU21111', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3487, 344, 'Ujung Batu', 'PKU21112', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3488, 337, 'Siak Indrapura', 'PKU21200', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3489, 337, 'Bunga Raya', 'PKU21202', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3490, 337, 'Dayun', 'PKU21203', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3491, 337, 'Kandis', 'PKU21204', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3492, 337, 'Kerinci Kanan', 'PKU21205', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3493, 337, 'Koto Gasip', 'PKU21206', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3494, 337, 'Lubuk Dalam', 'PKU21207', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3495, 337, 'Minas', 'PKU21208', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3496, 337, 'Siak', 'PKU21209', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3497, 337, 'Sungai Apit', 'PKU21210', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3498, 337, 'Sungai Mandau', 'PKU21211', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3499, 337, 'Tualang', 'PKU21212', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3500, 342, 'Meranti', 'PKU21300', 46000, 39000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3501, 342, 'Selat Panjang', 'PKU21301', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3502, 342, 'Merbau', 'PKU21302', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3503, 342, 'Rangsang', 'PKU21303', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3504, 342, 'Rangsang Barat', 'PKU21304', 87000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3505, 342, 'Tebing Tinggi', 'PKU21305', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3506, 342, 'Tebing Tinggi Barat', 'PKU21306', 58000, 50000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3507, 193, 'Palangkaraya', 'PKY10000', 37000, 32000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3508, 193, 'Bukit Batu ', 'PKY10001', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3509, 193, 'Jekan Raya', 'PKY10002', 37000, 32000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3510, 193, 'Pahandut ', 'PKY10003', 37000, 32000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3511, 193, 'Rakumpit ', 'PKY10004', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3512, 193, 'Sebangau', 'PKY10005', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3513, 198, 'Kasongan', 'PKY20200', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3514, 198, 'Kamipang', 'PKY20201', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3515, 198, 'Katingan Hulu', 'PKY20202', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3516, 198, 'Katingan Kuala', 'PKY20203', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3517, 198, 'Katingan Tengah', 'PKY20204', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3518, 198, 'Marikit', 'PKY20205', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3519, 198, 'Pulau Malan', 'PKY20206', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3520, 198, 'Sanaman Mantikei', 'PKY20207', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3521, 198, 'Tasik Payawan', 'PKY20208', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3522, 198, 'Tewang Sangalang Garing', 'PKY20209', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3523, 198, 'Mendawai ', 'PKY20212', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3524, 198, 'Katingan Hilir ', 'PKY20213', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3525, 202, 'Kuala Kapuas', 'PKY20300', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3526, 202, 'Basarang', 'PKY20302', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3527, 202, 'Kapuas Barat', 'PKY20306', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3528, 202, 'Kapuas Murung', 'PKY20307', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3529, 202, 'Kapuas Tengah', 'PKY20308', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3530, 202, 'Kapuas Timur', 'PKY20309', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3531, 202, 'Kapuas Kuala', 'PKY20310', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3532, 202, 'Mantangai', 'PKY20311', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3533, 202, 'Pulau Petak', 'PKY20313', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3534, 202, 'Timpah', 'PKY20314', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3535, 202, 'Kapuas Hulu', 'PKY20316', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3536, 202, 'Kapuas Hilir ', 'PKY20317', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3537, 202, 'Selat', 'PKY20318', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3538, 197, 'Kuala Kurun', 'PKY20400', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3539, 197, 'Kahayan Hulu Utara', 'PKY20401', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3540, 197, 'Rungan', 'PKY20404', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3541, 197, 'Sepang', 'PKY20405', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3542, 197, 'Tewah', 'PKY20406', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3543, 197, 'Munuhing', 'PKY20408', 119000, 0, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3544, 201, 'Kuala Pembuang', 'PKY20500', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3545, 201, 'Danau Sembuluh', 'PKY20501', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3546, 201, 'Hanau', 'PKY20502', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3547, 201, 'Seruyan Hulu', 'PKY20503', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3548, 201, 'Seruyan Tengah', 'PKY20504', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3549, 201, 'Seruyan Hilir ', 'PKY20505', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3550, 196, 'Pangkalan Bun', 'PKY20700', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3551, 196, 'Arut Utara', 'PKY20701', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3552, 196, 'Kota Waringin Lama', 'PKY20706', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3553, 196, 'Kumai', 'PKY20707', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3554, 196, 'Arut Selatan', 'PKY20710', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3555, 196, 'Pangkalan Lada ', 'PKY20711', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3556, 196, 'Pangkalan Banteng', 'PKY20712', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3557, 200, 'Sampit', 'PKY20800', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3558, 200, 'Cempaga', 'PKY20801', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3559, 200, 'Kota Besi', 'PKY20802', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3560, 200, 'Mentaya Hilir Selatan', 'PKY20803', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3561, 200, 'Mentaya Hulu', 'PKY20804', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3562, 200, 'Mentaya Hilir Utara', 'PKY20805', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3563, 200, 'Parenggean', 'PKY20806', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3564, 200, 'Pulau Hanaut', 'PKY20807', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3565, 200, 'Antang Kalang ', 'PKY20808', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3566, 200, 'Baamang ', 'PKY20809', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3567, 200, 'Mentawa Baru ', 'PKY20810', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3568, 195, 'Nanga Bulik', 'PKY21100', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3569, 195, 'Bulik', 'PKY21101', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3570, 195, 'Delang', 'PKY21102', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3571, 195, 'Lamandau', 'PKY21103', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3572, 199, 'Pulang Pisau', 'PKY21200', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3573, 199, 'Banamatingang', 'PKY21201', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3574, 199, 'Kahayan Hilir', 'PKY21202', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3575, 199, 'Kahayan Tengah', 'PKY21203', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3576, 199, 'Kahayan Kuala', 'PKY21204', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3577, 199, 'Pandih Batu', 'PKY21205', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3578, 199, 'Maliku ', 'PKY21206', 119000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3579, 194, 'Sukamara', 'PKY21300', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3580, 194, 'Balai Riam', 'PKY21301', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3581, 194, 'Jelai', 'PKY21302', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3582, 433, 'Palembang', 'PLM10000', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3583, 433, 'Plaju', 'PLM10001', 28000, 25000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3584, 433, 'Alang-Alang Lebar', 'PLM10003', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3585, 433, 'Bukit Kecil', 'PLM10004', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3586, 433, 'Gandus', 'PLM10005', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3587, 433, 'Ilir Barat I', 'PLM10006', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3588, 433, 'Ilir Barat II', 'PLM10007', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3589, 433, 'Ilir Timur I', 'PLM10008', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3590, 433, 'Ilir Timur II', 'PLM10009', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3591, 433, 'Kalidoni', 'PLM10010', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3592, 433, 'Kemuning', 'PLM10011', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3593, 433, 'Kertapati', 'PLM10012', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3594, 433, 'Sako', 'PLM10013', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3595, 433, 'Seberang Ulu I', 'PLM10014', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3596, 433, 'Seberang Ulu II', 'PLM10015', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3597, 433, 'Sematang Borang', 'PLM10016', 28000, 25000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3598, 433, 'Sukarame', 'PLM10017', 28000, 25000, 32000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3599, 440, 'Muara Beliti Baru', 'PLM10300', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3600, 440, 'BKL Ulu Terawas', 'PLM10301', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3601, 440, 'BTS Ulu', 'PLM10302', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3602, 440, 'Jayaloka', 'PLM10303', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3603, 440, 'Karang Dapo', 'PLM10304', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3604, 440, 'Karang Jaya', 'PLM10305', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3605, 440, 'Megang Sakti', 'PLM10306', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3606, 440, 'Muara Beliti', 'PLM10307', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3607, 440, 'Muara Kelingi', 'PLM10308', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3608, 440, 'Muara Lakitan', 'PLM10309', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3609, 440, 'Muara Rupit', 'PLM10310', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3610, 440, 'Nibung', 'PLM10311', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3611, 440, 'Purwodadi ', 'PLM10312', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3612, 440, 'Rawas Ulu', 'PLM10313', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3613, 440, 'Selangit', 'PLM10314', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3614, 440, 'Tugumulyo', 'PLM10315', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3615, 440, 'Ulu Rawas', 'PLM10316', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3616, 440, 'Rawas Ilir', 'PLM10317', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3617, 431, 'Baturaja', 'PLM20100', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3618, 431, 'Peninjauan', 'PLM20108', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3619, 431, 'Pengandonan', 'PLM20109', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3620, 431, 'Sosoh Buay Rayap', 'PLM20112', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3621, 431, 'Baturaja Barat', 'PLM20113', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3622, 431, 'Baturaja Timur', 'PLM20114', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3623, 431, 'Lengkiti', 'PLM20115', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3624, 431, 'Lubuk Batang', 'PLM20116', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3625, 431, 'Semidang Aji', 'PLM20119', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3626, 431, 'Ulu Ogan', 'PLM20120', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3627, 438, 'Kota Kayu Agung', 'PLM20200', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3628, 438, 'Mesuji ', 'PLM20202', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3629, 438, 'Pampangan', 'PLM20204', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3630, 438, 'Pedamaran', 'PLM20205', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3631, 438, 'Sirah Pulau Padang', 'PLM20207', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3632, 438, 'Tanjung Lubuk', 'PLM20208', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3633, 438, 'Tulung Selapan', 'PLM20211', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3634, 438, 'Air Sugihan', 'PLM20214', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3635, 438, 'Cengal', 'PLM20215', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3636, 438, 'Jejawi', 'PLM20216', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3637, 438, 'Lempuing', 'PLM20217', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3638, 438, 'Pematang Panggang', 'PLM20221', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3639, 429, 'Lahat', 'PLM20300', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3640, 429, 'Merapi', 'PLM20303', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3641, 429, 'Pulau Pinang', 'PLM20304', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3642, 429, 'Tanjung Sakti', 'PLM20314', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3643, 429, 'Jarai', 'PLM20316', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3644, 429, 'Kikim Barat', 'PLM20317', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3645, 429, 'Kikim Selatan', 'PLM20318', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3646, 429, 'Kikim Tengah', 'PLM20319', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3647, 429, 'Kikim Timur', 'PLM20320', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3648, 429, 'Kota Agung', 'PLM20321', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3649, 429, 'Mulak Ulu', 'PLM20323', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3650, 429, 'Pajar Bulan', 'PLM20324', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3651, 436, 'Muara Enim', 'PLM20500', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3652, 436, 'Gunung Megang', 'PLM20501', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3653, 436, 'Tanjung Agung', 'PLM20503', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3654, 436, 'Gelumbang', 'PLM20504', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3655, 436, 'Rembang Dangku', 'PLM20505', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3656, 436, 'Rembang ', 'PLM20506', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3657, 436, 'Benakat', 'PLM20507', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3658, 436, 'Lawang Kidul', 'PLM20508', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3659, 436, 'Lembak', 'PLM20509', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3660, 436, 'Lubai', 'PLM20510', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3661, 436, 'Penukal Abab', 'PLM20511', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3662, 436, 'Penukal Utara', 'PLM20512', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3663, 436, 'Semende Darat Laut', 'PLM20513', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3664, 436, 'Semende Darat Tengah', 'PLM20514', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3665, 436, 'Semende Darat Ulu', 'PLM20515', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3666, 436, 'Sungai Rotan', 'PLM20516', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3667, 436, 'Talang Ubi', 'PLM20517', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3668, 436, 'Tanah Abang', 'PLM20518', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3669, 436, 'Ujan Mas', 'PLM20519', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3670, 436, 'Abab (Penukal Abab)', 'PLM20520', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3671, 436, 'Muara Belikan', 'PLM20521', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3672, 436, 'Kelekar', 'PLM20522', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3673, 428, 'Pagar Alam', 'PLM20600', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3674, 428, 'Pagar Alam Utara', 'PLM20606', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3675, 428, 'Pagar Alam Selatan', 'PLM20607', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3676, 428, 'Dempo Selatan', 'PLM20608', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3677, 428, 'Dempo Tengah', 'PLM20609', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3678, 428, 'Dempo Utara', 'PLM20610', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3679, 435, 'Prabumulih', 'PLM20700', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3680, 435, 'Cambai', 'PLM20704', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3681, 435, 'Prabumulih Barat', 'PLM20705', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3682, 435, 'Rambang Kapak Tengah', 'PLM20707', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3683, 435, 'Prabumulih Timur', 'PLM20708', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3684, 427, 'Sekayu', 'PLM20800', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3685, 427, 'Babat Toman', 'PLM20801', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3686, 427, 'Bayung Lencir', 'PLM20805', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3687, 427, 'Sungai Lilin', 'PLM20806', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3688, 427, 'Batang Harileko', 'PLM20816', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3689, 427, 'Keluang', 'PLM20817', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3690, 427, 'Lais', 'PLM20818', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3691, 427, 'Sanga Desa', 'PLM20819', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3692, 427, 'Sungai Keruh', 'PLM20820', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3693, 434, 'Pangkalan Balai', 'PLM21000', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3694, 434, 'Banyuasin I', 'PLM21001', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3695, 434, 'Banyuasin II', 'PLM21002', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3696, 434, 'Banyuasin III', 'PLM21003', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3697, 434, 'Betung', 'PLM21005', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3698, 434, 'Makarti Jaya', 'PLM21006', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3699, 434, 'Muara Telang', 'PLM21007', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3700, 434, 'Pulau Rimau', 'PLM21008', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3701, 434, 'Rambutan', 'PLM21009', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3702, 434, 'Rantau Bayur', 'PLM21010', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3703, 434, 'Talang Kelapa', 'PLM21011', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3704, 434, 'Muara Padang', 'PLM21012', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3705, 426, 'Tebing Tinggi', 'PLM21100', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3706, 426, 'Lintang Kanan', 'PLM21101', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3707, 426, 'Muara Pinang', 'PLM21102', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3708, 426, 'Pasemah Air Keruh', 'PLM21103', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3709, 426, 'Pendopo', 'PLM21104', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3710, 426, 'Talang Padang', 'PLM21105', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3711, 426, 'Ulu Musi', 'PLM21106', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3712, 432, 'Indralaya', 'PLM21300', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3713, 432, 'Muara Kuang', 'PLM21301', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3714, 432, 'Pemulutan', 'PLM21302', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3715, 432, 'Rantau Alai', 'PLM21303', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3716, 432, 'Tanjung Batu', 'PLM21304', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3717, 432, 'Tanjung Raja', 'PLM21305', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3718, 439, 'Martapura', 'PLM21400', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3719, 439, 'Belitang ', 'PLM21401', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3720, 439, 'Belitang II', 'PLM21402', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3721, 439, 'Belitang III', 'PLM21403', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3722, 439, 'Buay Madang', 'PLM21404', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3723, 439, 'Buay Pemuka Peliung', 'PLM21405', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3724, 439, 'Cempaka', 'PLM21406', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3725, 439, 'Madang Suku I', 'PLM21407', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3726, 439, 'Madang Suku II', 'PLM21408', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3727, 439, 'Semendawa Suku III', 'PLM21409', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3728, 439, 'MADANG SUKU III', 'PLM21411', 47000, 40000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3729, 430, 'Muaradua', 'PLM21500', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3730, 430, 'Banding Agung', 'PLM21501', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3731, 430, 'Buay Pemaca', 'PLM21502', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3732, 430, 'Buay Runjung', 'PLM21503', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3733, 430, 'Buay Sandang Aji', 'PLM21504', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3734, 430, 'Kisam Tinggi', 'PLM21505', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3735, 430, 'Mekakau Ilir', 'PLM21506', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3736, 430, 'Muaradua Kisam', 'PLM21507', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3737, 430, 'Pulau Beringin', 'PLM21508', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3738, 430, 'Simpang', 'PLM21509', 47000, 40000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3739, 437, 'Lubuk Linggau', 'PLM21600', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3740, 437, 'Lubuklinggau Barat I ', 'PLM21601', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3741, 437, 'Lubuklinggau Barat II ', 'PLM21602', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3742, 437, 'Lubuklinggau Selatan I ', 'PLM21603', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3743, 437, 'Lubuklinggau Selatan II', 'PLM21604', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3744, 437, 'Lubuklinggau Timur I ', 'PLM21605', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3745, 437, 'Lubuklinggau Timur II ', 'PLM21606', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3746, 437, 'Lubuklinggau Utara I ', 'PLM21607', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3747, 437, 'Lubuklinggau Utara II', 'PLM21608', 37000, 32000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3748, 379, 'Palu', 'PLW10000', 51000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3749, 379, 'Palu Barat', 'PLW10017', 51000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3750, 379, 'Palu Selatan', 'PLW10018', 51000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3751, 379, 'Palu Timur', 'PLW10019', 51000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3752, 379, 'Palu Utara', 'PLW10020', 51000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3753, 384, 'Luwuk', 'PLW20100', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3754, 384, 'Balantak', 'PLW20101', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3755, 384, 'Batui', 'PLW20103', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3756, 384, 'Bunta', 'PLW20106', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3757, 384, 'Kintom', 'PLW20107', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3758, 384, 'Lamala', 'PLW20109', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3759, 384, 'Pagimana', 'PLW20111', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3760, 384, 'Boalemo', 'PLW20116', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3761, 384, 'Toili', 'PLW20117', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3762, 378, 'Poso Kota', 'PLW20200', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3763, 378, 'Lage', 'PLW20206', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3764, 378, 'Lore Utara', 'PLW20208', 80000, 69000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (3765, 378, 'Lore Selatan', 'PLW20209', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3766, 378, 'Pamona Selatan', 'PLW20212', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3767, 378, 'Pamona Utara', 'PLW20213', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3768, 378, 'Poso Pesisir', 'PLW20215', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3769, 378, 'Lore Tengah', 'PLW20221', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3770, 378, 'Pamona Timur', 'PLW20222', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3771, 383, 'Toli-Toli', 'PLW20300', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3772, 383, 'Dampal Utara', 'PLW20304', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3773, 383, 'Dampal Selatan', 'PLW20305', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3774, 383, 'Dondo', 'PLW20306', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3775, 383, 'Galang ', 'PLW20307', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3776, 383, 'Utara Toli Toli', 'PLW20310', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3777, 383, 'Baolan', 'PLW20311', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3778, 383, 'Basidondo', 'PLW20313', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3779, 383, 'Lampasio', 'PLW20314', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3780, 383, 'Ogo Deide', 'PLW20315', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3781, 377, 'Banggai', 'PLW20500', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3782, 377, 'Bokan Kepulauan', 'PLW20501', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3783, 377, 'Buko', 'PLW20502', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3784, 377, 'Bulagi', 'PLW20503', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3785, 377, 'Bulagi Selatan', 'PLW20504', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3786, 377, 'Liang', 'PLW20505', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3787, 377, 'Lo Bangkurung', 'PLW20506', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3788, 377, 'Tinangkung', 'PLW20507', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3789, 377, 'Totikum', 'PLW20508', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3790, 382, 'Buol', 'PLW20600', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3791, 382, 'Biau', 'PLW20601', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3792, 382, 'Bokat', 'PLW20602', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3793, 382, 'Bukal', 'PLW20603', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3794, 382, 'Bunobogu', 'PLW20604', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3795, 382, 'Gadung', 'PLW20605', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3796, 382, 'Lipunoto', 'PLW20606', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3797, 382, 'Momunu', 'PLW20607', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3798, 382, 'Paleleh', 'PLW20608', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3799, 382, 'Tiolan', 'PLW20609', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3800, 376, 'Banawa', 'PLW20700', 51000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3801, 376, 'Balaesang', 'PLW20701', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3802, 376, 'Damsol', 'PLW20702', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3803, 376, 'Dolo', 'PLW20703', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3804, 376, 'Kulawi', 'PLW20704', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3805, 376, 'Marawola', 'PLW20705', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3806, 376, 'Palolo', 'PLW20706', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3807, 376, 'Pipikoro', 'PLW20707', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3808, 376, 'Riopakawa', 'PLW20708', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3809, 376, 'Sigi Biromaru', 'PLW20709', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3810, 376, 'Sindue', 'PLW20710', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3811, 376, 'Sirenja', 'PLW20711', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3812, 376, 'Sojol', 'PLW20712', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3813, 376, 'Tawaeli', 'PLW20713', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3814, 381, 'Bungku', 'PLW20800', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3815, 381, 'Bungku Barat', 'PLW20801', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3816, 381, 'Bungku Selatan', 'PLW20802', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3817, 381, 'Bungku Tengah', 'PLW20803', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3818, 381, 'Bungku Utara', 'PLW20804', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3819, 381, 'Bahodopi', 'PLW20805', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3820, 381, 'Bumi Raya', 'PLW20806', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3821, 381, 'Lembo', 'PLW20807', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3822, 381, 'Mamosalato', 'PLW20808', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3823, 381, 'Menui Kepulauan', 'PLW20809', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3824, 381, 'Mori Atas', 'PLW20810', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3825, 381, 'Petasia', 'PLW20811', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3826, 381, 'Soyo Jaya', 'PLW20812', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3827, 381, 'Wita Ponda', 'PLW20813', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3828, 385, 'Parigi', 'PLW20900', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3829, 385, 'Ampibabo', 'PLW20901', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3830, 385, 'Bolano Lambunu', 'PLW20902', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3831, 385, 'Kasimbar', 'PLW20903', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3832, 385, 'Moutong', 'PLW20904', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3833, 385, 'Sausu', 'PLW20905', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3834, 385, 'Tinombo', 'PLW20906', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3835, 385, 'Tinombo Selatan', 'PLW20907', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3836, 385, 'Tomini', 'PLW20908', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3837, 385, 'Torue', 'PLW20909', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3838, 380, 'Ampana Kota', 'PLW21000', 64000, 55000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3839, 380, 'Ampana Tete', 'PLW21001', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3840, 380, 'Togean ', 'PLW21002', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3841, 380, 'Tojo', 'PLW21003', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3842, 380, 'Tojo Barat ', 'PLW21004', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3843, 380, 'Ulu Bongka', 'PLW21005', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3844, 380, 'Una-Una', 'PLW21006', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3845, 380, 'Walea Kepulauan', 'PLW21007', 80000, 69000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3846, 174, 'Pontianak', 'PNK10000', 35000, 30000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3847, 174, 'Pontianak Barat ', 'PNK10027', 35000, 30000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3848, 174, 'Pontianak Kota ', 'PNK10028', 35000, 30000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3849, 174, 'Pontianak Selatan ', 'PNK10029', 35000, 30000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3850, 174, 'Pontianak Tenggara ', 'PNK10030', 35000, 30000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3851, 174, 'Pontianak Timur/ Jungkat', 'PNK10031', 35000, 30000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3852, 174, 'Pontianak Utara', 'PNK10032', 35000, 30000, 40000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3853, 166, 'Ketapang', 'PNK10100', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3854, 166, 'Jelai Hulu', 'PNK10101', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3855, 166, 'Kendawangan', 'PNK10102', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3856, 166, 'Manis Mata', 'PNK10103', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3857, 166, 'Marau', 'PNK10104', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3858, 166, 'Nanga Tayap', 'PNK10105', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3859, 166, 'Sandai', 'PNK10107', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3860, 166, 'Simpang Hulu', 'PNK10109', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3861, 166, 'Sungai Laur', 'PNK10111', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3862, 166, 'Tumbang Titi', 'PNK10112', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3863, 166, 'Benua Kayong ', 'PNK10113', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3864, 166, 'Air Upas ', 'PNK10114', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3865, 166, 'Delta Pawan ', 'PNK10115', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3866, 166, 'Hulu Sungai ', 'PNK10116', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3867, 166, 'Matan Hilir Selatan ', 'PNK10118', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3868, 166, 'Matan Hilir Utara', 'PNK10119', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3869, 166, 'Muara Pawan ', 'PNK10120', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3870, 166, 'Pemaham ', 'PNK10121', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3871, 166, 'Simpang Dua ', 'PNK10123', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3872, 166, 'Singkup ', 'PNK10124', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3873, 172, 'Sanggau', 'PNK10200', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3874, 172, 'Balai', 'PNK10201', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3875, 172, 'Beduwai', 'PNK10202', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3876, 172, 'Bonti', 'PNK10205', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3877, 172, 'Jangkang', 'PNK10206', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3878, 172, 'Kembayan', 'PNK10207', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3879, 172, 'Meliau', 'PNK10208', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3880, 172, 'Mukok', 'PNK10209', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3881, 172, 'Noyan', 'PNK10212', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3882, 172, 'Parindu', 'PNK10213', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3883, 172, 'Sekayam', 'PNK10214', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3884, 172, 'Tayan Hulu', 'PNK10217', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3885, 172, 'Tayan Hilir', 'PNK10218', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3886, 172, 'Toba', 'PNK10219', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3887, 172, 'Entikong', 'PNK10223', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3888, 164, 'Singkawang', 'PNK10300', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3889, 164, 'Singkawang Barat ', 'PNK10325', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3890, 164, 'Singkawang Utara ', 'PNK10326', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3891, 164, 'Singkawang Selatan', 'PNK10327', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3892, 164, 'Singkawang Timur ', 'PNK10328', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3893, 164, 'Singkawang Tengah', 'PNK10329', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3894, 170, 'Sintang', 'PNK10400', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3895, 170, 'Ambalau ', 'PNK10401', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3896, 170, 'Dedai', 'PNK10403', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3897, 170, 'Kayan Hulu ', 'PNK10405', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3898, 170, 'Kayan Hilir', 'PNK10406', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3899, 170, 'Ketungau Hilir', 'PNK10407', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3900, 170, 'Ketungau Tengah', 'PNK10408', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3901, 170, 'Ketungau Hulu', 'PNK10409', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3902, 170, 'Sepauk', 'PNK10413', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3903, 170, 'Tempunak', 'PNK10417', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3904, 170, 'Binjai Hulu ', 'PNK10419', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3905, 170, 'Kelam Permai ', 'PNK10420', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3906, 170, 'Sungai Tebelian ', 'PNK10421', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3907, 163, 'Putussibau', 'PNK20200', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3908, 163, 'Badau', 'PNK20201', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3909, 163, 'Batang Lupar', 'PNK20202', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3910, 163, 'Bunut Hilir', 'PNK20203', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3911, 163, 'Bunut Hulu', 'PNK20204', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3912, 163, 'Embaloh Hilir', 'PNK20205', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3913, 163, 'Embaloh Hulu', 'PNK20206', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3914, 163, 'Embau ', 'PNK20207', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3915, 163, 'Empanang', 'PNK20208', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3916, 163, 'Hulu Gurung', 'PNK20209', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3917, 163, 'Manday', 'PNK20210', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3918, 163, 'Seberuang', 'PNK20211', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3919, 163, 'Selimbau', 'PNK20212', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3920, 163, 'Semitau', 'PNK20213', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3921, 163, 'Silat Hilir', 'PNK20214', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3922, 163, 'Silat Hulu', 'PNK20215', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3923, 163, 'Batu Datu ', 'PNK20218', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3924, 163, 'Boyan Tanjung ', 'PNK20219', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3925, 163, 'Kalis ', 'PNK20220', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3926, 163, 'Kedamin ', 'PNK20221', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3927, 163, 'Mentebah ', 'PNK20222', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3928, 163, 'Puring Kencana ', 'PNK20223', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3929, 163, 'Suhaid', 'PNK20224', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3930, 169, 'Bengkayang', 'PNK20600', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3931, 169, 'Ledo', 'PNK20601', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3932, 169, 'Samalantan', 'PNK20602', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3933, 169, 'Sanggau Ledo', 'PNK20603', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3934, 169, 'Seluas', 'PNK20604', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3935, 169, 'Sungai Raya', 'PNK20605', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3936, 169, 'Jagoi Babang', 'PNK20606', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3937, 169, 'Monterado', 'PNK20607', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3938, 169, 'Suti Semarang', 'PNK20608', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3939, 169, 'Teriak', 'PNK20609', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3940, 162, 'Ngabang', 'PNK20700', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3941, 162, 'Air Besar', 'PNK20701', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3942, 162, 'Mandor', 'PNK20702', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3943, 162, 'Mempawah Hulu', 'PNK20703', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3944, 162, 'Menjalin', 'PNK20704', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3945, 162, 'Menyuke', 'PNK20705', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3946, 162, 'Sengah Temila', 'PNK20706', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3947, 162, 'Kuala Behe', 'PNK20707', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3948, 162, 'Meranti', 'PNK20708', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3949, 162, 'Sebangki', 'PNK20709', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3950, 168, 'Nanga Pinoh', 'PNK20800', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3951, 168, 'Ambalau ', 'PNK20801', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3952, 168, 'Belimbing', 'PNK20802', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3953, 168, 'Ella Hilir', 'PNK20803', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3954, 168, 'Menukung', 'PNK20804', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3955, 168, 'Sayan', 'PNK20805', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3956, 168, 'Serawai', 'PNK20806', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3957, 168, 'Sokan', 'PNK20807', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3958, 168, 'Tanah Pinoh', 'PNK20808', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3959, 175, 'Mempawah', 'PNK20900', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3960, 175, 'Kubu ', 'PNK20902', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3961, 175, 'Sungai Ambawang', 'PNK20903', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3962, 175, 'Sungai Kakap', 'PNK20904', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3963, 175, 'Sungai Kunyit', 'PNK20905', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3964, 175, 'Sungai Pinyuh', 'PNK20906', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3965, 175, 'Sungai Raya ', 'PNK20907', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3966, 175, 'Siantan / Wajok', 'PNK20908', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3967, 175, 'Telok Pa\'kedai', 'PNK20909', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3968, 175, 'Toho', 'PNK20911', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3969, 175, 'Rasau Jaya', 'PNK20913', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3970, 167, 'Sambas', 'PNK21000', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3971, 167, 'Jawai', 'PNK21001', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3972, 167, 'Paloh', 'PNK21002', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3973, 167, 'Pemangkat', 'PNK21003', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3974, 167, 'Sejangkung', 'PNK21004', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3975, 167, 'Selakau', 'PNK21005', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3976, 167, 'Tebas', 'PNK21006', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3977, 167, 'Teluk Keramat', 'PNK21007', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3978, 167, 'Galing', 'PNK21008', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3979, 167, 'Jawai Selatan', 'PNK21009', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3980, 167, 'Sajingan', 'PNK21010', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3981, 167, 'Sajad', 'PNK21011', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3982, 167, 'Sebawi', 'PNK21012', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3983, 167, 'Semparuk', 'PNK21013', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3984, 167, 'Subah ', 'PNK21014', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3985, 167, 'Tanggaran', 'PNK21015', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3986, 167, 'Tekarang', 'PNK21016', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3987, 173, 'Sekadau Hilir', 'PNK21100', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3988, 173, 'Belitang Hilir', 'PNK21101', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3989, 173, 'Belitang Hulu', 'PNK21102', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3990, 173, 'Nanga Taman', 'PNK21103', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3991, 173, 'Nanga Mahap', 'PNK21104', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3992, 173, 'Sekadau Hulu', 'PNK21106', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3993, 173, 'Belitang ', 'PNK21107', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3994, 165, 'Kubu Raya', 'PNK21200', 35000, 30000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3995, 165, 'Kuala Mandor', 'PNK21201', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3996, 165, 'Sui Ambawang', 'PNK21202', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3997, 165, 'Sui Kakap', 'PNK21203', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3998, 165, 'Sui Raya', 'PNK21204', 65000, 57000, 69000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (3999, 165, 'Teluk Pakedai', 'PNK21205', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4000, 165, 'Terentang', 'PNK21206', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4001, 165, 'Rasau Jaya', 'PNK21207', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4002, 165, 'Batu Ampar', 'PNK21208', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4003, 165, 'Sungai Raya', 'PNK21209', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4004, 171, 'Sukadana', 'PNK21300', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4005, 171, 'Pulau Maya Karimata', 'PNK21301', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4006, 171, 'Simpang Hilir', 'PNK21302', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4007, 171, 'Teluk Batang', 'PNK21303', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4008, 171, 'Seponti Jaya', 'PNK21304', 65000, 57000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4009, 207, 'Samarinda', 'SMD10000', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4010, 207, 'Samarinda Seberang', 'SMD10005', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4011, 207, 'Palaran', 'SMD10007', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4012, 207, 'Samarinda Ilir', 'SMD10009', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4013, 207, 'Samarinda Ulu', 'SMD10010', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4014, 207, 'Samarinda Utara', 'SMD10011', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4015, 207, 'Sungai Kunjang', 'SMD10012', 49000, 42000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4016, 213, 'Tenggarong', 'SMD20100', 68000, 58000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4017, 213, 'Kembang Janggut', 'SMD20106', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4018, 213, 'Kenohan', 'SMD20107', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4019, 213, 'Kota Bangun', 'SMD20108', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4020, 213, 'Loa Kulu', 'SMD20109', 68000, 58000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4021, 213, 'Muara Kaman', 'SMD20117', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4022, 213, 'Muara Muntai', 'SMD20120', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4023, 213, 'Sebulu', 'SMD20123', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4024, 213, 'Tabang ', 'SMD20124', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4025, 213, 'Loa Janan', 'SMD20126', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4026, 213, 'Muara Badak', 'SMD20127', 68000, 58000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4027, 213, 'Sanga-Sanga', 'SMD20128', 68000, 58000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4028, 213, 'Marang Kayu', 'SMD20130', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4029, 213, 'Muara Wis', 'SMD20131', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4030, 213, 'Tenggarong Seberang', 'SMD20132', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4031, 213, 'Anggana', 'SMD20133', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4032, 205, 'Sendawar', 'SMD20200', 68000, 58000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4033, 205, 'Barong Tongkok', 'SMD20201', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4034, 205, 'Bentian Besar', 'SMD20202', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4035, 205, 'Bongan', 'SMD20203', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4036, 205, 'Damai', 'SMD20204', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4037, 205, 'Jempang', 'SMD20205', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4038, 205, 'Long Apari', 'SMD20206', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4039, 205, 'Long Iram', 'SMD20207', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4040, 205, 'Long Pahangai', 'SMD20208', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4041, 205, 'Long Bagun', 'SMD20209', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4042, 205, 'Melak', 'SMD20210', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4043, 205, 'Muara Pahu', 'SMD20211', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4044, 205, 'Muara Lawa', 'SMD20212', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4045, 205, 'Penyinggahan', 'SMD20213', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4046, 205, 'Linggang Bigung ', 'SMD20214', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4047, 205, 'Long Hubung ', 'SMD20215', 127000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4048, 205, 'Nyuwatan ', 'SMD20216', 84000, 72000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4049, 78, 'Sukabumi', 'SMI10000', 16000, 14000, 25000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4050, 78, 'Bantargadung', 'SMI10001', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4051, 62, 'Baros ', 'SMI10002', 16000, 14000, 25000, 24000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4052, 78, 'Bojong Genteng', 'SMI10003', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4053, 78, 'Caringin', 'SMI10004', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4054, 78, 'Cibadak ', 'SMI10005', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4055, 78, 'Cibitung', 'SMI10006', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4056, 78, 'Cicurug', 'SMI10007', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4057, 78, 'Cicantayan', 'SMI10008', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4058, 78, 'Cidahu ', 'SMI10009', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4059, 78, 'Cidolog ', 'SMI10010', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4060, 78, 'Cidadap', 'SMI10011', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4061, 78, 'Ciemas', 'SMI10012', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4062, 78, 'Cikidang', 'SMI10013', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4063, 78, 'Cikembar', 'SMI10014', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4064, 78, 'Cikakak', 'SMI10015', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4065, 78, 'Ciracap', 'SMI10016', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4066, 78, 'Cireunghas', 'SMI10017', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4067, 78, 'Cisaat', 'SMI10018', 16000, 14000, 25000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4068, 78, 'Cisolok', 'SMI10019', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4069, 78, 'Curugkembar', 'SMI10020', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4070, 78, 'Gegerbitung', 'SMI10021', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4071, 78, 'Gunung Guruh', 'SMI10022', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4072, 78, 'Jampang Kulon', 'SMI10023', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4073, 78, 'Jampang Tengah', 'SMI10024', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4074, 78, 'Kabandungan', 'SMI10025', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4075, 78, 'Kadudampit', 'SMI10026', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4076, 78, 'Kalapa Nunggal', 'SMI10027', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4077, 78, 'Kalibunder', 'SMI10028', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4078, 78, 'Kebonpedes', 'SMI10029', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4079, 78, 'Lengkong ', 'SMI10030', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4080, 78, 'Nagrak', 'SMI10031', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4081, 78, 'Nyalindung', 'SMI10032', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4082, 78, 'Pabuaran ', 'SMI10033', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4083, 78, 'Parung Kuda', 'SMI10034', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4084, 78, 'Parakan Salak', 'SMI10035', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4085, 78, 'Pelabuhan Ratu', 'SMI10036', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4086, 78, 'Purabaya', 'SMI10037', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4087, 78, 'Sagaranten', 'SMI10038', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4088, 78, 'Simpenan', 'SMI10039', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4089, 78, 'Sukalarang', 'SMI10040', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4090, 78, 'Sukaraja ', 'SMI10042', 16000, 14000, 25000, 0, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4091, 78, 'Surade', 'SMI10043', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4092, 78, 'Tegal Buleud', 'SMI10044', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4093, 78, 'Waluran', 'SMI10045', 21000, 19000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4094, 78, 'Warung Kiara', 'SMI10046', 21000, 19000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4095, 62, 'LEMBUR SITU', 'SMI10047', 16000, 14000, 25000, 24000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4096, 62, 'Cikole', 'SMI10051', 16000, 14000, 25000, 24000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4097, 62, 'Citamiang', 'SMI10052', 16000, 14000, 25000, 24000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4098, 62, 'GUNUNG PUYUH', 'SMI10053', 16000, 14000, 25000, 24000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4099, 62, 'Warudoyong', 'SMI10054', 16000, 14000, 25000, 24000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4100, 62, 'Cibeureum ', 'SMI10055', 16000, 14000, 25000, 24000, 14000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4101, 74, 'Cianjur', 'SMI20100', 18000, 16000, 25000, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4102, 74, 'Agrabinta', 'SMI20101', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4103, 74, 'Bojongpicung', 'SMI20102', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4104, 74, 'Campaka', 'SMI20103', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4105, 74, 'Cibeber ', 'SMI20104', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4106, 74, 'Cibinong', 'SMI20105', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4107, 74, 'Cidaun', 'SMI20106', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4108, 74, 'CIKALONG KULON', 'SMI20107', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4109, 74, 'Cilaku', 'SMI20108', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4110, 74, 'Ciranjang', 'SMI20109', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4111, 74, 'Cugenang', 'SMI20110', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4112, 74, 'Kadupandak', 'SMI20111', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4113, 74, 'Karangtengah ', 'SMI20112', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4114, 74, 'Mande', 'SMI20113', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4115, 74, 'Naringgul', 'SMI20114', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4116, 74, 'Pacet ', 'SMI20115', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4117, 74, 'Pagelaran ', 'SMI20116', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4118, 74, 'Sindangbarang', 'SMI20117', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4119, 74, 'Sukaluyu', 'SMI20118', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4120, 74, 'Sukanagara', 'SMI20119', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4121, 74, 'Sukaresmi ', 'SMI20120', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4122, 74, 'Takokak', 'SMI20121', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4123, 74, 'Tanggeung', 'SMI20122', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4124, 74, 'Warungkondang', 'SMI20123', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4125, 74, 'Cipanas ', 'SMI20125', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4126, 74, 'Campaka Mulya', 'SMI20127', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4127, 74, 'Cijati', 'SMI20128', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4128, 74, 'Cikadu', 'SMI20129', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4129, 74, 'Gekbrong', 'SMI20130', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4130, 74, 'Leles ', 'SMI20131', 22000, 19000, 0, 0, 16000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4131, 120, 'Solo', 'SOC10000', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4132, 120, 'Banjarsari ', 'SOC10001', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4133, 120, 'Jebres', 'SOC10002', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4134, 120, 'Laweyan', 'SOC10003', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4135, 120, 'Pasar Kliwon', 'SOC10004', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4136, 120, 'Serengan', 'SOC10005', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4137, 100, 'Boyolali', 'SOC20100', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4138, 100, 'Ampel', 'SOC20101', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4139, 100, 'Andong', 'SOC20102', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4140, 100, 'Banyudono', 'SOC20103', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4141, 100, 'Cepogo', 'SOC20104', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4142, 100, 'Juwangi', 'SOC20105', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4143, 100, 'Karanggede', 'SOC20106', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4144, 100, 'Kemusu', 'SOC20107', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4145, 100, 'Klego', 'SOC20108', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4146, 100, 'Ngemplak ', 'SOC20109', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4147, 100, 'Nogosari', 'SOC20110', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4148, 100, 'Sambi', 'SOC20111', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4149, 100, 'Sawit', 'SOC20112', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4150, 100, 'Selo', 'SOC20113', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4151, 100, 'Simo', 'SOC20114', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4152, 100, 'Teras', 'SOC20115', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4153, 100, 'Wonosegoro', 'SOC20116', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4154, 100, 'Musuk', 'SOC20118', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4155, 100, 'Mojosongo', 'SOC20119', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4156, 116, 'Karanganyar', 'SOC20200', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4157, 116, 'Jatipuro', 'SOC20201', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4158, 116, 'Jatiyoso', 'SOC20202', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4159, 116, 'Jenawi', 'SOC20203', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4160, 116, 'Jumantono', 'SOC20204', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4161, 116, 'Jumapolo', 'SOC20205', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4162, 116, 'Karangpandan', 'SOC20206', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4163, 116, 'Kebakkramat', 'SOC20207', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4164, 116, 'Kerjo', 'SOC20208', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4165, 116, 'Matesih', 'SOC20209', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4166, 116, 'Mojogedang', 'SOC20210', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4167, 116, 'Ngargoyoso', 'SOC20211', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4168, 116, 'Tawangmangu', 'SOC20212', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4169, 116, 'Colomadu', 'SOC20213', 31000, 27000, 35000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4170, 116, 'Gondangrejo', 'SOC20214', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4171, 116, 'Jaten', 'SOC20215', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4172, 116, 'Tasikmadu', 'SOC20216', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4173, 96, 'Klaten', 'SOC20300', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4174, 96, 'Bayat', 'SOC20301', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4175, 96, 'Cawas', 'SOC20302', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4176, 96, 'Ceper', 'SOC20303', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4177, 96, 'Delanggu', 'SOC20304', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4178, 96, 'Gantiwarno', 'SOC20305', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4179, 96, 'Jatinom', 'SOC20306', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4180, 96, 'Jogonalan', 'SOC20307', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4181, 96, 'Juwiring', 'SOC20308', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4182, 96, 'Kalikotes', 'SOC20309', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4183, 96, 'Karangnongko', 'SOC20310', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4184, 96, 'Karanganom', 'SOC20311', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4185, 96, 'Karangdowo', 'SOC20312', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4186, 96, 'Kebonarum', 'SOC20313', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4187, 96, 'Kemalang', 'SOC20314', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4188, 96, 'Manisrenggo', 'SOC20315', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4189, 96, 'Ngawen ', 'SOC20316', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4190, 96, 'Pedan', 'SOC20317', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4191, 96, 'Polanharjo', 'SOC20318', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4192, 96, 'Prambanan ', 'SOC20319', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4193, 96, 'Trucuk', 'SOC20320', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4194, 96, 'Tulung', 'SOC20321', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4195, 96, 'Wedi', 'SOC20322', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4196, 96, 'Wonosari', 'SOC20323', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4197, 96, 'Klaten Selatan', 'SOC20324', 31000, 27000, 35000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4198, 96, 'Klaten Tengah', 'SOC20325', 31000, 27000, 35000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4199, 96, 'Klaten Utara', 'SOC20326', 31000, 27000, 35000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4200, 112, 'Sragen', 'SOC20400', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4201, 112, 'Gesi', 'SOC20401', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4202, 112, 'Gemolong', 'SOC20402', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4203, 112, 'Gondang', 'SOC20403', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4204, 112, 'Jenar', 'SOC20404', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4205, 112, 'Kalijambe', 'SOC20405', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4206, 112, 'Kedawung', 'SOC20406', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4207, 112, 'Miri', 'SOC20407', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4208, 112, 'Masaran', 'SOC20408', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4209, 112, 'Mondokan', 'SOC20409', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4210, 112, 'Ngrampal', 'SOC20410', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4211, 112, 'Plupuh', 'SOC20411', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4212, 112, 'Sambungmacan', 'SOC20412', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4213, 112, 'Sambirejo', 'SOC20413', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4214, 112, 'Sidoharjo', 'SOC20414', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4215, 112, 'Sukodono', 'SOC20416', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4216, 112, 'Sumberlawang', 'SOC20417', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4217, 112, 'Tanon', 'SOC20418', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4218, 112, 'Tangen', 'SOC20419', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4219, 112, 'Karangmalang', 'SOC20420', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4220, 92, 'Sukoharjo ', 'SOC20500', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4221, 92, 'Bulu', 'SOC20501', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4222, 92, 'Mojolaban / Palur', 'SOC20502', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4223, 92, 'Nguter', 'SOC20503', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4224, 92, 'Polokarto', 'SOC20504', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4225, 92, 'Tawangsari', 'SOC20505', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4226, 92, 'Weru', 'SOC20506', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4227, 92, 'Baki', 'SOC20508', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4228, 92, 'Bendosari', 'SOC20509', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4229, 92, 'Gatak', 'SOC20510', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4230, 92, 'Grogol ', 'SOC20511', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4231, 92, 'Kartasura', 'SOC20512', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4232, 108, 'Wonogiri', 'SOC20600', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4233, 108, 'Baturetno', 'SOC20601', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4234, 108, 'Batuwarno', 'SOC20602', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4235, 108, 'Bulukerto ', 'SOC20603', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4236, 108, 'Eromoko', 'SOC20604', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4237, 108, 'Girimarto', 'SOC20605', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4238, 108, 'Giritontro', 'SOC20606', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4239, 108, 'Giriwoyo ', 'SOC20608', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4240, 108, 'Jatipurno ', 'SOC20609', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4241, 108, 'Jatiroto ', 'SOC20610', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4242, 108, 'Jatisrono', 'SOC20611', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4243, 108, 'Karangtengah ', 'SOC20612', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4244, 108, 'Kismantoro ', 'SOC20613', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4245, 108, 'Manyaran ', 'SOC20614', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4246, 108, 'Ngadirojo', 'SOC20615', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4247, 108, 'Nguntoronadi ', 'SOC20616', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4248, 108, 'Pracimantoro ', 'SOC20617', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4249, 108, 'Purwantoro ', 'SOC20618', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4250, 108, 'Selogiri', 'SOC20619', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4251, 108, 'Sidoharjo ', 'SOC20620', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4252, 108, 'Slogohimo ', 'SOC20621', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4253, 108, 'Tirtomoyo', 'SOC20622', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4254, 108, 'Wuryantoro', 'SOC20623', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4255, 108, 'Puhpelem', 'SOC20624', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4256, 108, 'Paranggupito', 'SOC20626', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4257, 328, 'Sorong', 'SOQ10000', 101000, 86000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4258, 328, 'Makbon', 'SOQ10006', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4259, 328, 'Moraid', 'SOQ10008', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4260, 328, 'Salawati', 'SOQ10009', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4261, 328, 'Sausapor', 'SOQ10010', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4262, 328, 'Seget', 'SOQ10011', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4263, 328, 'Abun', 'SOQ10018', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4264, 328, 'Aimas', 'SOQ10019', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4265, 328, 'Beraur', 'SOQ10020', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4266, 328, 'Fef', 'SOQ10021', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4267, 328, 'Klamono', 'SOQ10022', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4268, 328, 'Sayosa', 'SOQ10023', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4269, 328, 'Segun', 'SOQ10024', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4270, 333, 'Sorong Barat', 'SOQ10025', 101000, 86000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4271, 333, 'Sorong Timur', 'SOQ10026', 101000, 86000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4272, 327, 'FAK-FAK', 'SOQ20000', 131000, 113000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4273, 327, 'Fakfak Barat', 'SOQ20001', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4274, 327, 'Fakfak Timur', 'SOQ20002', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4275, 327, 'Kokas', 'SOQ20003', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4276, 332, 'Kaimana', 'SOQ20100', 131000, 113000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4277, 332, 'Buruwai', 'SOQ20101', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4278, 332, 'Teluk Arguni', 'SOQ20102', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4279, 332, 'Teluk Etna', 'SOQ20103', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4280, 335, 'Waisai', 'SOQ20300', 131000, 113000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4281, 335, 'Kepulauan Ayau', 'SOQ20301', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4282, 335, 'Kofiau', 'SOQ20302', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4283, 335, 'Misool', 'SOQ20303', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4284, 335, 'Misool Timur Selatan', 'SOQ20304', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4285, 335, 'Samate', 'SOQ20305', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4286, 335, 'Teluk Mayalibit', 'SOQ20306', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4287, 335, 'Waigeo Barat', 'SOQ20307', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4288, 335, 'Waigeo Selatan', 'SOQ20308', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4289, 335, 'Waigeo Timur', 'SOQ20309', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4290, 335, 'Waigeo Utara', 'SOQ20310', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4291, 330, 'Teminabuan', 'SOQ20400', 131000, 113000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4292, 330, 'Aifat', 'SOQ20401', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4293, 330, 'Aifat Timur', 'SOQ20402', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4294, 330, 'Aitinyo', 'SOQ20403', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4295, 330, 'Ayamaru', 'SOQ20404', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4296, 330, 'Ayamaru Utara', 'SOQ20405', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4297, 330, 'Inanwatan', 'SOQ20406', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4298, 330, 'Kais', 'SOQ20407', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4299, 330, 'Kokoda', 'SOQ20408', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4300, 330, 'Mare ', 'SOQ20409', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4301, 330, 'Moswaren', 'SOQ20410', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4302, 330, 'Sawiat', 'SOQ20411', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4303, 330, 'Seremuk', 'SOQ20412', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4304, 330, 'Wayer', 'SOQ20413', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4305, 334, 'Bintuni', 'SOQ20500', 131000, 113000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4306, 334, 'Aranday', 'SOQ20501', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4307, 334, 'Babo', 'SOQ20502', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4308, 334, 'Fafuwar', 'SOQ20503', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4309, 334, 'Door', 'SOQ20504', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4310, 334, 'Kuri', 'SOQ20505', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4311, 334, 'Merdey', 'SOQ20506', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4312, 334, 'Moskona Selatan', 'SOQ20507', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4313, 334, 'Moskona Utara', 'SOQ20508', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4314, 334, 'Tembuni', 'SOQ20509', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4315, 329, 'Rasei', 'SOQ20600', 131000, 113000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4316, 329, 'Rumberpon', 'SOQ20601', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4317, 329, 'Wamesa', 'SOQ20602', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4318, 329, 'Wasior', 'SOQ20603', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4319, 329, 'Wasior Barat', 'SOQ20604', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4320, 329, 'Wasior Selatan', 'SOQ20605', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4321, 329, 'Wasior Utara', 'SOQ20606', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4322, 329, 'Windesi', 'SOQ20607', 164000, 140000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4323, 331, 'Manokwari', 'SOQ20608', 131000, 113000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4324, 331, 'Amberbaken', 'SOQ20609', 164000, 140000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4325, 331, 'Anggi', 'SOQ20610', 164000, 140000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4326, 331, 'Kebar', 'SOQ20611', 164000, 140000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4327, 331, 'Oransbari', 'SOQ20612', 164000, 140000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4328, 331, 'Ransiki', 'SOQ20613', 164000, 140000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4329, 331, 'Warmare', 'SOQ20614', 164000, 140000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4330, 331, 'Masni', 'SOQ20615', 164000, 140000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4331, 331, 'Minyambouw', 'SOQ20616', 164000, 140000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4332, 331, 'Prafi', 'SOQ20617', 164000, 140000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4333, 331, 'Sugurey', 'SOQ20618', 164000, 140000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4334, 111, 'Semarang', 'SRG10000', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4335, 111, 'Banyumanik', 'SRG10001', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4336, 111, 'Candisari', 'SRG10002', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4337, 111, 'Gajahmungkur', 'SRG10003', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4338, 111, 'Gayamsari', 'SRG10004', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4339, 111, 'Genuk', 'SRG10005', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4340, 111, 'Gunungpati', 'SRG10006', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4341, 111, 'Mijen', 'SRG10007', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4342, 111, 'Ngaliyan', 'SRG10008', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4343, 111, 'Pedurungan', 'SRG10009', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4344, 111, 'Semarang Barat', 'SRG10010', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4345, 111, 'Semarang Selatan', 'SRG10011', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4346, 111, 'Semarang Tengah', 'SRG10012', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4347, 111, 'Semarang Timur', 'SRG10013', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4348, 111, 'Semarang Utara', 'SRG10014', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4349, 111, 'Tembalang', 'SRG10015', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4350, 111, 'Tugu', 'SRG10016', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4351, 91, 'Jepara', 'SRG10100', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4352, 91, 'Bangsri', 'SRG10101', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4353, 91, 'Batealit', 'SRG10102', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4354, 91, 'Karimunjawa', 'SRG10104', 47000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4355, 91, 'Kedung', 'SRG10105', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4356, 91, 'Keling', 'SRG10106', 31000, 27000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4357, 91, 'Mayong', 'SRG10107', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4358, 91, 'Mlonggo', 'SRG10108', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4359, 91, 'Nalumsari', 'SRG10109', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4360, 91, 'Pecangaan', 'SRG10110', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4361, 91, 'Welahan', 'SRG10111', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4362, 91, 'Kalinyamatan', 'SRG10112', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4363, 91, 'Kembang', 'SRG10113', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4364, 91, 'Tahunan', 'SRG10114', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4365, 107, 'Kudus', 'SRG10200', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4366, 107, 'Dawe', 'SRG10201', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4367, 107, 'Jekulo', 'SRG10202', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4368, 107, 'Mejobo', 'SRG10203', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4369, 107, 'Undaan', 'SRG10204', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4370, 107, 'Gebog', 'SRG10205', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4371, 107, 'Jati ', 'SRG10206', 31000, 27000, 35000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4372, 107, 'Kaliwungu ', 'SRG10207', 31000, 27000, 35000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4373, 107, 'Bae', 'SRG10208', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4374, 88, 'Pekalongan ', 'SRG10300', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4375, 88, 'Pekalongan Barat', 'SRG10322', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4376, 88, 'Pekalongan Selatan', 'SRG10323', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4377, 88, 'Pekalongan Timur', 'SRG10324', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4378, 88, 'Pekalongan Utara', 'SRG10325', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4379, 105, 'Purwokerto', 'SRG10400', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4380, 105, 'Ajibarang', 'SRG10401', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4381, 105, 'Banyumas', 'SRG10402', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4382, 105, 'Baturaden', 'SRG10403', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4383, 105, 'Cilongok', 'SRG10404', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4384, 105, 'Gumelar', 'SRG10405', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4385, 105, 'Jatilawang', 'SRG10406', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4386, 105, 'Kalibagor', 'SRG10407', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4387, 105, 'Karanglewas', 'SRG10408', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4388, 105, 'Kebasen', 'SRG10409', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4389, 105, 'Kedungbanteng ', 'SRG10410', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4390, 105, 'Kembaran', 'SRG10411', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4391, 105, 'Kemranjen', 'SRG10412', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4392, 105, 'Lumbir', 'SRG10413', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4393, 105, 'Pakuncen', 'SRG10414', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4394, 105, 'Patikraja', 'SRG10415', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4395, 105, 'Purwojati', 'SRG10416', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4396, 105, 'Rawalo', 'SRG10417', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4397, 105, 'Sokaraja', 'SRG10418', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4398, 105, 'Somagede', 'SRG10419', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4399, 105, 'Sumpyuh /Sumpiuh', 'SRG10420', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4400, 105, 'Sumbang', 'SRG10421', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4401, 105, 'Tambak', 'SRG10422', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4402, 105, 'Wangon', 'SRG10423', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4403, 105, 'Purwokerto Barat', 'SRG10424', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4404, 105, 'Purwokerto Selatan', 'SRG10425', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4405, 105, 'Purwokerto Timur', 'SRG10426', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4406, 105, 'Purwokerto Utara', 'SRG10427', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4407, 122, 'Batang', 'SRG20200', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4408, 122, 'Bandar', 'SRG20201', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4409, 122, 'Bawang', 'SRG20202', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4410, 122, 'Blado', 'SRG20203', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4411, 122, 'Gringsing', 'SRG20204', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4412, 122, 'Limpung', 'SRG20205', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4413, 122, 'Reban', 'SRG20206', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4414, 122, 'Subah', 'SRG20207', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4415, 122, 'Tersono', 'SRG20208', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4416, 122, 'Tulis', 'SRG20209', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4417, 122, 'Warungasem', 'SRG20210', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4418, 122, 'Wonotunggal', 'SRG20211', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4419, 122, 'Banyuputih', 'SRG20212', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4420, 122, 'Kandeman', 'SRG20213', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4421, 122, 'Pecalungan', 'SRG20214', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4422, 102, 'Blora', 'SRG20300', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4423, 102, 'Banjarejo', 'SRG20301', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4424, 102, 'Bogorejo', 'SRG20302', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4425, 102, 'Japah', 'SRG20303', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4426, 102, 'Jepon ', 'SRG20304', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4427, 102, 'Kunduran', 'SRG20305', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4428, 102, 'Ngawen ', 'SRG20306', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4429, 102, 'Todanan', 'SRG20307', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4430, 102, 'Tunjungan', 'SRG20308', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4431, 102, 'Cepu', 'SRG20309', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4432, 102, 'Jati ', 'SRG20310', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4433, 102, 'Jiken', 'SRG20311', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4434, 102, 'Kedungtuban', 'SRG20312', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4435, 102, 'Kradenan /menden', 'SRG20313', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4436, 102, 'Randublatung', 'SRG20314', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4437, 102, 'Sambong', 'SRG20315', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4438, 118, 'Bojonegoro', 'SRG20400', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4439, 118, 'Balen', 'SRG20401', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4440, 118, 'Baureno', 'SRG20402', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4441, 118, 'Bubulan', 'SRG20403', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4442, 118, 'Dander', 'SRG20404', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4443, 118, 'Kalitidu', 'SRG20405', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4444, 118, 'Kanor', 'SRG20406', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4445, 118, 'Kapas', 'SRG20407', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4446, 118, 'Kasiman', 'SRG20408', 31000, 27000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4447, 118, 'Kedungadem', 'SRG20409', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4448, 118, 'Kepohbaru', 'SRG20410', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4449, 118, 'Malo', 'SRG20411', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4450, 118, 'Margomulyo', 'SRG20412', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4451, 118, 'Ngambon', 'SRG20413', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4452, 118, 'Ngasem', 'SRG20414', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4453, 118, 'Ngraho', 'SRG20415', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4454, 118, 'Padangan', 'SRG20416', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4455, 118, 'Purwosari ', 'SRG20417', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4456, 118, 'Sugihwaras', 'SRG20418', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4457, 118, 'Sumberejo', 'SRG20419', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4458, 118, 'Tambakrejo', 'SRG20420', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4459, 118, 'Temayang', 'SRG20421', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4460, 118, 'Trucuk', 'SRG20422', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4461, 118, 'Kedewan', 'SRG20424', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4462, 118, 'Sekar', 'SRG20425', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4463, 118, 'Sukosewu', 'SRG20426', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4464, 118, 'Gondang ', 'SRG20427', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4465, 98, 'Brebes', 'SRG20500', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4466, 98, 'Banjarharjo', 'SRG20501', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4467, 98, 'Bantarkawung', 'SRG20502', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4468, 98, 'Bulakamba', 'SRG20503', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4469, 98, 'Bumiayu', 'SRG20504', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4470, 98, 'Jatibarang ', 'SRG20505', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4471, 98, 'Kersana', 'SRG20506', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4472, 98, 'Ketanggungan', 'SRG20507', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4473, 98, 'Larangan ', 'SRG20508', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4474, 98, 'Losari', 'SRG20509', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4475, 98, 'Paguyangan', 'SRG20510', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4476, 98, 'Salem', 'SRG20511', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4477, 98, 'Sirampog', 'SRG20512', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4478, 98, 'Songgom', 'SRG20513', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4479, 98, 'Tanjung', 'SRG20514', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4480, 98, 'Tonjong', 'SRG20515', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4481, 98, 'Wanasari', 'SRG20516', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4482, 114, 'Demak', 'SRG20700', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4483, 114, 'Bonang', 'SRG20701', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4484, 114, 'Dempet', 'SRG20702', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4485, 114, 'Gajah', 'SRG20703', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4486, 114, 'Guntur', 'SRG20704', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4487, 114, 'Karangtengah ', 'SRG20705', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4488, 114, 'Karangawen', 'SRG20706', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4489, 114, 'Karanganyar ', 'SRG20707', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4490, 114, 'Mijen', 'SRG20708', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4491, 114, 'Mranggen', 'SRG20709', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4492, 114, 'Sayung', 'SRG20710', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4493, 114, 'Wedung', 'SRG20711', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4494, 114, 'Wonosalam', 'SRG20712', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4495, 114, 'Kebonagung ', 'SRG20713', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4496, 94, 'Kendal', 'SRG20800', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4497, 94, 'Boja', 'SRG20801', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4498, 94, 'Brangsong', 'SRG20802', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4499, 94, 'Cepiring', 'SRG20803', 31000, 27000, 35000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4500, 94, 'Gemuh', 'SRG20805', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4501, 94, 'Kaliwungu', 'SRG20806', 31000, 27000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4502, 94, 'Limbangan', 'SRG20807', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4503, 94, 'Pagerruyung', 'SRG20808', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4504, 94, 'Patebon', 'SRG20809', 31000, 27000, 35000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4505, 94, 'Patean', 'SRG20810', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4506, 94, 'Pegandon', 'SRG20811', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4507, 94, 'Plantungan', 'SRG20812', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4508, 94, 'Sukorejo ', 'SRG20814', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4509, 94, 'Weleri', 'SRG20816', 31000, 27000, 35000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4510, 94, 'Kangkung', 'SRG20817', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4511, 94, 'Ngampel', 'SRG20819', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4512, 94, 'Ringinarum', 'SRG20820', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4513, 94, 'Rowosari /Weleri Utara', 'SRG20821', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4514, 94, 'Singorojo', 'SRG20822', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4515, 109, 'Pati', 'SRG20900', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4516, 109, 'Batangan', 'SRG20901', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4517, 109, 'Cluwak', 'SRG20902', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4518, 109, 'Dukuhseti', 'SRG20903', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4519, 109, 'Gabus ', 'SRG20904', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4520, 109, 'Gembong', 'SRG20905', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4521, 109, 'Gunungwungkal', 'SRG20906', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4522, 109, 'Jaken', 'SRG20907', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4523, 109, 'Jakenan', 'SRG20908', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4524, 109, 'Juwana', 'SRG20909', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4525, 109, 'Kayen', 'SRG20910', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4526, 109, 'Margorejo', 'SRG20911', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4527, 109, 'Margoyoso', 'SRG20912', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4528, 109, 'Pucakwangi', 'SRG20913', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4529, 109, 'Sukolilo ', 'SRG20914', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4530, 109, 'Tambakromo', 'SRG20915', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4531, 109, 'Tayu', 'SRG20916', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4532, 109, 'Telogowungu', 'SRG20917', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4533, 109, 'Trangkil', 'SRG20918', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4534, 109, 'Wedarijaksa', 'SRG20919', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4535, 109, 'Winong', 'SRG20920', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4536, 90, 'Pemalang', 'SRG21000', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4537, 90, 'Ampelgading ', 'SRG21001', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4538, 90, 'Bantarbolang', 'SRG21002', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4539, 90, 'Belik', 'SRG21003', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4540, 90, 'Bodeh', 'SRG21004', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4541, 90, 'Comal', 'SRG21005', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4542, 90, 'Moga', 'SRG21006', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4543, 90, 'Petarukan', 'SRG21007', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4544, 90, 'Pulosari', 'SRG21008', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4545, 90, 'Randudongkal', 'SRG21009', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4546, 90, 'Taman ', 'SRG21010', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4547, 90, 'Ulujami', 'SRG21011', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4548, 90, 'Warungpring', 'SRG21012', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4549, 90, 'Watukumpul', 'SRG21013', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4550, 106, 'Purwodadi ', 'SRG21100', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4551, 106, 'Brati', 'SRG21101', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4552, 106, 'Gabus ', 'SRG21102', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4553, 106, 'Geyer', 'SRG21103', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4554, 106, 'Godong', 'SRG21104', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4555, 106, 'Grobogan', 'SRG21105', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4556, 106, 'Gubug', 'SRG21106', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4557, 106, 'Karangrayung', 'SRG21107', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4558, 106, 'Kedungjati', 'SRG21108', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4559, 106, 'Klambu', 'SRG21109', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4560, 106, 'Kradenan', 'SRG21110', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4561, 106, 'Ngaringan', 'SRG21111', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4562, 106, 'Penawangan', 'SRG21112', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4563, 106, 'Pulokulon', 'SRG21113', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4564, 106, 'Tanggungharjo', 'SRG21114', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4565, 106, 'Tawangharjo', 'SRG21115', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4566, 106, 'Tegowanu', 'SRG21116', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4567, 106, 'Toroh', 'SRG21117', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4568, 106, 'Wirosari', 'SRG21118', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4569, 87, 'Rembang ', 'SRG21200', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4570, 87, 'Bulu ', 'SRG21201', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4571, 87, 'Gunem', 'SRG21202', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4572, 87, 'Kaliori', 'SRG21203', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4573, 87, 'Kragan', 'SRG21204', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4574, 87, 'Lasem', 'SRG21205', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4575, 87, 'Pamotan', 'SRG21206', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4576, 87, 'Pancur', 'SRG21207', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4577, 87, 'Sale', 'SRG21208', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4578, 87, 'Sarang', 'SRG21209', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4579, 87, 'Sedan', 'SRG21210', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4580, 87, 'Sluke', 'SRG21211', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4581, 87, 'Sulang', 'SRG21212', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4582, 87, 'Sumber', 'SRG21213', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4583, 104, 'Salatiga', 'SRG21300', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4584, 104, 'Argomulyo', 'SRG21308', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4585, 104, 'Sidomukti', 'SRG21309', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4586, 104, 'Sidorejo', 'SRG21310', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4587, 104, 'Tingkir', 'SRG21311', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4588, 121, 'Slawi', 'SRG21400', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4589, 121, 'Balapulang', 'SRG21401', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4590, 121, 'Bojong ', 'SRG21402', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4591, 121, 'Bumijawa', 'SRG21403', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4592, 121, 'Dukuhwaru', 'SRG21404', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4593, 121, 'Jatinegara', 'SRG21405', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4594, 121, 'Kedungbanteng ', 'SRG21406', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4595, 121, 'Lebaksiu', 'SRG21407', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4596, 121, 'Margasari', 'SRG21408', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4597, 121, 'Pagerbarang', 'SRG21409', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4598, 121, 'Pangkah', 'SRG21410', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4599, 121, 'Adiwerna', 'SRG21411', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4600, 121, 'Dukuhturi', 'SRG21412', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4601, 121, 'Kramat', 'SRG21413', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4602, 121, 'Suradadi', 'SRG21414', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4603, 121, 'Talang', 'SRG21415', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4604, 121, 'Tarub', 'SRG21416', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4605, 121, 'Warureja', 'SRG21417', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4606, 101, 'Tegal', 'SRG21500', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4607, 101, 'Margadana', 'SRG21508', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4608, 101, 'Tegal Barat', 'SRG21509', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4609, 101, 'Tegal Selatan', 'SRG21510', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4610, 101, 'Tegal Timur', 'SRG21511', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4611, 117, 'Ungaran', 'SRG21600', 19000, 17000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4612, 117, 'Ambarawa', 'SRG21602', 25000, 21000, 29000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4613, 117, 'Banyubiru', 'SRG21603', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4614, 117, 'Bancak', 'SRG21604', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4615, 117, 'Bawen / Doplang', 'SRG21605', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4616, 117, 'Bergas', 'SRG21606', 31000, 27000, 35000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4617, 117, 'Bringin ', 'SRG21607', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4618, 117, 'Getasan', 'SRG21608', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4619, 117, 'Jambu', 'SRG21609', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4620, 117, 'Kaliwungu ', 'SRG21610', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4621, 117, 'Pabelan', 'SRG21611', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4622, 117, 'Pringapus', 'SRG21612', 31000, 27000, 35000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4623, 117, 'Susukan ', 'SRG21613', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4624, 117, 'Suruh', 'SRG21614', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4625, 117, 'Sumowono', 'SRG21615', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4626, 117, 'Tengaran', 'SRG21616', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4627, 117, 'Tuntang', 'SRG21617', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4628, 117, 'Ungaran Barat', 'SRG21618', 31000, 27000, 35000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4629, 117, 'Ungaran Timur', 'SRG21619', 31000, 27000, 35000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4630, 97, 'Purbalingga', 'SRG21700', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4631, 97, 'Bobotsari', 'SRG21701', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4632, 97, 'Bojongsari', 'SRG21702', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4633, 97, 'Bukateja', 'SRG21703', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4634, 97, 'Kaligondang', 'SRG21704', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4635, 97, 'Kalimanah', 'SRG21705', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4636, 97, 'Karanganyar ', 'SRG21707', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4637, 97, 'Karangmoncol', 'SRG21708', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4638, 97, 'Karangreja', 'SRG21709', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4639, 97, 'Kejobong', 'SRG21710', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4640, 97, 'Kemangkon', 'SRG21711', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4641, 97, 'Kutasari', 'SRG21712', 31000, 27000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4642, 97, 'Mrebet', 'SRG21713', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4643, 97, 'Rembang', 'SRG21714', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4644, 97, 'Karangjambu', 'SRG21715', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4645, 97, 'Kertanegara', 'SRG21716', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4646, 97, 'Padamara', 'SRG21717', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4647, 97, 'Pengadegan', 'SRG21718', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4648, 113, 'Banjarnegara', 'SRG21800', 25000, 21000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4649, 113, 'Banjarmangu', 'SRG21801', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4650, 113, 'Batur', 'SRG21802', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4651, 113, 'Bawang ', 'SRG21803', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4652, 113, 'Kalibening', 'SRG21804', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4653, 113, 'Karangkobar', 'SRG21805', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4654, 113, 'Madukara', 'SRG21806', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4655, 113, 'Mandiraja', 'SRG21807', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4656, 113, 'Pagentan', 'SRG21808', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4657, 113, 'Pejawaran', 'SRG21809', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4658, 113, 'Punggelan', 'SRG21810', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4659, 113, 'Purwanegara', 'SRG21811', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4660, 113, 'Purworejo Klampok', 'SRG21812', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4661, 113, 'Rakit', 'SRG21813', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4662, 113, 'Sigaluh', 'SRG21814', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4663, 113, 'Susukan ', 'SRG21815', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4664, 113, 'Wanadadi', 'SRG21816', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4665, 113, 'Wanayasa', 'SRG21817', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4666, 93, 'Kajen', 'SRG22200', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4667, 93, 'Bojong ', 'SRG22201', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4668, 93, 'Buaran', 'SRG22202', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4669, 93, 'Doro', 'SRG22203', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4670, 93, 'KANDANG SERANG', 'SRG22204', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4671, 93, 'Karanganyar ', 'SRG22205', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4672, 93, 'Karangdadap', 'SRG22206', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4673, 93, 'Kedungwuni', 'SRG22207', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4674, 93, 'Kesesi', 'SRG22208', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4675, 93, 'LEBAK BARANG', 'SRG22209', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4676, 93, 'Paninggaran', 'SRG22210', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4677, 93, 'Petungkriono', 'SRG22211', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4678, 93, 'Siwalan', 'SRG22212', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4679, 93, 'Sragi', 'SRG22213', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4680, 93, 'Talun', 'SRG22214', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4681, 93, 'Tirto', 'SRG22215', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4682, 93, 'Wiradesa', 'SRG22216', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4683, 93, 'Wonokerto', 'SRG22217', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4684, 93, 'Wonopringgo', 'SRG22218', 31000, 27000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4685, 150, 'Surabaya', 'SUB10000', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4686, 128, 'Asemrowo', 'SUB10001', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4687, 128, 'Benowo', 'SUB10002', 20000, 18000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4688, 128, 'Bubutan', 'SUB10003', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4689, 128, 'Bulak', 'SUB10004', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4690, 128, 'Dukuh Pakis', 'SUB10005', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4691, 128, 'Gayungan', 'SUB10006', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4692, 128, 'Genteng ', 'SUB10007', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4693, 128, 'Gubeng', 'SUB10008', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4694, 128, 'Gununganyar', 'SUB10009', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4695, 128, 'Jambangan', 'SUB10010', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4696, 128, 'Karangpilang', 'SUB10011', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4697, 128, 'Kenjeran', 'SUB10012', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4698, 128, 'Krembangan', 'SUB10013', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4699, 128, 'Lakarsantri', 'SUB10014', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4700, 128, 'Mulyorejo', 'SUB10015', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4701, 128, 'Pabean Cantikan', 'SUB10016', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4702, 128, 'Pakal', 'SUB10017', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4703, 128, 'Rungkut', 'SUB10018', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4704, 128, 'Sambikerep', 'SUB10019', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4705, 128, 'Sawahan', 'SUB10020', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4706, 128, 'Semampir', 'SUB10021', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4707, 128, 'Simokerto ', 'SUB10022', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4708, 128, 'Sukolilo ', 'SUB10023', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4709, 128, 'Sukomanunggal ', 'SUB10024', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4710, 128, 'Tambaksari', 'SUB10025', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4711, 128, 'Tandes ', 'SUB10026', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4712, 128, 'Tegalsari ', 'SUB10027', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4713, 128, 'Tenggilis Mejoyo', 'SUB10028', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4714, 128, 'Wiyung ', 'SUB10029', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4715, 128, 'Wonocolo ', 'SUB10030', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4716, 128, 'Wonokromo', 'SUB10031', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4717, 146, 'Gresik', 'SUB10100', 20000, 18000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4718, 146, 'Balongpanggang', 'SUB10101', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4719, 146, 'Benjeng', 'SUB10102', 36000, 31000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4720, 146, 'Bungah', 'SUB10103', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4721, 146, 'Cerme', 'SUB10104', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4722, 146, 'Driyorejo', 'SUB10105', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4723, 146, 'Duduk Sampeyan', 'SUB10106', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4724, 146, 'Dukun ', 'SUB10107', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4725, 146, 'Kedamean', 'SUB10108', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4726, 146, 'Manyar', 'SUB10109', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4727, 146, 'Menganti', 'SUB10110', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4728, 146, 'Panceng', 'SUB10111', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4729, 146, 'Sangkapura', 'SUB10112', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4730, 146, 'Sidayu', 'SUB10113', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4731, 146, 'Tambak ', 'SUB10114', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4732, 146, 'Ujung Pangkah', 'SUB10115', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4733, 146, 'Wringinanom', 'SUB10116', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4734, 146, 'Kebomas', 'SUB10117', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4735, 125, 'Lamongan', 'SUB10200', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4736, 125, 'Babat', 'SUB10201', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4737, 125, 'Bluluk', 'SUB10202', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4738, 125, 'Brondong', 'SUB10203', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4739, 125, 'Deket', 'SUB10204', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4740, 125, 'Glagah', 'SUB10205', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4741, 125, 'Kalitengah', 'SUB10206', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4742, 125, 'Karangbinangun', 'SUB10207', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4743, 125, 'Karanggeneng', 'SUB10208', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4744, 125, 'Kedungpring', 'SUB10209', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4745, 125, 'Kembangbahu', 'SUB10210', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4746, 125, 'Laren', 'SUB10211', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4747, 125, 'Mantup', 'SUB10212', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4748, 125, 'Modo', 'SUB10213', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4749, 125, 'Ngimbang', 'SUB10214', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4750, 125, 'Paciran', 'SUB10215', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4751, 125, 'Pucuk', 'SUB10216', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4752, 125, 'Sambeng', 'SUB10217', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4753, 125, 'Solokuro', 'SUB10218', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4754, 125, 'Sekaran', 'SUB10219', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4755, 125, 'Sugio', 'SUB10220', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4756, 125, 'Sukodadi', 'SUB10221', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4757, 125, 'Sukorame', 'SUB10222', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4758, 125, 'Tikung', 'SUB10223', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4759, 125, 'Turi', 'SUB10224', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4760, 125, 'Maduran', 'SUB10225', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4761, 125, 'Sarirejo', 'SUB10226', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4762, 142, 'Bangkalan', 'SUB20100', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4763, 142, 'Arosbaya', 'SUB20101', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4764, 142, 'Balega', 'SUB20102', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4765, 142, 'Galis ', 'SUB20103', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4766, 142, 'Geger ', 'SUB20104', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4767, 142, 'Kamal', 'SUB20105', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4768, 142, 'Kwanyar', 'SUB20106', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4769, 142, 'Klampis', 'SUB20107', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4770, 142, 'Kokop', 'SUB20108', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4771, 142, 'Konang', 'SUB20109', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4772, 142, 'Labang', 'SUB20110', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4773, 142, 'Modung', 'SUB20111', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4774, 142, 'Sepulu', 'SUB20112', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4775, 142, 'Socah', 'SUB20113', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4776, 142, 'Tanah Merah', 'SUB20114', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4777, 142, 'Tanjungbumi', 'SUB20115', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4778, 142, 'Tragah', 'SUB20116', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4779, 142, 'Burneh', 'SUB20117', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4780, 159, 'Jombang', 'SUB20200', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4781, 159, 'Bandar Kedungmulyo', 'SUB20201', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4782, 159, 'Bareng', 'SUB20202', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4783, 159, 'Diwek', 'SUB20203', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4784, 159, 'Gudo', 'SUB20204', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4785, 159, 'Jogoroto', 'SUB20205', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4786, 159, 'Kabuh', 'SUB20206', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4787, 159, 'Kesamben ', 'SUB20207', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4788, 159, 'Kudu', 'SUB20208', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4789, 159, 'Megaluh', 'SUB20209', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4790, 159, 'Mojoagung', 'SUB20210', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4791, 159, 'Mojowarno', 'SUB20211', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4792, 159, 'Ngoro ', 'SUB20212', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4793, 159, 'Perak', 'SUB20213', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4794, 159, 'Peterongan', 'SUB20214', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4795, 159, 'Plandaan', 'SUB20215', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4796, 159, 'Ploso', 'SUB20216', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4797, 159, 'Sumobito', 'SUB20217', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4798, 159, 'Tembelang', 'SUB20218', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4799, 159, 'Wonosalam', 'SUB20219', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4800, 159, 'Ngusikan ', 'SUB20221', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4801, 137, 'Nganjuk', 'SUB20400', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4802, 137, 'Bagor', 'SUB20401', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4803, 137, 'Berbek', 'SUB20402', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4804, 137, 'Gondang ', 'SUB20403', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4805, 137, 'Loceret', 'SUB20404', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4806, 137, 'Ngetos', 'SUB20405', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4807, 137, 'Ngluyu', 'SUB20406', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4808, 137, 'Pace', 'SUB20407', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4809, 137, 'Prambon ', 'SUB20408', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4810, 137, 'Rejoso ', 'SUB20409', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4811, 137, 'Sawahan', 'SUB20410', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4812, 137, 'Sukomoro ', 'SUB20411', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4813, 137, 'Tanjunganom', 'SUB20412', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4814, 137, 'Wilangan', 'SUB20413', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4815, 137, 'Kertosono', 'SUB20414', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4816, 137, 'Baron', 'SUB20415', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4817, 137, 'Jatikalen', 'SUB20416', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4818, 137, 'Lengkong ', 'SUB20417', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4819, 137, 'Ngronggot', 'SUB20418', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4820, 137, 'Patianrowo', 'SUB20419', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4821, 154, 'Pamekasan', 'SUB20500', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4822, 154, 'Batu Marmar', 'SUB20501', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4823, 154, 'Galis ', 'SUB20502', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4824, 154, 'Kadur', 'SUB20503', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4825, 154, 'Larangan ', 'SUB20504', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4826, 154, 'Pakong', 'SUB20505', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4827, 154, 'Palengaan', 'SUB20506', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4828, 154, 'Pasean', 'SUB20507', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4829, 154, 'Pegantenan', 'SUB20508', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4830, 154, 'Proppo', 'SUB20509', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4831, 154, 'Tlanakan', 'SUB20510', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4832, 154, 'Waru', 'SUB20511', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4833, 154, 'Pademawu', 'SUB20512', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4834, 132, 'Sampang ', 'SUB20600', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4835, 132, 'Banyuates', 'SUB20601', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4836, 132, 'Camplong', 'SUB20602', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4837, 132, 'Jrengik', 'SUB20603', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4838, 132, 'KEDUNDUNG', 'SUB20604', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4839, 132, 'Ketapang', 'SUB20605', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4840, 132, 'Omben', 'SUB20606', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4841, 132, 'Robatal', 'SUB20607', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4842, 132, 'Sokobanah', 'SUB20608', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4843, 132, 'Sreseh', 'SUB20609', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4844, 132, 'Tambelangan', 'SUB20610', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4845, 132, 'Torjun', 'SUB20611', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4846, 151, 'Sidoarjo', 'SUB20700', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4847, 151, 'Buduran', 'SUB20702', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4848, 151, 'Candi', 'SUB20703', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4849, 151, 'Gedangan ', 'SUB20704', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4850, 151, 'Jabon', 'SUB20705', 20000, 18000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4851, 151, 'Krembung', 'SUB20706', 20000, 18000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4852, 151, 'Porong', 'SUB20708', 20000, 18000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4853, 151, 'Sedati', 'SUB20710', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4854, 151, 'Sukodono', 'SUB20711', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4855, 151, 'Tanggulangin', 'SUB20713', 20000, 18000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4856, 151, 'Tulangan', 'SUB20715', 20000, 18000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4857, 151, 'Waru', 'SUB20716', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4858, 151, 'Wonoayu', 'SUB20717', 20000, 18000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4859, 151, 'Krian', 'SUB20719', 20000, 18000, 30000, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4860, 151, 'Taman ', 'SUB20720', 20000, 18000, 30000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4861, 131, 'Sumenep', 'SUB20800', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4862, 131, 'Ambunten', 'SUB20801', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4863, 131, 'Arjasa ', 'SUB20802', 54000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4864, 131, 'Batang Batang', 'SUB20803', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4865, 131, 'Batuputih', 'SUB20804', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4866, 131, 'Bluto', 'SUB20805', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4867, 131, 'Dasuk', 'SUB20806', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4868, 131, 'Dungkek', 'SUB20807', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4869, 131, 'Ganding', 'SUB20808', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4870, 131, 'Gapura', 'SUB20809', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4871, 131, 'Gayam', 'SUB20810', 54000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4872, 131, 'Giligenteng', 'SUB20811', 54000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4873, 131, 'Guluk-Guluk', 'SUB20812', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4874, 131, 'Kalianget', 'SUB20813', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4875, 131, 'Lenteng', 'SUB20814', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4876, 131, 'Manding', 'SUB20815', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4877, 131, 'Masalembu', 'SUB20816', 54000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4878, 131, 'Nonggunong', 'SUB20817', 54000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4879, 131, 'Pasongsongan', 'SUB20818', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4880, 131, 'Pragaan', 'SUB20819', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4881, 131, 'Raas', 'SUB20820', 54000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4882, 131, 'Rubaru', 'SUB20821', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4883, 131, 'Sapeken', 'SUB20822', 54000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4884, 131, 'Saronggi', 'SUB20823', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4885, 131, 'Talango', 'SUB20824', 54000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4886, 131, 'Batuan', 'SUB20825', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4887, 131, 'Kangean', 'SUB20826', 54000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4888, 149, 'Tuban', 'SUB20900', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4889, 149, 'Bancar', 'SUB20901', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4890, 149, 'Bangilan', 'SUB20902', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4891, 149, 'Jatirogo', 'SUB20903', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4892, 149, 'Jenu', 'SUB20904', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4893, 149, 'Kenduruan', 'SUB20905', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4894, 149, 'Kerek', 'SUB20906', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4895, 149, 'Merakurak', 'SUB20907', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4896, 149, 'Montong', 'SUB20908', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4897, 149, 'Palang', 'SUB20909', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4898, 149, 'Parengan', 'SUB20910', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4899, 149, 'Plumbang', 'SUB20911', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4900, 149, 'Rengel', 'SUB20912', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4901, 149, 'Semanding', 'SUB20913', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4902, 149, 'Senori', 'SUB20914', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4903, 149, 'Singgahan', 'SUB20915', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4904, 149, 'Soko', 'SUB20916', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4905, 149, 'Tambakboyo', 'SUB20917', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4906, 149, 'Widang', 'SUB20918', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4907, 149, 'Grabagan', 'SUB20919', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4908, 127, 'Tulungagung', 'SUB21000', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4909, 127, 'Bandung ', 'SUB21001', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4910, 127, 'Besuki ', 'SUB21002', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4911, 127, 'Campurdarat', 'SUB21003', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4912, 127, 'Gondang ', 'SUB21004', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4913, 127, 'Kalidawir', 'SUB21005', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4914, 127, 'Karangrejo ', 'SUB21006', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4915, 127, 'Ngantru', 'SUB21008', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4916, 127, 'Ngunut', 'SUB21009', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4917, 127, 'Pagerwojo', 'SUB21010', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4918, 127, 'Pakel', 'SUB21011', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4919, 127, 'Pucanglaban', 'SUB21012', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4920, 127, 'Rejotangan', 'SUB21013', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4921, 127, 'Sendang', 'SUB21014', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4922, 127, 'Sumbergempol', 'SUB21015', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4923, 127, 'Tanggung Gunung', 'SUB21016', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4924, 127, 'Boyolangu', 'SUB21017', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4925, 127, 'Kauman ', 'SUB21018', 36000, 31000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (4926, 127, 'Kedungwaru', 'SUB21019', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4927, 144, 'Trenggalek', 'SUB22000', 29000, 26000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4928, 144, 'Bendungan', 'SUB22001', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4929, 144, 'Dongko', 'SUB22002', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4930, 144, 'Durenan', 'SUB22003', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4931, 144, 'Gandusari ', 'SUB22004', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4932, 144, 'Kampak', 'SUB22005', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4933, 144, 'Karangan', 'SUB22006', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4934, 144, 'Munjungan', 'SUB22007', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4935, 144, 'Panggul', 'SUB22008', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4936, 144, 'Pogalan', 'SUB22009', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4937, 144, 'Pule', 'SUB22010', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4938, 144, 'Tugu', 'SUB22011', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4939, 144, 'Watulimo', 'SUB22012', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4940, 144, 'Suruh', 'SUB22013', 36000, 31000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4941, 17, 'Tangerang', 'TGR10000', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4942, 17, 'Ciledug ', 'TGR10036', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4943, 17, 'Batuceper ', 'TGR10046', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4944, 17, 'Benda', 'TGR10047', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4945, 17, 'Cibodas ', 'TGR10048', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4946, 17, 'Cipondoh ', 'TGR10049', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4947, 17, 'Jatiuwung ', 'TGR10050', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4948, 17, 'Karangtengah ', 'TGR10051', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4949, 17, 'Karawaci ', 'TGR10052', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4950, 17, 'Larangan ', 'TGR10053', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4951, 17, 'Neglasari ', 'TGR10054', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4952, 17, 'Periuk ', 'TGR10055', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4953, 17, 'Pinang ', 'TGR10056', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4954, 20, 'Tigaraksa', 'TGR10100', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4955, 20, 'Balaraja', 'TGR10101', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4956, 20, 'Cikupa', 'TGR10102', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4957, 20, 'Cisoka', 'TGR10103', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4958, 20, 'Curug', 'TGR10104', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4959, 20, 'Kronjo', 'TGR10105', 15000, 14000, 0, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4960, 20, 'Kresek', 'TGR10106', 15000, 14000, 0, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4961, 20, 'Legok', 'TGR10107', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4962, 20, 'Mauk', 'TGR10108', 15000, 14000, 0, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4963, 20, 'PASAR KEMIS', 'TGR10109', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4964, 20, 'Pakuhaji', 'TGR10110', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4965, 20, 'Rajeg', 'TGR10111', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4966, 20, 'Sepatan /Jatimulya/Cengklong', 'TGR10112', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4967, 20, 'Serpong', 'TGR10113', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4968, 20, 'Teluknaga', 'TGR10114', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4969, 20, 'Pondok Aren / Jurang Mangu', 'TGR10115', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4970, 20, 'Pamulang', 'TGR10116', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4971, 20, 'Ciputat ', 'TGR10117', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4972, 20, 'Cisauk', 'TGR10118', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4973, 20, 'Jambe', 'TGR10119', 15000, 14000, 0, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4974, 20, 'Jayanti ', 'TGR10120', 15000, 14000, 0, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4975, 20, 'Kemiri ', 'TGR10121', 15000, 14000, 0, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4976, 20, 'Kosambi /Salembaran jati', 'TGR10122', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4977, 20, 'Pagedangan', 'TGR10123', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4978, 20, 'Panongan', 'TGR10124', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4979, 20, 'Sukadiri', 'TGR10125', 15000, 14000, 20000, 10800, 21000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4980, 324, 'Timika', 'TIM10000', 116000, 99000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4981, 324, 'Agimuga', 'TIM10001', 161000, 138000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4982, 324, 'Jila', 'TIM10002', 161000, 138000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4983, 324, 'Jita', 'TIM10003', 161000, 138000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4984, 324, 'Kuala Kencana', 'TIM10004', 161000, 138000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4985, 324, 'Mimika Barat', 'TIM10005', 128000, 109000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4986, 324, 'Mimika Barat Jauh', 'TIM10006', 128000, 109000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4987, 324, 'Mimika Barat Tengah', 'TIM10007', 128000, 109000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4988, 324, 'Mimika Baru', 'TIM10008', 128000, 109000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4989, 324, 'Mimika Timur', 'TIM10009', 128000, 109000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4990, 324, 'Mimika Timur Jauh', 'TIM10010', 128000, 109000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4991, 324, 'Mimika Timur Tengah', 'TIM10011', 128000, 109000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4992, 324, 'Tembaga Pura', 'TIM10012', 128000, 109000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4993, 13, 'Tanjung Pandan', 'TJQ10000', 33000, 29000, 0, 51200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4994, 13, 'Membalong', 'TJQ10005', 47000, 40000, 0, 51200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4995, 13, 'Selat Nasik', 'TJQ10006', 47000, 40000, 0, 51200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4996, 13, 'Badau', 'TJQ10008', 47000, 40000, 0, 51200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4997, 13, 'Sijuk', 'TJQ10009', 47000, 40000, 0, 51200, 30000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4998, 16, 'Manggar', 'TJQ10100', 37000, 32000, 0, 51200, 33000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (4999, 16, 'Dendang', 'TJQ10101', 47000, 40000, 0, 51200, 33000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5000, 16, 'Gantung', 'TJQ10102', 47000, 40000, 0, 51200, 33000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5001, 16, 'Kelapa Kampit', 'TJQ10103', 47000, 40000, 0, 51200, 33000, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5002, 227, 'Bandar Lampung', 'TKG10000', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5003, 227, 'Kedaton', 'TKG10032', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5004, 227, 'Kemiling', 'TKG10033', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5005, 227, 'Panjang', 'TKG10034', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5006, 227, 'Rajabasa', 'TKG10035', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5007, 227, 'Sukabumi ', 'TKG10036', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5008, 227, 'Sukarame', 'TKG10037', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5009, 227, 'Tanjung Karang Barat', 'TKG10038', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5010, 227, 'Tanjung Karang Pusat', 'TKG10039', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5011, 227, 'Tanjung Karang Timur', 'TKG10040', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5012, 227, 'Tanjung Senang', 'TKG10041', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5013, 227, 'Teluk Betung Barat', 'TKG10042', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5014, 227, 'Teluk Betung Selatan', 'TKG10043', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5015, 227, 'Teluk Betung Utara', 'TKG10044', 21000, 19000, 31000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5016, 232, 'Kalianda', 'TKG20100', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5017, 232, 'Palas', 'TKG20101', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5018, 232, 'Panengahan', 'TKG20102', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5019, 232, 'Natar', 'TKG20103', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5020, 232, 'Tanjung Bintang', 'TKG20104', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5021, 232, 'Candipuro', 'TKG20105', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5022, 232, 'Jati Agung', 'TKG20106', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5023, 232, 'Katibung', 'TKG20107', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5024, 232, 'Ketapang', 'TKG20108', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5025, 232, 'Merbau Mataram', 'TKG20109', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5026, 232, 'Rajabasa', 'TKG20111', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5027, 232, 'Sidomulyo', 'TKG20112', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5028, 232, 'Sragi', 'TKG20113', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5029, 232, 'Bakauheuni', 'TKG20115', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5030, 232, 'Tanjungsari', 'TKG20116', 52000, 44000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5031, 232, 'Way Panji', 'TKG20117', 52000, 44000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5032, 232, 'Way Sulan', 'TKG20118', 52000, 44000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5033, 226, 'Kotabumi', 'TKG20200', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5034, 226, 'Abung Barat', 'TKG20201', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5035, 226, 'Abung Selatan', 'TKG20202', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5036, 226, 'Abung Timur', 'TKG20203', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5037, 226, 'Bukit Kemuning', 'TKG20210', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5038, 226, 'Sungkai Selatan', 'TKG20219', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5039, 226, 'Sungkai Utara', 'TKG20220', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5040, 226, 'Tanjung Raja', 'TKG20221', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5041, 226, 'Abung Semuli', 'TKG20224', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5042, 226, 'Abung Surakarta', 'TKG20225', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5043, 226, 'Abung Tengah', 'TKG20226', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5044, 226, 'Abung Tinggi', 'TKG20228', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5045, 226, 'Bunga Mayang', 'TKG20229', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5046, 226, 'Kotabumi Selatan', 'TKG20230', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5047, 226, 'Kotabumi Utara', 'TKG20231', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5048, 226, 'Muara Sungkai', 'TKG20232', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5049, 231, 'Metro', 'TKG20300', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5050, 231, 'Metro Barat', 'TKG20355', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5051, 231, 'Metro Pusat', 'TKG20356', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5052, 231, 'Metro Selatan', 'TKG20357', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5053, 231, 'Metro Timur', 'TKG20358', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5054, 231, 'Metro Utara', 'TKG20359', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5055, 225, 'Liwa', 'TKG20400', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5056, 225, 'Balik Bukit', 'TKG20401', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5057, 225, 'Belalau', 'TKG20402', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5058, 225, 'Pesisir Utara', 'TKG20403', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5059, 225, 'Pesisir Tengah / Krui', 'TKG20404', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5060, 225, 'Pesisir Selatan', 'TKG20405', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5061, 225, 'Sumber Jaya', 'TKG20406', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5062, 225, 'Batu Brak', 'TKG20407', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5063, 225, 'Bengkunat', 'TKG20408', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5064, 225, 'Karya Penggawa', 'TKG20409', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5065, 225, 'Lemong', 'TKG20410', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5066, 225, 'Sekincau', 'TKG20411', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5067, 225, 'Sukau', 'TKG20412', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5068, 225, 'Suoh', 'TKG20413', 77000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5069, 225, 'Way Tenong', 'TKG20414', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5070, 230, 'Gunung Sugih', 'TKG20900', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5071, 230, 'Padang Ratu', 'TKG20901', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5072, 230, 'Seputih Mataram', 'TKG20902', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5073, 230, 'Seputih Banyak', 'TKG20903', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5074, 230, 'Seputih Surabaya', 'TKG20904', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5075, 230, 'Seputih Raman', 'TKG20905', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5076, 230, 'Terbanggi Besar / Bandar Jaya', 'TKG20906', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5077, 230, 'Anak Tuha', 'TKG20907', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5078, 230, 'Bandar Mataram', 'TKG20908', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5079, 230, 'Bandar Surabaya', 'TKG20909', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5080, 230, 'Bangunrejo', 'TKG20910', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5081, 230, 'Bekri', 'TKG20911', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5082, 230, 'Bumi Nabung', 'TKG20912', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5083, 230, 'Bumi Ratu Nuban', 'TKG20913', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5084, 230, 'Kalirejo', 'TKG20914', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5085, 230, 'Kota Gajah', 'TKG20915', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5086, 230, 'Pubian', 'TKG20916', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5087, 230, 'Punggur', 'TKG20917', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5088, 230, 'Rumbia', 'TKG20918', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5089, 230, 'Selagai Lingga', 'TKG20919', 77000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5090, 230, 'Sendang Agung', 'TKG20920', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5091, 230, 'Seputih Agung', 'TKG20921', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5092, 230, 'Terusan Nunyai', 'TKG20922', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5093, 230, 'Trimurjo', 'TKG20923', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5094, 230, 'WAY PANGUBUAN', 'TKG20924', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5095, 230, 'Way Seputih', 'TKG20925', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5096, 230, 'Bandar Jaya', 'TKG20927', 52000, 44000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5097, 224, 'Sukadana ', 'TKG21000', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5098, 224, 'BATANG HARI', 'TKG21001', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5099, 224, 'Jabung/Gunung balak', 'TKG21002', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5100, 224, 'Labuhan Maringgai', 'TKG21003', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5101, 224, 'Pekalongan', 'TKG21004', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5102, 224, 'Purbolinggo', 'TKG21005', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5103, 224, 'Raman Utara', 'TKG21006', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5104, 224, 'Sekampung', 'TKG21007', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5105, 224, 'Way Jepara', 'TKG21008', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5106, 224, 'BANDAR SRIBAWANO', 'TKG21009', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5107, 224, 'Batanghari Nuban', 'TKG21010', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5108, 224, 'Braja Slebah', 'TKG21011', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5109, 224, 'Bumi Agung', 'TKG21012', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5110, 224, 'Gunung Pelindung', 'TKG21013', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5111, 224, 'Labuhan Ratu', 'TKG21014', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5112, 224, 'Margatiga', 'TKG21015', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5113, 224, 'Mataram Baru', 'TKG21016', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5114, 224, 'Melinting', 'TKG21017', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5115, 224, 'Metro Kibang', 'TKG21018', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5116, 224, 'Pasir Sakti', 'TKG21019', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5117, 224, 'Sekampung Udik', 'TKG21020', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5118, 224, 'Waway Karya', 'TKG21021', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5119, 224, 'Way Bungur', 'TKG21022', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5120, 229, 'Blambangan umpu', 'TKG21100', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5121, 229, 'Bahuga', 'TKG21101', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5122, 229, 'Baradatu', 'TKG21102', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5123, 229, 'Kasui', 'TKG21103', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5124, 229, 'Banjit/Banjid', 'TKG21104', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5125, 229, 'Gunung Labuhan', 'TKG21106', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5126, 229, 'Negeri Batin', 'TKG21107', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5127, 229, 'Negeri Agung', 'TKG21108', 77000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5128, 229, 'Negeri Besar', 'TKG21109', 77000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5129, 229, 'Pakuan Ratu', 'TKG21110', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5130, 229, 'Rebang Tangkas', 'TKG21111', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5131, 229, 'Way Tuba', 'TKG21112', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5132, 234, 'Menggala', 'TKG21200', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5133, 234, 'Mesuji ', 'TKG21201', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5134, 234, 'Tulang Bawang Tengah', 'TKG21202', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5135, 234, 'Tulang Bawang Udik', 'TKG21203', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5136, 234, 'Banjar Agung', 'TKG21204', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5137, 234, 'Gedung Aji', 'TKG21205', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5138, 234, 'Gedung Meneng', 'TKG21206', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5139, 234, 'Gunung Terang', 'TKG21207', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5140, 234, 'Lambu Kibang', 'TKG21208', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5141, 234, 'Penawar Tama', 'TKG21209', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5142, 234, 'Rawajitu Selatan', 'TKG21210', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5143, 234, 'Rawajitu Utara', 'TKG21211', 77000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5144, 234, 'Simpang Pematang', 'TKG21212', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5145, 234, 'Tanjung Raya', 'TKG21213', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5146, 234, 'Tumi Jajar', 'TKG21214', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5147, 234, 'Way Serdang', 'TKG21215', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5148, 228, 'Kota Agung', 'TKG21300', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5149, 228, 'Gading Rejo', 'TKG21301', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5150, 228, 'Pagelaran', 'TKG21302', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5151, 228, 'Pringsewu', 'TKG21303', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5152, 228, 'Pulau Panggung', 'TKG21304', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5153, 228, 'Sukoharjo', 'TKG21305', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5154, 228, 'Talang Padang', 'TKG21306', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5155, 228, 'Wonosobo', 'TKG21307', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5156, 228, 'Adi Luwih', 'TKG21308', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5157, 228, 'Cukup Balak', 'TKG21309', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5158, 228, 'Kelumbayan', 'TKG21310', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5159, 228, 'Pardasuka', 'TKG21311', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5160, 228, 'Pematang Sawa', 'TKG21312', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5161, 228, 'Pugung', 'TKG21313', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5162, 228, 'Semaka', 'TKG21314', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5163, 228, 'Sumberejo', 'TKG21315', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5164, 228, 'Ulubelu', 'TKG21316', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5165, 233, 'Gedong Tataan', 'TKG21400', 41000, 36000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5166, 233, 'Kedondong', 'TKG21401', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5167, 233, 'Padang Cermin', 'TKG21402', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5168, 233, 'Negeri Katon', 'TKG21403', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5169, 233, 'Pundung Pidada', 'TKG21404', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5170, 233, 'Tegineneng', 'TKG21405', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5171, 233, 'Way Lima', 'TKG21406', 52000, 44000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5172, 220, 'Tanjung Pinang', 'TNJ10000', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5173, 220, 'Bukit Bestari', 'TNJ10016', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5174, 220, 'Tanjung Pinang Barat', 'TNJ10017', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5175, 220, 'Tanjung Pinang Kota', 'TNJ10018', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5176, 220, 'Tanjung Pinang Timur', 'TNJ10019', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5177, 220, 'Senggarang', 'TNJ10020', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5178, 220, 'Dompak', 'TNJ10021', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5179, 220, 'Pulau Penyengat', 'TNJ10023', 73000, 63000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5180, 223, 'Bandar Sri Bintan', 'TNJ10100', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5181, 223, 'Bintan Timur', 'TNJ10101', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5182, 223, 'Tanjung Uban/Bintan Utara', 'TNJ10102', 59000, 51000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5183, 223, 'Gunung Kijang', 'TNJ10103', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5184, 223, 'Tambelan', 'TNJ10104', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5185, 223, 'Teluk Bintan', 'TNJ10105', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5186, 223, 'Teluk Sebong', 'TNJ10106', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5187, 223, 'Lagoi', 'TNJ10107', 73000, 63000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5188, 206, 'Tarakan', 'TRK10000', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5189, 206, 'Tarakan Barat ', 'TRK10011', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5190, 206, 'Tarakan Tengah', 'TRK10012', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5191, 206, 'Tarakan Timur ', 'TRK10013', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5192, 206, 'Tarakan Utara', 'TRK10014', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5193, 212, 'Tanjung Selor', 'TRK20100', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5194, 212, 'Peso / Longpeso', 'TRK20103', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5195, 212, 'Pulau Bunyu', 'TRK20105', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5196, 212, 'Peso Hilir ', 'TRK20106', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5197, 212, 'Sekatak ', 'TRK20107', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5198, 212, 'Tanjung Palas ', 'TRK20108', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5199, 212, 'Tanjung Palas Barat', 'TRK20109', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5200, 212, 'Tanjung Palas Tengah ', 'TRK20110', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5201, 212, 'Tanjung Palas Timur ', 'TRK20111', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5202, 212, 'Tanjung Palas Utara ', 'TRK20112', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5203, 204, 'Malinau', 'TRK20200', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5204, 204, 'Mentarang', 'TRK20201', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5205, 204, 'Kayan Hulu ', 'TRK20202', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5206, 204, 'Kayan Hilir', 'TRK20203', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5207, 204, 'Pujungan', 'TRK20204', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5208, 204, 'Malinau Barat ', 'TRK20205', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5209, 204, 'Malinau Selatan ', 'TRK20206', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5210, 204, 'Malinau Utara', 'TRK20207', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5211, 204, 'Sungai Boh', 'TRK20208', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5212, 211, 'Nunukan', 'TRK20300', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5213, 211, 'Krayan', 'TRK20301', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5214, 211, 'Lumbis', 'TRK20302', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5215, 211, 'Krayan Selatan ', 'TRK20303', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5216, 211, 'Sebatik', 'TRK20304', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5217, 211, 'Sebuku', 'TRK20305', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5218, 203, 'Tanah Tidung', 'TRK20400', 70000, 60000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5219, 203, 'Sembakung', 'TRK20401', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5220, 203, 'Sesayap', 'TRK20402', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5221, 203, 'Sesayap Hilir', 'TRK20403', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5222, 203, 'Tanah Lia', 'TRK20404', 88000, 75000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5223, 246, 'Ternate', 'TTE10000', 69000, 59000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5224, 246, 'Moti', 'TTE10021', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5225, 246, 'Ternate Selatan', 'TTE10022', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5226, 246, 'Ternate Utara', 'TTE10023', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5227, 250, 'Jailolo', 'TTE20200', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5228, 250, 'Jailolo Selatan', 'TTE20201', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5229, 250, 'Ibu', 'TTE20202', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5230, 250, 'Loloda', 'TTE20203', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5231, 250, 'Sahu', 'TTE20204', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5232, 245, 'Labuha', 'TTE20300', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5233, 245, 'Bacan', 'TTE20301', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5234, 245, 'Bacan Selatan', 'TTE20302', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5235, 245, 'Bacan Timur', 'TTE20303', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5236, 245, 'Gane Barat', 'TTE20304', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5237, 245, 'Gane Timur', 'TTE20305', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5238, 245, 'Kayoa', 'TTE20306', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5239, 245, 'Obi', 'TTE20307', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5240, 245, 'Obi Selatan', 'TTE20308', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5241, 245, 'Pulau Makian', 'TTE20309', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5242, 249, 'Weda', 'TTE20400', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5243, 249, 'Gebe', 'TTE20401', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5244, 249, 'Patani', 'TTE20402', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5245, 244, 'Maba', 'TTE20500', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5246, 244, 'Maba Selatan', 'TTE20501', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5247, 244, 'Wasile', 'TTE20502', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5248, 244, 'Wasile Selatan', 'TTE20503', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5249, 248, 'Tobelo', 'TTE20600', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5250, 248, 'Tobelo Selatan', 'TTE20601', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5251, 248, 'Galela', 'TTE20602', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5252, 248, 'Kao', 'TTE20603', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5253, 248, 'Loloda Utara', 'TTE20604', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5254, 248, 'Malifut', 'TTE20605', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5255, 248, 'Morotai Selatan', 'TTE20606', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5256, 248, 'Morotai Selatan Barat', 'TTE20607', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5257, 248, 'Morotai Utara', 'TTE20608', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5258, 243, 'Sanana', 'TTE20700', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5259, 243, 'Mangoli Barat', 'TTE20701', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5260, 243, 'Mangoli Timur', 'TTE20702', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5261, 243, 'Nggele', 'TTE20703', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5262, 243, 'Sula Besi Barat', 'TTE20704', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5263, 243, 'Taliabu Barat', 'TTE20705', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5264, 243, 'Taliabu Timur', 'TTE20706', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5265, 247, 'Tidore', 'TTE20800', 92000, 79000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5266, 247, 'Oba', 'TTE20801', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5267, 247, 'Oba Utara', 'TTE20802', 172000, 0, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5268, 247, 'Tidore Selatan', 'TTE20803', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5269, 247, 'Tidore Utara', 'TTE20804', 115000, 98000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5270, 350, 'Ujung Pandang', 'UPG10000', 40000, 35000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5271, 350, 'Tamalate', 'UPG10003', 40000, 35000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5272, 350, 'Biring Kanaya', 'UPG10004', 40000, 35000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5273, 350, 'Bontoala', 'UPG10005', 40000, 35000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5274, 350, 'Mamajang', 'UPG10006', 40000, 35000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5275, 350, 'Manggala', 'UPG10007', 40000, 35000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5276, 350, 'Mariso', 'UPG10008', 40000, 35000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5277, 350, 'Panakkukang', 'UPG10009', 40000, 35000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5278, 350, 'Rappocini', 'UPG10010', 40000, 35000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5279, 350, 'Tallo', 'UPG10011', 40000, 35000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5280, 350, 'Tamalanrea', 'UPG10012', 40000, 35000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5281, 350, 'Ujung Tanah', 'UPG10013', 40000, 35000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5282, 350, 'Wajo', 'UPG10014', 40000, 35000, 49000, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5283, 363, 'Bantaeng', 'UPG20100', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5284, 363, 'Bissapu', 'UPG20101', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5285, 363, 'Tompobulu', 'UPG20102', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5286, 363, 'Eremerasa', 'UPG20104', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5287, 363, 'Pajukukang', 'UPG20105', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5288, 363, 'Uluere', 'UPG20106', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5289, 375, 'Barru', 'UPG20200', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5290, 375, 'Mallusetasi', 'UPG20201', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5291, 375, 'Soppeng Riaja', 'UPG20202', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5292, 375, 'Tanete Riaja', 'UPG20203', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5293, 375, 'Tanete Rilau', 'UPG20204', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5294, 375, 'Balusu', 'UPG20205', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5295, 375, 'Pujananting', 'UPG20206', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5296, 360, 'Bulukumba', 'UPG20300', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5297, 360, 'Bonto Bahari', 'UPG20301', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5298, 360, 'Bontotiro', 'UPG20302', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5299, 360, 'Gantarang /Gantarangkindang/Ganking', 'UPG20304', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5300, 360, 'Hero Lange-Lange /Heralangelange/Herlang', 'UPG20305', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5301, 360, 'Kajang', 'UPG20306', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5302, 360, 'Kindang', 'UPG20307', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5303, 360, 'Riau Ale', 'UPG20308', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5304, 360, 'Ujung Bulu', 'UPG20309', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5305, 360, 'Ujung Loe', 'UPG20310', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5306, 372, 'Enrekang', 'UPG20400', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5307, 372, 'Alla', 'UPG20401', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5308, 372, 'Anggeraja', 'UPG20402', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5309, 372, 'Baraka', 'UPG20403', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5310, 372, 'Maiwa', 'UPG20404', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5311, 372, 'Alla Timur', 'UPG20405', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5312, 372, 'Anggeraja Timur', 'UPG20406', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5313, 372, 'Enrekang Selatan', 'UPG20407', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5314, 372, 'Maiwa Atas', 'UPG20408', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5315, 357, 'Jeneponto', 'UPG20500', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5316, 357, 'Bangkala', 'UPG20501', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5317, 357, 'Batang ', 'UPG20502', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5318, 357, 'Kelara', 'UPG20503', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5319, 357, 'Tamalatea', 'UPG20504', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5320, 357, 'Bangkala Barat', 'UPG20505', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5321, 357, 'Binamu', 'UPG20506', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5322, 357, 'Bontoramba', 'UPG20507', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5323, 357, 'Turatea', 'UPG20508', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5324, 357, 'Arungkeke', 'UPG20509', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5325, 369, 'Makale', 'UPG20600', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5326, 369, 'Bonggakaradeng', 'UPG20601', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5327, 369, 'Mengkendek', 'UPG20602', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5328, 369, 'Saluputti', 'UPG20603', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5329, 369, 'Sangalla', 'UPG20604', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5330, 354, 'Rantepao', 'UPG20605', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5331, 354, 'Rindingalo', 'UPG20606', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5332, 354, 'Sanggalangi', 'UPG20607', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5333, 354, 'Sesean', 'UPG20608', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5334, 369, 'Bituang', 'UPG20609', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5335, 354, 'Buntao Rantebua', 'UPG20610', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5336, 369, 'Rantetayo', 'UPG20611', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5337, 354, 'Sa\'dan Balusu', 'UPG20612', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5338, 369, 'Simbuang', 'UPG20613', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5339, 354, 'Tondon Nanggala', 'UPG20614', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5340, 369, 'Gandang Batu Silang', 'UPG20616', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5341, 369, 'Kurra', 'UPG20617', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5342, 369, 'Makale Selatan', 'UPG20618', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5343, 369, 'Makale Utara', 'UPG20619', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5344, 369, 'Mappak', 'UPG20620', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5345, 369, 'Masanda', 'UPG20621', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5346, 369, 'Malimbong Balepe', 'UPG20622', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5347, 369, 'Rano', 'UPG20623', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5348, 369, 'Rembon', 'UPG20624', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5349, 354, 'Soloara', 'UPG20625', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5350, 354, 'Awan Rante Karua', 'UPG20626', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5351, 354, 'Balusu', 'UPG20627', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5352, 354, 'Bangkelekila', 'UPG20628', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5353, 354, 'Baruppu', 'UPG20629', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5354, 354, 'Buntu Pepasan', 'UPG20630', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5355, 354, 'Denpina/Dende Piongan Napo', 'UPG20631', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5356, 354, 'Kapala Pitu', 'UPG20632', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5357, 354, 'Kesu', 'UPG20633', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5358, 354, 'Nanggala', 'UPG20634', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5359, 354, 'Rantebua', 'UPG20635', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5360, 354, 'Sopai', 'UPG20637', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5361, 354, 'Tallunglipu', 'UPG20638', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5362, 354, 'Tikala', 'UPG20639', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5363, 367, 'Mamuju', 'UPG20700', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5364, 367, 'Budong-Budong', 'UPG20701', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5365, 367, 'Kalumpang', 'UPG20702', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5366, 367, 'Kalukku', 'UPG20703', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5367, 367, 'Tappalang', 'UPG20705', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5368, 367, 'Bonehau', 'UPG20706', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5369, 367, 'Karossa', 'UPG20707', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5370, 367, 'Pangale', 'UPG20708', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5371, 367, 'Papalang', 'UPG20709', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5372, 367, 'Sampaga', 'UPG20710', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5373, 367, 'Simboro dan Kepulauan', 'UPG20711', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5374, 367, 'Tapalang Barat', 'UPG20712', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5375, 367, 'Tobadak', 'UPG20713', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5376, 367, 'Tommo', 'UPG20714', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5377, 367, 'Topoyo', 'UPG20715', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5378, 352, 'Maros', 'UPG20800', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5379, 352, 'Bantimurung', 'UPG20801', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5380, 352, 'Camba', 'UPG20802', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5381, 352, 'Mallawa', 'UPG20803', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5382, 352, 'Mandai', 'UPG20804', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5383, 352, 'Maros Utara', 'UPG20805', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5384, 352, 'Tanralili', 'UPG20806', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5385, 352, 'Cenrana ', 'UPG20807', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5386, 352, 'Lau', 'UPG20808', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5387, 352, 'Maros Baru', 'UPG20809', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5388, 352, 'Marusu', 'UPG20810', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5389, 352, 'Moncongloe', 'UPG20811', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5390, 352, 'Simbang', 'UPG20812', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5391, 352, 'Tompu Bulu', 'UPG20813', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5392, 352, 'Turikale', 'UPG20814', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5393, 365, 'Majene', 'UPG20900', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5394, 365, 'Malunda', 'UPG20901', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5395, 365, 'Pamboang', 'UPG20902', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5396, 365, 'Sendana', 'UPG20903', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5397, 365, 'Banggae', 'UPG20904', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5398, 349, 'Palopo', 'UPG21000', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5399, 349, 'Telluwanua', 'UPG21020', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5400, 349, 'Wara', 'UPG21021', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5401, 349, 'Wara Selatan', 'UPG21022', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5402, 349, 'Wara Utara', 'UPG21023', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5403, 362, 'Pinrang', 'UPG21100', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5404, 362, 'Cempa', 'UPG21101', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5405, 362, 'Duampanua', 'UPG21102', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5406, 362, 'Lembang', 'UPG21103', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5407, 362, 'Mattiro Bulu', 'UPG21104', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5408, 362, 'Mattirosompe', 'UPG21105', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5409, 362, 'Patampanua', 'UPG21106', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5410, 362, 'Suppa', 'UPG21107', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5411, 362, 'Batulappa', 'UPG21108', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5412, 362, 'Lanrisang', 'UPG21109', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5413, 362, 'Paleteang', 'UPG21110', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5414, 362, 'Tiroang', 'UPG21111', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5415, 362, 'Watang Sawitto', 'UPG21112', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5416, 374, 'Polewali', 'UPG21200', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5417, 374, 'Campalagian', 'UPG21201', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5418, 374, 'Tinambung', 'UPG21206', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5419, 374, 'Tutallu', 'UPG21207', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5420, 374, 'Wonomulyo', 'UPG21208', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5421, 374, 'Allu', 'UPG21209', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5422, 374, 'Anreapi', 'UPG21210', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5423, 374, 'Balanipa', 'UPG21211', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5424, 374, 'Binuang', 'UPG21212', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5425, 374, 'Limboro', 'UPG21213', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5426, 374, 'Luyo', 'UPG21214', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5427, 374, 'Mapilli', 'UPG21215', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5428, 374, 'Matakali', 'UPG21216', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5429, 374, 'Matangnga', 'UPG21217', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5430, 374, 'Tapango', 'UPG21218', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5431, 359, 'Sidenreng', 'UPG21300', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5432, 359, 'Baranti', 'UPG21301', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5433, 359, 'Duapitue', 'UPG21302', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5434, 359, 'Panca Rijang', 'UPG21303', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5435, 359, 'Panca Lautang', 'UPG21304', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5436, 359, 'Tellulimpo E', 'UPG21305', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5437, 359, 'Watang Pulu', 'UPG21306', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5438, 359, 'Kulo', 'UPG21307', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5439, 359, 'Maritengngae', 'UPG21308', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5440, 359, 'Pitu Riase', 'UPG21309', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5441, 359, 'Pitu Riawa', 'UPG21310', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5442, 359, 'Sidrap', 'UPG21312', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5443, 371, 'Sengkang', 'UPG21400', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5444, 371, 'Belawa', 'UPG21401', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5445, 371, 'Majauleng', 'UPG21402', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5446, 371, 'Maniang Pajo', 'UPG21403', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5447, 371, 'Pamanna', 'UPG21404', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5448, 371, 'Pitumpanua', 'UPG21405', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5449, 371, 'Sabbang Paru', 'UPG21406', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5450, 371, 'Sajoanging', 'UPG21407', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5451, 371, 'Takkalalla', 'UPG21408', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5452, 371, 'Tana Sitolo', 'UPG21409', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5453, 371, 'Bola', 'UPG21410', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5454, 371, 'Gilireng', 'UPG21411', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5455, 371, 'Keera', 'UPG21412', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5456, 371, 'Penrang', 'UPG21413', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5457, 371, 'Tempe', 'UPG21414', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5458, 356, 'Sinjai', 'UPG21500', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5459, 356, 'Sinjai Barat', 'UPG21501', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5460, 356, 'Sinjai Selatan', 'UPG21503', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5461, 356, 'Sinjai Tengah', 'UPG21504', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5462, 356, 'Sinjai Timur', 'UPG21505', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5463, 356, 'Sinjai Utara', 'UPG21506', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5464, 356, 'Bulupoddo', 'UPG21507', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5465, 356, 'Pulau Sembilan', 'UPG21508', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5466, 356, 'Sinjai Borong', 'UPG21509', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5467, 356, 'Tellu Limpoe', 'UPG21510', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5468, 368, 'Sungguminasa', 'UPG21600', 40000, 35000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5469, 368, 'Bajeng', 'UPG21601', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5470, 368, 'Bontomarannu', 'UPG21602', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5471, 368, 'Bontonompo', 'UPG21603', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5472, 368, 'Bungaya', 'UPG21604', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5473, 368, 'Pallangga', 'UPG21605', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5474, 368, 'Parangloe', 'UPG21606', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5475, 368, 'Tinggimoncong', 'UPG21607', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5476, 368, 'Tompobulu', 'UPG21608', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5477, 368, 'Barombong', 'UPG21609', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5478, 368, 'Biringbulu', 'UPG21610', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5479, 368, 'Somba Opu', 'UPG21611', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5480, 368, 'Tombolo Pao', 'UPG21612', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5481, 353, 'Takalar', 'UPG21700', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5482, 353, 'Galesong Selatan', 'UPG21701', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5483, 353, 'Galesong Utara', 'UPG21702', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5484, 353, 'Mangara Bombang', 'UPG21703', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5485, 353, 'Mappakasunggu', 'UPG21704', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5486, 353, 'Patallassang', 'UPG21705', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5487, 353, 'Polobangkeng Selatan', 'UPG21706', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5488, 353, 'Polobangkeng Utara', 'UPG21707', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5489, 366, 'Watampone', 'UPG21800', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5490, 366, 'Ajangale', 'UPG21801', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5491, 366, 'Barebbo', 'UPG21802', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5492, 366, 'Bontocani', 'UPG21803', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5493, 366, 'Cenrana ', 'UPG21804', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5494, 366, 'Cina', 'UPG21805', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5495, 366, 'Dua Boccoe', 'UPG21806', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5496, 366, 'Kahu', 'UPG21807', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5497, 366, 'Kajuara', 'UPG21808', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5498, 366, 'Lamuru', 'UPG21809', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5499, 366, 'Lappariaja', 'UPG21810', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5500, 366, 'Libureng', 'UPG21811', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5501, 366, 'Mare ', 'UPG21812', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5502, 366, 'Ponre', 'UPG21813', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5503, 366, 'Salomekko', 'UPG21814', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5504, 366, 'Sibulue', 'UPG21815', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5505, 366, 'Tellu Siattinge', 'UPG21816', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5506, 366, 'Tonra', 'UPG21817', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5507, 366, 'Ulaweng', 'UPG21818', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5508, 366, 'Amali', 'UPG21819', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5509, 366, 'Awangpone', 'UPG21820', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5510, 366, 'Bengo', 'UPG21821', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5511, 366, 'Palakka', 'UPG21822', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5512, 366, 'Patimpeng', 'UPG21823', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5513, 366, 'Tanete Riattang', 'UPG21825', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5514, 366, 'Tanete Riattang Barat', 'UPG21826', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5515, 366, 'Tanete Riattang Timur', 'UPG21827', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5516, 351, 'Watansoppeng', 'UPG21900', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5517, 351, 'Donri Donri', 'UPG21901', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5518, 351, 'Lili Riaja', 'UPG21902', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5519, 351, 'Lili Rilau', 'UPG21903', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5520, 351, 'Mario Riwawo', 'UPG21904', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5521, 351, 'Mario Riawa', 'UPG21905', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5522, 351, 'Ganra', 'UPG21906', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5523, 351, 'Lalabata', 'UPG21907', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5524, 364, 'Benteng', 'UPG22000', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5525, 364, 'Bontosikuyu', 'UPG22001', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5526, 364, 'Bontomatene', 'UPG22002', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5527, 364, 'Pasimarannu', 'UPG22003', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5528, 364, 'Pasimasunggu', 'UPG22004', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5529, 364, 'Bontoharu', 'UPG22005', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5530, 364, 'Bontomanai', 'UPG22006', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5531, 364, 'Pasilambena', 'UPG22007', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5532, 364, 'Takabonerate', 'UPG22008', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5533, 348, 'Pangkajene', 'UPG22100', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5534, 348, 'Balocci', 'UPG22101', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5535, 348, 'Bungoro', 'UPG22102', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5536, 348, 'Labakkang', 'UPG22103', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5537, 348, 'Liukang Tangaya', 'UPG22105', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5538, 348, 'Liukang Tupabiring', 'UPG22106', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5539, 348, 'Ma\'Rang', 'UPG22107', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5540, 348, 'Segeri', 'UPG22108', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5541, 348, 'Kalukuang Masalima', 'UPG22109', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5542, 348, 'Mandalle', 'UPG22110', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5543, 348, 'Minasa Te\'ne', 'UPG22111', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5544, 348, 'Tondong Talasa', 'UPG22112', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5545, 361, 'Pare-Pare', 'UPG22300', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5546, 361, 'Bacukiki', 'UPG22301', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5547, 361, 'Soreang', 'UPG22302', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5548, 361, 'Ujung', 'UPG22303', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5549, 373, 'Belopa', 'UPG23100', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5550, 373, 'Bajo', 'UPG23101', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5551, 373, 'Bassesangtempe', 'UPG23102', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5552, 373, 'Bua', 'UPG23103', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5553, 373, 'Bua Ponrang', 'UPG23104', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5554, 373, 'Kamanre', 'UPG23105', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5555, 373, 'Lamasi', 'UPG23106', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5556, 373, 'Larompong', 'UPG23107', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5557, 373, 'Laronpong Selatan', 'UPG23108', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5558, 373, 'Latimojong', 'UPG23109', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5559, 373, 'Poncang', 'UPG23110', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5560, 373, 'Suli', 'UPG23111', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5561, 373, 'Walenrang', 'UPG23112', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5562, 358, 'Malili / Soroako', 'UPG23200', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5563, 358, 'Angkona', 'UPG23201', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5564, 358, 'Burau', 'UPG23202', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5565, 358, 'Mangkutana', 'UPG23203', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5566, 358, 'Nuha', 'UPG23204', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5567, 358, 'Tomoni', 'UPG23205', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5568, 358, 'Towuti', 'UPG23206', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5569, 358, 'Wotu', 'UPG23207', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5570, 373, 'Masamba', 'UPG23300', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5571, 373, 'Baebunta', 'UPG23301', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5572, 373, 'Bone-Bone', 'UPG23302', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5573, 373, 'Limbong', 'UPG23303', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5574, 373, 'Malangke', 'UPG23304', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5575, 373, 'Malangke Barat', 'UPG23305', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5576, 373, 'Mappedeceng', 'UPG23306', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5577, 373, 'Rampi', 'UPG23307', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5578, 373, 'Sabbang', 'UPG23308', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5579, 373, 'Seko', 'UPG23309', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5580, 373, 'Sukamaju', 'UPG23310', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5581, 370, 'Mamasa', 'UPG23400', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5582, 370, 'Aralle', 'UPG23401', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5583, 370, 'Mambi', 'UPG23402', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5584, 370, 'Messawa', 'UPG23403', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5585, 370, 'Pana', 'UPG23404', 71000, 61000, 0, 0, 0, '-', '-');
INSERT INTO `ae_kecamatan` VALUES (5586, 370, 'Nosu', 'UPG23405', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5587, 370, 'Sesena Padang', 'UPG23406', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5588, 370, 'Sumarorong', 'UPG23407', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5589, 370, 'Tabang ', 'UPG23408', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5590, 370, 'Tabulahan', 'UPG23409', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5591, 370, 'Tanduk Kalua', 'UPG23410', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5592, 355, 'Pasangkayu', 'UPG23500', 57000, 49000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5593, 355, 'Bambalamotu', 'UPG23501', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5594, 355, 'Baras', 'UPG23502', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5595, 355, 'Sarudu', 'UPG23503', 71000, 61000, 0, 0, 0, '2-3', ' 3-4 ');
INSERT INTO `ae_kecamatan` VALUES (5597, 490, 'Jamblang', '', 11000, 0, 0, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5598, 491, 'Gunung Jati', '', 11000, 0, 0, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5599, 72, 'Harjamukti', '', 8000, 7000, 11000, 10000, 9000, '', '');
INSERT INTO `ae_kecamatan` VALUES (5600, 72, 'Tengahtani', '', 7000, 6000, 8000, 12000, 9000, '', '');
INSERT INTO `ae_kecamatan` VALUES (5612, 281, 'Palibelo', '', 55000, 48000, 0, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5614, 219, 'Sagulung', '', 30000, 26000, 37000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5615, 68, 'DEPOK ', '', 7000, 6000, 0, 0, 9000, '', '');
INSERT INTO `ae_kecamatan` VALUES (5619, 68, 'Kedawung', '', 7000, 6000, 8000, 0, 9000, '', '');
INSERT INTO `ae_kecamatan` VALUES (5620, 68, 'Klangenan', '', 7000, 6000, 8000, 0, 9000, '', '');
INSERT INTO `ae_kecamatan` VALUES (5621, 68, 'Palimanan', '', 7000, 6000, 0, 0, 9000, '', '');
INSERT INTO `ae_kecamatan` VALUES (5622, 68, 'Plered', '', 7000, 6000, 8000, 0, 9000, '', '');
INSERT INTO `ae_kecamatan` VALUES (5623, 68, 'Plumbon', '', 7000, 6000, 8000, 0, 9000, '', '');
INSERT INTO `ae_kecamatan` VALUES (5624, 68, 'Sumber', '', 7000, 6000, 0, 0, 9000, '', '');
INSERT INTO `ae_kecamatan` VALUES (5625, 68, 'Tengah Tani', '', 7000, 6000, 8000, 0, 9000, '', '');
INSERT INTO `ae_kecamatan` VALUES (5626, 68, 'Weru', '', 7000, 6000, 8000, 0, 9000, '', '');
INSERT INTO `ae_kecamatan` VALUES (5627, 83, 'Ciloodong', '', 15000, 14000, 20000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5628, 68, '', '', 0, 0, 0, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5629, 68, '', '', 0, 0, 0, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5630, 72, '', '', 0, 0, 0, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5631, 72, '', '', 0, 0, 0, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5632, 72, 'Kejaksan', '', 8000, 7000, 11000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5633, 72, 'Kesambi', '', 8000, 7000, 11000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5634, 72, 'Lemahwungkuk', '', 8000, 7000, 11000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5635, 72, 'Pekalipan', '', 8000, 7000, 11000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5636, 83, 'Bojongsari', '', 15000, 14000, 20000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5637, 83, 'Cinere', '', 15000, 14000, 20000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5638, 83, 'Cinere', '', 15000, 14000, 20000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5639, 83, 'Cinere', '', 15000, 14000, 20000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5640, 83, 'Cipayung', '', 15000, 14000, 20000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5641, 83, 'Tapos', '', 15000, 14000, 20000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5642, 85, 'Juntinyuat', '', 8000, 7000, 11000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5643, 85, 'Pasekan', '', 8000, 7000, 11000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5644, 85, 'Patrol', '', 8000, 7000, 11000, 0, 0, '', '');
INSERT INTO `ae_kecamatan` VALUES (5645, 85, 'Tukdana', '', 8000, 7000, 11000, 0, 0, '', '');
COMMIT;

-- ----------------------------
-- Table structure for ae_provinsi
-- ----------------------------
DROP TABLE IF EXISTS `ae_provinsi`;
CREATE TABLE `ae_provinsi` (
  `id_provinsi` int(20) NOT NULL AUTO_INCREMENT,
  `nama_provinsi` varchar(100) NOT NULL,
  PRIMARY KEY (`id_provinsi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ae_provinsi
-- ----------------------------
BEGIN;
INSERT INTO `ae_provinsi` VALUES (1, 'Bali');
INSERT INTO `ae_provinsi` VALUES (2, 'Bangka Belitung');
INSERT INTO `ae_provinsi` VALUES (3, 'Banten');
INSERT INTO `ae_provinsi` VALUES (4, 'Bengkulu ');
INSERT INTO `ae_provinsi` VALUES (5, 'D.I Yogyakarta');
INSERT INTO `ae_provinsi` VALUES (6, 'Gorontalo');
INSERT INTO `ae_provinsi` VALUES (7, 'Jakarta');
INSERT INTO `ae_provinsi` VALUES (8, 'Jambi');
INSERT INTO `ae_provinsi` VALUES (9, 'Jawa Barat');
INSERT INTO `ae_provinsi` VALUES (10, 'Jawa Tengah');
INSERT INTO `ae_provinsi` VALUES (11, 'Jawa Timur');
INSERT INTO `ae_provinsi` VALUES (12, 'Kalimantan Barat');
INSERT INTO `ae_provinsi` VALUES (13, 'Kalimantan Selatan');
INSERT INTO `ae_provinsi` VALUES (14, 'Kalimantan Tengah');
INSERT INTO `ae_provinsi` VALUES (15, 'Kalimantan Timur');
INSERT INTO `ae_provinsi` VALUES (16, 'Kepulauan Riau');
INSERT INTO `ae_provinsi` VALUES (17, 'Lampung');
INSERT INTO `ae_provinsi` VALUES (18, 'Maluku');
INSERT INTO `ae_provinsi` VALUES (19, 'Maluku Utara');
INSERT INTO `ae_provinsi` VALUES (20, 'NAD');
INSERT INTO `ae_provinsi` VALUES (21, 'Nusa Tenggara Barat');
INSERT INTO `ae_provinsi` VALUES (22, 'Nusa Tenggara Timur');
INSERT INTO `ae_provinsi` VALUES (23, 'Papua');
INSERT INTO `ae_provinsi` VALUES (24, 'Papua Barat');
INSERT INTO `ae_provinsi` VALUES (25, 'Riau');
INSERT INTO `ae_provinsi` VALUES (26, 'Sulawesi Selatan');
INSERT INTO `ae_provinsi` VALUES (27, 'Sulawesi Tengah');
INSERT INTO `ae_provinsi` VALUES (28, 'Sulawesi Tenggara');
INSERT INTO `ae_provinsi` VALUES (29, 'Sulawesi Utara');
INSERT INTO `ae_provinsi` VALUES (30, 'Sumatera Barat');
INSERT INTO `ae_provinsi` VALUES (31, 'Sumatera Selatan');
INSERT INTO `ae_provinsi` VALUES (32, 'Sumatera Utara');
INSERT INTO `ae_provinsi` VALUES (33, 'Kalimantan Utara');
COMMIT;

-- ----------------------------
-- Table structure for ap_banner
-- ----------------------------
DROP TABLE IF EXISTS `ap_banner`;
CREATE TABLE `ap_banner` (
  `id_banner` int(11) NOT NULL AUTO_INCREMENT,
  `banner` varchar(50) NOT NULL,
  `banner_image` varchar(200) NOT NULL,
  PRIMARY KEY (`id_banner`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ap_banner
-- ----------------------------
BEGIN;
INSERT INTO `ap_banner` VALUES (28, 'Shayna Feeney', 'screencapture-localhost-wonderland-2019-11-28-00_30_453.png');
COMMIT;

-- ----------------------------
-- Table structure for ap_cart
-- ----------------------------
DROP TABLE IF EXISTS `ap_cart`;
CREATE TABLE `ap_cart` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `id_produk` varchar(100) NOT NULL,
  `quantity` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `harga` int(15) NOT NULL,
  `diskon` int(15) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_cart_diskon
-- ----------------------------
DROP TABLE IF EXISTS `ap_cart_diskon`;
CREATE TABLE `ap_cart_diskon` (
  `idUser` int(15) NOT NULL,
  `diskon` int(15) NOT NULL,
  PRIMARY KEY (`idUser`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_cart_diskon_member
-- ----------------------------
DROP TABLE IF EXISTS `ap_cart_diskon_member`;
CREATE TABLE `ap_cart_diskon_member` (
  `idUser` int(5) NOT NULL,
  `idMember` int(10) NOT NULL,
  `diskon` int(50) NOT NULL,
  `poinReimburs` int(11) NOT NULL,
  `poinValue` int(10) NOT NULL,
  PRIMARY KEY (`idUser`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_cart_diskon_member_temp
-- ----------------------------
DROP TABLE IF EXISTS `ap_cart_diskon_member_temp`;
CREATE TABLE `ap_cart_diskon_member_temp` (
  `noCart` varchar(15) NOT NULL,
  `idMember` int(15) DEFAULT NULL,
  `diskon` int(15) DEFAULT NULL,
  `poinReimburs` int(15) NOT NULL,
  `poinValue` int(15) NOT NULL,
  PRIMARY KEY (`noCart`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_cart_diskon_temp
-- ----------------------------
DROP TABLE IF EXISTS `ap_cart_diskon_temp`;
CREATE TABLE `ap_cart_diskon_temp` (
  `noCart` varchar(20) NOT NULL,
  `diskon` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_cart_ongkir
-- ----------------------------
DROP TABLE IF EXISTS `ap_cart_ongkir`;
CREATE TABLE `ap_cart_ongkir` (
  `idUser` int(12) NOT NULL,
  `ongkir` int(12) NOT NULL,
  PRIMARY KEY (`idUser`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_cart_ongkir_temp
-- ----------------------------
DROP TABLE IF EXISTS `ap_cart_ongkir_temp`;
CREATE TABLE `ap_cart_ongkir_temp` (
  `noCart` varchar(20) NOT NULL,
  `ongkir` int(10) NOT NULL,
  PRIMARY KEY (`noCart`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_cart_temp
-- ----------------------------
DROP TABLE IF EXISTS `ap_cart_temp`;
CREATE TABLE `ap_cart_temp` (
  `id_produk` varchar(15) NOT NULL,
  `quantity` int(10) NOT NULL,
  `noCart` varchar(20) NOT NULL,
  `harga` int(10) NOT NULL,
  `diskon` int(10) NOT NULL,
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_cart_temp_no
-- ----------------------------
DROP TABLE IF EXISTS `ap_cart_temp_no`;
CREATE TABLE `ap_cart_temp_no` (
  `cartNo` varchar(20) NOT NULL,
  `idUser` int(10) NOT NULL,
  `tanggal` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`cartNo`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_customer
-- ----------------------------
DROP TABLE IF EXISTS `ap_customer`;
CREATE TABLE `ap_customer` (
  `id_customer` bigint(255) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `kontak` varchar(100) NOT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `diskon` float NOT NULL,
  `tanggal_gabung` date NOT NULL,
  `point` int(15) NOT NULL,
  `alamat` text NOT NULL,
  `id_provinsi` int(15) NOT NULL,
  `id_kabupaten` int(15) NOT NULL,
  `id_kecamatan` int(15) NOT NULL,
  `kategori` int(15) NOT NULL,
  `limit_kredit` int(20) NOT NULL,
  `limit_day` int(10) NOT NULL,
  `jenis_cust` varchar(20) NOT NULL,
  `ppn` int(1) NOT NULL,
  `id_toko` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level_id` int(11) NOT NULL,
  PRIMARY KEY (`id_customer`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ap_customer
-- ----------------------------
BEGIN;
INSERT INTO `ap_customer` VALUES (1, 'Shayna Feeney', '081331294812', '2019-12-18', 15, '2019-12-01', 0, '81090 Marks Forest', 5, 36, 1835, 14, 10000000, 30, 'tkp', 1, 0, '', '', 0);
INSERT INTO `ap_customer` VALUES (2, 'Dolores Armstrong', '081331294812', '2019-11-27', 0, '2019-12-01', 0, '8209 Smitham Underpass', 1, 4, 1651, 14, 10000000, 30, 'tkp', 0, 0, '', '', 0);
INSERT INTO `ap_customer` VALUES (3, 'renald', '123456', '2020-03-30', 0, '2020-04-03', 6, 'solo', 9, 68, 1206, 0, 1000000, 20, 'tkp', 0, 11, '', '', 14);
COMMIT;

-- ----------------------------
-- Table structure for ap_customer_group
-- ----------------------------
DROP TABLE IF EXISTS `ap_customer_group`;
CREATE TABLE `ap_customer_group` (
  `id_group` int(15) NOT NULL AUTO_INCREMENT,
  `group_customer` varchar(35) NOT NULL,
  PRIMARY KEY (`id_group`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ap_customer_group
-- ----------------------------
BEGIN;
INSERT INTO `ap_customer_group` VALUES (14, 'level 1');
INSERT INTO `ap_customer_group` VALUES (15, 'level 2');
INSERT INTO `ap_customer_group` VALUES (16, 'level 3');
COMMIT;

-- ----------------------------
-- Table structure for ap_ekspedisi
-- ----------------------------
DROP TABLE IF EXISTS `ap_ekspedisi`;
CREATE TABLE `ap_ekspedisi` (
  `id_ekspedisi` int(10) NOT NULL AUTO_INCREMENT,
  `ekspedisi` varchar(30) NOT NULL,
  PRIMARY KEY (`id_ekspedisi`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ap_ekspedisi
-- ----------------------------
BEGIN;
INSERT INTO `ap_ekspedisi` VALUES (1, 'Budiman');
INSERT INTO `ap_ekspedisi` VALUES (2, 'Siska');
INSERT INTO `ap_ekspedisi` VALUES (3, 'Tono');
COMMIT;

-- ----------------------------
-- Table structure for ap_free_item
-- ----------------------------
DROP TABLE IF EXISTS `ap_free_item`;
CREATE TABLE `ap_free_item` (
  `no_invoice` varchar(100) NOT NULL,
  `id_produk` varchar(100) NOT NULL,
  `harga` int(20) NOT NULL,
  `qty` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_invoice_item
-- ----------------------------
DROP TABLE IF EXISTS `ap_invoice_item`;
CREATE TABLE `ap_invoice_item` (
  `no_invoice` varchar(100) NOT NULL,
  `id_produk` varchar(100) NOT NULL,
  `hpp` int(50) NOT NULL,
  `harga_jual` int(50) NOT NULL,
  `diskon` int(15) DEFAULT NULL,
  `qty` int(10) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ap_invoice_item
-- ----------------------------
BEGIN;
INSERT INTO `ap_invoice_item` VALUES ('INV20041310001', 'BESI-4NGFI-002', 0, 2500, 0, 4, '2020-04-13');
INSERT INTO `ap_invoice_item` VALUES ('INV20041310002', 'BESI-4NGFI-002', 0, 2500, 0, 4, '2020-04-13');
INSERT INTO `ap_invoice_item` VALUES ('INV20041310003', 'EMBER-27A04-002', 0, 1000000, 3, 3, '2020-04-13');
COMMIT;

-- ----------------------------
-- Table structure for ap_invoice_number
-- ----------------------------
DROP TABLE IF EXISTS `ap_invoice_number`;
CREATE TABLE `ap_invoice_number` (
  `no_invoice` varchar(100) NOT NULL,
  `tipe_bayar` int(5) NOT NULL,
  `sub_account` int(15) DEFAULT NULL,
  `jatuh_tempo` date DEFAULT NULL,
  `total` int(12) NOT NULL,
  `ongkir` int(15) NOT NULL,
  `diskon` int(10) DEFAULT NULL,
  `diskon_free` int(10) NOT NULL,
  `poin_value` int(15) NOT NULL,
  `poin` int(15) DEFAULT NULL,
  `poin_reimburs` int(10) DEFAULT NULL,
  `diskon_otomatis` int(15) DEFAULT NULL,
  `jumlah_bayar` int(100) NOT NULL,
  `id_pic` int(10) NOT NULL,
  `id_customer` int(255) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `tanggal` datetime NOT NULL,
  `nama_penerima` varchar(50) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `kontak_pengiriman` varchar(15) DEFAULT NULL,
  `id_provinsi` int(6) DEFAULT NULL,
  `id_kabupaten` int(6) DEFAULT NULL,
  `id_kecamatan` int(6) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `no_resi` varchar(30) DEFAULT NULL,
  `id_ekspedisi` int(3) DEFAULT NULL,
  `id_toko` int(10) DEFAULT NULL,
  PRIMARY KEY (`no_invoice`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ap_invoice_number
-- ----------------------------
BEGIN;
INSERT INTO `ap_invoice_number` VALUES ('INV20041310001', 1, 0, NULL, 10000, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, '', '2020-04-13 22:01:34', '', '', '', 0, 0, 0, 0, NULL, 0, 11);
INSERT INTO `ap_invoice_number` VALUES ('INV20041310002', 1, 0, NULL, 10000, 0, NULL, 0, 0, 0, 0, 0, 10000, 1, NULL, 'bayar', '2020-04-13 22:04:07', '', '', '', 0, 0, 0, 0, NULL, 0, 11);
INSERT INTO `ap_invoice_number` VALUES ('INV20041310003', 1, 0, NULL, 3000000, 0, 0, 0, 0, 6, 0, 3, 3000000, 1, 3, '', '2020-04-13 22:27:10', '', '', '', 0, 0, 0, 0, NULL, 0, 11);
COMMIT;

-- ----------------------------
-- Table structure for ap_invoice_voucher
-- ----------------------------
DROP TABLE IF EXISTS `ap_invoice_voucher`;
CREATE TABLE `ap_invoice_voucher` (
  `no_invoice` varchar(100) NOT NULL,
  `nama_voucher` varchar(100) NOT NULL,
  `nilai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_kategori
-- ----------------------------
DROP TABLE IF EXISTS `ap_kategori`;
CREATE TABLE `ap_kategori` (
  `id_kategori` int(100) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) NOT NULL,
  PRIMARY KEY (`id_kategori`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ap_kategori
-- ----------------------------
BEGIN;
INSERT INTO `ap_kategori` VALUES (1, 'Besi Beton');
INSERT INTO `ap_kategori` VALUES (2, 'Besi Plat');
INSERT INTO `ap_kategori` VALUES (3, 'Besi Ulir');
INSERT INTO `ap_kategori` VALUES (4, 'Pipa Besi');
INSERT INTO `ap_kategori` VALUES (5, 'Besi Holo');
INSERT INTO `ap_kategori` VALUES (6, 'Besi UNP');
INSERT INTO `ap_kategori` VALUES (7, 'Besi CNP');
INSERT INTO `ap_kategori` VALUES (8, 'Besi IWF');
INSERT INTO `ap_kategori` VALUES (9, 'Besi HWF');
COMMIT;

-- ----------------------------
-- Table structure for ap_kategori_1
-- ----------------------------
DROP TABLE IF EXISTS `ap_kategori_1`;
CREATE TABLE `ap_kategori_1` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(10) NOT NULL,
  `kategori_level_1` varchar(70) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `ap_kategori` (`id_kategori`) USING BTREE,
  CONSTRAINT `ap_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `ap_kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_kategori_2
-- ----------------------------
DROP TABLE IF EXISTS `ap_kategori_2`;
CREATE TABLE `ap_kategori_2` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_kategori_1` int(20) NOT NULL,
  `kategori_3` varchar(100) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `kategori2` (`id_kategori_1`) USING BTREE,
  CONSTRAINT `kategori2` FOREIGN KEY (`id_kategori_1`) REFERENCES `ap_kategori_1` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_no_void
-- ----------------------------
DROP TABLE IF EXISTS `ap_no_void`;
CREATE TABLE `ap_no_void` (
  `no_void` varchar(100) NOT NULL,
  `id_pic` int(10) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_meja` int(10) NOT NULL,
  `alasan` varchar(100) NOT NULL,
  `nama_customer` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_order_temp
-- ----------------------------
DROP TABLE IF EXISTS `ap_order_temp`;
CREATE TABLE `ap_order_temp` (
  `id_pending` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `id_produk` varchar(30) NOT NULL,
  `qty` int(10) NOT NULL,
  `harga_jual` int(10) NOT NULL,
  `hpp` int(10) NOT NULL,
  `diskon` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_order_temp_no
-- ----------------------------
DROP TABLE IF EXISTS `ap_order_temp_no`;
CREATE TABLE `ap_order_temp_no` (
  `id_pending` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  PRIMARY KEY (`id_pending`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_payment_account
-- ----------------------------
DROP TABLE IF EXISTS `ap_payment_account`;
CREATE TABLE `ap_payment_account` (
  `id_payment_account` int(12) NOT NULL AUTO_INCREMENT,
  `id_payment_type` int(10) NOT NULL,
  `account` varchar(100) NOT NULL,
  `surcharge` int(5) NOT NULL,
  PRIMARY KEY (`id_payment_account`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ap_payment_account
-- ----------------------------
BEGIN;
INSERT INTO `ap_payment_account` VALUES (1, 2, 'Mandiri', 0);
INSERT INTO `ap_payment_account` VALUES (2, 2, 'BCA', 0);
INSERT INTO `ap_payment_account` VALUES (3, 3, 'Mandiri', 0);
INSERT INTO `ap_payment_account` VALUES (4, 3, 'BCA', 0);
INSERT INTO `ap_payment_account` VALUES (5, 2, 'BRI', 0);
INSERT INTO `ap_payment_account` VALUES (6, 2, 'CIMB Niaga', 0);
INSERT INTO `ap_payment_account` VALUES (7, 3, 'CIMG Niaga', 0);
INSERT INTO `ap_payment_account` VALUES (8, 3, 'BRI', 0);
INSERT INTO `ap_payment_account` VALUES (9, 2, 'BNI', 0);
INSERT INTO `ap_payment_account` VALUES (10, 2, 'Mega', 0);
INSERT INTO `ap_payment_account` VALUES (11, 2, 'Bukopin', 0);
INSERT INTO `ap_payment_account` VALUES (12, 3, 'BNI ', 0);
INSERT INTO `ap_payment_account` VALUES (13, 3, 'Mega', 0);
INSERT INTO `ap_payment_account` VALUES (14, 3, 'Bukopin', 0);
COMMIT;

-- ----------------------------
-- Table structure for ap_payment_type
-- ----------------------------
DROP TABLE IF EXISTS `ap_payment_type`;
CREATE TABLE `ap_payment_type` (
  `id` int(25) NOT NULL AUTO_INCREMENT,
  `payment_type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ap_payment_type
-- ----------------------------
BEGIN;
INSERT INTO `ap_payment_type` VALUES (1, 'Cash');
INSERT INTO `ap_payment_type` VALUES (2, 'Debit');
INSERT INTO `ap_payment_type` VALUES (3, 'Kredit\r\n');
INSERT INTO `ap_payment_type` VALUES (4, 'Transfer');
INSERT INTO `ap_payment_type` VALUES (5, 'Hutang');
COMMIT;

-- ----------------------------
-- Table structure for ap_piutang
-- ----------------------------
DROP TABLE IF EXISTS `ap_piutang`;
CREATE TABLE `ap_piutang` (
  `no_invoice` varchar(15) NOT NULL,
  `status` int(11) NOT NULL,
  `jatuh_tempo` date NOT NULL,
  PRIMARY KEY (`no_invoice`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_piutang_pay
-- ----------------------------
DROP TABLE IF EXISTS `ap_piutang_pay`;
CREATE TABLE `ap_piutang_pay` (
  `no_seri` varchar(20) NOT NULL,
  `no_invoice` varchar(20) NOT NULL,
  `id_pic` int(5) NOT NULL,
  `id_payment` int(20) NOT NULL,
  `account` int(1) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `nominal` int(15) NOT NULL,
  `keterangan` text DEFAULT NULL,
  PRIMARY KEY (`no_seri`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_printer
-- ----------------------------
DROP TABLE IF EXISTS `ap_printer`;
CREATE TABLE `ap_printer` (
  `id_printer` int(3) NOT NULL,
  `ip_address` varchar(200) NOT NULL,
  `printer_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_produk
-- ----------------------------
DROP TABLE IF EXISTS `ap_produk`;
CREATE TABLE `ap_produk` (
  `id_produk` varchar(100) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `satuan` varchar(50) DEFAULT NULL,
  `hpp` int(10) DEFAULT NULL,
  `harga` int(10) NOT NULL,
  `diskon` int(11) NOT NULL,
  `id_kategori` int(5) DEFAULT NULL,
  `id_subkategori` int(10) DEFAULT NULL,
  `id_subkategori_2` int(15) DEFAULT NULL,
  `tempat` int(3) DEFAULT NULL,
  `status` int(5) DEFAULT NULL COMMENT '0 = non aktif, 1 = aktif, 2 = delete',
  `stok` int(15) DEFAULT NULL,
  `type` int(5) NOT NULL,
  `pajak` int(1) NOT NULL,
  PRIMARY KEY (`id_produk`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ap_produk
-- ----------------------------
BEGIN;
INSERT INTO `ap_produk` VALUES ('BESI BETON 10MM-WFDJI-002', 'Besi Beton 10mm', 'batang', 50000, 0, 0, 1, 0, 0, 9, 1, 0, 1, 1);
INSERT INTO `ap_produk` VALUES ('BESI BETON 12MM-CM4QU-002', 'Besi Beton 12mm', 'batang', 78000, 0, 0, 1, 0, 0, 11, 1, 0, 1, 1);
INSERT INTO `ap_produk` VALUES ('BESI PLAT-GL3EX-002', 'Besi Plat', 'Lembar', 200000, 0, 0, 2, 0, 0, 11, 1, 0, 1, 1);
INSERT INTO `ap_produk` VALUES ('BESI-4NGFI-002', 'besi', 'batang', 2000, 0, 0, 1, 0, 0, 9, 1, 0, 1, 0);
INSERT INTO `ap_produk` VALUES ('EMBER-27A04-002', 'ember', 'Box', 4000000, 0, 0, 2, 0, 0, 11, 1, 20, 1, 0);
INSERT INTO `ap_produk` VALUES ('SEMEN-OXFTG-001', 'semen', 'Sak', 30000, 0, 1, 1, 0, 0, 9, 1, 0, 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for ap_produk_bahan_baku
-- ----------------------------
DROP TABLE IF EXISTS `ap_produk_bahan_baku`;
CREATE TABLE `ap_produk_bahan_baku` (
  `id_produk` varchar(20) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `qty` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_produk_discount_rules
-- ----------------------------
DROP TABLE IF EXISTS `ap_produk_discount_rules`;
CREATE TABLE `ap_produk_discount_rules` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_produk` varchar(30) NOT NULL,
  `discount` int(15) NOT NULL,
  `qty` int(10) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_produk_price
-- ----------------------------
DROP TABLE IF EXISTS `ap_produk_price`;
CREATE TABLE `ap_produk_price` (
  `id_toko` int(10) NOT NULL,
  `id_produk` varchar(30) NOT NULL,
  `harga` int(20) NOT NULL,
  `harga2` int(20) NOT NULL,
  `harga3` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ap_produk_price
-- ----------------------------
BEGIN;
INSERT INTO `ap_produk_price` VALUES (10, 'SEMEN-OXFTG-001', 36000, 37440, 37800);
INSERT INTO `ap_produk_price` VALUES (11, 'BESI-4NGFI-002', 2500, 3000, 3250);
INSERT INTO `ap_produk_price` VALUES (9, 'BESI BETON 10MM-WFDJI-002', 70000, 73500, 77000);
INSERT INTO `ap_produk_price` VALUES (11, 'BESI BETON 12MM-CM4QU-002', 82000, 85000, 88000);
INSERT INTO `ap_produk_price` VALUES (11, 'BESI PLAT-GL3EX-002', 217000, 220000, 240000);
INSERT INTO `ap_produk_price` VALUES (11, 'EMBER-27A04-002', 1000000, 1200000, 1400000);
COMMIT;

-- ----------------------------
-- Table structure for ap_receipt
-- ----------------------------
DROP TABLE IF EXISTS `ap_receipt`;
CREATE TABLE `ap_receipt` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `alamat` varchar(100) NOT NULL,
  `kontak` varchar(15) NOT NULL,
  `footer` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ap_receipt
-- ----------------------------
BEGIN;
INSERT INTO `ap_receipt` VALUES (1, 'solo', '847324723', 'merbabudev.id', '', 'merbabudev');
COMMIT;

-- ----------------------------
-- Table structure for ap_reservasi
-- ----------------------------
DROP TABLE IF EXISTS `ap_reservasi`;
CREATE TABLE `ap_reservasi` (
  `no_reservasi` varchar(25) NOT NULL,
  `id_customer` varchar(100) NOT NULL,
  `total_reservasi` int(10) DEFAULT NULL,
  `diskon_produk` int(7) DEFAULT NULL,
  `diskon_promosi` int(10) DEFAULT NULL,
  `ongkir` int(7) DEFAULT NULL,
  `tanggal_reservasi` date NOT NULL,
  `keterangan` text NOT NULL,
  `tipe_bayar` int(10) NOT NULL,
  `account` int(10) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `id_pic` int(10) NOT NULL,
  PRIMARY KEY (`no_reservasi`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_reservasi_item
-- ----------------------------
DROP TABLE IF EXISTS `ap_reservasi_item`;
CREATE TABLE `ap_reservasi_item` (
  `no_reservasi` varchar(30) NOT NULL,
  `id_produk` varchar(50) NOT NULL,
  `hpp` int(20) NOT NULL,
  `harga` int(20) NOT NULL,
  `diskon` int(10) DEFAULT NULL,
  `qty` int(7) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_reservasi_item_ambil
-- ----------------------------
DROP TABLE IF EXISTS `ap_reservasi_item_ambil`;
CREATE TABLE `ap_reservasi_item_ambil` (
  `no_reservasi` varchar(30) NOT NULL,
  `id_produk` varchar(10) NOT NULL,
  `qty` int(10) NOT NULL,
  `tanggal` date NOT NULL,
  `id_pic` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_reservasi_payment
-- ----------------------------
DROP TABLE IF EXISTS `ap_reservasi_payment`;
CREATE TABLE `ap_reservasi_payment` (
  `no_reservasi` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `nominal` int(10) NOT NULL,
  `id_pic` int(5) NOT NULL,
  `keterangan` text NOT NULL,
  `id_payment_type` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_retur
-- ----------------------------
DROP TABLE IF EXISTS `ap_retur`;
CREATE TABLE `ap_retur` (
  `no_retur` varchar(30) NOT NULL,
  `no_invoice` varchar(30) NOT NULL,
  `pic` int(15) NOT NULL,
  `tanggal` datetime NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`no_retur`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_retur_item
-- ----------------------------
DROP TABLE IF EXISTS `ap_retur_item`;
CREATE TABLE `ap_retur_item` (
  `no_retur` varchar(100) NOT NULL,
  `id_produk` varchar(50) NOT NULL,
  `qty` int(15) NOT NULL,
  `harga` int(30) NOT NULL,
  `tanggal` date NOT NULL,
  `diskon` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_staff
-- ----------------------------
DROP TABLE IF EXISTS `ap_staff`;
CREATE TABLE `ap_staff` (
  `id_staff` int(25) NOT NULL AUTO_INCREMENT,
  `nama_staff` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `last_login` datetime NOT NULL,
  `id_segmentasi` int(5) NOT NULL,
  PRIMARY KEY (`id_staff`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_stand
-- ----------------------------
DROP TABLE IF EXISTS `ap_stand`;
CREATE TABLE `ap_stand` (
  `id_stand` int(15) NOT NULL AUTO_INCREMENT,
  `stand` varchar(30) NOT NULL,
  PRIMARY KEY (`id_stand`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ap_stand
-- ----------------------------
BEGIN;
INSERT INTO `ap_stand` VALUES (29, 'bahan bangunan');
INSERT INTO `ap_stand` VALUES (30, 'perlengkapan');
COMMIT;

-- ----------------------------
-- Table structure for ap_store
-- ----------------------------
DROP TABLE IF EXISTS `ap_store`;
CREATE TABLE `ap_store` (
  `id_store` int(15) NOT NULL AUTO_INCREMENT,
  `store` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `footer` text NOT NULL,
  `kontak` varchar(20) NOT NULL,
  PRIMARY KEY (`id_store`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of ap_store
-- ----------------------------
BEGIN;
INSERT INTO `ap_store` VALUES (11, 'Gunung Sakti', 'Jln Sumatera\r\nJambi', 'Kepuasan Anda Harapan Kami\r\nHotline Keluhan WA\r\n+620123456789', '+6281264637150');
COMMIT;

-- ----------------------------
-- Table structure for ap_tempat
-- ----------------------------
DROP TABLE IF EXISTS `ap_tempat`;
CREATE TABLE `ap_tempat` (
  `id_tempat` int(10) NOT NULL,
  `tempat` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for ap_voucher
-- ----------------------------
DROP TABLE IF EXISTS `ap_voucher`;
CREATE TABLE `ap_voucher` (
  `id_voucher` int(100) NOT NULL AUTO_INCREMENT,
  `nama_voucher` varchar(100) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `potongan` int(5) NOT NULL,
  `tipe_voucher` int(5) NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id_voucher`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for bahan_baku
-- ----------------------------
DROP TABLE IF EXISTS `bahan_baku`;
CREATE TABLE `bahan_baku` (
  `sku` varchar(30) NOT NULL,
  `nama_bahan` varchar(100) NOT NULL,
  `id_kategori` int(10) DEFAULT NULL,
  `satuan` varchar(30) NOT NULL,
  `harga` int(30) NOT NULL,
  `status` int(5) NOT NULL,
  `del` int(1) NOT NULL COMMENT '1 = aktif',
  `stok` int(30) DEFAULT NULL COMMENT '1 = aktif',
  PRIMARY KEY (`sku`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for cc_cartmutasi
-- ----------------------------
DROP TABLE IF EXISTS `cc_cartmutasi`;
CREATE TABLE `cc_cartmutasi` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `idProduk` varchar(50) NOT NULL,
  `idUser` int(10) NOT NULL,
  `qty` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for cc_cartpurchaseorder
-- ----------------------------
DROP TABLE IF EXISTS `cc_cartpurchaseorder`;
CREATE TABLE `cc_cartpurchaseorder` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `idProduk` varchar(100) NOT NULL,
  `qty` int(20) NOT NULL,
  `idUser` int(10) NOT NULL,
  `harga` int(30) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for cc_cartransfer
-- ----------------------------
DROP TABLE IF EXISTS `cc_cartransfer`;
CREATE TABLE `cc_cartransfer` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `idProduk` varchar(100) NOT NULL,
  `qty` int(10) NOT NULL,
  `idUser` int(5) NOT NULL,
  `idStore` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for cc_cartwaste
-- ----------------------------
DROP TABLE IF EXISTS `cc_cartwaste`;
CREATE TABLE `cc_cartwaste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idProduk` varchar(35) DEFAULT NULL,
  `qty` int(30) DEFAULT NULL,
  `idUser` int(30) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for cc_konversiproduk
-- ----------------------------
DROP TABLE IF EXISTS `cc_konversiproduk`;
CREATE TABLE `cc_konversiproduk` (
  `idProduk` varchar(30) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL,
  `hargaBeli` varchar(10) DEFAULT NULL,
  `idUser` int(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for cc_pomaterial
-- ----------------------------
DROP TABLE IF EXISTS `cc_pomaterial`;
CREATE TABLE `cc_pomaterial` (
  `idProduk` varchar(30) DEFAULT NULL,
  `qty` int(15) DEFAULT NULL,
  `idUser` int(15) DEFAULT NULL,
  `harga` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for cc_workorderconvert
-- ----------------------------
DROP TABLE IF EXISTS `cc_workorderconvert`;
CREATE TABLE `cc_workorderconvert` (
  `idProduk` varchar(50) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL,
  `idUser` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for cc_workorderitem
-- ----------------------------
DROP TABLE IF EXISTS `cc_workorderitem`;
CREATE TABLE `cc_workorderitem` (
  `sku` int(50) DEFAULT NULL,
  `harga` int(30) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL,
  `idUser` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for closing_account
-- ----------------------------
DROP TABLE IF EXISTS `closing_account`;
CREATE TABLE `closing_account` (
  `id_closing` varchar(20) NOT NULL,
  `id_kasir` int(15) NOT NULL,
  `payment_type` int(4) NOT NULL,
  `account` int(5) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `value` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for closing_account_temp
-- ----------------------------
DROP TABLE IF EXISTS `closing_account_temp`;
CREATE TABLE `closing_account_temp` (
  `id_kasir` int(15) NOT NULL,
  `payment_type` int(4) NOT NULL,
  `account` int(5) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `value` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for closing_id
-- ----------------------------
DROP TABLE IF EXISTS `closing_id`;
CREATE TABLE `closing_id` (
  `id_closing` varchar(15) NOT NULL,
  `id_kasir` int(30) NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  PRIMARY KEY (`id_closing`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of closing_id
-- ----------------------------
BEGIN;
INSERT INTO `closing_id` VALUES ('CLS-120719-021', 21, '2019-07-12', '16:11:33');
COMMIT;

-- ----------------------------
-- Table structure for closing_modal
-- ----------------------------
DROP TABLE IF EXISTS `closing_modal`;
CREATE TABLE `closing_modal` (
  `id_user` int(10) NOT NULL,
  `modal` int(10) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of closing_modal
-- ----------------------------
BEGIN;
INSERT INTO `closing_modal` VALUES (27, 9000000, '2020-04-08 07:31:39');
COMMIT;

-- ----------------------------
-- Table structure for closing_password
-- ----------------------------
DROP TABLE IF EXISTS `closing_password`;
CREATE TABLE `closing_password` (
  `password` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for footer_text
-- ----------------------------
DROP TABLE IF EXISTS `footer_text`;
CREATE TABLE `footer_text` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `footer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of footer_text
-- ----------------------------
BEGIN;
INSERT INTO `footer_text` VALUES (1, '2019 © Merbabudev.id');
COMMIT;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of groups
-- ----------------------------
BEGIN;
INSERT INTO `groups` VALUES (1, 'admin', 'Administrator');
INSERT INTO `groups` VALUES (2, 'members', 'General User');
COMMIT;

-- ----------------------------
-- Table structure for hutang
-- ----------------------------
DROP TABLE IF EXISTS `hutang`;
CREATE TABLE `hutang` (
  `no_tagihan` varchar(30) NOT NULL,
  `status_hutang` int(5) NOT NULL COMMENT '0 = ditagih\r\n1 = terbayar\r\n2 = transaksi selesai',
  PRIMARY KEY (`no_tagihan`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hutang
-- ----------------------------
BEGIN;
INSERT INTO `hutang` VALUES ('PO2003310010001', 1);
INSERT INTO `hutang` VALUES ('PO2004130010001', 2);
COMMIT;

-- ----------------------------
-- Table structure for hutang_order
-- ----------------------------
DROP TABLE IF EXISTS `hutang_order`;
CREATE TABLE `hutang_order` (
  `no_payment` varchar(30) NOT NULL,
  `no_po` varchar(30) NOT NULL,
  `id_pic` int(15) NOT NULL,
  `id_payment` int(10) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL,
  `pembayaran` bigint(25) DEFAULT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`no_payment`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of hutang_order
-- ----------------------------
BEGIN;
INSERT INTO `hutang_order` VALUES ('PY-2004080010001', 'PO2003310010001', 1, 1, '2020-04-08 07:34:02', 20000, 'cash');
INSERT INTO `hutang_order` VALUES ('PY-2004080010002', 'PO2003310010001', 1, 0, '2020-04-08 07:35:13', 0, '');
INSERT INTO `hutang_order` VALUES ('PY-2004130010001', 'PO2004130010001', 1, 1, '2020-04-13 22:29:51', 120000000, '');
COMMIT;

-- ----------------------------
-- Table structure for hutang_payment
-- ----------------------------
DROP TABLE IF EXISTS `hutang_payment`;
CREATE TABLE `hutang_payment` (
  `id_payment` int(5) NOT NULL AUTO_INCREMENT,
  `payment` varchar(50) NOT NULL,
  `type` int(3) NOT NULL,
  PRIMARY KEY (`id_payment`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for kartu_stok
-- ----------------------------
DROP TABLE IF EXISTS `kartu_stok`;
CREATE TABLE `kartu_stok` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `no_transaksi` varchar(15) NOT NULL,
  `tanggal` date NOT NULL,
  `pic` int(15) NOT NULL,
  `id_produk` varchar(15) NOT NULL,
  `stok_awal` float DEFAULT NULL,
  `barang_masuk` float DEFAULT NULL,
  `barang_keluar` float DEFAULT NULL,
  `retur` float DEFAULT NULL,
  `adjustment` float DEFAULT NULL,
  `stok_akhir` float DEFAULT NULL,
  `tipe` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for kategori
-- ----------------------------
DROP TABLE IF EXISTS `kategori`;
CREATE TABLE `kategori` (
  `id_kategori` int(100) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) NOT NULL,
  PRIMARY KEY (`id_kategori`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of kategori
-- ----------------------------
BEGIN;
INSERT INTO `kategori` VALUES (1, 'bahan bangunan');
COMMIT;

-- ----------------------------
-- Table structure for keterangan_waste
-- ----------------------------
DROP TABLE IF EXISTS `keterangan_waste`;
CREATE TABLE `keterangan_waste` (
  `id_keterangan` int(100) NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(100) NOT NULL,
  PRIMARY KEY (`id_keterangan`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for konversi_item
-- ----------------------------
DROP TABLE IF EXISTS `konversi_item`;
CREATE TABLE `konversi_item` (
  `no_convert` varchar(35) DEFAULT NULL,
  `idProduk` varchar(30) DEFAULT NULL,
  `qty` int(15) DEFAULT NULL,
  `hargaBeli` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for konversi_number
-- ----------------------------
DROP TABLE IF EXISTS `konversi_number`;
CREATE TABLE `konversi_number` (
  `no_convert` varchar(35) NOT NULL,
  `idUser` int(30) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  PRIMARY KEY (`no_convert`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of login_attempts
-- ----------------------------
BEGIN;
INSERT INTO `login_attempts` VALUES (42, '36.65.127.130', 'admin', 1586782550);
INSERT INTO `login_attempts` VALUES (43, '36.65.127.130', 'admin', 1586782553);
COMMIT;

-- ----------------------------
-- Table structure for payment_type_debt
-- ----------------------------
DROP TABLE IF EXISTS `payment_type_debt`;
CREATE TABLE `payment_type_debt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `paymentType` varchar(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of payment_type_debt
-- ----------------------------
BEGIN;
INSERT INTO `payment_type_debt` VALUES (1, 'Cash');
INSERT INTO `payment_type_debt` VALUES (2, 'Transfer');
INSERT INTO `payment_type_debt` VALUES (3, 'Giro');
COMMIT;

-- ----------------------------
-- Table structure for poin
-- ----------------------------
DROP TABLE IF EXISTS `poin`;
CREATE TABLE `poin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poin_pembelian` int(10) NOT NULL,
  `nilai_pembelian` int(10) NOT NULL,
  `poin_pengeluaran` int(10) NOT NULL,
  `nilai_pengeluaran` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of poin
-- ----------------------------
BEGIN;
INSERT INTO `poin` VALUES (1, 1, 500000, 10, 10000);
COMMIT;

-- ----------------------------
-- Table structure for proyeksi_bahan_baku
-- ----------------------------
DROP TABLE IF EXISTS `proyeksi_bahan_baku`;
CREATE TABLE `proyeksi_bahan_baku` (
  `id_produk` varchar(25) NOT NULL,
  `qty` int(10) NOT NULL,
  PRIMARY KEY (`id_produk`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for purchase_item
-- ----------------------------
DROP TABLE IF EXISTS `purchase_item`;
CREATE TABLE `purchase_item` (
  `no_po` varchar(30) NOT NULL,
  `sku` varchar(15) NOT NULL,
  `qty` float NOT NULL,
  `harga` int(30) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of purchase_item
-- ----------------------------
BEGIN;
INSERT INTO `purchase_item` VALUES ('PO2003310010001', 'BESI-4NGFI-002', 10, 2000, '2020-03-31');
INSERT INTO `purchase_item` VALUES ('PO2004130010001', 'EMBER-27A04-002', 30, 4000000, '2020-04-13');
COMMIT;

-- ----------------------------
-- Table structure for purchase_order
-- ----------------------------
DROP TABLE IF EXISTS `purchase_order`;
CREATE TABLE `purchase_order` (
  `no_po` varchar(30) NOT NULL,
  `tanggal_po` date NOT NULL,
  `tanggal_kirim` date NOT NULL,
  `jatuh_tempo` date NOT NULL,
  `alamat_pengiriman` text NOT NULL,
  `id_supplier` int(30) NOT NULL,
  `keterangan` text NOT NULL,
  `id_pic` int(30) NOT NULL,
  `status` int(4) NOT NULL,
  `ppn` int(5) DEFAULT NULL,
  `nilai_ppn` float DEFAULT NULL,
  `no_faktur_pajak` varchar(20) DEFAULT NULL,
  `type` int(1) NOT NULL COMMENT '0 = PO PRODUK 1 = PO MATERIAL',
  PRIMARY KEY (`no_po`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of purchase_order
-- ----------------------------
BEGIN;
INSERT INTO `purchase_order` VALUES ('PO2003310010001', '2020-03-31', '2020-03-19', '2020-03-18', '', 1, '', 1, 3, NULL, NULL, NULL, 0);
INSERT INTO `purchase_order` VALUES ('PO2004130010001', '2020-04-13', '2020-04-22', '2020-04-15', 'kirim', 1, 'kirim', 1, 3, NULL, NULL, NULL, 0);
COMMIT;

-- ----------------------------
-- Table structure for receive_item
-- ----------------------------
DROP TABLE IF EXISTS `receive_item`;
CREATE TABLE `receive_item` (
  `no_receive` varchar(30) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `qty` float NOT NULL,
  `price` float NOT NULL,
  `remark` text NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of receive_item
-- ----------------------------
BEGIN;
INSERT INTO `receive_item` VALUES ('RCV200331001001', 'BESI-4NGFI-002', 10, 2000, '', '2020-03-31');
INSERT INTO `receive_item` VALUES ('RCV200413001001', 'EMBER-27A04-002', 30, 4000000, '', '2020-04-13');
COMMIT;

-- ----------------------------
-- Table structure for receive_order
-- ----------------------------
DROP TABLE IF EXISTS `receive_order`;
CREATE TABLE `receive_order` (
  `no_receive` varchar(30) NOT NULL,
  `no_po` varchar(30) NOT NULL,
  `received_by` varchar(30) NOT NULL,
  `checked_by` varchar(30) NOT NULL,
  `tanggal_terima` date NOT NULL,
  `id_pic` int(10) NOT NULL,
  `id_supplier` int(10) NOT NULL,
  `surat_jalan` varchar(100) DEFAULT NULL,
  `diterimaDi` int(10) DEFAULT NULL,
  `type` int(1) DEFAULT NULL COMMENT 'null = produk, 1 = material, 2 = WO Produksi',
  PRIMARY KEY (`no_receive`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of receive_order
-- ----------------------------
BEGIN;
INSERT INTO `receive_order` VALUES ('RCV200331001001', 'PO2003310010001', 'andy', 'andy', '2020-03-31', 1, 1, NULL, 11, NULL);
INSERT INTO `receive_order` VALUES ('RCV200413001001', 'PO2004130010001', 'bambang', 'bambang', '2020-04-13', 1, 1, NULL, 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for retur
-- ----------------------------
DROP TABLE IF EXISTS `retur`;
CREATE TABLE `retur` (
  `no_retur` varchar(30) NOT NULL,
  `no_po` varchar(30) NOT NULL,
  `id_pic` int(100) NOT NULL,
  `tanggal_retur` datetime NOT NULL,
  PRIMARY KEY (`no_retur`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for retur_item
-- ----------------------------
DROP TABLE IF EXISTS `retur_item`;
CREATE TABLE `retur_item` (
  `no_retur` varchar(30) NOT NULL,
  `sku` varchar(30) NOT NULL,
  `qty` float NOT NULL,
  `harga` int(30) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for returstore
-- ----------------------------
DROP TABLE IF EXISTS `returstore`;
CREATE TABLE `returstore` (
  `NoRetur` varchar(30) NOT NULL,
  `tanggal` datetime NOT NULL,
  `id_user` int(10) NOT NULL,
  `idStoreFrom` int(10) NOT NULL,
  PRIMARY KEY (`NoRetur`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for returstoreitem
-- ----------------------------
DROP TABLE IF EXISTS `returstoreitem`;
CREATE TABLE `returstoreitem` (
  `NoRetur` varchar(30) NOT NULL,
  `sku` varchar(30) NOT NULL,
  `qty` int(10) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for rq_purchase_approved
-- ----------------------------
DROP TABLE IF EXISTS `rq_purchase_approved`;
CREATE TABLE `rq_purchase_approved` (
  `purchase_no` varchar(20) NOT NULL,
  `approved_date` datetime NOT NULL,
  `approved_by` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for rq_purchase_item
-- ----------------------------
DROP TABLE IF EXISTS `rq_purchase_item`;
CREATE TABLE `rq_purchase_item` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `purchase_no` varchar(15) NOT NULL,
  `sku` varchar(30) NOT NULL,
  `id_supplier` int(10) NOT NULL,
  `harga` int(25) NOT NULL,
  `remark` varchar(50) NOT NULL,
  `isChoose` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for rq_purchase_no
-- ----------------------------
DROP TABLE IF EXISTS `rq_purchase_no`;
CREATE TABLE `rq_purchase_no` (
  `purchase_no` varchar(15) NOT NULL,
  `id_pic` int(15) NOT NULL,
  `tanggal_request` datetime NOT NULL,
  `sku` varchar(30) NOT NULL,
  `qty` float NOT NULL,
  `status` int(2) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`purchase_no`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for satuan
-- ----------------------------
DROP TABLE IF EXISTS `satuan`;
CREATE TABLE `satuan` (
  `id_satuan` int(100) NOT NULL AUTO_INCREMENT,
  `satuan` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  PRIMARY KEY (`id_satuan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of satuan
-- ----------------------------
BEGIN;
INSERT INTO `satuan` VALUES (10, 'Pcs', '');
INSERT INTO `satuan` VALUES (11, 'Kg', '');
INSERT INTO `satuan` VALUES (12, 'Box', '');
INSERT INTO `satuan` VALUES (13, 'Kaplet', '');
INSERT INTO `satuan` VALUES (14, 'Liter', '');
INSERT INTO `satuan` VALUES (15, 'Kaleng', '');
INSERT INTO `satuan` VALUES (16, 'Sak', '');
INSERT INTO `satuan` VALUES (17, 'Meter', '');
INSERT INTO `satuan` VALUES (18, 'lsn', '');
INSERT INTO `satuan` VALUES (19, 'Batang', '');
INSERT INTO `satuan` VALUES (20, 'Lembar', '');
COMMIT;

-- ----------------------------
-- Table structure for settingemail
-- ----------------------------
DROP TABLE IF EXISTS `settingemail`;
CREATE TABLE `settingemail` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `SMTPHost` varchar(50) NOT NULL,
  `SMTPPort` int(10) NOT NULL,
  `SMTPUser` varchar(30) NOT NULL,
  `SMTPPas` varchar(50) NOT NULL,
  `UserName` varchar(30) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of settingemail
-- ----------------------------
BEGIN;
INSERT INTO `settingemail` VALUES (1, 'smtp.sendgrid.net', 587, '', '', '');
COMMIT;

-- ----------------------------
-- Table structure for sp_bahan_keluar
-- ----------------------------
DROP TABLE IF EXISTS `sp_bahan_keluar`;
CREATE TABLE `sp_bahan_keluar` (
  `no_bahan_keluar` varchar(100) NOT NULL,
  `sku` varchar(30) NOT NULL,
  `qty` float NOT NULL,
  `harga` int(15) NOT NULL,
  `tanggal_keluar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sp_bahan_keluar
-- ----------------------------
BEGIN;
INSERT INTO `sp_bahan_keluar` VALUES ('TR-2004130010001', 'EMBER-27A04-002', 10, 0, '2020-04-13');
COMMIT;

-- ----------------------------
-- Table structure for sp_no_bahan_keluar
-- ----------------------------
DROP TABLE IF EXISTS `sp_no_bahan_keluar`;
CREATE TABLE `sp_no_bahan_keluar` (
  `no_bahan_keluar` varchar(30) NOT NULL,
  `id_user` int(15) NOT NULL,
  `tanggal_keluar` datetime NOT NULL,
  `nama_penerima` varchar(30) NOT NULL,
  `keterangan` text NOT NULL,
  `store_tujuan` int(5) NOT NULL,
  PRIMARY KEY (`no_bahan_keluar`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sp_no_bahan_keluar
-- ----------------------------
BEGIN;
INSERT INTO `sp_no_bahan_keluar` VALUES ('TR-2004130010001', 1, '2020-04-13 22:26:03', 'dir gungung sakti', 'jus', 11);
COMMIT;

-- ----------------------------
-- Table structure for stock_opname
-- ----------------------------
DROP TABLE IF EXISTS `stock_opname`;
CREATE TABLE `stock_opname` (
  `no_so` varchar(30) NOT NULL,
  `sku` varchar(30) NOT NULL,
  `last_stok` float NOT NULL,
  `new_stok` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of stock_opname
-- ----------------------------
BEGIN;
INSERT INTO `stock_opname` VALUES ('SOTK-2003311001', 'EMBER-27A04-002', 0, 20);
INSERT INTO `stock_opname` VALUES ('SOTK-2003311002', 'EMBER-27A04-002', 10, 20);
INSERT INTO `stock_opname` VALUES ('SOTK-2003311003', 'BESI-4NGFI-002', 10, 40);
COMMIT;

-- ----------------------------
-- Table structure for stock_opname_info
-- ----------------------------
DROP TABLE IF EXISTS `stock_opname_info`;
CREATE TABLE `stock_opname_info` (
  `no_so` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `id_pic` int(15) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `type` int(5) NOT NULL,
  `store` int(13) DEFAULT NULL,
  PRIMARY KEY (`no_so`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of stock_opname_info
-- ----------------------------
BEGIN;
INSERT INTO `stock_opname_info` VALUES ('SOTK-2003311001', '2020-03-31', 1, 'SO By Excel', 2, 3);
INSERT INTO `stock_opname_info` VALUES ('SOTK-2003311002', '2020-03-31', 1, 'SO By Excel', 2, 3);
INSERT INTO `stock_opname_info` VALUES ('SOTK-2003311003', '2020-03-31', 1, 'SO By Excel', 2, 11);
COMMIT;

-- ----------------------------
-- Table structure for stok_store
-- ----------------------------
DROP TABLE IF EXISTS `stok_store`;
CREATE TABLE `stok_store` (
  `id_produk` varchar(30) NOT NULL,
  `id_store` int(5) NOT NULL,
  `stok` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of stok_store
-- ----------------------------
BEGIN;
INSERT INTO `stok_store` VALUES ('BESI-4NGFI-002', 11, 32);
INSERT INTO `stok_store` VALUES ('EMBER-27A04-002', 11, 7);
COMMIT;

-- ----------------------------
-- Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `id_supplier` int(100) NOT NULL AUTO_INCREMENT,
  `supplier` varchar(100) NOT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `kontak` varchar(15) DEFAULT NULL,
  `no_rekening` varchar(25) DEFAULT NULL,
  `bank` varchar(25) DEFAULT NULL,
  `atas_nama` varchar(25) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_supplier`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of supplier
-- ----------------------------
BEGIN;
INSERT INTO `supplier` VALUES (1, 'PT Riau Steel', 'Jln Pasir Putih\nPekan Baru', '+620123456789', '0123456789', 'Bank Central Asia', 'PT Riau Steel', 'aaa@gmail.com');
INSERT INTO `supplier` VALUES (2, 'PT Bilah Baja', 'Jln Asia', 'Budiman', '123456789', 'BCA', 'PT Bilah Baja', 'BBMA@GMAIL.COM');
COMMIT;

-- ----------------------------
-- Table structure for transferstokitem
-- ----------------------------
DROP TABLE IF EXISTS `transferstokitem`;
CREATE TABLE `transferstokitem` (
  `noTransfer` varchar(25) DEFAULT NULL,
  `idProduk` varchar(50) DEFAULT NULL,
  `qty` int(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for transferstoknumber
-- ----------------------------
DROP TABLE IF EXISTS `transferstoknumber`;
CREATE TABLE `transferstoknumber` (
  `noTransfer` varchar(40) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `idUser` int(10) DEFAULT NULL,
  `transferFrom` int(10) DEFAULT NULL,
  `transferTo` int(10) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`noTransfer`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for tw_bahan
-- ----------------------------
DROP TABLE IF EXISTS `tw_bahan`;
CREATE TABLE `tw_bahan` (
  `sku` varchar(45) NOT NULL,
  `nama_bahan` varchar(100) NOT NULL,
  `harga` decimal(20,2) NOT NULL,
  `satuan` varchar(20) DEFAULT NULL,
  `date_added` date DEFAULT NULL,
  `date_update` date DEFAULT NULL,
  `id_pic` int(11) NOT NULL,
  `id_category` int(5) DEFAULT NULL,
  `status` int(2) NOT NULL,
  `en_dis` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for tw_category
-- ----------------------------
DROP TABLE IF EXISTS `tw_category`;
CREATE TABLE `tw_category` (
  `id_category` int(5) NOT NULL,
  `category` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for user_access_store
-- ----------------------------
DROP TABLE IF EXISTS `user_access_store`;
CREATE TABLE `user_access_store` (
  `id_store` int(10) DEFAULT NULL,
  `id_user` int(10) DEFAULT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user_access_store
-- ----------------------------
BEGIN;
INSERT INTO `user_access_store` VALUES (1, 24, 0);
INSERT INTO `user_access_store` VALUES (2, 24, 0);
INSERT INTO `user_access_store` VALUES (5, 24, 0);
INSERT INTO `user_access_store` VALUES (6, 24, 0);
INSERT INTO `user_access_store` VALUES (7, 24, 0);
INSERT INTO `user_access_store` VALUES (8, 24, 0);
INSERT INTO `user_access_store` VALUES (9, 24, 0);
INSERT INTO `user_access_store` VALUES (10, 24, 0);
INSERT INTO `user_access_store` VALUES (11, 24, 0);
INSERT INTO `user_access_store` VALUES (1, 1, 1);
INSERT INTO `user_access_store` VALUES (2, 1, 1);
INSERT INTO `user_access_store` VALUES (5, 1, 1);
INSERT INTO `user_access_store` VALUES (6, 1, 1);
INSERT INTO `user_access_store` VALUES (7, 1, 1);
INSERT INTO `user_access_store` VALUES (8, 1, 1);
INSERT INTO `user_access_store` VALUES (9, 1, 1);
INSERT INTO `user_access_store` VALUES (10, 1, 1);
INSERT INTO `user_access_store` VALUES (11, 1, 1);
INSERT INTO `user_access_store` VALUES (1, 19, 0);
INSERT INTO `user_access_store` VALUES (2, 19, 0);
INSERT INTO `user_access_store` VALUES (5, 19, 0);
INSERT INTO `user_access_store` VALUES (6, 19, 0);
INSERT INTO `user_access_store` VALUES (7, 19, 0);
INSERT INTO `user_access_store` VALUES (8, 19, 0);
INSERT INTO `user_access_store` VALUES (9, 19, 0);
INSERT INTO `user_access_store` VALUES (10, 19, 0);
INSERT INTO `user_access_store` VALUES (11, 19, 0);
INSERT INTO `user_access_store` VALUES (1, 8, 0);
INSERT INTO `user_access_store` VALUES (2, 8, 0);
INSERT INTO `user_access_store` VALUES (5, 8, 0);
INSERT INTO `user_access_store` VALUES (6, 8, 0);
INSERT INTO `user_access_store` VALUES (7, 8, 0);
INSERT INTO `user_access_store` VALUES (8, 8, 0);
INSERT INTO `user_access_store` VALUES (9, 8, 0);
INSERT INTO `user_access_store` VALUES (10, 8, 0);
INSERT INTO `user_access_store` VALUES (11, 8, 0);
INSERT INTO `user_access_store` VALUES (1, 10, 0);
INSERT INTO `user_access_store` VALUES (2, 10, 0);
INSERT INTO `user_access_store` VALUES (5, 10, 0);
INSERT INTO `user_access_store` VALUES (6, 10, 0);
INSERT INTO `user_access_store` VALUES (7, 10, 0);
INSERT INTO `user_access_store` VALUES (8, 10, 0);
INSERT INTO `user_access_store` VALUES (9, 10, 0);
INSERT INTO `user_access_store` VALUES (10, 10, 0);
INSERT INTO `user_access_store` VALUES (11, 10, 0);
INSERT INTO `user_access_store` VALUES (1, 15, 1);
INSERT INTO `user_access_store` VALUES (2, 15, 1);
INSERT INTO `user_access_store` VALUES (5, 15, 1);
INSERT INTO `user_access_store` VALUES (6, 15, 1);
INSERT INTO `user_access_store` VALUES (7, 15, 1);
INSERT INTO `user_access_store` VALUES (8, 15, 1);
INSERT INTO `user_access_store` VALUES (9, 15, 1);
INSERT INTO `user_access_store` VALUES (10, 15, 1);
INSERT INTO `user_access_store` VALUES (11, 15, 1);
INSERT INTO `user_access_store` VALUES (1, 49, 0);
INSERT INTO `user_access_store` VALUES (2, 49, 0);
INSERT INTO `user_access_store` VALUES (5, 49, 0);
INSERT INTO `user_access_store` VALUES (6, 49, 0);
INSERT INTO `user_access_store` VALUES (7, 49, 0);
INSERT INTO `user_access_store` VALUES (8, 49, 0);
INSERT INTO `user_access_store` VALUES (9, 49, 0);
INSERT INTO `user_access_store` VALUES (10, 49, 0);
INSERT INTO `user_access_store` VALUES (11, 49, 0);
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `menu` text DEFAULT NULL,
  `sub_menu` text DEFAULT NULL,
  `toko` int(19) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, '127.0.0.1', 'administrator', '$2y$08$5UQfG4c.cC2hbWThIwGo/.LWE3O2N2MXm98OH6r7XY0CGaolrE6qy', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1586788435, 1, 'Admin', 'istrator', 'ADMIN', '0', '[1,2,3,4,5,6,7,8,9,10,11]', '[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,47,27,28,29,30,31,32,35,36,37,38,39,40,42,43,46]', 11);
INSERT INTO `users` VALUES (26, '::1', 'demo', '$2y$08$5UQfG4c.cC2hbWThIwGo/.LWE3O2N2MXm98OH6r7XY0CGaolrE6qy', NULL, 'demo@gmail.com', NULL, NULL, NULL, NULL, 1574694117, 1585878559, 1, 'demo', 'demo', NULL, '0932190321', '[1,2,3,4,6,7,8,10,11]', '[1,3,4,5,7,8,9,10,11,13,14,15,16,17,20,21,22,23,24,26,27,29,32,40,42,43,46]', 11);
INSERT INTO `users` VALUES (27, '36.81.6.251', '123456', '$2y$08$/sob2tv/oXarlz.9dd0z8OpBK7/i1SikNpfynV0cxqCpL4D.I8z1K', NULL, 'a@gmail.com', NULL, NULL, NULL, NULL, 1585877590, 1586299093, 1, 'andy', NULL, NULL, NULL, NULL, NULL, 11);
COMMIT;

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`) USING BTREE,
  KEY `fk_users_groups_users1_idx` (`user_id`) USING BTREE,
  KEY `fk_users_groups_groups1_idx` (`group_id`) USING BTREE,
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
BEGIN;
INSERT INTO `users_groups` VALUES (1, 1, 1);
INSERT INTO `users_groups` VALUES (27, 26, 2);
INSERT INTO `users_groups` VALUES (28, 27, 2);
COMMIT;

-- ----------------------------
-- Table structure for virtual_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `virtual_warehouse`;
CREATE TABLE `virtual_warehouse` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_customer` int(15) NOT NULL,
  `id_produk` varchar(20) NOT NULL,
  `stok` float NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for waste
-- ----------------------------
DROP TABLE IF EXISTS `waste`;
CREATE TABLE `waste` (
  `no_waste` varchar(30) NOT NULL,
  `tanggal_waste` date NOT NULL,
  `id_pic` int(10) NOT NULL,
  `id_keterangan` int(10) NOT NULL,
  `keterangan` text NOT NULL,
  `image` text DEFAULT NULL,
  PRIMARY KEY (`no_waste`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for waste_item
-- ----------------------------
DROP TABLE IF EXISTS `waste_item`;
CREATE TABLE `waste_item` (
  `no_waste` varchar(30) NOT NULL,
  `sku` varchar(20) NOT NULL,
  `qty` float NOT NULL,
  `harga` int(100) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `id_keterangan` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for work_order
-- ----------------------------
DROP TABLE IF EXISTS `work_order`;
CREATE TABLE `work_order` (
  `no_order` varchar(25) NOT NULL,
  `tanggalPenyelesaian` date DEFAULT NULL,
  `tanggalWO` date DEFAULT NULL,
  `id_user` int(15) DEFAULT NULL,
  `id_supplier` int(15) DEFAULT NULL,
  `pemohon` varchar(40) DEFAULT NULL,
  `alamatPengiriman` text DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status` int(1) DEFAULT NULL COMMENT '0 = dalam proses, 1 = diterima, 2 = selesai, 3 = batal ',
  PRIMARY KEY (`no_order`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for work_order_adjustment
-- ----------------------------
DROP TABLE IF EXISTS `work_order_adjustment`;
CREATE TABLE `work_order_adjustment` (
  `no_adjusment` varchar(30) NOT NULL,
  `noWO` varchar(30) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `id_user` int(15) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  PRIMARY KEY (`no_adjusment`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for work_order_adjustment_item
-- ----------------------------
DROP TABLE IF EXISTS `work_order_adjustment_item`;
CREATE TABLE `work_order_adjustment_item` (
  `no_adjusment` varchar(30) DEFAULT NULL,
  `sku` int(15) DEFAULT NULL,
  `qty` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for work_order_biaya
-- ----------------------------
DROP TABLE IF EXISTS `work_order_biaya`;
CREATE TABLE `work_order_biaya` (
  `no_order` varchar(35) DEFAULT NULL,
  `nama_biaya` text DEFAULT NULL,
  `biaya` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for work_order_convert
-- ----------------------------
DROP TABLE IF EXISTS `work_order_convert`;
CREATE TABLE `work_order_convert` (
  `no_order` varchar(35) DEFAULT NULL,
  `idProduk` varchar(35) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for work_order_item
-- ----------------------------
DROP TABLE IF EXISTS `work_order_item`;
CREATE TABLE `work_order_item` (
  `no_order` varchar(25) DEFAULT NULL,
  `sku` int(15) DEFAULT NULL,
  `qty` int(15) DEFAULT NULL,
  `harga` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for z_menu
-- ----------------------------
DROP TABLE IF EXISTS `z_menu`;
CREATE TABLE `z_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(40) DEFAULT NULL,
  `slug` varchar(40) DEFAULT NULL,
  `icon` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of z_menu
-- ----------------------------
BEGIN;
INSERT INTO `z_menu` VALUES (1, 'Dashboard', 'dashboard', 'fa fa-dashboard');
INSERT INTO `z_menu` VALUES (2, 'Master Data', '', 'fa fa-cube');
INSERT INTO `z_menu` VALUES (3, 'Transaksi Penjualan', '', 'fa fa-exchange');
INSERT INTO `z_menu` VALUES (4, 'Inventory', '', 'fa fa-cubes');
INSERT INTO `z_menu` VALUES (5, 'Finance', '', 'fa fa-money');
INSERT INTO `z_menu` VALUES (6, 'Laporan', '', 'fa fa-book');
INSERT INTO `z_menu` VALUES (7, 'Parameter', '', 'fa fa-anchor');
INSERT INTO `z_menu` VALUES (8, 'Promotion', '', 'fa fa-dollar');
INSERT INTO `z_menu` VALUES (9, 'Manufaktur', '', 'fa fa-puzzle-piece');
INSERT INTO `z_menu` VALUES (10, 'Customer', '', 'fa fa-user');
INSERT INTO `z_menu` VALUES (11, 'Setting', '', 'fa fa-gears');
COMMIT;

-- ----------------------------
-- Table structure for z_submenu
-- ----------------------------
DROP TABLE IF EXISTS `z_submenu`;
CREATE TABLE `z_submenu` (
  `idSub` int(10) NOT NULL AUTO_INCREMENT,
  `id` int(10) DEFAULT NULL,
  `menu` varchar(40) DEFAULT NULL,
  `slug` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`idSub`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of z_submenu
-- ----------------------------
BEGIN;
INSERT INTO `z_submenu` VALUES (1, 2, 'Produk', 'produk');
INSERT INTO `z_submenu` VALUES (2, 2, 'Bahan Baku', 'bahan_baku');
INSERT INTO `z_submenu` VALUES (3, 2, 'Supplier', 'supplier');
INSERT INTO `z_submenu` VALUES (4, 2, 'Salesman', 'ekspedisi');
INSERT INTO `z_submenu` VALUES (5, 2, 'Store', 'store');
INSERT INTO `z_submenu` VALUES (6, 2, 'Tempat', 'stand');
INSERT INTO `z_submenu` VALUES (7, 3, 'Penjualan', 'penjualan');
INSERT INTO `z_submenu` VALUES (8, 3, 'Data Piutang', 'data_piutang');
INSERT INTO `z_submenu` VALUES (9, 4, 'Purchase Order', 'purchase_order');
INSERT INTO `z_submenu` VALUES (10, 4, 'Barang Masuk', 'bahan_masuk');
INSERT INTO `z_submenu` VALUES (11, 4, 'Mutasi Barang', 'bahan_keluar');
INSERT INTO `z_submenu` VALUES (12, 4, 'Waste', 'waste');
INSERT INTO `z_submenu` VALUES (13, 4, 'Retur', 'retur');
INSERT INTO `z_submenu` VALUES (14, 4, 'Transfer Stok', 'transferStok');
INSERT INTO `z_submenu` VALUES (15, 4, 'Stock Opname Gudang', 'stock_opname');
INSERT INTO `z_submenu` VALUES (16, 4, 'Stock Opname Toko', 'stock_opname_toko');
INSERT INTO `z_submenu` VALUES (17, 4, 'Data Stok', 'dataStok');
INSERT INTO `z_submenu` VALUES (18, 5, 'Hutang Pembelian', 'finance');
INSERT INTO `z_submenu` VALUES (19, 5, 'Buka / Tutup Kasir', 'kasir');
INSERT INTO `z_submenu` VALUES (20, 6, 'Laporan Pembelian', 'laporan/pembelian');
INSERT INTO `z_submenu` VALUES (21, 6, 'Laporan Penjualan', 'laporan/penjualan');
INSERT INTO `z_submenu` VALUES (22, 6, 'Laporan Mutasi Barang', 'laporan/mutasiBarang');
INSERT INTO `z_submenu` VALUES (23, 6, 'Laporan Transfer Stok', 'laporan/transferStok');
INSERT INTO `z_submenu` VALUES (24, 6, 'Penerimaan Barang', 'laporan/penerimaanBarang');
INSERT INTO `z_submenu` VALUES (25, 6, 'Waste', 'laporan/waste');
INSERT INTO `z_submenu` VALUES (26, 6, 'Stock Opname', 'laporan/stock_opname');
INSERT INTO `z_submenu` VALUES (27, 7, 'Satuan', 'parameter/satuan');
INSERT INTO `z_submenu` VALUES (28, 7, 'Kategori', 'parameter/kategori');
INSERT INTO `z_submenu` VALUES (29, 7, 'Kategori Produk Jual', 'parameter/kategori_produk_jual');
INSERT INTO `z_submenu` VALUES (30, 7, 'Kategori Waste', 'parameter/keterangan_waste');
INSERT INTO `z_submenu` VALUES (31, 8, 'Poin', 'promotion/poin');
INSERT INTO `z_submenu` VALUES (32, 8, 'Banner', 'promotion/banner');
INSERT INTO `z_submenu` VALUES (35, 9, 'Purchase Order Material', 'purchaseOrderMaterial');
INSERT INTO `z_submenu` VALUES (36, 9, 'Bahan Masuk Material', 'bahanMasukMaterial');
INSERT INTO `z_submenu` VALUES (37, 9, 'Work Order', 'workOrder');
INSERT INTO `z_submenu` VALUES (38, 9, 'Penerimaan Work Order', 'penerimaanWorkOrder');
INSERT INTO `z_submenu` VALUES (39, 9, 'Konversi Produk', 'konversiProduk');
INSERT INTO `z_submenu` VALUES (40, 10, 'Data Customer', 'customer');
INSERT INTO `z_submenu` VALUES (41, 10, 'Customer Group', 'customer_group');
INSERT INTO `z_submenu` VALUES (42, 11, 'Info Perusahaan', 'setting/info_perusahaan');
INSERT INTO `z_submenu` VALUES (43, 11, 'Email', 'setting/email');
INSERT INTO `z_submenu` VALUES (46, 11, 'User', 'setting/user');
INSERT INTO `z_submenu` VALUES (47, 6, 'Retur Pembelian', 'laporan/returPembelian');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
